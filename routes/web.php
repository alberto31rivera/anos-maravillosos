<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CategoryAdvertisingController;
use App\Http\Controllers\Admin\AdvertisingController;
use App\Http\Controllers\Admin\BankTransferController;
use App\Http\Controllers\Admin\CommentsController;
use App\Http\Controllers\Admin\CurrencyController;
use App\Http\Controllers\Admin\EmailSettings;
use App\Http\Controllers\Admin\HomepageSettingsController;
use App\Http\Controllers\Admin\ManageGoogleController;
use App\Http\Controllers\Admin\ManageLanguageController;
use App\Http\Controllers\Admin\ManagePaymentController;
use App\Http\Controllers\Admin\ManageUsers;
use App\Http\Controllers\Admin\MercadoPagoController;
use App\Http\Controllers\Admin\PaypalController;
use App\Http\Controllers\Admin\PlanController;
use App\Http\Controllers\Admin\VendorController;
use App\Http\Controllers\Admin\PostsController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ProgramsController;
use App\Http\Controllers\Admin\RegardController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\SocialLinkController;
use App\Http\Controllers\Admin\SponsorController;
use App\Http\Controllers\Admin\ThemeSettingController;
use App\Http\Controllers\Admin\TransmissionController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\SocialAuthController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Front\ApiMercadoPagoController;
use App\Http\Controllers\Front\ApiPaypalController;
use App\Http\Controllers\Front\ContactController;
use App\Http\Controllers\Front\TransactionTransferController;
use App\Http\Controllers\Front\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('panel', [UserController::class, 'index'])->name('f.panel.user');
Route::get('shopping', [UserController::class, 'shopping'])->name('f.shopping.user');

Route::get('auth/{provider}', [SocialAuthController::class, 'redirectToProvider'])->name('social.auth');

Route::get('/admin', function () {
    return view('admin.login');
})->name('admin.login');

Route::delete('/admin/delete/category/{id}', [CategoryController::class, 'destroy'])->name('delete.category');
Route::delete('/admin/delete/advertisements/{id}', [CategoryAdvertisingController::class, 'destroy'])->name('delete.advertisements');
Route::delete('/admin/delete/plan/{id}', [PlanController::class, 'destroy'])->name('delete.plan');
Route::delete('/admin/delete/vendor/{id}', [VendorController::class, 'destroy'])->name('delete.vendor');
Route::delete('/admin/delete/program/{id}', [ProgramsController::class, 'destroy'])->name('delete.program');
Route::post('/authenticate-admin', [LoginController::class, 'authenticateAdmin'])->name('authenticate.admin');
Route::post('/authenticate-user', [LoginController::class, 'authenticateUser'])->name('authenticate.user');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
Route::get('admin/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('admin/publications/categories', [CategoryController::class, 'index'])->name('admin.categories');
Route::get('admin/publications/posts', [PostsController::class, 'index'])->name('admin.posts');
Route::post('admin/publications/posts/create', [PostsController::class, 'store'])->name('admin.create.posts');
Route::get('admin/publications/posts/edit/{id}', [PostsController::class, 'edit'])->name('admin.edit.posts');
Route::put('admin/publications/posts/{id}', [PostsController::class, 'update'])->name('admin.update.posts');
Route::delete('admin/publications/posts/{id}', [PostsController::class, 'destroy'])->name('admin.destroy.posts');
Route::get('admin/programs/list', [ProgramsController::class, 'index'])->name('admin.programs');
Route::get('admin/users/comments', [CommentsController::class, 'index'])->name('admin.comments');
Route::get('admin/advertising/categories', [CategoryAdvertisingController::class, 'index'])->name('admin.categories.advertising');
Route::get('admin/advertising/vendors', [VendorController::class, 'index'])->name('admin.vendors');
Route::get('admin/manager/regards', [RegardController::class, 'index'])->name('admin.regards');
Route::get('admin/settings/currencies', [CurrencyController::class, 'index'])->name('admin.currencies');
Route::get('admin/settings/emails', [EmailSettings::class, 'index'])->name('admin.setting.email');
Route::get('admin/settings/languages', [ManageLanguageController::class, 'index'])->name('admin.languages');

//Services
Route::get('service/1/{slug}', [HomeController::class, 'serviceOne'])->name('service.footer.one');
Route::get('service/2/{slug}', [HomeController::class, 'serviceTwo'])->name('service.footer.two');
Route::get('service/3/{slug}', [HomeController::class, 'serviceThree'])->name('service.footer.three');
Route::get('service/4/{slug}', [HomeController::class, 'serviceFour'])->name('service.footer.four');
Route::get('service/5/{slug}', [HomeController::class, 'serviceFive'])->name('service.footer.five');
Route::get('service/6/{slug}', [HomeController::class, 'serviceSix'])->name('service.footer.six');

Route::prefix('admin')->group(function () {
    Route::resource('settings/homepage', HomepageSettingsController::class);
    Route::resource('settings/themes', ThemeSettingController::class);
    Route::resource('settings/social-links', SocialLinkController::class);
    Route::resource('settings/transmissions', TransmissionController::class);
    Route::resource('settings/services', ServiceController::class);
    Route::resource('settings/google', ManageGoogleController::class);
    Route::resource('advertising/advertisements', AdvertisingController::class);
    Route::resource('advertising/plans', PlanController::class);
    Route::resource('payments/paypal', PaypalController::class);
    Route::resource('payments/mercado-pago', MercadoPagoController::class);
    Route::resource('payments/transfer', BankTransferController::class);
    Route::resource('programs/sponsor', SponsorController::class);
    Route::resource('manager/profile', ProfileController::class);
    Route::resource('users/list', ManageUsers::class);
});

Route::get('admin/payments/list', [ManagePaymentController::class, 'index'])->name('admin.payments');

Route::post('/admin/advertisements/cost', [AdvertisingController::class, 'cost'])->name('advertisements.cost');

// Front-End:

Route::get('registro', [HomeController::class, 'register'])->name('register');
Route::get('/iniciar-sesion', [HomeController::class, 'login'])->name('login');
Route::get('/restablecer-clave', [HomeController::class, 'resetPassword'])->name('reset-password');
Route::get('contactanos', [HomeController::class, 'contanctUs'])->name('pages.contanct-us');
Route::get('/planes-publicitarios', [HomeController::class, 'adPlans'])->name('pages.ad-plans');

Route::post('contact-us', [ContactController::class, 'contactUs'])->name('contact.us');

Route::get('/buscar', [HomeController::class, 'search'])->name('f.pages.searches');

//* Payments:

Route::get('/pagos-paypal/{plan}', [ApiPaypalController::class, 'paymentsPaypal'])->name('pages.paypal');
Route::get('/pagos-mercadopago/{plan}', [HomeController::class, 'paymentsMercadoPago'])->name('pages.mercado-pago');
Route::get('/pagos-transferencia/{id}', [HomeController::class, 'paymentsTransfer'])->name('pages.transfer');

// -------------------------
// preview: status transfer
Route::get('/payments-transfer/{e}', [HomeController::class, 'paymentsTransferResponse']);
// -------------------------

//Paypal
Route::post('/pagos-paypal', [ApiPaypalController::class, 'SendBillingInformation'])->name('paypal.billing.information');
Route::get('/payments-paypal/failed-response', [ApiPaypalController::class, 'failedResponse'])->name('paypal.failed.response');
Route::get('/payments-paypal/succes-response', [ApiPaypalController::class, 'successResponse'])->name('paypal.succes.response');
Route::post('/gateway', [ApiPaypalController::class, 'gateway'])->name('gateway.submit');

//Mercadopago
Route::post('pagos-mercadopago', [ApiMercadoPagoController::class, 'sendBillingInformation'])->name('mp.billing.information');
Route::get('mp-payment/success', [ApiMercadoPagoController::class, 'success'])->name('mp.payment.success');
Route::get('mp-payment/pending', [ApiMercadoPagoController::class, 'pending'])->name('mp.payment.pending');
Route::get('mp-payment/failure', [ApiMercadoPagoController::class, 'failure'])->name('mp.payment.failure');

//Bank Transfer
Route::post('/pagos-transferencia/pay/{id}', [TransactionTransferController::class, 'payTransfer'])->name('pay.transfer');

Route::get('/publicidad', [HomeController::class, 'advert'])->name('pages.advert');
Route::get('/en-vivo', [HomeController::class, 'programs'])->name('pages.programs');

Route::post('/registro', [RegisterController::class, 'registerUser'])->name('register.user');
Route::post('/actualizar/{id}', [UserController::class, 'updateUser'])->name('update.user');
Route::post('/comment/{id}', [HomeController::class, 'addComment'])->name('comment.user');
Route::get('/register/verify/{code}', [RegisterController::class, 'verify'])->name('verify.email');
Route::get('/', [HomeController::class, 'index'])->name('f.home.index');
Route::get('{slug}', [HomeController::class, 'categories'])->name('f.pages.categories');
Route::get('{slug1}/{slug2}', [HomeController::class, 'posts'])->name('f.pages.post');

//URL para SEO - Beto
Route::get('{slug1}/{slug2}/{id}', [HomeController::class, 'postsRef'])->name('f.pages.post.ref');
//End URL para SEO - Beto

Route::get('auth/{provider}/callback', [SocialAuthController::class, 'handleProviderCallback']);
Route::post('/forgot-password', [ForgotPasswordController::class, 'forgot'])->name('forgot.password.user');
Route::get('/recovery/password/{token}', [ForgotPasswordController::class, 'recovery'])->name('recovery.password.user');
Route::post('/reset/password', [ForgotPasswordController::class, 'reset'])->name('reset.password.user');


$('.toast').toast();

// $(".trigger-ad").click(function()
// {
//     $('#s-float-ad').toggleClass('element-hidden')
//     $('body').toggleClass('overflow-hidden')
// })

function switchEventAd(i=1)
{
    // Abre el modal (simulado) de publicidad
    if( i==1 )
    {
        $('#s-float-ad').removeClass('element-hidden');
    }
    // Cierra el modal (simulado) de publicidad
    else if(i==0)
    {
        $('#s-float-ad').addClass('element-hidden');
    }
    // Recarga la pagina actual
    else if(i==-1)
    {
        location.reload();
    }
}

function switchEventSearch(i=1)
{
    if( i==1 ) {
        $('#wrapper-search').css('display','block');
        console.log('open')
    }else if(i==0) {
        $('#wrapper-search').css('display','none');
        console.log('close')
    }
}

jQuery(document).ready(function(){
    jQuery("#gallery").unitegallery({
        gallery_theme: "compact",
        theme_hide_panel_under_width: 7680,
        strippanel_enable_handle: false,
        slider_enable_play_button: false,
        slider_enable_arrows: false,
        slider_fullscreen_button_align_hor:"right",
        slider_fullscreen_button_offset_hor:46,
        slider_fullscreen_button_offset_vert:13,
        // tiles_min_columns: 1,
        // tiles_max_columns: 1,
    });
});
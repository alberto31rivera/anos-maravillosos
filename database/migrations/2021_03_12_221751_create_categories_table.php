<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('image_location_id')->nullable();
            $table->string('title');
            $table->string('slug')->nullable()->unique();
            $table->longText('description')->nullable();
            $table->string('image')->nullable();
            $table->string('color')->nullable();
            $table->string('display_order')->nullable();
            $table->string('status_home_page')->default('1')->nullable();
            $table->string('status_header')->default('0')->nullable();
            $table->timestamps();

            $table->foreign('image_location_id')->references('id')->on('image_locations')->onUpdate('cascade')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

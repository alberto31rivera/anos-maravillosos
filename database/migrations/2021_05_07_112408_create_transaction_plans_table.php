<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_plans', function (Blueprint $table) {
            $table->id();
            $table->string('id_transaction')->nullable();
            $table->unsignedBigInteger('payment_method_id')->nullable();
            $table->unsignedBigInteger('plan_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('payment_method');
            $table->string('plan');
            $table->string('cost');
            $table->string('cost_dolar');
            $table->longText('description')->nullable();
            $table->string('duration')->nullable();
            $table->string('payment_date');
            $table->string('name')->nullable();
            $table->string('cellphone')->nullable();
            $table->string('number_document')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('type_document')->nullable();
            $table->string('email')->nullable();
            $table->string('proof_transfer')->nullable();
            $table->string('transfer_date');
            $table->string('status');
            $table->timestamps();

            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('plan_id')->references('id')->on('plans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_plans');
    }
}

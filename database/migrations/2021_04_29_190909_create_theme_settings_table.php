<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemeSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theme_settings', function (Blueprint $table) {
            $table->id();
            $table->string('color_regards')->nullable();
            $table->string('color_text_regards')->nullable();
            $table->string('color_header')->nullable();
            $table->string('color_highlight_header')->nullable();
            $table->string('color_text_header')->nullable();
            $table->string('color_footer')->nullable();
            $table->string('color_text_footer')->nullable();
            $table->string('color_contact')->nullable();
            $table->string('color_buttons')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theme_settings');
    }
}

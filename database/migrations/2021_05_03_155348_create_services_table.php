<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('title_one')->nullable();
            $table->string('slug_one')->nullable();
            $table->longText('description_one')->nullable();
            $table->string('status_one')->default('0')->nullable();
            $table->string('title_two')->nullable();
            $table->string('slug_two')->nullable();
            $table->longText('description_two')->nullable();
            $table->string('status_two')->default('0')->nullable();
            $table->string('title_three')->nullable();
            $table->string('slug_three')->nullable();
            $table->longText('description_three')->nullable();
            $table->string('status_three')->default('0')->nullable();
            $table->string('title_four')->nullable();
            $table->string('slug_four')->nullable();
            $table->longText('description_four')->nullable();
            $table->string('status_four')->default('0')->nullable();
            $table->string('title_five')->nullable();
            $table->string('slug_five')->nullable();
            $table->longText('description_five')->nullable();
            $table->string('status_five')->default('0')->nullable();
            $table->string('title_six')->nullable();
            $table->string('slug_six')->nullable();
            $table->longText('description_six')->nullable();
            $table->string('status_six')->default('0')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomepageSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepage_settings', function (Blueprint $table) {
            $table->id();
            $table->string('advertising_one')->nullable();
            $table->string('url_advertising_one')->nullable();
            $table->string('advertising_two')->nullable();
            $table->string('url_advertising_two')->nullable();
            $table->string('advertising_three')->nullable();
            $table->string('url_advertising_three')->nullable();
            $table->string('advertising_four')->nullable();
            $table->string('url_advertising_four')->nullable();
            $table->string('video_one')->nullable();
            $table->string('video_two')->nullable();
            $table->string('video_three')->nullable();
            $table->string('video_four')->nullable();
            $table->string('video_five')->nullable();
            $table->string('video_six')->nullable();
            $table->longText('description_one')->nullable();
            $table->longText('description_two')->nullable();
            $table->longText('description_three')->nullable();
            $table->longText('description_four')->nullable();
            $table->longText('description_five')->nullable();
            $table->longText('description_six')->nullable();
            $table->string('image_one')->nullable();
            $table->string('image_two')->nullable();
            $table->string('image_three')->nullable();
            $table->string('image_four')->nullable();
            $table->string('image_five')->nullable();
            $table->string('image_six')->nullable();
            $table->string('title_one')->nullable();
            $table->string('title_two')->nullable();
            $table->string('title_three')->nullable();
            $table->string('title_four')->nullable();
            $table->string('title_five')->nullable();
            $table->string('title_six')->nullable();
            $table->string('title_seven')->nullable();
            $table->string('title_eight')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homepage_settings');
    }
}

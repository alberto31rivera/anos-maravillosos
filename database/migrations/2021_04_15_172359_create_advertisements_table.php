<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vendor_id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('plan_id');
            $table->string('title');
            $table->string('cost')->nullable();
            $table->string('cost_dolar')->nullable();
            $table->string('status')->nullable();
            $table->longText('description');
            $table->string('type_advertising');
            $table->string('image');
            $table->string('pago')->nullable();
            $table->string('video')->nullable();
            $table->string('audio')->nullable();
            $table->string('start_date');
            $table->string('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
}

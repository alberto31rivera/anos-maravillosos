<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ManageGoogleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('manage_google')->insert([
            'recaptcha_key' => '6LepY9UaAAAAANIaWvPJsMwh5P2yd-IPPzkZAhfk',
            'recaptcha_secret_key' => '6LepY9UaAAAAAIsiBfG-n9ZETG_chLT-Dggua99C',
        ]);
    }
}

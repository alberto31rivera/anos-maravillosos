<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_configurations')->insert([
            'payment_method_id' => 2,
            'public_key' => 'APP_USR-8fd70288-3133-41e0-9b2b-28912a66d3fa',
            'access_token' => 'APP_USR-2203524920481329-051221-c78c64d9ca5e14059a70a6ae9f203423-656923757',
            'client_id' => '2203524920481329',
            'client_secret' => '3H8LTkCKcsNqzZJDXtiSTvvykXZgpW4C',
        ]);

        DB::table('payment_configurations')->insert([
            'payment_method_id' => 1,
            'email' => 'juan.colca@gmail.com',
        ]);
    }
}

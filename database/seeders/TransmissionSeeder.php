<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransmissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transmissions')->insert([
            'online_transmission' => '<iframe src="https://innovatestream.pe/players/player4/?RADIO=&amp;titulo=RADIO SAN FRANCISCO&amp;port=7324&amp;server=119&amp;back=C20610&amp;control=F2EB1D&amp;text=FFFFFF&amp;overcontrol=FFFFFF&amp;fa=&amp;tw=&amp;wa=" scrolling="no" frameborder="no" style="width: 100%;height:112px"></iframe>',
            'online_chat' => 'http://sanfranciscoradiofm.com',
            ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_methods')->insert([
            'title' => 'Paypal',
        ]);

        DB::table('payment_methods')->insert([
            'title' => 'Mercado Pago',
        ]);

        DB::table('payment_methods')->insert([
            'title' => 'Transferencia Bancaria',
        ]);
    }
}

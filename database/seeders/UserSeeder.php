<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => 1,
            'confirmed' => 1,
            'name' => 'Juan Colca',
            'email' => 'jcolca@jiraco.com',
            'password' => Hash::make('!aN9Vkx&!zZmxHmtsU'),
        ]);
    }
}

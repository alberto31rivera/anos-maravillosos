<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImageLocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('image_locations')->insert([
            'title' => 'Sección 1',
        ]);

        DB::table('image_locations')->insert([
            'title' => 'Sección 2',
        ]);

        DB::table('image_locations')->insert([
            'title' => 'Sección 3',
        ]);

        DB::table('image_locations')->insert([
            'title' => 'Sección 4',
        ]);

        DB::table('image_locations')->insert([
            'title' => 'Sección 5',
        ]);

        DB::table('image_locations')->insert([
            'title' => 'Sección 6',
        ]);

        DB::table('image_locations')->insert([
            'title' => 'Sección 7',
        ]);

        DB::table('image_locations')->insert([
            'title' => 'Sección 8',
        ]);

        DB::table('image_locations')->insert([
            'title' => 'Sección 9',
        ]);
    }
}

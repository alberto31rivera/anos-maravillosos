<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SocialLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('social_links')->insert([
            'facebook_client_id' => '493889921614127',
            'facebook_client_secret' => '39a9d6b1b71f3754837ee92233c0db86',
            'facebook_status' => 0,
            'google_client_id' => '574414019530-vvdukqglu0hf3l0i9ktmjtkob9bbtv2m.apps.googleusercontent.com',
            'google_client_secret' => 'LBTdX_ZS53KqKcL1JN6HAd3t',
            'google_status' => 0,
        ]);
    }
}

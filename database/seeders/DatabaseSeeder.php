<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            ImageLocationsSeeder::class,
            PaymentMethodSeeder::class,
            SocialLinkSeeder::class,
            ManageGoogleSeeder::class,
            PaymentConfigurationSeeder::class,
            CurrencySeeder::class,
            TransmissionSeeder::class,
        ]);
    }
}

@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_plans') }}
@endsection

@section('styles')
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
            @if ($view == 'create')
                @include('admin.plans.create')
            @else
                @include('admin.plans.edit')
            @endif
        </div>

        <div class="col-md-12">
            <livewire:manage-table-plans/>
        </div>

    </div>
</div>
@endsection


@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {

        $('#summernote').summernote({
            height: 150,
        });
    });

    function deleteItem(idForm)
    {

        const url = "/admin/delete/plan/" + idForm;
            const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

            Swal.fire({
                title: "{{ trans('multi-leng.plans_title_delete') }}",
                text: "{{ trans('multi-leng.plans_text_delete') }}",
                showCancelButton: true,
                confirmButtonText: "{{ trans('multi-leng.categories_title_confirm') }}",
                cancelButtonText: "{{ trans('multi-leng.categories_title_cancel') }}",
                showLoaderOnConfirm: true,
                preConfirm: () => {

                    return fetch(url, {
                            headers: {
                                "Content-Type": "application/json",
                                "Accept": "application/json, text-plain, */*",
                                "X-Requested-With": "XMLHttpRequest",
                                "X-CSRF-TOKEN": token
                            },
                            method: 'DELETE',
                        })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error(
                                    "{{ trans('multi-leng.plans_text_error') }}"
                                )
                            }
                            return response.json()
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: "{{ trans('multi-leng.categories_swal_delete') }}",
                    })
                }
            }).then(function() {
                window.location.reload();
            })



    }
</script>

@endsection


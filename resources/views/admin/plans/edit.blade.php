<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.plans_edit') }}</h3>
    </div>

    @if (Session::has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success-message') }}
        </div>
    @endif

    @if (Session::has('error-message'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('error-message') }}
        </div>
    @endif


    <form action="{{ route('plans.update', $plan->id) }}" method="POST" enctype="multipart/form-data">
        <div class="card-body">
            @csrf
            @method('PUT')
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="title">{{ trans('multi-leng.plans_title') }} *</label>
                    @error('title')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" value="{{old('title', $plan->title)}}" name="title" id="title"
                        placeholder="{{ trans('multi-leng.plans_title') }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="cost">{{ trans('multi-leng.plans_local_cost') }} *</label>
                    @error('cost')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="number" class="form-control"  value="{{old('cost', $plan->cost)}}" name="cost" id="cost"
                        placeholder="{{ $currencyLocal->name }}">
                </div>

                <div class="form-group col-md-3">
                    <label for="cost_dolar">{{ trans('multi-leng.plans_dolar_cost') }} *</label>
                    @error('cost_dolar')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                         {{ $message }}
                    </div>
                    @enderror
                    <input type="number" class="form-control"  value="{{old('cost_dolar', $plan->cost_dolar)}}" name="cost_dolar" id="cost_dolar"
                     placeholder="{{ $currencyDolar->name }}">
                  </div>

                <div class="form-group col-md-3">
                    <label for="duration">{{ trans('multi-leng.plans_time') }} *</label>
                    @error('duration')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                         {{ $message }}
                    </div>
                    @enderror
                    <input type="number" class="form-control"  value="{{old('duration', $plan->duration)}}" name="duration" id="duration" placeholder="{{ trans('multi-leng.plans_time') }}">
                  </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="description">{{ trans('multi-leng.plans_description') }} *</label>
                    @error('description')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea name="description" id="summernote">{!! old('description', $plan->description) !!}</textarea>
                </div>
            </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.plans_update') }}</button>
            <a href="{{ route('plans.index') }}" class="btn btn-default float-right"><i class="fas fa-times"></i>&nbsp; {{ trans('multi-leng.plans_cancel') }}</a>
        </div>
        <!-- /.card-footer -->
    </form>

</div>

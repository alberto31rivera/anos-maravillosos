<div class="card card-dark">
    <div class="card-header">
      <h3 class="card-title">{{ trans('multi-leng.new_plan') }}</h3>
    </div>

    @if (session()->has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('success-message') }}
        </div>
    @endif

    @if (session()->has('error-message'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ session('error-message') }}
    </div>
    @endif


    <form action="{{ route('plans.store') }}" method="POST" enctype="multipart/form-data">
        <div class="card-body">
            @include('admin.plans.form')
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.plans_save') }}</button>
          </div>
          <!-- /.card-footer -->
     </form>
</div>


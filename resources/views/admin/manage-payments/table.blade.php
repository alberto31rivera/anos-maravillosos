<div class="card card-dark">
    <div class="card-header">
      <div class="row">

        <h3 class="card-title col-sm-6 col-md-7 col-lg-8 mb-2 mb-sm-0 d-flex align-items-center">{{ trans('multi-leng.manage_payments_list') }}</h3>

        <div class="card-tools col-sm-6 col-md-5 col-lg-4">
          <div class="input-group input-group-sm">
            <input type="text" wire:model="search" class="form-control float-right" placeholder="{{ trans('multi-leng.manage_payments_search_id') }}">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
      <table class="table table-hover text-nowrap">
        <thead>
          <tr>
            <th>{{ trans('multi-leng.manage_payments_id') }}</th>
            <th>{{ trans('multi-leng.manage_payments_transaction_id') }}</th>
            <th>{{ trans('multi-leng.manage_payments_document_number') }}</th>
            <th>{{ trans('multi-leng.manage_payments_payment_gateway') }}</th>
            <th>{{ trans('multi-leng.manage_payments_plan') }}</th>
            <th>{{ trans('multi-leng.manage_payments_days') }}</th>
            <th>{{ trans('multi-leng.manage_payments_local_cost') }}</th>
            <th>{{ trans('multi-leng.manage_payments_dolar_cost') }}</th>
            <th>{{ trans('multi-leng.manage_payments_transaction_date') }}</th>
            <th>{{ trans('multi-leng.manage_payments_transfer_status') }}</th>
            <th>{{ trans('multi-leng.manage_payments_actions') }}</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($transactions as $item)
            <tr>
               <td>{{ $item->id}}</td>
               <td>{{ $item->id_transaction }}</td>
               <td>{{ $item->number_document }}</td>
               <td>{{ $item->payment_method }}</td>
               <td>{{ $item->plan }}</td>
               <td>{{ $item->duration }}</td>
               <td>{{ $item->cost }} {{ $currencyLocal->name }}</td>
               <td>{{ $item->cost_dolar }} {{ $currencyDolar->name }}</td>
               <td>{{ $item->payment_date }}</td>
               @if ($item->status == 0)
               <td>{{ trans('multi-leng.manage_payments_transfer_status_pending') }}</td>
               @elseif ($item->status == 1)
               <td>{{ trans('multi-leng.manage_payments_transfer_select_approved') }}</td>
               @else
               <td>{{ trans('multi-leng.manage_payments_transfer_select_rejected') }}</td>
               @endif

                <td>
                    <a class="btn btn-sm btn-warning" wire:click="setValue({{ $item->id }})" title="Ver Recibo" data-toggle="modal" data-target="#receiptModal" type="button" ><i class="fas fa-file"></i></a>
                    <button wire:click="edit({{ $item->id }})" class="btn btn-sm btn-info"><i class='fas fa-edit'></i></button>
                    <button wire:click="$emit('destroy', {{$item->id}})" class="btn btn-sm btn-danger"><i class='fas fa-trash'></i></button>
                </td>
            </tr>
            @endforeach
        </tbody>

      </table>

    </div>

    <div class="card-footer">
        {{ $transactions->links() }}
    </div>

  </div>

  @if ($document != null)
 <!-- Modal -->
 <div wire:ignore.self class="modal fade modal--radios" id="receiptModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
      <div class="modal-content rounded-0 border-0" style="background-color:#343A40">
        <div class="modal-header pt-1 pb-1 rounded-0 border-0 bg-white">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body pt-1 pb-0 px-0">

          <!--Content-->
          <div class="card rounded-0 border-0 p-3 p-md-4">
            <div class="card-header bg-white d-flex justify-content-between align-items-end">
               @if ($admin->logo == '')
                <img class="img-fluid rounded" src="{{asset('assets/front/template/images/logo/logo-v2.png')}}" alt="">
               @else
                <img class="img-fluid rounded" src="{{asset('img/logos-users/'.$admin->logo)}}" alt="">
               @endif
               <span class="text-muted pl-2 ml-auto">{{ trans('multi-leng.user_transactions_date_two') }}: {{ $document->created_at->toFormattedDateString() }}</span>
            </div>
            <div class="card-body pb-0">
              <div class="contaner">
                <div class="row">
                  <div class="col-md-4 mb-2 mb-md-0">
                    <h3 class="mb-0">{{ $admin->name }}</h3>
                    <p>
                      <span>{{ $admin->number_document }}</span>
                      <span>{{ $admin->address }}</span>
                      <span>{{ $admin->cellphone }}</span>
                      <span>{{ $admin->city }}</span>
                      <span>{{ $admin->country }}</span>
                      <span>{{ env('MAIL_USERNAME') }}</span>
                    </p>
                  </div>
                  <div class="col-md-4 mb-2 mb-md-0">
                    <p>
                      <span>{{ $document->user->name }}</span>
                      <span>{{ $document->user->number_document }}</span>
                      <span>{{ $document->user->address }}</span>
                      <span>{{ $document->user->cellphone }}</span>
                      <span>{{ $document->user->city }}</span>
                      <span>{{ $document->user->country }}</span>
                      <span>{{ $document->user->email }}</span>
                    </p>
                  </div>
                  <div class="col-md-4 mb-2 mb-md-0">
                    <p>
                      <span><strong>{{ trans('multi-leng.user_transactions_invoice') }}</strong> #{{ str_pad($document->id, 4, '0', STR_PAD_LEFT) }}</span>
                       @if ($document->payment_method == 'Transferencia Bancaria')
                       <span><strong>{{ trans('multi-leng.user_transactions_transfer_date') }} </strong>{{ $document->transfer_date }} </span>
                       @endif
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th scope="col">{{ trans('multi-leng.user_transactions_quantity') }}</th>
                          <th scope="col">{{ trans('multi-leng.user_transactions_description') }}</th>
                          <th scope="col" class="text-right">{{ trans('multi-leng.user_transactions_subtotal') }}</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>{!! $document->description !!}</td>
                          @if ($document->payment_method == 'Transferencia Bancaria')
                          <td class="wspace-nrap text-right">{{ $document->cost }} {{ $currencyLocal->name }}</td>
                          @elseif ($document->payment_method == 'Mercado Pago')
                          <td class="wspace-nrap text-right">{{ $document->cost }} {{ $currencyLocal->name }}</td>
                          @else
                          <td class="wspace-nrap text-right">{{ $document->cost_dolar }} {{ $currencyDolar->name }}</td>
                          @endif
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right">
                            @if ($document->payment_method == 'Transferencia Bancaria')
                            <span class="font-weight-bold">{{ trans('multi-leng.user_transactions_total') }}:</span> {{ $document->cost }} {{ $currencyLocal->name }}
                            @elseif ($document->payment_method == 'Mercado Pago')
                            <span class="font-weight-bold">{{ trans('multi-leng.user_transactions_total') }}:</span> {{ $document->cost }} {{ $currencyLocal->name }}
                            @else
                            <span class="font-weight-bold">{{ trans('multi-leng.user_transactions_total') }}:</span> {{ $document->cost_dolar }} {{ $currencyDolar->name }}
                            @endif
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              {{-- <button wire:click="dowloandPdf({{ $item->id }})" class="btn btn-primary"><i class="fa fa-print"></i> {{ trans('multi-leng.user_transactions_print') }}</button> --}}
            </div>
          </div> <!--/card-->
          <!--/Content-->

        </div>
      </div>
    </div>
  </div>
@endif
  @section('scripts')

  <script>
        document.addEventListener('livewire:load', function () {
          @this.on('destroy', idTransaction => {
              swal({
              title: "{{ trans('multi-leng.manage_payments_title_delete') }}",
              text: "{{ trans('multi-leng.manage_payments_text_delete') }}",
              icon: "warning",
              buttons: true,
              dangerMode: true,
              })
              .then((willDelete) => {
              if (willDelete) {
                  @this.call('destroy', idTransaction)
                  swal("{{ trans('multi-leng.manage_payments_swal_delete') }}", {
                  icon: "success",
                  });
              } else {

              }
              });
          })

          })
  </script>

  @endsection

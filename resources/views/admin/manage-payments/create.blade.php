<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.manage_payments_payment_details') }}</h3>
    </div>

    @if (session()->has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('success-message') }}
        </div>
    @endif

    @if (session()->has('error-message'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('error-message') }}
        </div>
    @endif


    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="id_transfer">{{ trans('multi-leng.manage_payments_id') }}</label>
                @error('id_transfer')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="id_transfer">
            </div>
            <div class="form-group col-md-4">
                <label for="id_transaction">{{ trans('multi-leng.manage_payments_transaction_id') }}</label>
                @error('id_transaction')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" readonly wire:model="id_transaction">
            </div>
            <div class="form-group col-md-4">
                <label for="payment_method ">{{ trans('multi-leng.manage_payments_payment_gateway') }}</label>
                @error('payment_method ')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="payment_method">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="plan">{{ trans('multi-leng.manage_payments_plan') }}</label>
                @error('plan')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="plan">
            </div>
            <div class="form-group col-md-4">
                <label for="days">{{ trans('multi-leng.manage_payments_days') }}</label>
                @error('days')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="days">
            </div>
            <div class="form-group col-md-4">
                <label for="cost">{{ trans('multi-leng.manage_payments_local_cost') }}</label>
                @error('cost')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="cost">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="cost_dolar">{{ trans('multi-leng.manage_payments_dolar_cost') }}</label>
                @error('cost_dolar')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="cost_dolar">
            </div>
            <div class="form-group col-md-4">
                <label for="name">{{ trans('multi-leng.manage_payments_name') }}</label>
                @error('name')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="name">
            </div>
            <div class="form-group col-md-4">
                <label for="type_document">{{ trans('multi-leng.manage_payments_type_document') }}</label>
                @error('type_document')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="type_document">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="number_document">{{ trans('multi-leng.manage_payments_document_number') }}</label>
                @error('number_document')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="number_document">
            </div>
            <div class="form-group col-md-4">
                <label for="cellphone">{{ trans('multi-leng.manage_payments_cellphone') }}</label>
                @error('cellphone')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="cellphone">
            </div>
            <div class="form-group col-md-4">
                <label for="email">{{ trans('multi-leng.manage_payments_email') }}</label>
                @error('email')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="email" readonly class="form-control" wire:model="email">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="address">{{ trans('multi-leng.manage_payments_address') }}</label>
                @error('address')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="address">
            </div>
            <div class="form-group col-md-4">
                <label for="city">{{ trans('multi-leng.manage_payments_city') }}</label>
                @error('city')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="city">
            </div>
            <div class="form-group col-md-4">
                <label for="country">{{ trans('multi-leng.manage_payments_country') }}</label>
                @error('country')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="country">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="payment_date">{{ trans('multi-leng.manage_payments_transaction_date') }}</label>
                @error('payment_date')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="payment_date">
            </div>
        </div>
    </div>
</div>

<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.manage_payments_transfer_payment_details') }}</h3>
    </div>

    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="proof_transfer">{{ trans('multi-leng.manage_payments_proof_transfer') }}</label>
                @error('proof_transfer')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="proof_transfer"><br>
                <button type="button" class="btn btn-secondary px-3" data-toggle="modal" data-target="#exampleModalCenter">
                    <i class="fas fa-eye"></i>&nbsp; {{ trans('multi-leng.manage_payments_view_file') }}
                </button>
            </div>
            <div class="form-group col-md-4">
                <label for="transfer_date"> {{ trans('multi-leng.manage_payments_transfer_date') }}</label>
                @error('transfer_date')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" readonly class="form-control" wire:model="transfer_date">
            </div>

            <div class="form-group col-md-4">
                <label for="status">{{ trans('multi-leng.manage_payments_transfer_status') }}</label>
                @error('status')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <select @if ($payment_method != 'Transferencia Bancaria' || $refStatus->status == '1' || $refStatus->status == '2') disabled @endif wire:model="status" class="form-control">
                    <option value="">{{ trans('multi-leng.manage_payments_transfer_select_status') }}</option>
                    <option value="0">{{ trans('multi-leng.manage_payments_transfer_status_pending') }}</option>
                    <option value="1">{{ trans('multi-leng.manage_payments_transfer_select_approved') }}</option>
                    <option value="2">{{ trans('multi-leng.manage_payments_transfer_select_rejected') }}</option>
                </select>
            </div>
        </div>


        @if ($proof_transfer != null)
          <!-- Modal -->
        <div class="modal fade modal--radios" wire:ignore.self id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="max-height:95vh">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content rounded-0 border-0" style="background-color:#343A40">
                <div class="modal-header pt-1 pb-1 rounded-0 border-0 bg-white">
                    <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('multi-leng.manage_payments_proof_transfer') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body d-flex justify-content-center align-items-center p-1">
                  <img src="{{ asset('documents/voucher-transfer/'.$proof_transfer) }}" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
        @endif

    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button wire:click="store" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.manage_payments_save') }}</button>
        @if ($flag == 1)
        <button wire:click="default" class="btn btn-default float-right"><i class="fas fa-times"></i>&nbsp; {{ trans('multi-leng.manage_payments_cancel') }}</button>
        @endif


    </div>
    <!-- /.card-footer -->
</div>

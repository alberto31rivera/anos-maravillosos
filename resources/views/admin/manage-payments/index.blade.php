@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_payments') }}
@endsection


@section('content')
    <livewire:manage-payments/>
@endsection


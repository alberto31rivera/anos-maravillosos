@extends('layouts.template')

@section('title')
{{ trans('multi-leng.profile_admin') }}
@endsection


@section('content')
<div>
    <div class="row">
        <div class="col-md-12">
                @include('admin.profile.create')
        </div>
    </div>
</div>

@endsection


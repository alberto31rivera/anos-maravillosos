<form action="{{ route('profile.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.profile_data') }}</h3>
    </div>

    @if (Session::has('success-message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ Session::get('success-message') }}
    </div>
@endif

@if (Session::has('error-message'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ Session::get('error-message') }}
    </div>
@endif

    <div class="card-body">
        <div class="form-row">

            <div class="form-group col-md-6">
                <label for="avatar">{{ trans('multi-leng.profile_avatar') }}</label>
                @error('avatar')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="file" class="form-control-file file-decoration px-0" id="avatar" onchange="loadPreviewAvatar(this);" name="avatar">
                <div class="contains-img-avatar">
                @if ($admin->avatar != '')
                {{-- <a id="close-image" role="button" onclick="clearContentImg('image')" class="btn btn-light btn--close-ad position-absolute">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a> --}}
                    <img id="preview_avatar" class="img-fluid" src={{ asset('img/avatar-users/'.$admin->avatar)}}>
                @else
                    <img id="preview_avatar" class="img-fluid"
                        src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png"
                       />
                @endif
                </div>

            </div>

            <div class="form-group col-md-6">
                <label for="logo">{{ trans('multi-leng.profile_logo') }}</label>
                @error('logo')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="file" class="form-control-file file-decoration px-0" id="logo" onchange="loadPreviewLogo(this);" name="logo">
                <div class="contains-img-avatar">
                @if ($admin->logo != '')
                {{-- <a id="close-image" role="button" onclick="clearContentImg('image')" class="btn btn-light btn--close-ad position-absolute">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a> --}}
                    <img id="preview_logo" class="img-fluid" src={{ asset('img/logos-users/'.$admin->logo)}}>
                @else
                    <img id="preview_logo" class="img-fluid"
                        src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png"
                       />
                @endif
                </div>

            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">{{ trans('multi-leng.profile_name') }} *</label>
                @error('name')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('name', $admin->name)}}" @endif placeholder="{{ trans('multi-leng.profile_name') }}" name="name">

            </div>

            <div class="form-group col-md-6">
                <label for="email">{{ trans('multi-leng.profile_email') }}</label>
                @error('email')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="email" @if($admin != null) value="{{old('email', $admin->email)}}" @endif class="form-control" disabled name="email">

            </div>

        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="number_document">{{ trans('multi-leng.profile_number_document') }} *</label>
                @error('number_document')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('number_document', $admin->number_document)}}" @endif placeholder="{{ trans('multi-leng.profile_number_document') }}" name="number_document">

            </div>

            <div class="form-group col-md-6">
                <label for="address">{{ trans('multi-leng.profile_address') }} *</label>
                @error('address')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('address', $admin->address)}}" @endif placeholder="{{ trans('multi-leng.profile_address') }}" name="address">

            </div>

        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="city">{{ trans('multi-leng.profile_city') }} *</label>
                @error('city')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('city', $admin->city)}}" @endif placeholder="{{ trans('multi-leng.profile_city') }}" name="city">

            </div>

            <div class="form-group col-md-6">
                <label for="country">{{ trans('multi-leng.profile_country') }} *</label>
                @error('country')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('country', $admin->country)}}" @endif placeholder="{{ trans('multi-leng.profile_country') }}" name="country">

            </div>

        </div>

        <div class="form-row">

            <div class="form-group col-md-6">
                <label for="description">{{ trans('multi-leng.profile_description') }} *</label>
                @error('description')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <textarea class="form-control" rows="3" placeholder="{{ trans('multi-leng.profile_description') }}" name="description">@if($admin != null) {{old('description', $admin->description)}} @endif</textarea>
            </div>

            <div class="form-group col-md-6">
                <label for="description_contact">{{ trans('multi-leng.profile_contact_description') }} *</label>
                @error('description_contact')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <textarea class="form-control" rows="3" placeholder="{{ trans('multi-leng.profile_contact_description') }}"
                    name="description_contact">@if($admin != null) {{old('description_contact', $admin->description_contact)}} @endif</textarea>
            </div>

        </div>

    </div>
</div>

<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.contact_information') }}</h3>
    </div>

    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="phone">{{ trans('multi-leng.profile_phone') }}</label>
                @error('phone')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('phone', $admin->phone)}}" @endif placeholder="{{ trans('multi-leng.profile_phone') }}" name="phone">
            </div>
            <div class="form-group col-md-4">
                <label for="cellphone">{{ trans('multi-leng.profile_cellphone') }} *</label>
                @error('cellphone')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('cellphone', $admin->cellphone)}}" @endif placeholder="{{ trans('multi-leng.profile_cellphone') }}" name="cellphone">
            </div>
            <div class="form-group col-md-4">
                <label for="whatsapp">{{ trans('multi-leng.profile_whatsapp') }} *</label>
                @error('whatsapp')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('whatsapp', $admin->whatsapp)}}" @endif placeholder="{{ trans('multi-leng.profile_whatsapp') }}" name="whatsapp">
            </div>
        </div>

    </div>
</div>

<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.social_networks') }}</h3>
    </div>

    <div class="card-body">

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="web_site">{{ trans('multi-leng.web_site') }}</label>
                @error('web_site')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('web_site', $admin->web_site)}}" @endif placeholder="{{ trans('multi-leng.web_site') }}" name="web_site">
            </div>
            <div class="form-group col-md-4">
                <label for="facebook">{{ trans('multi-leng.profile_facebook') }}</label>
                @error('facebook')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('facebook', $admin->facebook)}}" @endif placeholder="{{ trans('multi-leng.profile_facebook') }}" name="facebook">
            </div>
            <div class="form-group col-md-4">
                <label for="instagram">{{ trans('multi-leng.profile_instagram') }}</label>
                @error('instagram')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('instagram', $admin->instagram)}}" @endif placeholder="{{ trans('multi-leng.profile_instagram') }}" name="instagram">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="twitter">{{ trans('multi-leng.profile_twitter') }}</label>
                @error('twitter')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('twitter', $admin->twitter)}}" @endif placeholder="{{ trans('multi-leng.profile_twitter') }}" name="twitter">
            </div>
            <div class="form-group col-md-4">
                <label for="linkedin">{{ trans('multi-leng.profile_linkedin') }}</label>
                @error('linkedin')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('linkedin', $admin->linkedin)}}" @endif placeholder="{{ trans('multi-leng.profile_linkedin') }}" name="linkedin">
            </div>
            <div class="form-group col-md-4">
                <label for="youtube">{{ trans('multi-leng.profile_youtube') }}</label>
                @error('youtube')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" @if($admin != null) value="{{old('youtube', $admin->youtube)}}" @endif placeholder="{{ trans('multi-leng.profile_youtube') }}" name="youtube">
            </div>
        </div>
    </div>
</div>

<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.profile_change_password') }}</h3>
    </div>

    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="oldPassword">{{ trans('multi-leng.previous_password') }}</label>
                @error('oldPassword')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="password" class="form-control" placeholder="{{ trans('multi-leng.previous_password') }}" name="oldPassword">

            </div>
            <div class="form-group col-md-6">
                <label for="newPassword">{{ trans('multi-leng.new_password') }}</label>
                @error('newPassword')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="password" class="form-control" placeholder="{{ trans('multi-leng.new_password') }}" name="newPassword">

            </div>
        </div>
    </div>

    <!-- /.card-body -->
    <div class="card-footer">
        <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.profile_save') }}</button>
    </div>
    <!-- /.card-footer -->

</div>
</form>

<script>
    function loadPreviewAvatar(input, id) {
        id = id || '#preview_avatar';
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(id)
                    .attr('src', e.target.result)
                    // .width(200)
                    // .height(150)
                    ;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

<script>
    function loadPreviewLogo(input, id) {
        id = id || '#preview_logo';
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(id)
                    .attr('src', e.target.result)
                    // .width(200)
                    // .height(150)
                    ;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

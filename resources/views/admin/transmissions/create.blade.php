<form action="{{ route('transmissions.store') }}" method="POST">
    @csrf

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{trans('multi-leng.transmissions') }}</h3>
        </div>

        @if (Session::has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success-message') }}
            </div>
        @endif

        @if (Session::has('error-message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('error-message') }}
            </div>
        @endif

        <div class="card-body">
              <div class="form-group row">
                <label for="online_transmission" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.online_transmission') }}</label>
                <div class="col-sm-9">
                <textarea class="form-control" name="online_transmission" placeholder="{{trans('multi-leng.online_transmission_placeholder') }}" rows="7">@if($transmission != null) {{$transmission->online_transmission}} @endif</textarea>
              </div>
            </div><br>
            <div class="form-group row">
                <label for="online_chat" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.online_chat') }}</label>
                <div class="col-sm-9">
                <input type="text" class="form-control" name="online_chat" value="@if($transmission != null) {{$transmission->online_chat}} @endif" placeholder="{{trans('multi-leng.online_chat_placeholder') }}">
              </div>
            </div>
        </div>
         <!-- /.card-body -->
         <div class="card-footer">
            <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp;  {{trans('multi-leng.transmissions_save') }}</button>
        </div>
        <!-- /.card-footer -->

        </div>
    </div>
</form>

@extends('layouts.template')

@section('title')
{{trans('multi-leng.manage_transmissions') }}
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
             @include('admin.transmissions.create')
        </div>

    </div>
</div>
@endsection




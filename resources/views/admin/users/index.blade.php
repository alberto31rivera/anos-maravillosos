@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_users') }}
@endsection

@section('content')
<div>
        <div class="row">
            <div class="col-md-12">
                @if ($view == 'edit')
                @include('admin.users.form')
                @endif
                <livewire:manage-table-users/>
            </div>

        </div>
</div>
@endsection

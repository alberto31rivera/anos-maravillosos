
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.users_edit') }}</h3>

    </div>

    @if (Session::has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success-message') }}
        </div>
    @endif

    @if (Session::has('error-message'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('error-message') }}
        </div>
    @endif

    <form action="{{ route('list.update', $user->id) }}" method="POST" enctype="multipart/form-data">
        <div class="card-body">
            @csrf
            @method('PUT')
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="avatar">{{ trans('multi-leng.profile_avatar') }}</label>
                    @error('avatar')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="file" class="form-control-file file-decoration px-0" id="avatar" onchange="loadPreviewAvatar(this);" name="avatar">
                    <div class="contains-img-avatar">
                    @if ($user->avatar != '')
                    {{-- <a id="close-image" role="button" onclick="clearContentImg('image')" class="btn btn-light btn--close-ad position-absolute">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a> --}}
                        <img id="preview_avatar" class="img-fluid" src={{ asset('img/avatar-users/'.$user->avatar)}}>
                    @else
                        <img id="preview_avatar" class="img-fluid"
                            src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png"
                           />
                    @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <label for="is_banned">{{ trans('multi-leng.users_banned') }}</label>
                    @error('is_banned')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                     <select class="form-control" name="is_banned">
                         <option value="0" {{ 0 == old('is_banned', $user->is_banned) ? 'selected' : '' }}>{{ trans('multi-leng.users_banned_no') }}</option>
                         <option value="1" {{ 1 == old('is_banned', $user->is_banned) ? 'selected' : '' }}>{{ trans('multi-leng.users_banned_yes') }}</option>
                    </select>
                </div>
            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="name">{{ trans('multi-leng.users_name') }} *</label>
                    @error('name')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if($user != null) value="{{old('name', $user->name)}}" @endif  name="name">

                </div>

                <div class="form-group col-md-6">
                    <label for="cellphone">{{ trans('multi-leng.users_cellphone') }} *</label>
                    @error('cellphone')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if($user != null) value="{{old('cellphone', $user->cellphone)}}" @endif name="cellphone">

                </div>

            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="email">{{ trans('multi-leng.users_email') }}</label>
                    @error('email')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="email" class="form-control" readonly name="email" @if($user != null) value="{{old('email', $user->email)}}" @endif>

                </div>

                <div class="form-group col-md-6">
                    <label for="type_document">{{ trans('multi-leng.users_type_document') }}</label>
                    @error('type_document')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                     <select class="form-control" name="type_document">
                        <option value="dni" {{ 'dni' == old('type_document', $user->type_document) ? 'selected' : '' }}>{{ trans('multi-leng.users_number_dni') }}</option>
                        <option value="ruc" {{ 'ruc' == old('type_document', $user->type_document) ? 'selected' : '' }}>{{ trans('multi-leng.users_number_ruc') }}</option>
                        <option value="ce" {{ 'ce' == old('type_document', $user->type_document) ? 'selected' : '' }}>{{ trans('multi-leng.users_number_ce') }}</option>
                    </select>
                </div>

            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="number_document">{{ trans('multi-leng.users_number_document') }}</label>
                    @error('number_document')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" name="number_document" @if($user != null) value="{{old('number_document', $user->number_document)}}" @endif>

                </div>

                <div class="form-group col-md-6">
                    <label for="address">{{ trans('multi-leng.users_address') }}</label>
                    @error('address')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" name="address" @if($user != null) value="{{old('address', $user->address)}}" @endif>

                </div>

            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="city">{{ trans('multi-leng.users_city') }}</label>
                    @error('city')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" name="city" @if($user != null) value="{{old('city', $user->city)}}" @endif>

                </div>

                <div class="form-group col-md-6">
                    <label for="country">{{ trans('multi-leng.users_country') }}</label>
                    @error('country')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" name="country" @if($user != null) value="{{old('country', $user->country)}}" @endif>

                </div>

            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="password">{{ trans('multi-leng.users_new_password') }}</label>
                    @error('password')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="password" class="form-control" placeholder="{{ trans('multi-leng.users_new_password') }}" name="password">

                </div>
                <div class="form-group col-md-6">
                    <label for="confirm_password">{{ trans('multi-leng.users_confirm_password') }}</label>
                    @error('confirm_password')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="password" class="form-control" placeholder="{{ trans('multi-leng.users_confirm_password') }}" name="confirm_password">

                </div>
            </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.users_update') }}</button>
            <a href="{{ route('list.index') }}" class="btn btn-default float-right"><i class="fas fa-times"></i>&nbsp; {{ trans('multi-leng.users_cancel') }}</a>
        </div>
        <!-- /.card-footer -->
    </form>


</div>

<script>
    function loadPreviewAvatar(input, id) {
        id = id || '#preview_avatar';
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(id)
                    .attr('src', e.target.result)
                    .width(200)
                    .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

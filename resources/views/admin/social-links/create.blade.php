<form action="{{ route('social-links.store') }}" method="POST">
    @csrf

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{trans('multi-leng.social_links') }}</h3>
        </div>

        @if (Session::has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success-message') }}
            </div>
        @endif

        @if (Session::has('error-message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('error-message') }}
            </div>
        @endif

        <div class="card-body">
            <div class="form-group row">
                <label for="facebook_client_id" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.facebook_client_id') }}</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="facebook_client_id" @if($socialLinks != null) value="{{ $socialLinks->facebook_client_id }}" @endif placeholder="{{trans('multi-leng.facebook_client_id') }}">
                </div>
              </div>
              <br>
              <div class="form-group row">
                <label for="facebook_client_secret" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.facebook_client_secret') }}</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" name="facebook_client_secret" @if($socialLinks != null) value="{{ $socialLinks->facebook_client_secret }}" @endif placeholder="{{trans('multi-leng.facebook_client_secret') }}">
                </div>
              </div>

              <div class="form-group row">
                <label for="facebook_status" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.facebook_login') }}</label>
                <div class="col-sm-9">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="facebook_status" id="facebook_status" value="1" @if ($socialLinks != null) @if ($socialLinks->facebook_status == 1) checked @endif @endif>
                        <label class="form-check-label" for="facebook_status">
                            {{trans('multi-leng.facebook_login_on') }}
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="facebook_status" id="facebook_status" value="0" @if ($socialLinks != null) @if ($socialLinks->facebook_status == 0) checked @endif @else checked @endif>
                        <label class="form-check-label" for="facebook_status">
                            {{trans('multi-leng.facebook_login_off') }}
                        </label>
                      </div>
                </div>
              </div>
              <br>
              <div class="form-group row">
                <label for="google_client_id" class="col-sm-3 col-md-2 col-form-label text-sm-right"> {{trans('multi-leng.google_client_id') }}</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="google_client_id" @if($socialLinks != null) value="{{ $socialLinks->google_client_id }}" @endif placeholder=" {{trans('multi-leng.google_client_id') }} ">
                </div>
              </div>
              <br>
              <div class="form-group row">
                <label for="google_client_secret" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.google_client_secret') }}</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" name="google_client_secret" @if($socialLinks != null) value="{{ $socialLinks->google_client_secret }}" @endif placeholder="{{trans('multi-leng.google_client_secret') }}">
                </div>
              </div>

              <div class="form-group row">
                <label for="google_status" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.google_login') }}</label>
                <div class="col-sm-9">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="google_status" id="google_status" value="1" @if ($socialLinks != null) @if ($socialLinks->google_status == 1) checked @endif @endif>
                        <label class="form-check-label" for="google_status">
                            {{trans('multi-leng.google_login_on') }}
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="google_status" id="google_status" value="0" @if ($socialLinks != null) @if ($socialLinks->google_status == 0) checked @endif @else checked @endif>
                        <label class="form-check-label" for="google_status">
                            {{trans('multi-leng.google_login_off') }}
                        </label>
                      </div>
                </div>
              </div>

        </div>

         <!-- /.card-body -->
         <div class="card-footer">
            <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{trans('multi-leng.social_links_save') }}</button>
        </div>
        <!-- /.card-footer -->

    </div>
</form>

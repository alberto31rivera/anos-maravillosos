@extends('layouts.template')

@section('title')
{{trans('multi-leng.manage_social_links') }}
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
             @include('admin.social-links.create')
        </div>

    </div>
</div>
@endsection



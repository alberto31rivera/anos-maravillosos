@extends('layouts.template')

@section('title')
    Dashboard
@endsection


@section('content')

    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{ $advertisements }}</h3>

                    <p>{{trans('multi-leng.dashborad_advertisements') }}</p>
                </div>
                <div class="icon">
                    <i class="fas fa-money-bill"></i>
                </div>
                <a href="{{ route('advertisements.index') }}" class="small-box-footer">{{trans('multi-leng.see_more') }}... <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{ $netEarnings }} {{ $currencyLocal->name }}</h3>

                    <p>{{trans('multi-leng.net_earnings') }}</p>
                </div>
                <div class="icon">
                    <i class="fas fa-dollar-sign"></i>
                </div>
                <span class="small-box-footer">{{trans('multi-leng.net_earnings') }}</span>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{ $vendors }}</h3>

                    <p>{{trans('multi-leng.dashborad_vendors') }}</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-friends"></i>
                </div>
                <a href="{{ route('admin.vendors') }}" class="small-box-footer">{{trans('multi-leng.see_more') }}... <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{ $posts }}</h3>

                    <p>{{trans('multi-leng.dashborad_posts') }}</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-edit"></i>
                </div>
                <a href="{{ route('admin.posts') }}" class="small-box-footer">{{trans('multi-leng.see_more') }}... <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <div class="card">
                <div class="card-header ui-sortable-handle">
                    <h3 class="card-title">
                        <i class="fas fa-chart-pie mr-1"></i>
                        {{trans('multi-leng.monthly_statistics') }}
                    </h3>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content p-0">
                        <!-- Morris chart - Sales -->
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="description-block border-right">
                                    <h2 class="text-red">{{ $stat_revenue_today }} {{ $currencyLocal->name }}</h2>
                                    <span class="description-text text-black">{{trans('multi-leng.today_income') }}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 col-xs-12">
                                <div class="description-block border-right">
                                    <h2 class="text-red">{{ $stat_revenue_week }} {{ $currencyLocal->name }}</h2>
                                    <span class="description-text text-black">{{trans('multi-leng.this_week_income') }}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 col-xs-12">
                                <div class="description-block">
                                    <h2 class="text-red">{{ $stat_revenue_month }} {{ $currencyLocal->name }}</h2>
                                    <span class="description-text text-black">{{trans('multi-leng.this_month_income') }}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div><!-- /.card-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <h4 class="mb-4"><b>{{trans('multi-leng.current_year_earnings') }}</b></h4>
                <form  id="filterByMonth" method="GET">

                  <span>{{trans('multi-leng.month') }}</span>
                  <select name="monthFilter" id="monthSelect" class="form-control">
                    <option value="01" {{ $monthNumber == '01' ? 'selected' : ''}}>{{trans('multi-leng.january') }}</option>
                    <option value="02" {{ $monthNumber == '02' ? 'selected' : ''}}>{{trans('multi-leng.february') }}</option>
                    <option value="03" {{ $monthNumber == '03' ? 'selected' : ''}}>{{trans('multi-leng.march') }}</option>
                    <option value="04" {{ $monthNumber == '04' ? 'selected' : ''}}>{{trans('multi-leng.april') }}</option>
                    <option value="05" {{ $monthNumber == '05' ? 'selected' : ''}}>{{trans('multi-leng.may') }}</option>
                    <option value="06" {{ $monthNumber == '06' ? 'selected' : ''}}>{{trans('multi-leng.june') }}</option>
                    <option value="07" {{ $monthNumber == '07' ? 'selected' : ''}}>{{trans('multi-leng.july') }}</option>
                    <option value="08" {{ $monthNumber == '08' ? 'selected' : ''}}>{{trans('multi-leng.august') }}</option>
                    <option value="09" {{ $monthNumber == '09' ? 'selected' : ''}}>{{trans('multi-leng.september') }}</option>
                    <option value="10" {{ $monthNumber == '10' ? 'selected' : ''}}>{{trans('multi-leng.october') }}</option>
                    <option value="11" {{ $monthNumber == '11' ? 'selected' : ''}}>{{trans('multi-leng.november') }}</option>
                    <option value="12" {{ $monthNumber == '12' ? 'selected' : ''}}>{{trans('multi-leng.december') }}</option>
                  </select>

                </form>

                <br>
        <canvas id="myChart"></canvas>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function() {
       $("#monthSelect").change(function(){
         $("#filterByMonth").submit();
     });
  });
  </script>


<script>
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [{!!$label!!}],
        datasets: [{
            label: 'Ganancias',
            data: [{!!$data!!}],
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 2,
            fill: true,
            lineTension: 0,
        }]
    },
    options: {
        scales: {
              yAxes: [{
                  ticks: {
                      min: 0, // it is for ignoring negative step.
                      beginAtZero: true,
                      callback: function(value, index, values) {
                        return value + ' {{$currencyLocal->name}}';
                      }
                  }
              }]
          },
          tooltips: {
              callbacks: {
                  label: function(t, d) {
                      var xLabel = d.datasets[t.datasetIndex].label;
                      var yLabel = t.yLabel + ' {{$currencyLocal->name}}';
                      return xLabel + ': ' + yLabel;
                  }
              }
          },
          legend: {
              display: false
          }
    }
});
</script>

@endsection

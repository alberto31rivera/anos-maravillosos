@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_sponsors') }}
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
            @if ($view == 'create')
                @include('admin.sponsors.create')
            @else
                @include('admin.sponsors.edit')
            @endif
        </div>

        <div class="col-md-12">
            <livewire:manage-table-sponsors/>
        </div>

    </div>
</div>
@endsection


@section('scripts')

<script>
    function deleteItem(idForm)
    {
    swal({
    title: "{{ trans('multi-leng.sponsors_title_delete') }}",
    text: "{{ trans('multi-leng.sponsors_text_delete') }}",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    })
    .then((willDelete) => {
    if (willDelete) {
        document.getElementById(idForm).submit();
    }
    });
    }
</script>

@endsection


@csrf

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="title">{{ trans('multi-leng.sponsors_title') }} *</label>
        @error('title')
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ $message }}
            </div>
        @enderror
        <input type="text" class="form-control" value="{{ old('title') }}" name="title" id="title" placeholder="{{ trans('multi-leng.sponsors_title') }}">
    </div>
    <div class="form-group col-md-6">
        <label for="url">{{ trans('multi-leng.sponsors_url') }}</label>
        @error('url')
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ $message }}
            </div>
        @enderror
        <input type="text" class="form-control" value="{{ old('url') }}" name="url" id="url" placeholder="{{ trans('multi-leng.sponsors_url') }}">
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="image">{{ trans('multi-leng.sponsors_featured_image') }} *</label>
        @error('image')
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ $message }}
            </div>
        @enderror
        <input type="file" class="form-control-file file-decoration" id="image" onchange="loadPreview(this);" name="image">

        <div class="contains-img-media">

            {{-- <a id="close-image" role="button" onclick="clearContentImg('image')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                <i class="fa fa-times" aria-hidden="true"></i>
            </a> --}}

        <img id="preview_img" class="img-fluid"
            src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png"/>
        </div>

    </div>

    <div class="form-group col-md-6">
        <label for="program_id">{{ trans('multi-leng.sponsors_program') }} *</label>
        @error('program_id')
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ $message }}
            </div>
        @enderror
        <select class="form-control" name="program_id">
            <option selected disabled >{{ trans('multi-leng.sponsors_select_program') }}</option>
            @foreach ($programs as $item)
                <option value="{{ $item->id }}" {{ (old("program_id") == $item->id ? "selected":"") }}>{{ $item->title }}</option>
            @endforeach
        </select>
    </div>

</div>


<script>
    function loadPreview(input, id) {
        id = id || '#preview_img';
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(id)
                    .attr('src', e.target.result)
                    // .width(200)
                    // .height(150)
                    ;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

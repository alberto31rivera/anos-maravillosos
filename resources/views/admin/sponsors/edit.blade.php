<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.sponsors_edit') }}</h3>
    </div>

    @if (Session::has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success-message') }}
        </div>
    @endif

    @if (Session::has('error-message'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('error-message') }}
        </div>
    @endif


    <form action="{{ route('sponsor.update', $sponsor->id) }}" method="POST" enctype="multipart/form-data">
        <div class="card-body">
            @csrf
            @method('PUT')

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title">{{ trans('multi-leng.sponsors_title') }} *</label>
                    @error('title')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" value="{{old('title', $sponsor->title)}}" name="title" id="title" placeholder="{{ trans('multi-leng.sponsors_title') }}">
                </div>
                <div class="form-group col-md-6">
                    <label for="url">{{ trans('multi-leng.sponsors_url') }}</label>
                    @error('url')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" value="{{old('url', $sponsor->url)}}" name="url" id="url" placeholder="{{ trans('multi-leng.sponsors_url') }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="image">{{ trans('multi-leng.sponsors_featured_image') }} *</label>
                    @error('image')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="file" class="form-control-file file-decoration px-0" id="image" onchange="loadPreview(this);" name="image">

                    <div class="contains-img-media">
                    {{-- <a id="close-image" role="button" onclick="clearContentImg('image')" class="btn btn-light btn--close-ad position-absolute">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a> --}}
                    @if ($sponsor->image != '')
                        <img id="preview_img" class="img-fluid" src={{ asset('img/sponsors/' . $sponsor->image) }}>
                    @else
                        <img id="preview_img" class="img-fluid"
                            src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png"
                           />
                    @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <label for="program_id">{{ trans('multi-leng.sponsors_program') }} *</label>
                    @error('program_id')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <select class="form-control" name="program_id">
                        <option selected disabled >{{ trans('multi-leng.sponsors_select_program') }}</option>
                        @foreach ($programs as $item)
                        <option value="{{ $item->id }}"
                            {{ $item->id == old('category_id', $sponsor->program_id) ? 'selected' : '' }}>{{ $item->title }}</option>
                    @endforeach
                    </select>
                </div>
            </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.sponsors_update') }}</button>
            <a href="{{ route('sponsor.index') }}" class="btn btn-default float-right"><i class="fas fa-times"></i>&nbsp; {{ trans('multi-leng.sponsors_cancel') }}</a>
        </div>
        <!-- /.card-footer -->
    </form>


</div>

<script>
    function loadPreview(input, id) {
        id = id || '#preview_img';
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(id)
                    .attr('src', e.target.result)
                    .width(200)
                    .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

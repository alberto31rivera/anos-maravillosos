@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_vendors') }}
@endsection


@section('content')
    <livewire:manage-vendors/>
@endsection


<div class="card card-dark">
    <div class="card-header">
        <div class="row">

            <h3 class="card-title col-sm-6 col-md-7 col-lg-8 mb-2 mb-sm-0 d-flex align-items-center">{{ trans('multi-leng.vendors_list') }}</h3>

            <div class="card-tools col-sm-6 col-md-5 col-lg-4">
                <div class="input-group input-group-sm">
                    <input type="text" wire:model="search" class="form-control float-right"
                        placeholder="{{ trans('multi-leng.vendors_search_name') }}">

                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <table class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th>{{ trans('multi-leng.vendors_document_number') }}</th>
                    <th>{{ trans('multi-leng.vendors_name') }} </th>
                    <th>{{ trans('multi-leng.vendors_email') }}</th>
                    <th>{{ trans('multi-leng.vendors_cellphone') }}</th>
                    <th>{{ trans('multi-leng.vendors_actions') }}</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($vendors as $item)
                    <tr>
                        <td>{{ $item->dni }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->cellphone }}</td>
                        <td>
                            <button wire:click="edit({{ $item->id }})" class="btn btn-sm btn-info"><i
                                    class='fas fa-edit'></i></button>
                            <button onclick="sendVendorId({{ $item->id }})" class="btn btn-sm btn-danger"><i
                                    class='fas fa-trash'></i></button>
                        </td>
                    </tr>
                @endforeach

            </tbody>

        </table>

    </div>

    <div class="card-footer">
        {{ $vendors->links() }}
    </div>

</div>


@section('scripts')

    <script>
        function sendVendorId(idVendor) {

            const url = "/admin/delete/vendor/" + idVendor;
            const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

            Swal.fire({
                title: "{{ trans('multi-leng.vendors_title_delete') }}",
                text: "{{ trans('multi-leng.vendors_text_delete') }}",
                showCancelButton: true,
                confirmButtonText: "{{ trans('multi-leng.categories_title_confirm') }}",
                cancelButtonText: "{{ trans('multi-leng.categories_title_cancel') }}",
                showLoaderOnConfirm: true,
                preConfirm: () => {

                    return fetch(url, {
                            headers: {
                                "Content-Type": "application/json",
                                "Accept": "application/json, text-plain, */*",
                                "X-Requested-With": "XMLHttpRequest",
                                "X-CSRF-TOKEN": token
                            },
                            method: 'DELETE',
                        })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error(
                                    "{{ trans('multi-leng.vendors_text_error') }}"
                                )
                            }
                            return response.json()
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: "{{ trans('multi-leng.categories_swal_delete') }}",
                    })
                }
            }).then(function() {
                window.location.reload();
            })
        }

    </script>

@endsection

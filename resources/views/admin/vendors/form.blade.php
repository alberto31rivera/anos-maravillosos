
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="avatar">{{ trans('multi-leng.vendors_avatar') }} *</label>
                @error('avatar')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="file" class="form-control-file file-decoration" id="avatar" wire:model="avatar">

                <div class="contains-img-avatar">
                {{-- <a id="close-avatar" role="button" onclick="clearContentImg('avatar')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a> --}}
                @if ($avatar)
                    <img class="img-fluid" src="{{ $avatar->temporaryUrl() }}">
                @elseif($srcEditAvatar != '')
                    <img class="img-fluid" src={{ asset('img/' . $srcEditAvatar) }}>
                @endif
                </div>

            </div>
        </div>
    </div>
</div>

<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.profile_data') }}</h3>
    </div>

    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">{{ trans('multi-leng.vendors_name') }} *</label>
                @error('name')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_name') }}" wire:model="name">
            </div>
            <div class="form-group col-md-6">
                <label for="dni">{{ trans('multi-leng.vendors_document_number') }} *</label>
                @error('dni')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_document_number') }}" wire:model="dni">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="email">{{ trans('multi-leng.vendors_email') }} *</label>
                @error('email')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="email" class="form-control" placeholder="{{ trans('multi-leng.vendors_email') }}" wire:model="email">
            </div>
            <div class="form-group col-md-6">
                <label for="address">{{ trans('multi-leng.vendors_address') }}</label>
                @error('address')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_address') }}" wire:model="address">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="city">{{ trans('multi-leng.vendors_city') }}</label>
                @error('city')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_city') }}" wire:model="city">
            </div>
            <div class="form-group col-md-6">
                <label for="country">{{ trans('multi-leng.vendors_country') }}</label>
                @error('country')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_country') }}" wire:model="country">
            </div>
        </div>
    </div>
</div>

<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.vendors_contact_information') }}</h3>
    </div>

    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="phone">{{ trans('multi-leng.vendors_phone') }}</label>
                @error('phone')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_phone') }}" wire:model="phone">
            </div>
            <div class="form-group col-md-4">
                <label for="cellphone">{{ trans('multi-leng.vendors_cellphone') }} *</label>
                @error('cellphone')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_cellphone') }}" wire:model="cellphone">
            </div>
            <div class="form-group col-md-4">
                <label for="whatsapp">{{ trans('multi-leng.vendors_whatsapp') }} *</label>
                @error('whatsapp')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_whatsapp') }}" wire:model="whatsapp">
            </div>
        </div>
    </div>
</div>

<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.vendors_social_networks') }}</h3>
    </div>

    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="web_site">{{ trans('multi-leng.vendors_web_site') }}</label>
                @error('web_site')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_web_site') }}" wire:model="web_site">
            </div>
            <div class="form-group col-md-4">
                <label for="facebook">{{ trans('multi-leng.vendors_facebook') }}</label>
                @error('facebook')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_facebook') }}" wire:model="facebook">
            </div>
            <div class="form-group col-md-4">
                <label for="instagram">{{ trans('multi-leng.vendors_instagram') }}</label>
                @error('instagram')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_instagram') }}" wire:model="instagram">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="twitter">{{ trans('multi-leng.vendors_twitter') }}</label>
                @error('twitter')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_twitter') }}" wire:model="twitter">
            </div>
            <div class="form-group col-md-4">
                <label for="linkedin">{{ trans('multi-leng.vendors_linkedin') }}</label>
                @error('linkedin')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_linkedin') }}" wire:model="linkedin">
            </div>
            <div class="form-group col-md-4">
                <label for="youtube">{{ trans('multi-leng.vendors_youtube') }}</label>
                @error('youtube')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" class="form-control" placeholder="{{ trans('multi-leng.vendors_youtube') }}" wire:model="youtube">
            </div>
        </div>
    </div>






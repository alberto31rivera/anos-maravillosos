@extends('layouts.template')

@section('title')
{{trans('multi-leng.manage_emails') }}
@endsection


@section('content')
    <livewire:manage-email/>
@endsection


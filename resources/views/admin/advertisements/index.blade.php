@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_advertisements') }}
@endsection

@section('styles')
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
            @if ($view == 'create')
                @include('admin.advertisements.create')
            @else
                @include('admin.advertisements.edit')
            @endif
        </div>

        <div class="col-md-12">
            <livewire:manage-table-advertisements/>
        </div>

    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {

        $('#summernote').summernote({
            height: 150,
        });
    });

    function deleteItem(idForm)
    {
    swal({
    title: "{{ trans('multi-leng.manage_advertisements_title_delete') }}",
    text: "{{ trans('multi-leng.manage_advertisements_text_delete') }}",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    })
    .then((willDelete) => {
    if (willDelete) {
        document.getElementById(idForm).submit();
    }
    });
    }
</script>

<script>
    $("select[name='plan_id']").on("change", () => {
    checkCost();
    });

    function checkCost() {

        var parametros = {
                    "_token": "{{ csrf_token() }}",
                    "idPlan": $("select[name='plan_id']").val(),
                }
        $.ajax({
            url:"{{route('advertisements.cost')}}",
            type: "POST",
            data: parametros,

            success: function(data){
               $('#cost').val(data.cost + ' {{ $currencyLocal->name }}');
               $('#cost_dolar').val(data.cost_dolar + ' {{ $currencyDolar->name }}');
            }
          });
    };

</script>

@endsection

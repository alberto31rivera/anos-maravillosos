@csrf
<div class="form-row">
  <div class="form-group col-md-4">
    <label for="title">{{ trans('multi-leng.manage_advertisements_title') }} *</label>
    @error('title')
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         {{ $message }}
    </div>
    @enderror
    <input type="text" class="form-control" value="{{ old('title') }}" name="title" placeholder="Título">
  </div>
  <div class="form-group col-md-4">
    <label for="vendor_id">{{ trans('multi-leng.manage_advertisements_seller') }} *</label>
    @error('vendor_id')
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         {{ $message }}
    </div>
    @enderror
    <select class="form-control"  name="vendor_id">
        <option selected disabled >{{ trans('multi-leng.manage_advertisements_select_seller') }}</option>
         @foreach ($vendors as $item)
             <option value="{{ $item->id }}" {{ (old("vendor_id") == $item->id ? "selected":"") }}>{{ $item->name }}</option>
         @endforeach
       </select>
  </div>
  <div class="form-group col-md-4">
    <label for="category_id">{{ trans('multi-leng.manage_advertisements_category') }} *</label>
    @error('category_id')
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         {{ $message }}
    </div>
    @enderror
    <select class="form-control"  name="category_id">
        <option selected disabled >{{ trans('multi-leng.manage_advertisements_select_category') }}</option>
         @foreach ($categories as $item)
             <option value="{{ $item->id }}" {{ (old("category_id") == $item->id ? "selected":"") }}>{{ $item->title }}</option>
         @endforeach
       </select>
  </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
      <label for="description">{{ trans('multi-leng.manage_advertisements_description') }} *</label>
      @error('description')
      <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
           {{ $message }}
      </div>
      @enderror
      <textarea name="description" id="summernote">{!! old('description') !!}</textarea>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-4">
        <label for="type_advertising">{{ trans('multi-leng.manage_advertisements_advertising_type') }} *</label>
        @error('type_advertising')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
        @enderror
        <select class="form-control"  name="type_advertising">
            <option selected disabled >{{ trans('multi-leng.manage_advertisements_select_advertising_type') }}</option>
            <option value="Foto" {{ (old("type_advertising") == 'Foto' ? "selected":"") }}>{{ trans('multi-leng.manage_advertisements_photo') }}</option>
            <option value="Audio" {{ (old("type_advertising") == 'Audio' ? "selected":"") }}>{{ trans('multi-leng.manage_advertisements_audio') }}</option>
            <option value="Video" {{ (old("type_advertising") == 'Video' ? "selected":"") }}>{{ trans('multi-leng.manage_advertisements_video') }}</option>
           </select>
      </div>

      <div class="form-group col-md-4">
        <label for="start_date">{{ trans('multi-leng.manage_advertisements_start_date') }} *</label>
        @error('start_date')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
    @enderror
    <input type="date" class="form-control" value="{{ old('start_date') }}" placeholder="{{ trans('multi-leng.manage_advertisements_start_date') }}" name="start_date">
      </div>
      <div class="form-group col-md-4">
        <label for="end_date">{{ trans('multi-leng.manage_advertisements_end_date') }} *</label>
        @error('end_date')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
    @enderror
    <input type="date" class="form-control" value="{{ old('end_date') }}" placeholder="{{ trans('multi-leng.manage_advertisements_end_date') }}" name="end_date">
      </div>

  </div>

  <div class="form-row">
    <div class="form-group col-md-4">
        <label for="plan_id">{{ trans('multi-leng.manage_advertisements_plan') }} *</label>
        @error('plan_id')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
        @enderror
        <select class="form-control"  name="plan_id">
            <option selected disabled >{{ trans('multi-leng.manage_advertisements_select_plan') }}</option>
             @foreach ($plans as $item)
             <option value="{{ $item->id }}" {{ (old("plan_id") == $item->id ? "selected":"") }}>{{ $item->title }}</option>
             @endforeach
           </select>
      </div>

      <div class="form-group col-md-4">
        <label for="cost">{{ trans('multi-leng.manage_advertisements_local_cost') }}</label>
        @error('cost')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
    @enderror
    <input type="text" class="form-control" readonly placeholder="{{ trans('multi-leng.manage_advertisements_local_cost') }}" id="cost" name="cost">
      </div>
      <div class="form-group col-md-4">
        <label for="cost_dolar">{{ trans('multi-leng.manage_advertisements_dolar_cost') }}</label>
        @error('cost_dolar')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
    @enderror
    <input type="text" class="form-control" readonly placeholder="{{ trans('multi-leng.manage_advertisements_dolar_cost') }}" id="cost_dolar" name="cost_dolar">
      </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
        <label for="image">{{ trans('multi-leng.manage_advertisements_photo') }} *</label>
        @error('image')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
        @enderror
        <input type="file" class="form-control-file file-decoration" id="image" onchange="loadPreview(this);" name="image">

        <div class="contains-img-media">

          {{-- <a id="close-image" role="button" onclick="clearContentImg('image')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
            <i class="fa fa-times" aria-hidden="true"></i>
        </a> --}}

          <img id="preview_img" class="img-fluid" src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png"/>
        </div>

      </div>

      <div class="form-group col-md-6">
        <label for="pago">{{ trans('multi-leng.manage_advertisements_payment') }}</label>
        @error('pago')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
        @enderror
        <input type="text" class="form-control" value="{{ old('pago') }}" placeholder="{{ trans('multi-leng.manage_advertisements_payment') }}" name="pago">
      </div>

  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
        <label for="audio">{{ trans('multi-leng.manage_advertisements_audio') }}</label>
        @error('audio')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
        @enderror
        <input type="file" class="form-control-file file-decoration" name="audio" value="" onchange="PreviewAudio(this, $('#audioPreview'))" />

<audio controls="controls" id="audioPreview" style="display:none">
      <source src="" type="audio/mp4" />
</audio>
      </div>
    <div class="form-group col-md-6">
        <label for="video">{{ trans('multi-leng.manage_advertisements_url_video') }}</label>
        @error('video')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
        @enderror
        <input type="text" class="form-control" value="{{ old('video') }}" placeholder="{{ trans('multi-leng.manage_advertisements_url_video') }}" name="video">
      </div>
  </div>

  <script>
    function loadPreview(input, id) {
      id = id || '#preview_img';
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $(id)
                      .attr('src', e.target.result)
                      // .width(200)
                      // .height(150)
                      ;
          };

          reader.readAsDataURL(input.files[0]);
      }
   }
  </script>

  <script>
      function PreviewAudio(inputFile, previewElement) {

if (inputFile.files && inputFile.files[0] && $(previewElement).length > 0) {

    $(previewElement).stop();

    var reader = new FileReader();

    reader.onload = function (e) {

        $(previewElement).attr('src', e.target.result);
        var playResult = $(previewElement).get(0).play();

        if (playResult !== undefined) {
            playResult.then(_ => {
                // Automatic playback started!
                // Show playing UI.

                $(previewElement).show();
            })
                .catch(error => {
                    // Auto-play was prevented
                    // Show paused UI.

        $(previewElement).hide();
                    alert("File Is Not Valid Media File");
                });
        }
    };

    reader.readAsDataURL(inputFile.files[0]);
}
else {
    $(previewElement).attr('src', '');
    $(previewElement).hide();
    alert("File Not Selected");
}
}
  </script>


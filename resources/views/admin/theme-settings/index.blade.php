@extends('layouts.template')

@section('title')
{{trans('multi-leng.manage_themes') }}
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
             @include('admin.theme-settings.create')
        </div>

    </div>
</div>
@endsection



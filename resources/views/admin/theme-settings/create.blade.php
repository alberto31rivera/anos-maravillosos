<form action="{{ route('themes.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{trans('multi-leng.web_colors') }}</h3>
        </div>

        @if (Session::has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success-message') }}
            </div>
        @endif

        @if (Session::has('error-message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('error-message') }}
            </div>
        @endif

        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>{{trans('multi-leng.greeting_bar_color') }}</label>
                    @error('color_regards')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <div class="input-group my-colorpicker1">
                        <input type="text" name="color_regards" @if ($themeSettings != null) value="{{ $themeSettings->color_regards }}" @endif
                            class="form-control">

                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-square" style="color:@if ($themeSettings !=null) {{ $themeSettings->color_regards }} @endif "></i></span>
                        </div>
                      </div>
                </div>

                <div class="form-group col-md-4">
                    <label>{{ trans('multi-leng.color_text_regards') }}</label>
                    @error('color_text_regards')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <div class="input-group my-colorpicker2">
                        <input type="text" name="color_text_regards" @if ($themeSettings != null) value="{{ $themeSettings->color_text_regards }}" @endif
                            class="form-control">

                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-square" style="color:@if ($themeSettings !=null) {{ $themeSettings->color_text_regards }} @endif "></i></span>
                        </div>
                      </div>
                </div>

                <div class=" form-group col-md-4">
                                    <label>{{trans('multi-leng.header_bar_color') }}</label>
                                    @error('color_header')
                                        <div class="alert alert-danger alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert"
                                                aria-hidden="true">×</button>
                                            {{ $message }}
                                        </div>
                                    @enderror
                                    <div class="input-group my-colorpicker3">
                                        <input type="text" name="color_header" @if ($themeSettings != null) value="{{ $themeSettings->color_header }}" @endif class="form-control">

                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-square"
                                                    style="color:@if ($themeSettings !=null) {{ $themeSettings->color_header }} @endif "></i></span>
                        </div>
                      </div>
                </div>


                <div class=" form-group col-md-4">
                    <label>{{trans('multi-leng.color_text_header') }}</label>
                    @error('color_text_header')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <div class="input-group my-colorpicker4">
                        <input type="text" name="color_text_header" @if ($themeSettings != null) value="{{ $themeSettings->color_text_header }}" @endif class="form-control">

                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-square"
                                    style="color:@if ($themeSettings !=null) {{ $themeSettings->color_text_header }} @endif "></i></span>
        </div>
      </div>
</div>

<div class=" form-group col-md-4">
    <label>{{trans('multi-leng.color_highlight_header') }}</label>
    @error('color_highlight_header')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true">×</button>
            {{ $message }}
        </div>
    @enderror
    <div class="input-group my-colorpicker5">
        <input type="text" name="color_highlight_header" @if ($themeSettings != null) value="{{ $themeSettings->color_highlight_header }}" @endif class="form-control">

        <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-square"
                    style="color:@if ($themeSettings !=null) {{ $themeSettings->color_highlight_header }} @endif "></i></span>
</div>
</div>
</div>

                <div class=" form-group col-md-4">
                                                    <label>{{trans('multi-leng.footer_bar_color') }}</label>
                                                    @error('color_footer')
                                                        <div class="alert alert-danger alert-dismissible">
                                                            <button type="button" class="close" data-dismiss="alert"
                                                                aria-hidden="true">×</button>
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                    <div class="input-group my-colorpicker6">
                                                        <input type="text" name="color_footer" @if ($themeSettings != null) value="{{ $themeSettings->color_footer }}" @endif
                                                            class="form-control">

                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fas fa-square" style="color:@if ($themeSettings !=null) {{ $themeSettings->color_footer }} @endif "></i></span>
                                                        </div>
                                                      </div>

                                        </div>
                                    </div>

                                    <div class="form-row">

                                        <div class=" form-group col-md-4">
                                            <label>{{trans('multi-leng.color_text_footer') }}</label>
                                            @error('color_text_footer')
                                                <div class="alert alert-danger alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button>
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                            <div class="input-group my-colorpicker7">
                                                <input type="text" name="color_text_footer" @if ($themeSettings != null) value="{{ $themeSettings->color_text_footer }}" @endif
                                                    class="form-control">

                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fas fa-square" style="color:@if ($themeSettings !=null) {{ $themeSettings->color_text_footer }} @endif "></i></span>
                                                </div>
                                              </div>

                                </div>

                                        <div class="form-group col-md-4">
                                            <label>{{trans('multi-leng.contact_us_color') }}</label>
                                            @error('color_contact')
                                                <div class="alert alert-danger alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button>
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                            <div class="input-group my-colorpicker8">
                                                <input type="text" name="color_contact" @if ($themeSettings != null) value="{{ $themeSettings->color_contact }}" @endif
                                                    class="form-control">

                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fas fa-square" style="color:@if ($themeSettings !=null) {{ $themeSettings->color_contact }} @endif "></i></span>
                                                </div>
                                              </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>{{trans('multi-leng.button_color') }}</label>
                                            @error('color_buttons')
                                                <div class="alert alert-danger alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button>
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                            <div class="input-group my-colorpicker9">
                                                <input type="text" name="color_buttons" @if ($themeSettings != null) value="{{ $themeSettings->color_buttons }}" @endif
                                                    class="form-control">

                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fas fa-square" style="color:@if ($themeSettings !=null) {{ $themeSettings->color_buttons }} @endif "></i></span>
                                                </div>
                                              </div>
                                        </div>
                                    </div>
                        </div>

                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{trans('multi-leng.theme_save') }}</button>
                        </div>
                        <!-- /.card-footer -->

                    </div>

</form>

@section('scripts')
    <script>
        $('.my-colorpicker1').colorpicker()
        $('.my-colorpicker2').colorpicker()
        $('.my-colorpicker3').colorpicker()
        $('.my-colorpicker4').colorpicker()
        $('.my-colorpicker5').colorpicker()
        $('.my-colorpicker6').colorpicker()
        $('.my-colorpicker7').colorpicker()
        $('.my-colorpicker8').colorpicker()
        $('.my-colorpicker9').colorpicker()

        $('.my-colorpicker1').on('colorpickerChange', function(event) {
            $('.my-colorpicker1 .fa-square').css('color', event.color.toString());
        });

        $('.my-colorpicker2').on('colorpickerChange', function(event) {
            $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
        });

        $('.my-colorpicker3').on('colorpickerChange', function(event) {
            $('.my-colorpicker3 .fa-square').css('color', event.color.toString());
        });

        $('.my-colorpicker4').on('colorpickerChange', function(event) {
            $('.my-colorpicker4 .fa-square').css('color', event.color.toString());
        });

        $('.my-colorpicker5').on('colorpickerChange', function(event) {
            $('.my-colorpicker5 .fa-square').css('color', event.color.toString());
        });

        $('.my-colorpicker6').on('colorpickerChange', function(event) {
            $('.my-colorpicker6 .fa-square').css('color', event.color.toString());
        });

        $('.my-colorpicker7').on('colorpickerChange', function(event) {
            $('.my-colorpicker7 .fa-square').css('color', event.color.toString());
        });

        $('.my-colorpicker8').on('colorpickerChange', function(event) {
            $('.my-colorpicker8 .fa-square').css('color', event.color.toString());
        });

        $('.my-colorpicker9').on('colorpickerChange', function(event) {
            $('.my-colorpicker9 .fa-square').css('color', event.color.toString());
        });

    </script>
@endsection

<form action="{{ route('services.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{trans('multi-leng.footer_documents') }}</h3>
        </div>

        @if (Session::has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success-message') }}
            </div>
        @endif

        @if (Session::has('error-message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('error-message') }}
            </div>
        @endif

        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title_one">{{trans('multi-leng.service_title_one') }}</label>
                    @error('title_one')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if($services != null) value="{{ $services->title_one }}" @endif name="title_one" placeholder="{{trans('multi-leng.service_title_one') }}">
                </div>
                <div class="form-group col-md-6">
                    <label for="title_two">{{trans('multi-leng.service_title_two') }}</label>
                    @error('title_two')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if($services != null) value="{{ $services->title_two }}" @endif name="title_two" placeholder="{{trans('multi-leng.service_title_two') }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="description_one">{{trans('multi-leng.service_description_one') }}</label>
                    @error('description_one')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea name="description_one"
                        id="summernote">@if ($services != null) {!! $services->description_one !!} @endif</textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="description_two">{{trans('multi-leng.service_description_two') }}</label>
                    @error('description_two')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea name="description_two"
                        id="summernote2">@if ($services != null) {!! $services->description_two !!} @endif</textarea>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="status_one">{{trans('multi-leng.service_show_in_footer_one') }}</label>
                    @error('status_one')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                         {{ $message }}
                    </div>
                @enderror
                <select class="form-control" name="status_one">
                     <option value="0" @if ($services != null) @if ($services->status_one == 0) selected @endif @endif>{{trans('multi-leng.service_status_no') }}</option>
                     <option value="1" @if ($services != null) @if ($services->status_one == 1) selected @endif @endif>{{trans('multi-leng.service_status_yes') }}</option>
                   </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="status_two">{{trans('multi-leng.service_show_in_footer_two') }}</label>
                    @error('status_two')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                         {{ $message }}
                    </div>
                @enderror
                <select class="form-control" name="status_two">
                     <option value="0" @if ($services != null) @if ($services->status_two == 0) selected @endif @endif>{{trans('multi-leng.service_status_no') }}</option>
                     <option value="1" @if ($services != null) @if ($services->status_two == 1) selected @endif @endif>{{trans('multi-leng.service_status_yes') }}</option>
                   </select>
                </div>
            </div>
            <br>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title_three">{{trans('multi-leng.service_title_three') }}</label>
                    @error('title_three')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if($services != null) value="{{ $services->title_three }}" @endif name="title_three" placeholder="{{trans('multi-leng.service_title_three') }}">
                </div>
                <div class="form-group col-md-6">
                    <label for="title_four">{{trans('multi-leng.service_title_four') }}</label>
                    @error('title_four')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if($services != null) value="{{ $services->title_four }}" @endif name="title_four" placeholder="{{trans('multi-leng.service_title_four') }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="description_three">{{trans('multi-leng.service_description_three') }}</label>
                    @error('description_three')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea name="description_three"
                        id="summernote3">@if ($services != null) {!! $services->description_three !!} @endif</textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="description_four">{{trans('multi-leng.service_description_four') }}</label>
                    @error('description_four')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea name="description_four"
                        id="summernote4">@if ($services != null) {!! $services->description_four !!} @endif</textarea>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="status_three">{{trans('multi-leng.service_show_in_footer_three') }}</label>
                    @error('status_three')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                         {{ $message }}
                    </div>
                @enderror
                <select class="form-control" name="status_three">
                    <option value="0" @if ($services != null) @if ($services->status_three == 0) selected @endif @endif>{{trans('multi-leng.service_status_no') }}</option>
                     <option value="1" @if ($services != null) @if ($services->status_three == 1) selected @endif @endif>{{trans('multi-leng.service_status_yes') }}</option>
                   </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="status_four">{{trans('multi-leng.service_show_in_footer_four') }}</label>
                    @error('status_four')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                         {{ $message }}
                    </div>
                @enderror
                <select class="form-control" name="status_four">
                    <option value="0" @if ($services != null) @if ($services->status_four == 0) selected @endif @endif>{{trans('multi-leng.service_status_no') }}</option>
                    <option value="1" @if ($services != null) @if ($services->status_four == 1) selected @endif @endif>{{trans('multi-leng.service_status_yes') }}</option>
                   </select>
                </div>
            </div>
            <br>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title_five">{{trans('multi-leng.service_title_five') }}</label>
                    @error('title_five')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if($services != null) value="{{ $services->title_five }}" @endif name="title_five" placeholder="{{trans('multi-leng.service_title_five') }}">
                </div>
                <div class="form-group col-md-6">
                    <label for="title_six">{{trans('multi-leng.service_title_six') }}</label>
                    @error('title_six')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if($services != null) value="{{ $services->title_six }}" @endif name="title_six" placeholder="{{trans('multi-leng.service_title_six') }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="description_five">{{trans('multi-leng.service_description_five') }}</label>
                    @error('description_five')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea name="description_five"
                        id="summernote5">@if ($services != null) {!! $services->description_five !!} @endif</textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="description_six">{{trans('multi-leng.service_description_six') }}</label>
                    @error('description_six')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea name="description_six"
                        id="summernote6">@if ($services != null) {!! $services->description_six !!} @endif</textarea>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="status_five">{{trans('multi-leng.service_show_in_footer_five') }}</label>
                    @error('status_five')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                         {{ $message }}
                    </div>
                @enderror
                <select class="form-control" name="status_five">
                    <option value="0" @if ($services != null) @if ($services->status_five == 0) selected @endif @endif>{{trans('multi-leng.service_status_no') }}</option>
                    <option value="1" @if ($services != null) @if ($services->status_five == 1) selected @endif @endif>{{trans('multi-leng.service_status_yes') }}</option>
                   </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="status_six">{{trans('multi-leng.service_show_in_footer_six') }}</label>
                    @error('status_six')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                         {{ $message }}
                    </div>
                @enderror
                <select class="form-control" name="status_six">
                    <option value="0" @if ($services != null) @if ($services->status_six == 0) selected @endif @endif>{{trans('multi-leng.service_status_no') }}</option>
                    <option value="1" @if ($services != null) @if ($services->status_six == 1) selected @endif @endif>{{trans('multi-leng.service_status_yes') }}</option>
                   </select>
                </div>
            </div>

        </div>

        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{trans('multi-leng.service_save') }}</button>
        </div>
        <!-- /.card-footer -->

    </div>
</form>

@extends('layouts.template')

@section('title')
{{trans('multi-leng.manage_documents') }}
@endsection

@section('styles')
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection



@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
             @include('admin.services.create')
        </div>

    </div>
</div>
@endsection


@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {

        $('#summernote').summernote({
            height: 150,
        });

        $('#summernote2').summernote({
            height: 150,
        });

        $('#summernote3').summernote({
            height: 150,
        });

        $('#summernote4').summernote({
            height: 150,
        });

        $('#summernote5').summernote({
            height: 150,
        });

        $('#summernote6').summernote({
            height: 150,
        });
    });
</script>

@endsection


<div class="card card-dark">
    <div class="card-header">
      <h3 class="card-title">{{ trans('multi-leng.edit_regard') }}</h3>
    </div>

    @if (session()->has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('success-message') }}
        </div>
    @endif


      <div class="card-body">
        @include('admin.regards.form')
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button wire:click="update" class="btn btn-primary px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.regards_update') }}</button>
        <button wire:click="default" class="btn btn-default float-right"><i class="fas fa-times"></i>&nbsp; {{ trans('multi-leng.regards_cancel') }}</button>
      </div>
      <!-- /.card-footer -->

  </div>

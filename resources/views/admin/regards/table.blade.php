<div class="card card-dark">
    <div class="card-header">
      <div class="row">

      <h3 class="card-title col-sm-6 col-md-7 col-lg-8 mb-2 mb-sm-0 d-flex align-items-center">{{ trans('multi-leng.list_regards') }}</h3>

      <div class="card-tools col-sm-6 col-md-5 col-lg-4">
        <div class="input-group input-group-sm">
          <input type="text" wire:model="search" class="form-control float-right" placeholder="Buscar por Titulo">

          <div class="input-group-append">
            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
          </div>
        </div>
      </div>

      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
      <table class="table table-hover text-nowrap">
        <thead>
          <tr>
            <th>{{ trans('multi-leng.regards_title') }}</th>
            <th>{{ trans('multi-leng.regards_day') }}</th>
            <th>{{ trans('multi-leng.regards_start_time') }}</th>
            <th>{{ trans('multi-leng.regards_end_time') }}</th>
            <th>{{ trans('multi-leng.regards_actions') }}</th>
          </tr>
        </thead>
        <tbody>

        @foreach ($regards as $item)
            <tr>
                <td>{{ $item->title }}</td>
                @if ($item->day == 'lunes')
                <td>{{ trans('multi-leng.regards_monday') }}</td>
                @elseif ($item->day == 'martes')
                <td>{{ trans('multi-leng.regards_tuesday') }}</td>
                @elseif ($item->day == 'miercoles')
                <td>{{ trans('multi-leng.regards_wednesday') }}</td>
                @elseif ($item->day == 'jueves')
                <td>{{ trans('multi-leng.regards_thursday') }}</td>
                @elseif ($item->day == 'viernes')
                <td>{{ trans('multi-leng.regards_friday') }}</td>
                @elseif ($item->day == 'sabado')
                <td>{{ trans('multi-leng.regards_saturday') }}</td>
                @else
                <td>{{ trans('multi-leng.regards_sunday') }}</td>
                @endif
                <td>{{ $item->start_time }}</td>
                <td>{{ $item->end_time }}</td>
                <td>
                    <button wire:click="edit({{ $item->id }})" class="btn btn-sm btn-info"><i class='fas fa-edit'></i></button>
                    <button wire:click="$emit('destroy', {{$item->id}})" class="btn btn-sm btn-danger"><i class='fas fa-trash'></i></button>
                </td>
            </tr>
        @endforeach

        </tbody>

      </table>

    </div>

    <div class="card-footer">
        {{ $regards->links() }}
    </div>

  </div>


  @section('scripts')

  <script>
        document.addEventListener('livewire:load', function () {
          @this.on('destroy', idRegard => {
              swal({
              title: "Desea borrar este Saludo?",
              text: "Esta operación es irreversible!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
              })
              .then((willDelete) => {
              if (willDelete) {
                  @this.call('destroy', idRegard)
                  swal("Registro eliminado correctamente.!", {
                  icon: "success",
                  });
              } else {

              }
              });
          })

          })
  </script>

  @endsection

@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_comments') }}
@endsection


@section('content')
    <livewire:manage-comments/>
@endsection

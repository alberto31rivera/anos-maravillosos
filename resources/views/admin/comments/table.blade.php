<div class="card card-dark">
    <div class="card-header">
      <div class="row">

        <h3 class="card-title col-sm-6 col-md-7 col-lg-8 mb-2 mb-sm-0 d-flex align-items-center">{{ trans('multi-leng.comments_list') }}</h3>

        <div class="card-tools col-sm-6 col-md-5 col-lg-4">
          <div class="input-group input-group-sm">
            <input type="text" wire:model="search" class="form-control float-right" placeholder="{{ trans('multi-leng.comments_search_name') }}">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </div>

        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
      <table class="table table-hover text-nowrap">
        <thead>
          <tr>
            <th>{{ trans('multi-leng.comments_user') }}</th>
            <th>{{ trans('multi-leng.comments_title') }}</th>
            <th>{{ trans('multi-leng.comments_date') }}</th>
            <th>{{ trans('multi-leng.comments_actions') }}</th>
          </tr>
        </thead>
        <tbody>

        @foreach ($comments as $item)
            <tr>
                <td>{{ $item->users->name }}</td>
                <td>{{ $item->title }}</td>
                <td>{{ $item->date }}</td>
                <td>
                    <a class="btn btn-sm btn-info" target="_blank" href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2'=> $item->post->slug]) }}"><i class='fas fa-eye'></i></a>
                    <button wire:click="$emit('destroy', {{$item->id}})" class="btn btn-sm btn-danger"><i class='fas fa-trash'></i></button>
                </td>
            </tr>
        @endforeach

        </tbody>

      </table>

    </div>

    <div class="card-footer">
        {{ $comments->links() }}
    </div>

  </div>

  @section('scripts')

  <script>
        document.addEventListener('livewire:load', function () {
          @this.on('destroy', idComment => {
              swal({
              title: "{{ trans('multi-leng.comments_title_delete') }}",
              text: "{{ trans('multi-leng.comments_text_delete') }}",
              icon: "warning",
              buttons: true,
              dangerMode: true,
              })
              .then((willDelete) => {
              if (willDelete) {
                  @this.call('destroy', idComment)
                  swal("{{ trans('multi-leng.comments_swal_delete') }}", {
                  icon: "success",
                  });
              } else {

              }
              });
          })

          })
  </script>

  @endsection

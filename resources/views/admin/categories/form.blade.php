
    <div class="form-row">
        <div class="form-group col-md-6">
          <label for="title">{{ trans('multi-leng.categories_title') }} *</label>
          @error('title')
          <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               {{ $message }}
          </div>
      @enderror
      <input type="text" class="form-control" placeholder="{{ trans('multi-leng.categories_title') }}" wire:model="title">

        </div>

        <div class="form-group col-md-6">
            <label for="image_location_id">{{ trans('multi-leng.categories_location') }} *</label>
            @error('image_location_id')
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 {{ $message }}
            </div>
        @enderror
        <select class="form-control" required wire:model="image_location_id">
            <option value="" selected>{{ trans('multi-leng.categories_select_location') }}</option>
             @foreach ($image_locations as $item)
                 @if ($item->title == 'Sección 1')
                 <option value="{{ $item->id }}">{{ trans('multi-leng.categories_section_one') }}</option>
                 @elseif ($item->title == 'Sección 2')
                 <option value="{{ $item->id }}">{{ trans('multi-leng.categories_section_two') }}</option>
                 @elseif ($item->title == 'Sección 3')
                 <option value="{{ $item->id }}">{{ trans('multi-leng.categories_section_three') }}</option>
                 @elseif ($item->title == 'Sección 4')
                 <option value="{{ $item->id }}">{{ trans('multi-leng.categories_section_four') }}</option>
                 @elseif ($item->title == 'Sección 5')
                 <option value="{{ $item->id }}">{{ trans('multi-leng.categories_section_five') }}</option>
                 @elseif ($item->title == 'Sección 6')
                 <option value="{{ $item->id }}">{{ trans('multi-leng.categories_section_six') }}</option>
                 @elseif ($item->title == 'Sección 7')
                 <option value="{{ $item->id }}">{{ trans('multi-leng.categories_section_seven') }}</option>
                 @elseif ($item->title == 'Sección 8')
                 <option value="{{ $item->id }}">{{ trans('multi-leng.categories_section_eight') }}</option>
                 @else
                 <option value="{{ $item->id }}">{{ trans('multi-leng.categories_section_nine') }}</option>
                 @endif
             @endforeach
           </select>
          </div>

      </div>


        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="status_header">{{ trans('multi-leng.categories_show_header') }}</label>
                @error('status_header')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                     {{ $message }}
                </div>
            @enderror
            <select class="form-control"  wire:model="status_header">
                    <option value=0>{{ trans('multi-leng.categories_status_header_no') }}</option>
                    <option value=1>{{ trans('multi-leng.categories_status_header_yes') }}</option>
                </select>
              </div>
            <div class="form-group col-md-6">
                <label for="display_order">{{ trans('multi-leng.display_order') }}</label>
                @error('display_order')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                     {{ $message }}
                </div>
            @enderror
            <select class="form-control"  wire:model="display_order">
                    @if ($status_header == 0)
                    <option value=""></option>
                    @else
                    <option value="">Seleccionar</option>
                    <option value=1>1</option>
                    <option value=2>2</option>
                    <option value=3>3</option>
                    <option value=4>4</option>
                    @endif

                </select>
              </div>
          </div>

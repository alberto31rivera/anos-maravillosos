@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_categories') }}
@endsection


@section('content')
    <livewire:manage-categories/>
@endsection


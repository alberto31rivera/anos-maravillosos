@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_programs') }}
@endsection


@section('content')
    <livewire:manage-programs/>
@endsection

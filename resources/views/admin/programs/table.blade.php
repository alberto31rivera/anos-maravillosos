<div class="card card-dark">
    <div class="card-header">
        <div class="row">

          <h3 class="card-title col-sm-6 col-md-7 col-lg-8 mb-2 mb-sm-0 d-flex align-items-center">{{ trans('multi-leng.programs_list') }}</h3>

          <div class="card-tools col-sm-6 col-md-5 col-lg-4">
            <div class="input-group input-group-sm">
              <input type="text" wire:model="search" class="form-control float-right" placeholder="{{ trans('multi-leng.programs_search_title') }}">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </div>

        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
      <table class="table table-hover text-nowrap">
        <thead>
          <tr>
            <th>{{ trans('multi-leng.programs_title') }}</th>
            <th>{{ trans('multi-leng.programs_day') }}</th>
            <th>{{ trans('multi-leng.programs_start_time') }}</th>
            <th>{{ trans('multi-leng.programs_end_time') }}</th>
            <th>{{ trans('multi-leng.programs_actions') }}</th>
          </tr>
        </thead>
        <tbody>

        @foreach ($programs as $item)
            <tr>
                <td>{{ $item->title }}</td>
                @if ($item->day == 'lunes')
                <td>{{ trans('multi-leng.programs_monday') }}</td>
                @elseif ($item->day == 'martes')
                <td>{{ trans('multi-leng.programs_tuesday') }}</td>
                @elseif ($item->day == 'miercoles')
                <td>{{ trans('multi-leng.programs_wednesday') }}</td>
                @elseif ($item->day == 'jueves')
                <td>{{ trans('multi-leng.programs_thursday') }}</td>
                @elseif ($item->day == 'viernes')
                <td>{{ trans('multi-leng.programs_friday') }}</td>
                @elseif ($item->day == 'sabado')
                <td>{{ trans('multi-leng.programs_saturday') }}</td>
                @else
                <td>{{ trans('multi-leng.programs_sunday') }}</td>
                @endif
                <td>{{ $item->start_time}}</td>
                <td>{{ $item->end_time }}</td>
                <td>
                    <button wire:click="edit({{ $item->id }})" class="btn btn-sm btn-info"><i class='fas fa-edit'></i></button>
                    <button onclick="sendProgramId({{ $item->id }})" class="btn btn-sm btn-danger"><i
                        class='fas fa-trash'></i></button>
                </td>
            </tr>
        @endforeach

        </tbody>

      </table>

    </div>

    <div class="card-footer">
        {{ $programs->links() }}
    </div>

  </div>

  @section('scripts')

  <script>

function sendProgramId(idProgram) {

const url = "/admin/delete/program/" + idProgram;
const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

Swal.fire({
    title: "{{ trans('multi-leng.programs_title_delete') }}",
    text: "{{ trans('multi-leng.programs_text_delete') }}",
    showCancelButton: true,
    confirmButtonText: "{{ trans('multi-leng.categories_title_confirm') }}",
    cancelButtonText: "{{ trans('multi-leng.categories_title_cancel') }}",
    showLoaderOnConfirm: true,
    preConfirm: () => {

        return fetch(url, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json, text-plain, */*",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-TOKEN": token
                },
                method: 'DELETE',
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error(
                        "{{ trans('multi-leng.programs_text_error') }}"
                    )
                }
                return response.json()
            })
            .catch(error => {
                Swal.showValidationMessage(
                    `${error}`
                )
            })
    },
    allowOutsideClick: () => !Swal.isLoading()
}).then((result) => {
    if (result.value) {
        Swal.fire({
            title: "{{ trans('multi-leng.categories_swal_delete') }}",
        })
    }
}).then(function() {
    window.location.reload();
})
}

  </script>

  @endsection

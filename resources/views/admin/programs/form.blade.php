<div class="form-row">
    <div class="form-group col-md-6">
      <label for="title">{{ trans('multi-leng.programs_title') }} *</label>
      @error('title')
      <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
           {{ $message }}
      </div>
  @enderror
  <input type="text" class="form-control" placeholder="{{ trans('multi-leng.programs_title') }}" wire:model="title">

    </div>
    <div class="form-group col-md-6">
        <label for="url">{{ trans('multi-leng.programs_url') }}</label>
        @error('url')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
    @enderror
    <input type="text" class="form-control" placeholder="{{ trans('multi-leng.programs_url') }}" wire:model="url">

      </div>
  </div>

  <div class="form-row">
      <div class="form-group col-md-6">
        <label for="image">{{ trans('multi-leng.programs_featured_image') }}</label>
        @error('image')
  <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
       {{ $message }}
  </div>
  @enderror
        <input type="file" class="form-control-file file-decoration" id="image" wire:model="image">
        <div class="contains-img-media" style="height:185px">
          {{-- <a id="close-image" role="button" onclick="clearContentImg('image')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
            <i class="fa fa-times" aria-hidden="true"></i>
          </a> --}}
          @if ($image)
              <img src="{{ $image->temporaryUrl() }}">
          @elseif($srcEditImage != "")
          <img src= {{ asset('img/'.$srcEditImage) }}>
          @endif
        </div>

      </div>

      <div class="form-group col-md-6">
        <label for="description">{{ trans('multi-leng.programs_description') }} *</label>
        @error('description')
  <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
       {{ $message }}
  </div>
  @enderror
    <textarea class="form-control" id="description" style="height:230px" placeholder="{{ trans('multi-leng.programs_description') }}" wire:model="description"></textarea>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-4">
        <label for="day">{{ trans('multi-leng.programs_day') }} *</label>
        @error('day')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
    @enderror
    <select class="form-control"  wire:model="day">
        <option value="" selected>{{ trans('multi-leng.programs_select_day') }}</option>
         <option value="lunes">{{ trans('multi-leng.programs_monday') }}</option>
         <option value="martes">{{ trans('multi-leng.programs_tuesday') }}</option>
         <option value="miercoles">{{ trans('multi-leng.programs_wednesday') }}</option>
         <option value="jueves">{{ trans('multi-leng.programs_thursday') }}</option>
         <option value="viernes">{{ trans('multi-leng.programs_friday') }}</option>
         <option value="sabado">{{ trans('multi-leng.programs_saturday') }}</option>
         <option value="domingo">{{ trans('multi-leng.programs_sunday') }}</option>
       </select>
      </div>
    <div class="form-group col-md-4">
        <label for="start_time">{{ trans('multi-leng.programs_start_time') }} *</label>
        @error('start_time')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
    @enderror
    <input type="time" class="form-control" placeholder="{{ trans('multi-leng.programs_start_time') }}" wire:model="start_time">

      </div>
      <div class="form-group col-md-4">
        <label for="end_time">{{ trans('multi-leng.programs_end_time') }} *</label>
        @error('end_time')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             {{ $message }}
        </div>
    @enderror
    <input type="time" class="form-control" placeholder="{{ trans('multi-leng.programs_end_time') }}" wire:model="end_time">

      </div>
  </div>

<div class="card card-dark">
    <div class="card-header">
      <h3 class="card-title">{{ trans('multi-leng.new_program') }}</h3>
    </div>

    @if (session()->has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('success-message') }}
        </div>
    @endif


      <div class="card-body">
        @include('admin.programs.form')
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button wire:click="store" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.programs_save') }}</button>
      </div>
      <!-- /.card-footer -->

  </div>

<form action="{{ route('paypal.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{ trans('multi-leng.manage_payments_paypal') }}</h3>
        </div>

        @if (Session::has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success-message') }}
            </div>
        @endif

        @if (Session::has('error-message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('error-message') }}
            </div>
        @endif

        <div class="card-body">

            <div class="form-group row">
                <label for="email" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_paypal_account') }} *</label>
                @error('email')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
                <div class="col-sm-9">
                  <input type="email" class="form-control" name="email" @if($paypalSettings != null) value="{{ $paypalSettings->email }}" @endif placeholder="email">
                </div>
              </div>

              <div class="form-group row">
                <label for="image" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_image') }}</label>
                @error('image')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
                <div class="col-sm-9">
                    <input id="uploadImage1" class="file-decoration col-sm-6 px-0" type="file" name="image" onchange="previewImage(1);" />

                    <div class="contains-img-media col-sm-9 col-md-6">
                    {{-- <a id="close-uploadImage1" role="button" onclick="clearContentImg('uploadImage1')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                         <i class="fa fa-times" aria-hidden="true"></i>
                     </a> --}}
                    @if ($paypalSettings->image != null)
                        <img id="uploadPreview1" class="img-fluid"
                            src="{{ asset('img/payments/paypal/'.$paypalSettings->image) }}">
                    @else
                        <img id="uploadPreview1" class="img-fluid" src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png">
                    @endif
                    </div>

                </div>
              </div>

              <div class="form-group row">
                <label for="status" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_status') }}</label>
                <div class="col-sm-9">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" id="status" value="1" @if ($paypalSettings != null) @if ($paypalSettings->status == 1) checked @endif @endif>
                        <label class="form-check-label" for="status">
                            {{ trans('multi-leng.manage_payments_status_on') }}
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" id="status" value="0" @if ($paypalSettings != null) @if ($paypalSettings->status == 0) checked @endif @else checked @endif>
                        <label class="form-check-label" for="status">
                            {{ trans('multi-leng.manage_payments_status_off') }}
                        </label>
                      </div>
                </div>
              </div>
              <br>

              <div class="form-group row">
                <label for="sandbox" class="col-sm-3 col-md-2 col-form-label text-sm-right"> {{ trans('multi-leng.manage_payments_paypal_sandbox') }}</label>
                <div class="col-sm-9">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sandbox" id="sandbox" value="1" @if ($paypalSettings != null) @if ($paypalSettings->sandbox == 1) checked @endif @endif>
                        <label class="form-check-label" for="sandbox">
                            {{ trans('multi-leng.manage_payments_status_on') }}
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="sandbox" id="sandbox" value="0" @if ($paypalSettings != null) @if ($paypalSettings->sandbox == 0) checked @endif @else checked @endif>
                        <label class="form-check-label" for="sandbox">
                            {{ trans('multi-leng.manage_payments_status_off') }}
                        </label>
                      </div>
                </div>
              </div>
        </div>

         <!-- /.card-body -->
         <div class="card-footer">
            <button type="submit" class="btn btn-success bg--olive ps-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.manage_payments_save') }}</button>
        </div>
        <!-- /.card-footer -->

    </div>
</form>

<script>
    function previewImage(nb) {
        var reader = new FileReader();
        reader.readAsDataURL(document.getElementById('uploadImage' + nb).files[0]);
        reader.onload = function(e) {
            document.getElementById('uploadPreview' + nb).src = e.target.result;
        };
    }
</script>

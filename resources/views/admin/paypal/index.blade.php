@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_payments') }}
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
             @include('admin.paypal.create')
        </div>

    </div>
</div>
@endsection



@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_advertising_categories') }}
@endsection


@section('content')
    <livewire:manage-category-advertising/>
@endsection



    <div class="form-row">
        <div class="form-group col-md-6">
          <label for="title">{{ trans('multi-leng.manage_advertising_title') }} *</label>
          @error('title')
          <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               {{ $message }}
          </div>
      @enderror
      <input type="text" class="form-control" placeholder="{{ trans('multi-leng.manage_advertising_title') }}" wire:model="title">

        </div>
      </div>

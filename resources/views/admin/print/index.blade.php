
        <div class="modal-body pt-1 pb-0 px-0">

          <!--Content-->
          <div class="card rounded-0 border-0 ts-grid-box">
            <div class="card-header bg-white d-flex justify-content-md-between align-items-end">
               @if ($admin->logo == '')
                <img class="img-fluid" src="{{asset('assets/front/template/images/logo/logo-v2.png')}}" alt="">
               @else
                <img class="img-fluid" src="{{asset('img/logos-users/'.$admin->logo)}}" alt="">
               @endif
               <span class="text-muted">{{ trans('multi-leng.user_transactions_date_two') }}: {{ $document->created_at->toFormattedDateString() }}</span>
            </div>
            <div class="card-body">
              <div class="contaner">
                <div class="row">
                  <div class="col-md-4">
                    <h3 class="mb-0">{{ $admin->name }}</h3>
                    <p>
                      <span>{{ $admin->number_document }}</span>
                      <span>{{ $admin->address }}</span>
                      <span>{{ $admin->cellphone }}</span>
                      <span>{{ $admin->city }}</span>
                      <span>{{ $admin->country }}</span>
                      <span>{{ env('MAIL_USERNAME') }}</span>
                    </p>
                  </div>
                  <div class="col-md-4">
                    <p>
                      <span>{{ $document->user->name }}</span>
                      <span>{{ $document->user->number_document }}</span>
                      <span>{{ $document->user->address }}</span>
                      <span>{{ $document->user->cellphone }}</span>
                      <span>{{ $document->user->city }}</span>
                      <span>{{ $document->user->country }}</span>
                      <span>{{ $document->user->email }}</span>
                    </p>
                  </div>
                  <div class="col-md-4">
                    <p>
                      <span><strong>{{ trans('multi-leng.user_transactions_invoice') }}</strong> #{{ str_pad($document->id, 4, '0', STR_PAD_LEFT) }}</span>
                       @if ($document->payment_method == 'Transferencia Bancaria')
                       <span><strong>{{ trans('multi-leng.user_transactions_transfer_date') }} </strong>{{ $document->transfer_date }} </span>
                       @endif
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th scope="col">{{ trans('multi-leng.user_transactions_quantity') }}</th>
                          <th scope="col">{{ trans('multi-leng.user_transactions_description') }}</th>
                          <th scope="col" class="text-right">{{ trans('multi-leng.user_transactions_subtotal') }}</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>{!! $document->description !!}</td>
                          @if ($document->payment_method == 'Transferencia Bancaria')
                          <td class="wspace-nrap text-right">{{ $document->cost }} {{ $currencyLocal->name }}</td>
                          @elseif ($document->payment_method == 'Mercado Pago')
                          <td class="wspace-nrap text-right">{{ $document->cost }} {{ $currencyLocal->name }}</td>
                          @else
                          <td class="wspace-nrap text-right">{{ $document->cost_dolar }} {{ $currencyDolar->name }}</td>
                          @endif
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right">
                            @if ($document->payment_method == 'Transferencia Bancaria')
                            <span class="font-weight-bold">{{ trans('multi-leng.user_transactions_total') }}:</span> {{ $document->cost }} {{ $currencyLocal->name }}
                            @elseif ($document->payment_method == 'Mercado Pago')
                            <span class="font-weight-bold">{{ trans('multi-leng.user_transactions_total') }}:</span> {{ $document->cost }} {{ $currencyLocal->name }}
                            @else
                            <span class="font-weight-bold">{{ trans('multi-leng.user_transactions_total') }}:</span> {{ $document->cost_dolar }} {{ $currencyDolar->name }}
                            @endif
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div> <!--/card-->
          <!--/Content-->

        </div>

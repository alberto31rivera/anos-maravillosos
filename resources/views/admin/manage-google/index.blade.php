@extends('layouts.template')

@section('title')
{{trans('multi-leng.manage_google') }}
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
             @include('admin.manage-google.create')
        </div>

    </div>
</div>
@endsection



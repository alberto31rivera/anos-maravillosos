<form action="{{ route('google.store') }}" method="POST">
    @csrf

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{trans('multi-leng.manage_google_title') }}</h3>
        </div>

        @if (Session::has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success-message') }}
            </div>
        @endif

        @if (Session::has('error-message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('error-message') }}
            </div>
        @endif

        <div class="card-body">

            <div class="form-group row">
                <label for="recaptcha_key" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.recaptcha_key') }}</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" required name="recaptcha_key" @if($manageGoogle != null) value="{{ $manageGoogle->recaptcha_key }}" @endif placeholder="{{trans('multi-leng.recaptcha_key') }}">
                </div>
              </div>
              <br>
              <div class="form-group row">
                <label for="recaptcha_secret_key" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.recaptcha_secret') }}</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" required name="recaptcha_secret_key" @if($manageGoogle != null) value="{{ $manageGoogle->recaptcha_secret_key }}" @endif placeholder="{{trans('multi-leng.recaptcha_secret') }}">
                </div>
              </div>
              <br>
              <div class="form-group row">
                <label for="google_analytics" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{trans('multi-leng.google_analytics') }}</label>
                <div class="col-sm-9">
                <textarea class="form-control" name="google_analytics" placeholder="{{trans('multi-leng.google_analytics') }}" rows="7">@if($manageGoogle != null) {{ $manageGoogle->google_analytics }} @endif</textarea>
              </div>
            </div>
        </div>
         <!-- /.card-body -->
         <div class="card-footer">
            <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp;  {{trans('multi-leng.google_save') }}</button>
        </div>
        <!-- /.card-footer -->

        </div>
    </div>
</form>

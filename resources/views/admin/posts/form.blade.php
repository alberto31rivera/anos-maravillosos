    @csrf
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="image">{{ trans('multi-leng.posts_featured_image') }} *</label>
            @error('image')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
            <input type="file" class="form-control-file file-decoration" id="image" re onchange="loadPreview(this);" name="image">

            <div class="contains-img-media">

                {{-- <a id="close-image" role="button" onclick="clearContentImg('image')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a> --}}

            <img id="preview_img" class="img-fluid"
                src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png"/>
            </div>

        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="title">{{ trans('multi-leng.posts_title') }} *</label>
            @error('title')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
            <input type="text" class="form-control" value="{{ old('title') }}" name="title" id="title" placeholder="{{ trans('multi-leng.posts_title') }}">
        </div>
        <div class="form-group col-md-6">
            <label for="category_id">{{ trans('multi-leng.posts_category') }} *</label>
            @error('category_id')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
            <select class="form-control" name="category_id">
                <option value="" selected>{{ trans('multi-leng.posts_select_category') }}</option>
                @foreach ($categories as $item)
                    <option value="{{ $item->id }}" {{ (old("category_id") == $item->id ? "selected":"") }}>{{ $item->title }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="description">{{ trans('multi-leng.posts_description') }} *</label>
            @error('description')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
            <textarea name="description" id="summernote">{!! old('description') !!}</textarea>
        </div>
    </div>

    <div class="form-row">

        <div class="form-group col-md-6">
            <label for="post_summary">{{ trans('multi-leng.posts_summary') }} *</label>
            @error('post_summary')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
            <textarea class="form-control" id="post_summary" rows="5" placeholder="{{ trans('multi-leng.posts_summary') }}"
                name="post_summary">{{ old('post_summary') }}</textarea>
        </div>

        <div class="form-group col-md-6">
            <label for="sentence">{{ trans('multi-leng.posts_sentence') }} *</label>
            @error('sentence')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
            <textarea class="form-control" id="sentence"  rows="5" placeholder="{{ trans('multi-leng.posts_sentence') }}" name="sentence">{{ old('sentence') }}</textarea>
        </div>

    </div>

    <div class="form-row">

        <div class="form-group col-md-6">
            <label for="tag">{{ trans('multi-leng.posts_seo') }}</label><br>
            <label for="tag">{{ trans('multi-leng.posts_tags') }}</label><br>
            @error('tags')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
            <input type="text" data-role="tagsinput" name="tags" class="form-control tags">

        </div>

    </div>

    {{-- <div class="form-row">
        <div class="form-group col-md-6">
          <label for="videdo">Video</label>
          @error('video')
          <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               {{ $message }}
          </div>
          @enderror
          <input type="text" class="form-control" placeholder="Video" name="video">
        </div>
        <div class="form-group col-md-6">
            <label for="audio">Audio</label>
            @error('audio')
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 {{ $message }}
            </div>
            @enderror
            <input type="text" class="form-control" placeholder="Audio" name="audio">
        </div>
      </div> --}}


    <script>
        function loadPreview(input, id) {
            id = id || '#preview_img';
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $(id)
                        .attr('src', e.target.result)
                        // .width(200)
                        // .height(150)
                        ;
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

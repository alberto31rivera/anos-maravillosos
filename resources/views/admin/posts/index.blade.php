@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_posts') }}
@endsection

@section('styles')
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" />
<style type="text/css">
    .label-info{
        background-color: #17a2b8;

    }
    .label {
        display: inline-block;
        padding: .25em .4em;
        font-size: 75%;
        font-weight: 700;
        line-height: 1;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,
        border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
</style>
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
            @if ($view == 'create')
                @include('admin.posts.create')
            @else
                @include('admin.posts.edit')
            @endif
        </div>

        <div class="col-md-12">
            <livewire:manage-table-posts/>
          {{-- @include('admin.posts.table') --}}
        </div>

    </div>
</div>
@endsection


@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js" integrity="sha512-VvWznBcyBJK71YKEKDMpZ0pCVxjNuKwApp4zLF3ul+CiflQi6aIJR+aZCP/qWsoFBA28avL5T5HA+RE+zrGQYg==" crossorigin="anonymous"></script>


<script>
    $(document).ready(function() {

        $('#summernote').summernote({
            height: 150,
        });
    });

    function deleteItem(idForm)
    {
    Swal.fire({
    title: "{{ trans('multi-leng.posts_title_delete') }}",
    text: "{{ trans('multi-leng.posts_text_delete') }}",

    title: "{{ trans('multi-leng.categories_title_delete') }}",
                text: "{{ trans('multi-leng.categories_text_delete') }}",
                showCancelButton: true,
                confirmButtonText: "{{ trans('multi-leng.categories_title_confirm') }}",
                cancelButtonText: "{{ trans('multi-leng.categories_title_cancel') }}",
                showLoaderOnConfirm: true,
    })
    .then((result) => {
    if (result.isConfirmed) {
        document.getElementById(idForm).submit();
    }
    });

    }
</script>

@endsection


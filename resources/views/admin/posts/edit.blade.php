<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.posts_edit') }}</h3>
    </div>

    @if (Session::has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success-message') }}
        </div>
    @endif

    @if (Session::has('error-message'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('error-message') }}
        </div>
    @endif


    <form action="{{ route('admin.update.posts', $post->id) }}" method="POST" enctype="multipart/form-data">
        <div class="card-body">
            @csrf
            @method('PUT')
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="image">{{ trans('multi-leng.posts_featured_image') }} *</label>
                    @error('image')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="file" class="form-control-file file-decoration col-sm-6 px-0" id="image" onchange="loadPreview(this);" name="image">
                    <div class="contains-img-media col-sm-6">
                    @if ($post->image != '')
                    {{-- <a id="close-image" role="button" onclick="clearContentImg('image')" class="btn btn-light btn--close-ad position-absolute">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a> --}}
                        <img id="preview_img" class="img-fluid" src={{ asset('img/'.$post->image)}}>
                    @else
                        <img id="preview_img" class="img-fluid"
                            src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png"
                           />
                    @endif
                    </div>

                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title">{{ trans('multi-leng.posts_title') }} *</label>
                    @error('title')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" value="{{old('title', $post->title)}}" name="title" id="title"
                        placeholder="{{ trans('multi-leng.posts_title') }}">
                </div>
                <div class="form-group col-md-6">
                    <label for="category_id">{{ trans('multi-leng.posts_category') }} *</label>
                    @error('category_id')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <select class="form-control" name="category_id">
                        <option selected disabled>{{ trans('multi-leng.posts_select_category') }}</option>
                        @foreach ($categories as $item)
                            <option value="{{ $item->id }}"
                                {{ $item->id == old('category_id', $post->category_id) ? 'selected' : '' }}>{{ $item->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="description">{{ trans('multi-leng.posts_description') }} *</label>
                    @error('description')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea name="description" id="summernote">{!! old('description', $post->description) !!}</textarea>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="post_summary">{{ trans('multi-leng.posts_summary') }} *</label>
                    @error('post_summary')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea class="form-control" id="post_summary" rows="9" placeholder="{{ trans('multi-leng.posts_summary') }}"
                        name="post_summary">{{old('post_summary', $post->post_summary)}}</textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="sentence">{{ trans('multi-leng.posts_sentence') }} *</label>
                    @error('sentence')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea class="form-control" id="sentence" rows="9" placeholder="{{ trans('multi-leng.posts_sentence') }}"
                     name="sentence">{{old('sentence', $post->sentence)}}</textarea>
                </div>
            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="tag">{{ trans('multi-leng.posts_seo') }}</label><br>
                    <label for="tag">{{ trans('multi-leng.posts_tags') }}</label><br>
                    @error('tags')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" data-role="tagsinput" name="tags" value="{{old('tags', $post->tags)}}" class="form-control tags">

                </div>

            </div>

            {{--
                 <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="videdo">Video</label>
                    @error('video')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" placeholder="Video" value="{{ $post->video }}"
                        name="video">
                </div>
                <div class="form-group col-md-6">
                    <label for="audio">Audio</label>
                    @error('audio')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" placeholder="Audio" value="{{ $post->audio }}"
                        name="audio">
                </div>
            </div>

                --}}

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.posts_update') }}</button>
            <a href="{{ route('admin.posts') }}" class="btn btn-default float-right"><i class="fas fa-times"></i>&nbsp; {{ trans('multi-leng.posts_cancel') }}</a>
        </div>
        <!-- /.card-footer -->
    </form>


</div>

<script>
    function loadPreview(input, id) {
        id = id || '#preview_img';
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(id)
                    .attr('src', e.target.result)
                    .width(200)
                    .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

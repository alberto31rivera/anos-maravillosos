<form action="{{ route('transfer.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{ trans('multi-leng.manage_payments_transfer') }}</h3>
        </div>

        @if (Session::has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success-message') }}
            </div>
        @endif

        @if (Session::has('error-message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('error-message') }}
            </div>
        @endif

        <div class="card-body">

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="bank_details">{{ trans('multi-leng.manage_payments_bank_details') }} *</label>
                    @error('bank_details')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <textarea name="bank_details" id="summernote">@if ($transferSettings != null) {!! $transferSettings->bank_details !!} @endif</textarea>
                </div>
            </div>
                <br>
              <div class="form-group row">
                <label for="status" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_transfer_status') }}</label>
                <div class="col-sm-9">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" id="status" value="1" @if ($transferSettings != null) @if ($transferSettings->status == 1) checked @endif @endif>
                        <label class="form-check-label" for="status">
                            {{ trans('multi-leng.manage_payments_transfer_status_on') }}
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" id="status" value="0" @if ($transferSettings != null) @if ($transferSettings->status == 0) checked @endif @else checked @endif>
                        <label class="form-check-label" for="status">
                            {{ trans('multi-leng.manage_payments_transfer_status_off') }}
                        </label>
                      </div>
                </div>
              </div>
              <br>

        </div>

         <!-- /.card-body -->
         <div class="card-footer">
            <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.manage_payments_transfer_save') }}</button>
        </div>
        <!-- /.card-footer -->

    </div>
</form>

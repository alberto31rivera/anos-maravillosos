<form action="{{ route('homepage.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{trans('multi-leng.homepage_announcements') }}</h3>
        </div>

        @if (Session::has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success-message') }}
            </div>
        @endif

        @if (Session::has('error-message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('error-message') }}
            </div>
        @endif

        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="advertising_one">{{trans('multi-leng.advertisement_one') }}</label><br>
                    @error('advertising_one')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror

                    <input id="uploadImage1" class="file-decoration" type="file" name="advertising_one" onchange="previewImage(1);" />

                    <div class="contains-img-ad">

                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage1" role="button" onclick="clearContentImg('uploadImage1')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($homepageSettings->advertising_one != null)
                            <img id="uploadPreview1" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/' . $homepageSettings->advertising_one) }}" />
                        @else
                            <img id="uploadPreview1" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/default.png') }}" />
                        @endif
                    @else
                        <img id="uploadPreview1" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/default.png') }}" />
                    @endif
                    </div>

                </div>
                <div class="form-group col-md-6">
                    <label for="advertising_two">{{trans('multi-leng.advertisement_two') }}</label><br>
                    @error('advertising_two')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input id="uploadImage2" class="file-decoration" type="file" name="advertising_two" onchange="previewImage(2);" />

                    <div class="contains-img-ad">
                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage2" role="button" onclick="clearContentImg('uploadImage2')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($homepageSettings->advertising_two != null)
                            <img id="uploadPreview2" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/' . $homepageSettings->advertising_two) }}" />
                        @else
                            <img id="uploadPreview2" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/default.png') }}" />
                        @endif
                    @else
                        <img id="uploadPreview2" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/default.png') }}" />
                    @endif
                    </div>

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="url_advertising_one">{{trans('multi-leng.link_one') }}</label><br>
                    @error('url_advertising_one')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->url_advertising_one }}" @endif
                        placeholder="{{trans('multi-leng.link_one') }}" name="url_advertising_one">
                </div>
                <div class="form-group col-md-6">
                    <label for="url_advertising_two">{{trans('multi-leng.link_two') }}</label><br>
                    @error('url_advertising_two')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->url_advertising_two }}" @endif
                        placeholder="{{trans('multi-leng.link_two') }}" name="url_advertising_two">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="advertising_three">{{trans('multi-leng.advertisement_three') }}</label><br>
                    @error('advertising_three')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input id="uploadImage3" class="file-decoration" type="file" name="advertising_three" onchange="previewImage(3);" />

                    <div class="contains-img-ad">
                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage3" role="button" onclick="clearContentImg('uploadImage3')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($homepageSettings->advertising_three != null)
                            <img id="uploadPreview3" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/' . $homepageSettings->advertising_three) }}" />
                        @else
                            <img id="uploadPreview3" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/default.png') }}" />
                        @endif
                    @else
                        <img id="uploadPreview3" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/default.png') }}" />
                    @endif
                    </div>

                </div>
                <div class="form-group col-md-6">
                    <label for="advertising_four">{{trans('multi-leng.advertisement_four') }}</label><br>
                    @error('advertising_four')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input id="uploadImage4" class="file-decoration" type="file" name="advertising_four" onchange="previewImage(4);" />

                    <div class="contains-img-ad">
                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage4" role="button" onclick="clearContentImg('uploadImage4')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($homepageSettings->advertising_four != null)
                            <img id="uploadPreview4" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/' . $homepageSettings->advertising_four) }}" />
                        @else
                            <img id="uploadPreview4" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/default.png') }}" />
                        @endif
                    @else
                        <img id="uploadPreview4" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/default.png') }}" />
                    @endif
                    </div>

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="url_advertising_three">{{trans('multi-leng.link_three') }}</label><br>
                    @error('url_advertising_three')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->url_advertising_three }}" @endif
                        placeholder="{{trans('multi-leng.link_three') }}" name="url_advertising_three">
                </div>
                <div class="form-group col-md-6">
                    <label for="url_advertising_four">{{trans('multi-leng.link_four') }}</label><br>
                    @error('url_advertising_four')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->url_advertising_four }}" @endif
                        placeholder="{{trans('multi-leng.link_four') }}" name="url_advertising_four">
                </div>
            </div>
        </div>

    </div>

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{trans('multi-leng.homepage_videos') }}</h3>
        </div>

        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="video_one">{{trans('multi-leng.url_video_one') }}</label>
                    @error('video_one')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->video_one }}" @endif
                        placeholder="{{trans('multi-leng.url_video_one') }}" name="video_one">

                </div>
                <div class="form-group col-md-6">
                    <label for="video_two">{{trans('multi-leng.url_video_two') }}</label>
                    @error('video_two')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->video_two }}" @endif
                        placeholder="{{trans('multi-leng.url_video_two') }}" name="video_two">

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="description_one">{{trans('multi-leng.description_one') }}</label>
                    @error('description_one')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->description_one }}" @endif
                        placeholder="{{trans('multi-leng.description_one') }}" name="description_one">

                </div>
                <div class="form-group col-md-6">
                    <label for="description_two">{{trans('multi-leng.description_two') }}</label>
                    @error('description_two')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->description_two }}" @endif
                        placeholder="{{trans('multi-leng.description_two') }}" name="description_two">

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="image_one">{{trans('multi-leng.image_one') }}</label><br>
                    @error('image_one')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror

                    <input id="uploadImage5" class="file-decoration" type="file" name="image_one" onchange="previewImage(5);" />

                    <div class="contains-img-media">
                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage5" role="button" onclick="clearContentImg('uploadImage5')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($homepageSettings->image_one != null)
                            <img id="uploadPreview5" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/' . $homepageSettings->image_one) }}" />
                        @else
                            <img id="uploadPreview5" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />
                        @endif
                    @else
                        <img id="uploadPreview5" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />

                        <span class="tags-size-img">Recomendado: 840x500</span>
                    @endif
                    </div>

                </div>
                <div class="form-group col-md-6">
                    <label for="image_two">{{trans('multi-leng.image_two') }}</label><br>
                    @error('image_two')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input id="uploadImage6" class="file-decoration" type="file" name="image_two" onchange="previewImage(6);" />

                    <div class="contains-img-media">
                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage6" role="button" onclick="clearContentImg('uploadImage6')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($homepageSettings->image_two != null)
                            <img id="uploadPreview6" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/' . $homepageSettings->image_two) }}" />
                        @else
                            <img id="uploadPreview6" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />
                        @endif
                    @else
                        <img id="uploadPreview6" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />

                        <span class="tags-size-img">Recomendado: 840x500</span>
                    @endif
                    </div>

                </div>
            </div>
            <br>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="video_three">{{trans('multi-leng.url_video_three') }}</label>
                    @error('video_three')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->video_three }}" @endif
                        placeholder="{{trans('multi-leng.url_video_three') }}" name="video_three">

                </div>
                <div class="form-group col-md-6">
                    <label for="video_four">{{trans('multi-leng.url_video_four') }}</label>
                    @error('video_four')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->video_four }}" @endif
                        placeholder="{{trans('multi-leng.url_video_four') }}" name="video_four">

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="description_three">{{trans('multi-leng.description_three') }}</label>
                    @error('description_three')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->description_three }}" @endif
                        placeholder="{{trans('multi-leng.description_three') }}" name="description_three">

                </div>
                <div class="form-group col-md-6">
                    <label for="description_four">{{trans('multi-leng.description_four') }}</label>
                    @error('description_four')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->description_four }}" @endif
                        placeholder="{{trans('multi-leng.description_four') }}" name="description_four">

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="image_three">{{trans('multi-leng.image_three') }}</label><br>
                    @error('image_three')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror

                    <input id="uploadImage7" class="file-decoration" type="file" name="image_three" onchange="previewImage(7);" />

                    <div class="contains-img-media">
                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage7" role="button" onclick="clearContentImg('uploadImage7')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($homepageSettings->image_three != null)
                            <img id="uploadPreview7" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/' . $homepageSettings->image_three) }}" />
                        @else
                            <img id="uploadPreview7" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />
                        @endif
                    @else
                        <img id="uploadPreview7" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />

                        <span class="tags-size-img">Recomendado: 840x500</span>
                    @endif
                    </div>

                </div>
                <div class="form-group col-md-6">
                    <label for="image_four">{{trans('multi-leng.image_four') }}</label><br>
                    @error('image_four')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input id="uploadImage8" class="file-decoration" type="file" name="image_four" onchange="previewImage(8);" />

                    <div class="contains-img-media">

                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage8" role="button" onclick="clearContentImg('uploadImage8')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}

                        @if ($homepageSettings->image_four != null)
                            <img id="uploadPreview8" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/' . $homepageSettings->image_four) }}" />
                        @else
                            <img id="uploadPreview8" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />
                        @endif
                    @else
                        <img id="uploadPreview8" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />

                        <span class="tags-size-img">Recomendado: 840x500</span>
                    @endif
                    </div>

                </div>
            </div>
            <br>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="video_five">{{trans('multi-leng.url_video_five') }}</label>
                    @error('video_five')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->video_five }}" @endif
                        placeholder="{{trans('multi-leng.url_video_five') }}" name="video_five">

                </div>
                <div class="form-group col-md-6">
                    <label for="video_six">{{trans('multi-leng.url_video_six') }}</label>
                    @error('video_six')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->video_six }}" @endif
                        placeholder="{{trans('multi-leng.url_video_six') }}" name="video_six">

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="description_five">{{trans('multi-leng.description_five') }}</label>
                    @error('description_five')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->description_five }}" @endif
                        placeholder="{{trans('multi-leng.description_five') }}" name="description_five">

                </div>
                <div class="form-group col-md-6">
                    <label for="description_six">{{trans('multi-leng.description_six') }}</label>
                    @error('description_six')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->description_six }}" @endif
                        placeholder="{{trans('multi-leng.description_six') }}" name="description_six">

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="image_five">{{trans('multi-leng.image_five') }}</label><br>
                    @error('image_five')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror

                    <input id="uploadImage9" class="file-decoration" type="file" name="image_five" onchange="previewImage(9);" />

                    <div class="contains-img-media">
                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage9" role="button" onclick="clearContentImg('uploadImage9')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($homepageSettings->image_five != null)
                            <img id="uploadPreview9" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/' . $homepageSettings->image_five) }}" />
                        @else
                            <img id="uploadPreview9" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />
                        @endif
                    @else
                        <img id="uploadPreview9" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />

                        <span class="tags-size-img">Recomendado: 840x500</span>
                    @endif
                    </div>

                </div>
                <div class="form-group col-md-6">
                    <label for="image_six">{{trans('multi-leng.image_six') }}</label><br>
                    @error('image_six')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input id="uploadImage10" class="file-decoration" type="file" name="image_six" onchange="previewImage(10);" />

                    <div class="contains-img-media">
                    @if ($homepageSettings != null)

                        {{-- <a id="close-uploadImage10" role="button" onclick="clearContentImg('uploadImage10')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($homepageSettings->image_six != null)
                            <img id="uploadPreview10" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/' . $homepageSettings->image_six) }}" />
                        @else
                            <img id="uploadPreview10" class="img-fluid"
                                src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />
                        @endif
                    @else
                        <img id="uploadPreview10" class="img-fluid"
                            src="{{ asset('img/homepage/advertisements/videos/default.jpg') }}" />

                        <span class="tags-size-img">Recomendado: 840x500</span>
                    @endif
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{trans('multi-leng.web_titles') }}</h3>
        </div>

        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title_one">{{trans('multi-leng.title_one') }}</label>
                    @error('title_one')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->title_one }}" @endif
                        placeholder="{{trans('multi-leng.title_one') }}" name="title_one">

                </div>
                <div class="form-group col-md-6">
                    <label for="title_two">{{trans('multi-leng.title_two') }}</label>
                    @error('title_two')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->title_two }}" @endif
                        placeholder="{{trans('multi-leng.title_two') }}" name="title_two">

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title_three">{{trans('multi-leng.title_three') }}</label>
                    @error('title_three')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->title_three }}" @endif
                        placeholder="{{trans('multi-leng.title_three') }}" name="title_three">

                </div>
                <div class="form-group col-md-6">
                    <label for="title_four">{{trans('multi-leng.title_four') }}</label>
                    @error('title_four')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->title_four }}" @endif
                        placeholder="{{trans('multi-leng.title_four') }}" name="title_four">

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title_five">{{trans('multi-leng.title_five') }}</label>
                    @error('title_five')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->title_five }}" @endif
                        placeholder="{{trans('multi-leng.title_five') }}" name="title_five">

                </div>
                <div class="form-group col-md-6">
                    <label for="title_six">{{trans('multi-leng.title_six') }}</label>
                    @error('title_six')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->title_six }}" @endif
                        placeholder="{{trans('multi-leng.title_six') }}" name="title_six">

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title_seven">{{trans('multi-leng.title_seven') }}</label>
                    @error('title_seven')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->title_seven }}" @endif
                        placeholder="{{trans('multi-leng.title_seven') }}" name="title_seven">

                </div>
                <div class="form-group col-md-6">
                    <label for="title_eight">{{trans('multi-leng.title_eight') }}</label>
                    @error('title_eight')
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @enderror
                    <input type="text" class="form-control" @if ($homepageSettings != null) value="{{ $homepageSettings->title_eight }}" @endif
                        placeholder="{{trans('multi-leng.title_eight') }}" name="title_eight">

                </div>
            </div>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{trans('multi-leng.homepage_save') }}</button>
        </div>
        <!-- /.card-footer -->

    </div>

</form>

<script>
    function previewImage(nb) {
        var reader = new FileReader();
        reader.readAsDataURL(document.getElementById('uploadImage' + nb).files[0]);
        reader.onload = function(e) {
            document.getElementById('uploadPreview' + nb).src = e.target.result;
        };
    }

</script>

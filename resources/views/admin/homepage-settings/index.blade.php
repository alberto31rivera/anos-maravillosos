@extends('layouts.template')

@section('title')
{{trans('multi-leng.manage_homepage') }}
@endsection

@section('content')
<div>
    <div class="row">

        <div class="col-md-12">
             @include('admin.homepage-settings.create')
        </div>

    </div>
</div>
@endsection



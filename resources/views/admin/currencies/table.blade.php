<div class="card card-dark">
    <div class="card-header">
      <h3 class="card-title">{{ trans('multi-leng.list_currencies') }}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
      <table class="table table-hover text-nowrap">
        <thead>
          <tr>
            <th>{{ trans('multi-leng.symbol') }}</th>
            <th>{{ trans('multi-leng.currency') }}</th>
            <th>{{ trans('multi-leng.actions') }}</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($currencies as $item)
            <tr>
               <td>{{ $item->name }}</td>
               <td>{{ $item->currency }}</td>
                <td>
                    @if ($item->id != 1)
                    <button wire:click="edit({{ $item->id }})" class="btn btn-sm btn-info"><i class='fas fa-edit'></i></button>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>

      </table>

    </div>

  </div>



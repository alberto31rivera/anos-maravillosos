@extends('layouts.template')

@section('title')
{{trans('multi-leng.manage_currencies') }}
@endsection


@section('content')
    <livewire:manage-currencies/>
@endsection


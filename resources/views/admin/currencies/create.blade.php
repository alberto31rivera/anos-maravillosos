<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{trans('multi-leng.payment_details') }}</h3>
    </div>

    @if (session()->has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('success-message') }}
        </div>
    @endif

    @if (session()->has('error-message'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('error-message') }}
        </div>
    @endif

    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">{{trans('multi-leng.symbol') }}</label>
                @error('name')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @enderror
                <input type="text" @if ($flag == 0) readonly @endif class="form-control" wire:model="name">
            </div>
        </div>

    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button wire:click="store" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{trans('multi-leng.manage_currencies_save') }}</button>
        <button wire:click="default" class="btn btn-default float-right"><i class="fas fa-times"></i>&nbsp; {{trans('multi-leng.manage_currencies_cancel') }}</button>
    </div>
    <!-- /.card-footer -->
</div>

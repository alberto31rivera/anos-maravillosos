<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{ trans('multi-leng.language') }}</h3>
    </div>

    @if (session()->has('success-message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('success-message') }}
        </div>
    @endif

    @if (session()->has('error-message'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('error-message') }}
        </div>
    @endif

    <div class="card-body">

        <div class="form-row">
            <p>{{ trans('multi-leng.manage_language_text_one') }}</p><br>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <button wire:click="downloadFile" class="btn btn-primary">{{ trans('multi-leng.manage_language_btn_one') }}</button>
            </div>
            <div class="form-group col-md-4">
                <button wire:click="setLanguage('en')" class="btn btn-primary">{{ trans('multi-leng.manage_language_btn_two') }}</button>
            </div>
            <div class="form-group col-md-4">
                <button wire:click="setLanguage('new')" class="btn btn-primary">{{ trans('multi-leng.manage_language_btn_three') }}</button>
            </div>
        </div>
        <br>
        <div class="form-row">
            <p>{{ trans('multi-leng.manage_language_text_two') }}</p><br>
        </div>

    </div>
</div>

@section('scripts')
<script>
    window.addEventListener('refresh-page', event => {
       window.location.reload(false);
    })
  </script>
@endsection

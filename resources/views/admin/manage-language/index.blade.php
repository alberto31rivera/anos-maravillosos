@extends('layouts.template')

@section('title')
{{ trans('multi-leng.manage_language') }}
@endsection


@section('content')
    <livewire:manage-languages/>
@endsection


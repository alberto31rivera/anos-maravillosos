<form action="{{ route('mercado-pago.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">{{ trans('multi-leng.manage_payments_mp') }}</h3>
        </div>

        @if (Session::has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success-message') }}
            </div>
        @endif

        @if (Session::has('error-message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('error-message') }}
            </div>
        @endif

        <div class="card-body">

            <div class="form-group row">
                <label for="public_key" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_mp_public_key') }} *</label>
                @error('public_key')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="public_key" @if($mercadoPagoSettings != null) value="{{ $mercadoPagoSettings->public_key }}" @endif placeholder="{{ trans('multi-leng.manage_payments_mp_public_key') }}">
                </div>
              </div>

              <div class="form-group row">
                <label for="access_token" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_mp_access_token') }} *</label>
                @error('access_token')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="access_token" @if($mercadoPagoSettings != null) value="{{ $mercadoPagoSettings->access_token }}" @endif placeholder="{{ trans('multi-leng.manage_payments_mp_access_token') }}">
                </div>
              </div>

              <div class="form-group row">
                <label for="client_id" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_mp_client_id') }} *</label>
                @error('client_id')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="client_id" @if($mercadoPagoSettings != null) value="{{ $mercadoPagoSettings->client_id }}" @endif placeholder="{{ trans('multi-leng.manage_payments_mp_client_id') }}">
                </div>
              </div>

              <div class="form-group row">
                <label for="client_secret" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_mp_client_secret') }} *</label>
                @error('client_secret')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="client_secret" @if($mercadoPagoSettings != null) value="{{ $mercadoPagoSettings->client_secret }}" @endif placeholder="{{ trans('multi-leng.manage_payments_mp_client_secret') }}">
                </div>
              </div>

              <div class="form-group row">
                <label for="image" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_mp_image') }}</label>
                @error('image')
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror
                <div class="col-sm-9">
                    <input id="uploadImage1" class="file-decoration col-sm-8 col-md-6 px-0" type="file" name="image" onchange="previewImage(1);" />

                    <div class="contains-img-media col-sm-8 col-md-6">
                        {{-- <a id="close-uploadImage1" role="button" onclick="clearContentImg('uploadImage1')" class="btn btn-light btn--close-ad position-absolute" data-toggle="tooltip" data-placement="top" title="Remover el archivo">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a> --}}
                        @if ($mercadoPagoSettings->image != null)
                        <img id="uploadPreview1" class="img-fluid"
                            src="{{ asset('img/payments/mercado-pago/' . $mercadoPagoSettings->image) }}"/>
                    @else
                    <img id="uploadPreview1" class="img-fluid" src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png">
                    @endif
                    </div>

                </div>
              </div>

              <div class="form-group row">
                <label for="status" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_mp_status') }}</label>
                <div class="col-sm-9">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" id="status" value="1" @if ($mercadoPagoSettings != null) @if ($mercadoPagoSettings->status == 1) checked @endif @endif>
                        <label class="form-check-label" for="status">
                            {{ trans('multi-leng.manage_payments_mp_status_on') }}
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" id="status" value="0" @if ($mercadoPagoSettings != null) @if ($mercadoPagoSettings->status == 0) checked @endif @else checked @endif>
                        <label class="form-check-label" for="status">
                            {{ trans('multi-leng.manage_payments_mp_status_off') }}
                        </label>
                      </div>
                </div>
              </div>
              <br>

              <div class="form-group row">
                <label for="sandbox" class="col-sm-3 col-md-2 col-form-label text-sm-right">{{ trans('multi-leng.manage_payments_mp_sandbox') }}</label>
                <div class="col-sm-9">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sandbox" id="sandbox" value="1" @if ($mercadoPagoSettings != null) @if ($mercadoPagoSettings->sandbox == 1) checked @endif @endif>
                        <label class="form-check-label" for="sandbox">
                            {{ trans('multi-leng.manage_payments_mp_status_on') }}
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="sandbox" id="sandbox" value="0" @if ($mercadoPagoSettings != null) @if ($mercadoPagoSettings->sandbox == 0) checked @endif @else checked @endif>
                        <label class="form-check-label" for="sandbox">
                            {{ trans('multi-leng.manage_payments_mp_status_off') }}
                        </label>
                      </div>
                </div>
              </div>
        </div>

         <!-- /.card-body -->
         <div class="card-footer">
            <button type="submit" class="btn btn-success bg--olive px-3"><i class="fas fa-save"></i>&nbsp; {{ trans('multi-leng.manage_payments_mp_save') }}</button>
        </div>
        <!-- /.card-footer -->

    </div>
</form>

<script>
    function previewImage(nb) {
        var reader = new FileReader();
        reader.readAsDataURL(document.getElementById('uploadImage' + nb).files[0]);
        reader.onload = function(e) {
            document.getElementById('uploadPreview' + nb).src = e.target.result;
        };
    }
</script>

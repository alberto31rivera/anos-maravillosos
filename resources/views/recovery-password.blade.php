@extends('front.pages.layouts')

@section('content')

    <!-- post wraper start-->
    <section class="block-wrapper content-min-h py-3 py-md-4 my-3 my-md-5 d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 mx-auto">
                    @if (session('notification'))
                        <div class="alert alert-success">
                            {{ session('notification') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="callout callout-danger">
                            <h5>Error!</h5>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="ts-grid-box  mb-0">
                        <div class="login-page">
                            <h3 class="log-sign-title text-center mb-25">{{ trans('multi-leng.frontend_recovery_password') }}</h3>

                            <form action="{{ route('reset.password.user') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="inputUsernameEmail">{{ trans('multi-leng.frontend_recovery_email') }}</label>
                                    <input type="email" name="email" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="inputUsernameEmail">{{ trans('multi-leng.frontend_recovery_pass') }}</label>
                                    <input type="password" name="password" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="inputUsernameEmail">{{ trans('multi-leng.frontend_recovery_confirm_password') }}</label>
                                    <input type="password" name="confirmed_password" class="form-control">
                                </div>

                                <input type="hidden" name="token" value="{{ $token }}" />

                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn btn-primary">
                                        {{ trans('multi-leng.frontend_recovery_pass_btn') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div><!-- grid box end -->
                </div>
                <!-- col end-->

            </div>
            <!-- row end-->
        </div>
        <!-- container end-->
    </section>
    <!-- post wraper end-->

@endsection

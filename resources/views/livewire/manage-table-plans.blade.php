<div>
    <div class="card card-dark">
        <div class="card-header">
          <div class="row">

            <h3 class="card-title col-sm-6 col-md-7 col-lg-8 mb-2 mb-sm-0 d-flex align-items-center">{{ trans('multi-leng.plans_list') }}</h3>

            <div class="card-tools col-sm-6 col-md-5 col-lg-4">
              <div class="input-group input-group-sm">
                <input type="text" name="search" class="form-control float-right" wire:model="search" placeholder="{{ trans('multi-leng.plans_search_title') }}">

                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>{{ trans('multi-leng.plans_title') }}</th>
                <th>{{ trans('multi-leng.plans_local_cost') }}</th>
                <th>{{ trans('multi-leng.plans_dolar_cost') }}</th>
                <th>{{ trans('multi-leng.plans_time') }}</th>
                <th>{{ trans('multi-leng.plans_actions') }}</th>
              </tr>
            </thead>
            <tbody>

            @foreach ($plans as $item)
                <tr>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->cost}} {{ $currencyLocal->name }}</td>
                    <td>{{ $item->cost_dolar }} {{ $currencyDolar->name }}</td>
                    <td>{{ $item->duration}}</td>
                    <td>
                       <a href="{{ route('plans.edit',$item->id) }}"><button class="btn btn-sm btn-info"><i class='fas fa-edit'></i></button></a>

                       <a href="javascript:;" onclick="deleteItem({{ $item->id }})" class="btn btn-sm btn-danger"><i class='fas fa-trash'></i></a>
                       <form action="{{ route('plans.destroy', $item->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                       </form>
                    </td>
                </tr>
            @endforeach

            </tbody>

          </table>

        </div>

        <div class="card-footer">
            {{ $plans->links() }}
        </div>

      </div>


</div>

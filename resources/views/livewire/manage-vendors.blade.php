<div>
    <div class="row">

        <div class="col-md-12">
            @if ($view == 'create')
                @include('admin.vendors.create')
            @else
                @include('admin.vendors.edit')
            @endif
        </div>

        <div class="col-md-12">
            @include('admin.vendors.table')
        </div>

    </div>
</div>

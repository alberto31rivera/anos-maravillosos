<div> <!--DIV LIVEWIRE-->
    <!--Advertising section: Show all-->
    <section class="block-wrapper">
        <div class="container" wire:ignore.self>
            <div class="row mt-4 s-nav-advert">
                <div class="col-12">
                    <h2 class="pl-3 mb-3">{{ trans('multi-leng.frontend_advertising') }}</h2>
                    <hr>
                </div>
                <div class="col">
                    <a href="javascript:;" wire:click="setCategoryValue(0)" class="btn btn-light text-danger active">{{ trans('multi-leng.frontend_advertising_all') }}</a>
                    @foreach ($categoriesAdvertising as $item)
                        <a href="javascript:;" wire:click="setCategoryValue({{ $item->id }})" class="btn btn-light text-danger">{{ $item->title }}</a>
                    @endforeach
                </div>
            </div>
            <!-- row (1:nav) end-->

            <div class="row s-advert mt-3">
                @foreach ($advertisements as $item)
                    @if ($item->status == 'Activo')
                    <div class="col-md-6 col-lg-4 col-content">
                        <div class="card rounded-0 card-custom card-custom--edit">
                            <!--.border-light-->
                            {{-- <a href="#" wire:click="setItemValue({{ $item->id }})" class="trigger-ad" data-toggle="tooltip" data-placement="top" title="Ver más">

                            </a> --}}

                            <a role="button" onclick="switchEventAd(1)" data-toggle="tooltip" data-placement="top" title="Ver más" wire:click="setItemValue({{ $item->id }})">

                                @if ($item->type_advertising == 'Foto')
                                    <img class="card-img-top rounded-0"
                                        src="{{ asset('img/advertisements/' . $item->image) }}" alt="">

                                @elseif( $item->type_advertising == 'Video' )
                                    <div
                                        class="embed-responsive embed-responsive-16by9 card-img-top rounded-0 bg-deep-dark">
                                        <iframe src="https://www.youtube.com/embed/{{str_replace('https://www.youtube.com/watch?v=','',$item->video)}}" frameborder="0" allowfullscreen></iframe>
                                    </div>

                                @else
                                    <div
                                        class="content-audio card-img-top bg-dark rounded-0 d-flex justify-content-center align-items-center">
                                        <audio controls style="height:revert">
                                            <source src="{{ asset('audio/advertisements/' . $item->audio) }}"
                                                type="audio/mp3">
                                            Your browser does not support the audio tag.
                                        </audio>
                                    </div>
                                @endif

                                <div
                                    class="card-header bg-white d-flex flex-column justify-content-between border-bottom-0">
                                    <h4 class="card-title mb-1 pt-1">
                                        {{ $item->title }}
                                    </h4>
                                </div>
                                <!--/card-header-->

                            </a>

                            {{--

                                <div class="modal fade" wire:ignore.self id="exampleModalLong" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            @if ($advertising != null)
                                            <p><b>Vendedor: </b>{{ $advertising->vendor->name }}</p>
                                                <p><b>Titulo: </b>{{ $advertising->title }}</p>
                                                <img class="img-fluid rounded-circle"
                                        src="{{ asset('img/' . $item->vendor->avatar) }}" alt="">
                                                <p><b>Description:</b> {!! $advertising->description !!}</p>
                                            @endif
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                --}}
                            <!-- Modal -->

                            <div class="card-footer d-flex align-items-center rounded-0 bg-white py-2">
                                <div>
                                    <h6>{{ $item->vendor->name }}</h6>
                                    <ul class="list-unstyled mb-0 d-inline-flex">
                                        <li>
                                            @if ($item->vendor->cellphone != '')
                                                <a href="tel:{{ $item->vendor->cellphone }}"><i class="fa fa-phone fa-lg"
                                                        aria-hidden="true"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if ($item->vendor->whatsapp != '')
                                                <a
                                                    href="https://api.whatsapp.com/send?phone={{ $item->vendor->whatsapp }}" target="_blank"><i
                                                        class="fa fa-whatsapp fa-lg" aria-hidden="true"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if ($item->vendor->email != '')
                                                <a href="mailto:{{ $item->vendor->email }}"><i
                                                        class="fa fa-envelope fa-lg" aria-hidden="true"></i></a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                @if ($item->vendor->avatar == '')
                                    <img class="img-fluid rounded-circle"
                                        src="https://milanuncios.club/storage/app/categories/custom/971efff756052df0fc215c2c1b7de8bd.png"
                                        alt="">
                                @else
                                    <img class="img-fluid rounded-circle"
                                        src="{{ asset('img/' . $item->vendor->avatar) }}" alt="">
                                @endif
                            </div>
                            <!--/card-footer-->
                        </div>
                        <!--/card-->
                    </div>
                    @endif

                @endforeach

            </div>
            <!-- row (2:cards) -->

        </div><!-- container end-->
    </section>

    <!--Advertising: show one-->
    @include('front.splits.wire-ad-float', ['action'=>0])

</div> <!--/DIV LIVEWIRE-->

<div>
    <div class="row">

        <div class="col-md-12">
            @if ($view == 'create')
                @include('admin.categories-advertising.create')
            @else
                @include('admin.categories-advertising.edit')
            @endif
        </div>

        <div class="col-md-12">
            @include('admin.categories-advertising.table')
        </div>

    </div>
</div>

<div>
    @if ($randomAdvertising != null)
    @foreach ($randomAdvertising as $item)

    <div class="col-12 col-sm-6 col-md-4 col-lg-3 box-toast-ad px-0">
        <div class="toast fade show rounded-0 ml-auto" role="alert" aria-live="assertive"
            aria-atomic="true">
            <div class="toast-header">
                <span class="mr-auto">
                    <a href="{{ route('pages.advert') }}" data-toggle="tooltip" data-placement="top"
                        title="Listar todos los anuncios"><i class="fa fa-eye" aria-hidden="true"></i>{{ trans('multi-leng.frontend_more_ads') }}</a>
                </span>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="toast-body px-0 py-0">
                <!--Card-->
                <div class="card border-0 rounded-0">
                    <div class="row no-gutters align-items-center bg-audio-default">

                        <div class="col-7 col-sm-12" style="height:max-content">
                            <a role="button" onclick="switchEventAd(1)" wire:click="setItemValue({{ $item->id }})" data-toggle="tooltip" data-placement="top"
                                title="Ver este anuncio">
                                @if ($item->type_advertising == 'Foto')
                                    <img class="card-img-top rounded-0"
                                        src="{{ asset('img/advertisements/' . $item->image) }}" alt="">

                                @elseif( $item->type_advertising == 'Video' )
                                    <div
                                        class="embed-responsive embed-responsive-16by9 card-img-top rounded-0 bg-deep-dark">
                                        <iframe src="https://www.youtube.com/embed/{{str_replace('https://www.youtube.com/watch?v=','',$item->video)}}" frameborder="0" allowfullscreen></iframe>
                                    </div>

                                @else
                                    <div
                                        class="content-audio card-img-top rounded-0 d-flex justify-content-center align-items-center">
                                        <audio controls style="height:revert">
                                            <source src="{{ asset('audio/advertisements/' . $item->audio) }}"
                                                type="audio/mp3">
                                            Your browser does not support the audio tag.
                                        </audio>
                                    </div>
                                @endif
                            </a>
                        </div>
                        <div class="col-5 col-sm-12">
                            <div class="card-body bg-white py-2">
                                <h3 class="card-title mb-0">{{ $item->title }}</h3>
                            </div>
                            <div
                                class="card-footer d-flex justify-content-between align-items-center bg-white rounded-0 py-2">
                                <div>
                                    <h6>{{ $item->vendor->name }}</h6>

                                    <ul class="list-unstyled mb-0 d-inline-flex">
                                        @if ($item->vendor->cellphone != '')
                                            <li>
                                                <a href="tel:{{ $item->vendor->cellphone }}"><i
                                                        class="fa fa-phone fa-lg" aria-hidden="true"></i></a>
                                            </li>
                                        @endif
                                        @if ($item->vendor->whatsapp != '')
                                            <li>
                                                <a
                                                    href="https://api.whatsapp.com/send?phone={{ $item->vendor->whatsapp }}" target="_blank"><i
                                                        class="fa fa-whatsapp fa-lg" aria-hidden="true"></i></a>
                                            </li>
                                        @endif
                                        @if ($item->vendor->email != '')
                                            <li>
                                                <a href="mailto:{{ $item->vendor->email }}"><i
                                                        class="fa fa-envelope fa-lg" aria-hidden="true"></i></a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                                @if ($item->vendor->avatar == '')
                                    <img class="img-fluid rounded-circle d-none d-sm-block"
                                        src="https://milanuncios.club/storage/app/categories/custom/971efff756052df0fc215c2c1b7de8bd.png"
                                        alt="">
                                @else
                                    <img class="img-fluid rounded-circle d-none d-sm-block"
                                        src="{{ asset('img/' . $item->vendor->avatar) }}" alt="">
                                @endif
                            </div>
                        </div>

                    </div>
                    <!--/row-->
                </div>
                <!--/Card-->
            </div>
        </div>
    </div>
@endforeach
 <!--Advertising: show one-->
 @include('front.splits.wire-ad-float', ['action'=>0])
    @endif
</div>

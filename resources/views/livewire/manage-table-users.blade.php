    <div class="card card-dark">
        <div class="card-header">
          <div class="row">

            <h3 class="card-title col-sm-6 col-md-7 col-lg-8 mb-2 mb-sm-0 d-flex align-items-center">{{ trans('multi-leng.users_list') }}</h3>

            <div class="card-tools col-sm-6 col-md-5 col-lg-4">
              <div class="input-group input-group-sm">
                <input type="text" wire:model="search" class="form-control float-right" placeholder="{{ trans('multi-leng.users_search') }}">

                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>{{ trans('multi-leng.users_name') }}</th>
                <th>{{ trans('multi-leng.users_email') }}</th>
                <th>{{ trans('multi-leng.users_cellphone') }}</th>
                <th>{{ trans('multi-leng.users_banned') }}</th>
                <th>{{ trans('multi-leng.users_actions') }}</th>
              </tr>
            </thead>
            <tbody>

            @foreach ($users as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->cellphone }}</td>
                    @if($item->is_banned == 0)
                    <td>{{ trans('multi-leng.users_banned_no') }}</td>
                    @else
                     <td>{{ trans('multi-leng.users_banned_yes') }}</td>
                    @endif
                    <td>
                        <a class="btn btn-sm btn-info" href="{{ route('list.edit', $item->id) }}"><i class='fas fa-edit'></i></a>
                    </td>
                </tr>
            @endforeach

            </tbody>

          </table>

        </div>

        <div class="card-footer">
            {{ $users->links() }}
        </div>

      </div>

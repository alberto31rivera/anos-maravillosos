<div>
    <div class="card card-dark">
        <div class="card-header">
          <div class="row">

            <h3 class="card-title col-sm-6 col-md-7 col-lg-8 mb-2 mb-sm-0 d-flex align-items-center">{{ trans('multi-leng.posts_list') }}</h3>

            <div class="card-tools col-sm-6 col-md-5 col-lg-4">
              <div class="input-group input-group-sm">
                <input type="text" name="search" class="form-control float-right" wire:model="search" placeholder="{{ trans('multi-leng.posts_search_title') }}">

                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>{{ trans('multi-leng.posts_title') }}</th>
                <th>{{ trans('multi-leng.posts_category') }}</th>
                <th>{{ trans('multi-leng.posts_actions') }}</th>
              </tr>
            </thead>
            <tbody>

            @foreach ($posts as $item)
                <tr>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->category->title }}</td>
                    <td>
                        <a class="btn btn-sm btn-secondary" target="_blank" href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}"><i class='fas fa-eye'></i></a>
                       <a href="{{ route('admin.edit.posts',$item->id) }}"><button class="btn btn-sm btn-info"><i class='fas fa-edit'></i></button></a>
                       <a href="javascript:;" onclick="deleteItem('formDelete{{ $item->id }}')" class="btn btn-sm btn-danger"><i class='fas fa-trash'></i></a>
                       <form action="{{ route('admin.destroy.posts', $item->id) }}" method="POST" id="formDelete{{ $item->id }}">
                        @csrf
                        @method('DELETE')
                       </form>
                    </td>
                </tr>
            @endforeach

            </tbody>

          </table>

        </div>

        <div class="card-footer">
            {{ $posts->links() }}
        </div>

      </div>


</div>

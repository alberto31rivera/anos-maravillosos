<div>
    <div class="row">

        <div class="col-md-12">
            @if ($view == 'create')
                @include('admin.regards.create')
            @else
                @include('admin.regards.edit')
            @endif
        </div>

        <div class="col-md-12">
            @include('admin.regards.table')
        </div>

    </div>
</div>

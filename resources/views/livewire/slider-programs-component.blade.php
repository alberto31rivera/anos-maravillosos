<div>
    <div class="item">
        <span class="post-cat post-cat--min-w" href="#" style="background-color:#C20610;margin-left:20px">{{ trans('multi-leng.frontend_advertising') }}</span>
    </div>

    <!-- Swiper AD -->
    <div id="swiper-ad-container">
        <div class="swiper-wrapper" wire:ignore.self>

            @if (count($advertisements->where('status', 'Activo')) >= 4)
                @foreach ($advertisements->where('status', 'Activo') as $item)

                    <div class="swiper-slide">
                        <div class="card rounded-0 card-custom">
                            <!--.border-light-->
                            <a role="button" onclick="switchEventAd(1)" data-toggle="tooltip" data-placement="top"
                                title="Ver más" wire:click="setItemValue({{ $item->id }})">

                                @if ($item->type_advertising == 'Foto')
                                    <img class="card-img-top rounded-0"
                                        src="{{ asset('img/advertisements/' . $item->image) }}" alt="">

                                @elseif( $item->type_advertising === 'Video' )
                                    <div
                                        class="embed-responsive embed-responsive-16by9 card-img-top rounded-0 bg-deep-dark">
                                        <iframe
                                            src="https://www.youtube.com/embed/{{ str_replace('https://www.youtube.com/watch?v=', '', $item->video) }}"
                                            frameborder="0" allowfullscreen></iframe>
                                    </div>

                                @elseif( $item->type_advertising === 'Audio' )
                                    <div
                                        class="content-audio card-img-top bg-dark d-flex justify-content-center align-items-center">
                                        <audio controls style="height:revert">
                                            <source src="{{ asset('audio/advertisements/' . $item->audio) }}"
                                                type="audio/mp3">
                                            Your browser does not support the audio tag.
                                        </audio>
                                    </div>
                                @endif

                                <div
                                    class="card-header bg-white d-flex flex-column justify-content-between border-bottom-0">
                                    <h4 class="card-title mb-1 pt-1">
                                        {{ $item->title }}
                                    </h4>
                                </div>
                                <!--/card-header-->
                            </a>
                            <div class="card-footer d-flex align-items-center bg-white py-2">
                                <div>
                                    <h6>{{ $item->vendor->name }}</h6>
                                    <ul class="list-unstyled mb-0 d-inline-flex">
                                        <li>
                                            @if ($item->vendor->cellphone != '')
                                                <a href="tel:{{ $item->vendor->cellphone }}"><i
                                                        class="fa fa-phone fa-lg" aria-hidden="true"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if ($item->vendor->whatsapp != '')
                                                <a
                                                    href="https://api.whatsapp.com/send?phone={{ $item->vendor->whatsapp }}" target="_blank"><i
                                                        class="fa fa-whatsapp fa-lg" aria-hidden="true"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if ($item->vendor->email != '')
                                                <a href="mailto:{{ $item->vendor->email }}"><i
                                                        class="fa fa-envelope fa-lg" aria-hidden="true"></i></a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                @if ($item->vendor->avatar == '')
                                    <img class="img-fluid rounded-circle"
                                        src="https://milanuncios.club/storage/app/categories/custom/971efff756052df0fc215c2c1b7de8bd.png"
                                        alt="">
                                @else
                                    <img class="img-fluid rounded-circle"
                                        src="{{ asset('img/' . $item->vendor->avatar) }}" alt="">
                                @endif
                            </div>
                            <!--/card-footer-->
                        </div>
                        <!--/card-->
                    </div>

                @endforeach
            @endif
        </div>
    </div>
    <!--Advertising: show one-->
    @include('front.splits.wire-ad-float', ['action'=>-1])
</div>

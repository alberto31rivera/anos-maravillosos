<div>

{{-- BUSCADOR --}}
<div class="container mb-30">
	<div class="row row-search-form">
		<div class="col-12 col-search-form">


                <div class="form-row justify-content-center position-relative">
                    
                    <div class="col-md-5 px-0">
                        <input type="text" class="form-control form-control-lg" wire:model="search" wire:keydown.enter="search"  placeholder="{{ trans('multi-leng.frontend_search') }}">
                    </div>
                    <div class="col-md-1 col-auto px-0">
                        <button wire:click="search" class="btn btn-lg btn-block" data-toggle="tooltip" data-placement="top"
                            title="Buscar todas las coincidencias">
                            <i class="icon icon-search fa-lg"></i>
                        </button>
                    </div>

                    <div class="col-md-6 px-0">
                    </div>    

                    <div id="prueba" class="box-response position-absolute ts-grid-box p-0" style="display:block; padding-top:80px;">
                        <ul class="list-group rounded-0 border-0"> 
                            @if ($post != '')
                                @foreach ($post->take(10) as $item)
                                    <li class="list-group-item">
                                        <a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}" data-toggle="tooltip" data-placement="top" title="Ir a la publicación">
                                            {{ $item->title }}</a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>


		</div>
	</div>
</div>

{{-- TITULO SECCIÓN --}}
<!-- <div class="ts-grid-box clearfix ts-category-title">
	<h2 class="ts-title float-left">{{ trans('multi-leng.frontend_result') }}</h2>
</div> -->

{{-- RESULTADOS DE LA BUSQUEDA --}}
<div class="post-list container">
    <div class="row mb-10 ts-grid-box">

        @if ($result != null)
            @foreach ($result as $item)
            <div class="col-md-5 h-category-cols">
                <div class="ts-post-thumb d-flex justify-content-center align-items-center mb-0" style="background-color: #f0f0f0">
                    <a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
                        <img class="img-fluid mxh-cat-img" src="{{ asset('img/'.$item->image) }}" alt="">
                    </a>
                </div>
            </div>
            <!-- col lg end-->
            <div class="col-md-7 h-category-cols overflow-auto">
                <div class="post-content">
                    <h3 class="post-title md mt-3 mt-md-0">
                        <a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ Str::limit($item->title, 80) }}
                        </a>
                    </h3>
                    <ul class="post-meta-info">
                        <li>
                            <i class="fa fa-clock-o"></i>
                            {{ $item->created_at->toFormattedDateString() }}
                        </li>
                        <li class="active">
                            <i class="icon-fire"></i>
                            @if ($item->visits != null)
                                {{ $item->visits->counter }}
                            @else
                                0
                            @endif
                        </li>
                    </ul>
                    <p class="mb-0">
                        {{ Str::limit($item->post_summary, 180) }}
                    </p>

                </div>
            </div>
            <div class="col-12 mb-2"><hr></div>
            @endforeach
        @endif

    </div>
</div>

</div> <!--/livewire-->

@section('scripts')
<script>
    $(document).on("click",function(e) {

                    var container = $("#prueba");

                       if (!container.is(e.target) && container.has(e.target).length === 0) {
                        $("#prueba").css('display','none');
                       }
                });
</script>
@endsection

<div>
    <div class="card card-dark">
        <div class="card-header">
          <div class="row">

            <h3 class="card-title col-sm-6 col-md-7 col-lg-8 mb-2 mb-sm-0 d-flex align-items-center">{{ trans('multi-leng.manage_advertisements_list') }}</h3>

            <div class="card-tools col-sm-6 col-md-5 col-lg-4">
              <div class="input-group input-group-sm">
                <input type="text" name="search" class="form-control float-right" wire:model="search" placeholder="{{ trans('multi-leng.manage_advertisements_search_title') }}">

                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>{{ trans('multi-leng.manage_advertisements_title') }}</th>
                <th>{{ trans('multi-leng.manage_advertisements_seller') }}</th>
                <th>{{ trans('multi-leng.manage_advertisements_advertising_type') }}</th>
                <th>{{ trans('multi-leng.manage_advertisements_plan') }}</th>
                <th>{{ trans('multi-leng.manage_advertisements_local_cost') }}</th>
                <th>{{ trans('multi-leng.manage_advertisements_dolar_cost') }}</th>
                <th>{{ trans('multi-leng.manage_advertisements_start_date') }}</th>
                <th>{{ trans('multi-leng.manage_advertisements_end_date') }}</th>
                <th>{{ trans('multi-leng.manage_advertisements_status') }}</th>
                <th>{{ trans('multi-leng.manage_advertisements_actions') }}</th>
              </tr>
            </thead>
            <tbody>

            @foreach ($advertisements as $item)
                <tr>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->vendor->name }}</td>
                    @if ($item->type_advertising == 'Foto')
                    <td>{{ trans('multi-leng.manage_advertisements_photo') }}</td>
                    @elseif ($item->type_advertising == 'Audio')
                    <td>{{ trans('multi-leng.manage_advertisements_audio') }}</td>
                    @else
                    <td>{{ trans('multi-leng.manage_advertisements_video') }}</td>
                    @endif


                    <td>{{ $item->plan->title }}</td>
                    <td>{{ $item->cost }}</td>
                    <td>{{ $item->cost_dolar }}</td>
                    <td>{{ $item->start_date }}</td>
                    <td>{{ $item->end_date }}</td>
                    <td>{{ $item->status }}</td>
                    <td>
                       <a href="{{ route('advertisements.edit',$item->id) }}"><button class="btn btn-sm btn-info"><i class='fas fa-edit'></i></button></a>
                       <a href="javascript:;" onclick="deleteItem('formDelete{{ $item->id }}')" class="btn btn-sm btn-danger"><i class='fas fa-trash'></i></a>
                       <form action="{{ route('advertisements.destroy', $item->id) }}" method="POST" id="formDelete{{ $item->id }}">
                        @csrf
                        @method('DELETE')
                       </form>
                    </td>
                </tr>
            @endforeach

            </tbody>

          </table>

        </div>

        <div class="card-footer">
            {{ $advertisements->links() }}
        </div>

      </div>


</div>

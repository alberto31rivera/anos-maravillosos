<div>
    <div class="row">

        <div class="col-md-12">
            @if ($view == 'create')
                @include('admin.categories.create')
            @else
                @include('admin.categories.edit')
            @endif
        </div>

        <div class="col-md-12">
            @include('admin.categories.table')
        </div>

    </div>
</div>

<div >
    <div class="ts-grid-box pb-5 details-account  mb-3">
            @if (session()->has('success-message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ session('success-message') }}
            </div>
        @endif

        @if (session()->has('error-message'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session('error-message') }}
        </div>
        @endif
        <h2 class="widget-title" style="margin-bottom: 5px;">{{ trans('multi-leng.user_shopping_details') }}</h2>
        <div class="author-box author-box-item px-0">
            <div class="ts-grid-box  px-0 pb-0" style="box-shadow:none">
                <div class="reg-page">

                    <div class="card rounded-0">
                        <div class="card-header rounded-0">
                          <h3 class="card-title">{{ trans('multi-leng.user_transactions_list') }}</h3>

                          <div class="card-tools">
                            <div class="input-group input-group-sm col-12 col-sm-8 col-lg-6 px-0">
                              <input type="text" name="search" class="form-control float-right" wire:model="search" placeholder="{{ trans('multi-leng.user_search_id') }}">

                              <div class="input-group-append">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                          <table class="table table-hover text-nowrap">
                            <thead>
                              <tr>
                                <th>{{ trans('multi-leng.user_transactions_id') }}</th>
                                <th>{{ trans('multi-leng.user_transactions_payment_gateway') }}</th>
                                <th>{{ trans('multi-leng.user_transactions_payment_plan') }}</th>
                                <th>{{ trans('multi-leng.user_transactions_cost') }}</th>
                                <th>{{ trans('multi-leng.user_transactions_time') }}</th>
                                <th>{{ trans('multi-leng.user_transactions_date') }}</th>
                                <th>{{ trans('multi-leng.user_transactions_status') }}</th>
                                <th>{{ trans('multi-leng.user_transactions_details') }}</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($transactions as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->payment_method }}</td>
                                    <td>{{ $item->plan }}</td>
                                    @if ($item->payment_method == 'Transferencia Bancaria')
                                    <td>{{ $item->cost }} {{ $currencyLocal->name }}</td>
                                    @elseif ($item->payment_method == 'Mercado Pago')
                                    <td>{{ $item->cost }} {{ $currencyLocal->name }}</td>
                                    @else
                                    <td>{{ $item->cost_dolar }} {{ $currencyDolar->name }}</td>
                                    @endif
                                    <td>{{ $item->duration }}</td>
                                    <td>{{ $item->payment_date }}</td>
                                    @if ($item->status == 0)
                                    <td>{{ trans('multi-leng.user_transactions_status_pending') }}</td>
                                    @elseif ($item->status == 1)
                                    <td>{{ trans('multi-leng.user_transactions_status_approved') }}</td>
                                    @else
                                    <td>{{ trans('multi-leng.user_transactions_status_rejected') }}</td>
                                    @endif
                                    <td>
                                        <a class="btn btn-dark px-2 py-1" wire:click="setValue({{ $item->id }})" title="Ver Recibo" data-toggle="modal" data-target="#receiptModal" type="button"  style="border-radius:.25rem"><i class="fa fa-file-text fa-lg"></i></a>
                                        @if ($item->payment_method == 'Transferencia Bancaria')
                                        <a class="btn btn-dark px-2 py-1" wire:click="setValue({{ $item->id }})" title="Ver Voucher" data-toggle="modal" data-target="#voucherModal" type="button" style="border-radius:.25rem"><i class="fa fa-file-image-o fa-lg"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                          </table>

                        </div>

                        <div class="card-footer">
                            {{ $transactions->links() }}
                        </div>

                      </div>

                </div> <!-- card.// -->

            </div><!-- grid box end -->

        </div>
    </div>

    @if ($document != null)
    <div wire:ignore.self class="modal fade modal--radios" id="voucherModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content rounded-0 border-0" style="background-color:#E91E63">
            <div class="modal-header pt-1 pb-1 rounded-0 border-0 bg-white">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body pt-1 pb-0 px-0">

              <!--Content-->
              <div class="card rounded-0 border-0 ts-grid-box">
                <div class="card-body">
                  <div class="contaner">
                    <div class="row justify-content-center align-items-center">
                      <div class="col-auto">
                        <img class="img-fluid" src="{{asset('documents/voucher-transfer/'.$document->proof_transfer)}}" alt="" style="max-height:85vh;max-width:85vw">
                      </div>
                    </div>
                  </div>
                </div>
              </div> <!--/card-->
              <!--/Content-->

            </div>
          </div>
        </div>
      </div>

          <!-- Modal -->
<div wire:ignore.self class="modal fade modal--radios" id="receiptModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
      <div class="modal-content rounded-0 border-0" style="background-color:#E91E63">
        <div class="modal-header pt-1 pb-1 rounded-0 border-0 bg-white">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body pt-1 pb-0 px-0">

          <!--Content-->
          <div class="card rounded-0 border-0 ts-grid-box">
            <div class="card-header bg-white d-flex justify-content-between align-items-end">
               @if ($admin->logo == '')
                <img class="img-fluid rounded" src="{{asset('assets/front/template/images/logo/logo-v2.png')}}" alt="">
               @else
                <img class="img-fluid rounded" src="{{asset('img/logos-users/'.$admin->logo)}}" alt="">
               @endif
               <span class="text-muted pl-2 ml-auto">{{ trans('multi-leng.user_transactions_date_two') }}: {{ $document->created_at->toFormattedDateString() }}</span>
            </div>
            <div class="card-body pb-0">
              <div class="contaner">
                <div class="row">
                  <div class="col-md-4 mb-2 mb-md-0">
                    <h3 class="mb-0">{{ $admin->name }}</h3>
                    <p>
                      <span>{{ $admin->number_document }}</span>
                      <span>{{ $admin->address }}</span>
                      <span>{{ $admin->cellphone }}</span>
                      <span>{{ $admin->city }}</span>
                      <span>{{ $admin->country }}</span>
                      <span>{{ env('MAIL_USERNAME') }}</span>
                    </p>
                  </div>
                  <div class="col-md-4 mb-2 mb-md-0">
                    <p>
                      <span>{{ $document->user->name }}</span>
                      <span>{{ $document->user->number_document }}</span>
                      <span>{{ $document->user->address }}</span>
                      <span>{{ $document->user->cellphone }}</span>
                      <span>{{ $document->user->city }}</span>
                      <span>{{ $document->user->country }}</span>
                      <span>{{ $document->user->email }}</span>
                    </p>
                  </div>
                  <div class="col-md-4 mb-2 mb-md-0">
                    <p>
                      <span><strong>{{ trans('multi-leng.user_transactions_invoice') }}</strong> #{{ str_pad($document->id, 4, '0', STR_PAD_LEFT) }}</span>
                       @if ($document->payment_method == 'Transferencia Bancaria')
                       <span><strong>{{ trans('multi-leng.user_transactions_transfer_date') }} </strong>{{ $document->transfer_date }} </span>
                       @endif
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th scope="col">{{ trans('multi-leng.user_transactions_quantity') }}</th>
                          <th scope="col">{{ trans('multi-leng.user_transactions_description') }}</th>
                          <th scope="col" class="text-right">{{ trans('multi-leng.user_transactions_subtotal') }}</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>{!! $document->description !!}</td>
                          @if ($document->payment_method == 'Transferencia Bancaria')
                          <td class="wspace-nrap text-right">{{ $document->cost }} {{ $currencyLocal->name }}</td>
                          @elseif ($document->payment_method == 'Mercado Pago')
                          <td class="wspace-nrap text-right">{{ $document->cost }} {{ $currencyLocal->name }}</td>
                          @else
                          <td class="wspace-nrap text-right">{{ $document->cost_dolar }} {{ $currencyDolar->name }}</td>
                          @endif
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right">
                            @if ($document->payment_method == 'Transferencia Bancaria')
                            <span class="font-weight-bold">{{ trans('multi-leng.user_transactions_total') }}:</span> {{ $document->cost }} {{ $currencyLocal->name }}
                            @elseif ($document->payment_method == 'Mercado Pago')
                            <span class="font-weight-bold">{{ trans('multi-leng.user_transactions_total') }}:</span> {{ $document->cost }} {{ $currencyLocal->name }}
                            @else
                            <span class="font-weight-bold">{{ trans('multi-leng.user_transactions_total') }}:</span> {{ $document->cost_dolar }} {{ $currencyDolar->name }}
                            @endif
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              {{-- <button wire:click="dowloandPdf({{ $item->id }})" class="btn btn-primary"><i class="fa fa-print"></i> {{ trans('multi-leng.user_transactions_print') }}</button> --}}
            </div>
          </div> <!--/card-->
          <!--/Content-->

        </div>
      </div>
    </div>
  </div>

    @endif
</div>

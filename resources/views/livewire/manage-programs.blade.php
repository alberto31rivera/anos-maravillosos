<div>
    <div class="row">

        <div class="col-md-12">
            @if ($view == 'create')
                @include('admin.programs.create')
            @else
                @include('admin.programs.edit')
            @endif
        </div>

        <div class="col-md-12">
            @include('admin.programs.table')
        </div>

    </div>
</div>

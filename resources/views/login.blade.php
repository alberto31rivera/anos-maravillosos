@extends('front.pages.layouts')

@section('content')


<section class="blog-content" style="padding:10px 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
				</div>
				<div class="col-md-6 col-sm-12">
						@if (session('notification'))
							<div class="alert alert-success">
								{{ session('notification') }}
							</div>
						@endif
						@if (session('error'))
						<div class="alert alert-danger">
							{{ session('error') }}
						</div>
					@endif
						@if (count($errors) > 0)
						<div class="callout callout-danger">
							<h5>Error!</h5>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
				</div>

				<div class="col-md-3 col-sm-12">
				</div>
			</div>
		</div>
</section>

 

 


	<section id="account" class="account" style="padding:10px 0;">
        <div class="container">
			
		<!-- insertado -->
		<h3 class="log-sign-title text-center mb-25">Login con:
			 </h3>
		<form action="{{ route('authenticate.user') }}" method="POST">
			@csrf
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4">
				 
			</div>

			@if ($socialLinks != null)
			@if ($socialLinks->facebook_status == 1 && $socialLinks->facebook_client_id != '' && $socialLinks->facebook_client_secret != '')
			<div class="col-xs-12 col-sm-6 col-md-2" style="padding-top:5px;">
				<a href="{{ route('social.auth', 'facebook') }}" class="btn btn-lg btn-primary btn-fb btn-block">{{ trans('multi-leng.frontend_login_facebook') }}</a>
				</div>
			@endif
			@endif
			@if ($socialLinks != null)
			@if ($socialLinks->google_status == 1 && $socialLinks->google_client_id != '' && $socialLinks->google_client_secret != '')
			<div class="col-xs-12 col-sm-6 col-md-2" style="padding-top:5px;">
				<a href="{{ route('social.auth', 'google') }}" class="btn btn-lg btn-info btn-block">{{ trans('multi-leng.frontend_login_google') }}</a>
				</div>
			@endif
			@endif

			<div class="col-xs-12 col-sm-6 col-md-4">
				 
			</div>
		</div>
		<!-- fin de insertado -->

            <div class="row" style="padding:40px 0;">
				<div class="col-xs-12 col-sm-6 col-md-4"></div>

                <div class="col-md-4 col-xs-12">
                    <div class="account-inner">
                        <div class="section-title">
                            <h3>Inicia Sesión</h3>
                        </div>
                        <form class="" method="">
							<div class="form-group">
								<label for="inputUsernameEmail">{{ trans('multi-leng.frontend_login_email') }}</label>
													<input type="text" name="email" class="form-control" id="inputUsernameEmail">
							</div>
							<div class="form-group">
								<a class="pull-right" href="{{ route('reset-password') }}">{{ trans('multi-leng.frontend_login_forgot_password') }}</a>
													<label for="inputPassword">{{ trans('multi-leng.frontend_login_password') }}</label>
													<input type="password" name="password" class="form-control" id="inputPassword">
							</div>
							<div class="form-group">

								<button type="submit" class="btn btn-red" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
									@else white @endif  @endif">
								{{ trans('multi-leng.frontend_login') }}
								</button>
								<input class="checkbox" type="checkbox"> <span>{{ trans('multi-leng.frontend_login_remember') }} </span>
							</div>    
							 
						</form>
                    </div>
                </div>

				<div class="col-xs-12 col-sm-6 col-md-4"></div>
              
            </div>
        </div>
    </section>

@endsection

@extends('front.pages.layouts')

@section('content')

<!-- post wraper start-->
<section class="block-wrapper content-min-h py-3 py-md-4 my-3 my-md-5 d-flex align-items-center">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 mx-auto">
                @if (session('notification'))
                    <div class="alert alert-success">
                        {{ session('notification') }}
                    </div>
                @endif
                @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
                @if (count($errors) > 0)
                <div class="callout callout-danger">
                    <h5>Error!</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
				<div class="ts-grid-box  mb-0">
					<div class="login-page">
						<h3 class="log-sign-title text-center mb-25">{{ trans('multi-leng.frontend_reset_password') }} <a href="{{ route('login') }}">{{ trans('multi-leng.frontend_reset_login') }}</a></h3>

                        <form action="{{ route('forgot.password.user') }}" method="POST">
                            @csrf

							<div class="form-group">
							<label for="inputUsernameEmail">{{ trans('multi-leng.frontend_reset_email') }}</label>
							<input type="email" name="email" class="form-control" id="inputUsernameEmail">
							</div>

							<div class="form-group text-center">
							<button type="submit" class="btn btn btn-primary" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
                                @else #e91e63 @endif  @endif">
							{{ trans('multi-leng.frontend_reset_send_link') }}
							</button>
							</div>
						</form>
					</div>
				</div><!-- grid box end -->
			</div>
			<!-- col end-->

		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>
<!-- post wraper end-->

@endsection

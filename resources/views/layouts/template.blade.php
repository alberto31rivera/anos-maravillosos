@php
$getPath = Request::path().'/*';
$getRoute = Request::route()->getName();
@endphp

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Radios</title>

    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet"
        href="{{ asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <!-- Font Awesome Icons -->
   <!--  <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}"> -->
    <!-- Ionicons -->
   <!--  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
   <!--  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->
    <!-- Custom styles -->
  <!--   <link rel="stylesheet" href="{{ asset('assets/custom/generic.css') }}"> -->
    <!-- Select2 -->
<!--     <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css">

    @yield('styles')
    @livewireStyles
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{ route('dashboard') }}" class="brand-link">
                @if (Auth::user()->logo != '')
                <img src="{{ asset('img/logos-users/'. Auth::user()->logo) }}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                @else
                <img src="{{ asset('assets/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
                class="brand-image img-circle elevation-3" style="opacity: .8">
                @endif
                <span class="brand-text {{Request::url()===route('dashboard')?'active':''}}"> 
                    Panel Control
                </span>
                
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-2 pb-3 mb-2 d-flex">
                    <div class="image">
                        @if (Auth::user()->avatar != '')
                        <img src="{{ asset('img/avatar-users/'. Auth::user()->avatar) }}" class="img-circle elevation-2"
                        alt="User Image">
                        @else
                        <img src="{{ asset('assets/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                        alt="User Image">
                        @endif
                    </div>
                    <div class="info">
                        <a href="{{ route('profile.index') }}" class="d-block {{Request::url()===route('profile.index')?'active':''}}">{{ Auth::user()->name }}</a>
                    </div>
                </div>
                <div><a target="_blank" href="{{route('f.home.index')}}"> Ver sitio</a></div>
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview {{request()->is('admin/settings/*')?'menu-open':''}}">
                        
                            <a href="#" class="nav-link {{request()->is('admin/settings/*')?'active':''}}"> <!--style="background:#17a2b8"-->
                                <i class="nav-icon fas fa-cogs"></i>
                                <p>
                                    {{trans('multi-leng.general_settings') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview" >
                                <li class="nav-item">
                                    <a href="{{ route('homepage.index') }}" class="nav-link {{Request::url()===route('homepage.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.homepage') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('themes.index') }}" class="nav-link {{Request::url()===route('themes.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.themes') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.setting.email') }}" class="nav-link {{Request::url()===route('admin.setting.email')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.emails') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.languages') }}" class="nav-link {{Request::url()===route('admin.languages')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.language') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('social-links.index') }}" class="nav-link {{Request::url()===route('social-links.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.social_links') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('transmissions.index') }}" class="nav-link {{Request::url()===route('transmissions.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.transmissions') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('google.index') }}" class="nav-link {{Request::url()===route('google.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.google') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('services.index') }}" class="nav-link {{Request::url()===route('services.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.documents') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.currencies') }}" class="nav-link {{Request::url()===route('admin.currencies')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.currencies') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{request()->is('admin/manager/*')?'menu-open':''}}">
                            <a href="#" class="nav-link {{request()->is('admin/manager/*')?'active':''}}">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p>
                                    {{trans('multi-leng.manage_admin') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('profile.index') }}" class="nav-link {{Request::url()===route('profile.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.profile') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.regards') }}" class="nav-link {{Request::url()===route('admin.regards')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.regards') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{request()->is('admin/users/*')?'menu-open':''}}">
                            <a href="#" class="nav-link {{request()->is('admin/users/*')?'active':''}}">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    {{trans('multi-leng.manage_users') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('list.index') }}" class="nav-link {{Request::url()===route('list.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.users') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.comments') }}" class="nav-link {{Request::url()===route('admin.comments')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.comments') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{request()->is('admin/publications/*')?'menu-open':''}}">
                            <a href="#" class="nav-link {{request()->is('admin/publications/*')?'active':''}}">
                                <i class="nav-icon fas fa-book"></i>
                                <p>
                                    {{trans('multi-leng.manage_publications') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('admin.categories') }}" class="nav-link {{Request::url()===route('admin.categories')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.categories') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.posts') }}" class="nav-link {{Request::url()===route('admin.posts')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.posts') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{request()->is('admin/programs/*')?'menu-open':''}}">
                            <a href="#" class="nav-link {{request()->is('admin/programs/*')?'active':''}}">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    {{trans('multi-leng.manage_programs') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('admin.programs') }}" class="nav-link {{Request::url()===route('admin.programs')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.programs') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('sponsor.index') }}" class="nav-link {{Request::url()===route('sponsor.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.sponsors') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{request()->is('admin/advertising/*')?'menu-open':''}}">
                            <a href="#" class="nav-link {{request()->is('admin/advertising/*')?'active':''}}">
                                <i class="nav-icon fas fa-fw fa-newspaper"></i>
                                <p>
                                    {{trans('multi-leng.manage_advertising') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('admin.categories.advertising') }}" class="nav-link {{Request::url()===route('admin.categories.advertising')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.categories_advertising') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('plans.index') }}" class="nav-link {{Request::url()===route('plans.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.plans') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.vendors') }}" class="nav-link {{Request::url()===route('admin.vendors')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.vendors') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('advertisements.index') }}" class="nav-link {{Request::url()===route('advertisements.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.advertisements') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{request()->is('admin/payments/*')?'menu-open':''}}">
                            <a href="#" class="nav-link {{request()->is('admin/payments/*')?'active':''}}">
                                <i class="nav-icon fas fa-dollar-sign"></i>
                                <p>
                                    {{trans('multi-leng.manage_payments') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('paypal.index') }}" class="nav-link {{Request::url()===route('paypal.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.paypal') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('mercado-pago.index') }}" class="nav-link {{Request::url()===route('mercado-pago.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.mercado_pago') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('transfer.index') }}" class="nav-link {{Request::url()===route('transfer.index')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.transfer') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.payments') }}" class="nav-link {{Request::url()===route('admin.payments')?'active':''}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{trans('multi-leng.payments') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-item--exit">
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                this.closest('form').submit();">
                                    {{-- <i class="nav-icon fas fa-lock"></i> --}}
                                    <i class="nav-icon fas fa-sign-out-alt"></i>
                                    <p>
                                        {{trans('multi-leng.exit') }}
                                    </p>
                                </a>

                            </form>

                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">@yield('title')</h1>
                        </div><!-- /.col -->
                        <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li>
            </ol>
          </div>/.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    @yield('content')
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">

            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                soporte@jiraco.com <br>
               <a href="https://api.whatsapp.com/send?phone=+51993434076" target="_blank">+51 916 288 550</a>
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2022. ERIVERA.</strong> All rights reserved. <br>
            <a href="https://wordprecero.com/" target="_blank">www.wordprecero.com</a>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- ChartJS -->
    <script src="{{ asset('assets/plugins/chart.js/Chart.min.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>
    <!-- Custom scripts -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <!-- Select2 -->
    <script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>

    <script>
        function clearContentImg(id)
        {
            let file = $('#'+id)
            file.val('')
            file.next().find('img').attr('src','')

            $('#close-'+id).css("display","none")
        }

        $(".file-decoration").on("change",function()
        {
          let file = $(this).attr("id")
          $('#close-'+file).css("display","block")
        });
    </script>

    @yield('scripts')
    @livewireScripts
</body>

</html>

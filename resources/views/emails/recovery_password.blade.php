<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
</head>
<body>
    <h2>Hola {{ $name }},<br>
    <p>Haz recibido este Email porque recibimos una solicitud para restablecer la contraseña de tu cuenta en Radios.</p>
    <a href="{{ url('/recovery/password/' . $token) }}">
        Click aqui para restablecer contraseña.
    </a>
    <p>Si no enviaste ninguna solicitud, no se requiere ninguna acción adicional.</p>
    <p>Saludos</p>
</body>
</html>

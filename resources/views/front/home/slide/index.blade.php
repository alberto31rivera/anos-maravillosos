 

 

	<section class="content-style-1">
        <div class="container">
            <div class="row">
                <!-- col-md-6  -->
				@include('front.home.slide.inc.slider')
				 

                <!-- col-md-3  -->
				@include('front.home.slide.inc.izquierdo')
				
                <div class="col-md-3 col-sm-12">
                    <div class="item-sidebar margin-up">
                         
                        <div class="post-categories sidebar-box">
                            <div class="sidebar-title">
                                <h3>Categorías</h3>
                            </div>
                            <ul>
							@foreach ($allCategories as $item)
                    		<li><a href="{{ route('f.pages.categories', $item->slug ) }}">{{ $item->title }} </a>({{ $item->posts->count() }})</li>
   
                			@endforeach
                            </ul>
                        </div>

                        <div class="recent-post clearfix sidebar-box">
                            <div class="sidebar-title">
                                <h3>Últimas publicaciones  </h3>
                            </div>
							@foreach ($recentPosts as $item)
                            <div class="recent-item">
                                <div class="recent-image">
                                    <img src="{{ asset('thumbnail/'.$item->thumbnail) }}" alt="{{ Str::limit($item->title,80) }}">
                                </div>
                                <div class="recent-content">
                                    <a href="{{ route('f.pages.categories', $item->category->slug ) }}" class="tag tag-blue">{{ $item->category->title }}</a>
                                    <h4><a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ Str::limit($item->title,60) }}</a></h4>
                                     
                                </div>
                            </div>  
							@endforeach   
                           
                              
                                                       
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

<div class="ts-post-overlay-style-1">
    @if(!$postsSectionThree->isEmpty())
    @foreach ($postsSectionThree as $item)
    @if ($item->category->status_home_page == 1)
	<div class="ts-overlay-style">
		<div class="item">
			<div class="ts-post-thumb">
				<a class="post-cat ts-{{ $item->category->color }}-bg" href="{{ route('f.pages.categories', $item->category->slug ) }}">{{ $item->category->title }}</a>
				<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
					<img class="img-fluid" src="{{ asset('img/'.$item->image) }}" alt="">
				</a>
			</div>

			<div class="overlay-post-content">
				<div class="post-content">
					<h3 class="post-title">
						<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
							{{ Str::limit($item->title,50) }}
						</a>
					</h3>
					<ul class="post-meta-info">
						<li>
                            <span class="post-date-info">
                                <i class="fa fa-clock-o"></i>
                                {{ $item->created_at->toFormattedDateString() }}
                            </span>
                        </li>

                        <li class="active">
                            <i class="icon-fire"></i>
                            @if ($item->visits != null)
                                {{ $item->visits->counter }}
                            @else
                                0
                            @endif
                        </li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end item-->
	</div>
    @else
    @endif
    @endforeach
    @else
    <div class="ts-overlay-style">
		<div class="item">
			<div class="ts-post-thumb">
				<a class="post-cat ts-blue-light-bg" href="#">Título de la Categoría</a>
				<a href="#">
					<img class="img-fluid" src="{{ asset('img/image-posts/default.jpg') }}" alt="">
				</a>
			</div>

			<div class="overlay-post-content">
				<div class="post-content">
					<h3 class="post-title">
						<a href="#">Título del Post</a>
					</h3>
					<ul class="post-meta-info">
						<li>
							<i class="fa fa-clock-o"></i>
							Mar 30, 2021
						</li>
                        <li class="active">
                            <i class="icon-fire"></i>
                                0
                        </li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end item-->
	</div>
    @endif

</div>

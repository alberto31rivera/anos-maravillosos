 

	<div class="col-md-6 col-md-push-3 col-sm-12">
                    <div class="section-content margin-up">
                        <div id="home_banner_blog" class="content-slider">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
								
								@if(!$postsSectionOne->isEmpty())
								@foreach ($postsSectionOne as $item)
								@if ($item->category->status_home_page == 1)

                                    <div class="swiper-slide">
                                        <div class="slide-inner slide-overlay" style="background-image:url({{ asset('img/'.$item->image) }})">  
                                            <div class="home_banner_text">
                                                <div class="banner-tag">
                                                    <a href="{{ route('f.pages.categories', $item->category->slug ) }}" class="tag tag-blue">#{{ $item->category->title }}</a>
                                                     
                                                </div>
                                                <h2><a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ Str::limit($item->title,60) }}</a></h2>
                                                <div class="author-detail">
                                                    <p><a href="#"><i class="icon-view"></i>{{ config('app.name') }}</a></p>
                                                    <p><i class="icon-clock"></i> {{ $item->created_at->toFormattedDateString() }}</p>                                
                                                    <p><a href="#"><i class="fa fa-eye"></i>
													@if ($item->visits != null)
													{{ $item->visits->counter }}
													@else
													0
													@endif
						  							</a></p>
                                                </div>
                                            </div>                                      
                                        </div> 
                                    </div>
                                     
								@endif
								@endforeach
									@else
								
								No hay contenido 

								@endif    
                                </div>
                                <div class="swiper-button-next swiper-button-white"></div>
                                <div class="swiper-button-prev swiper-button-white"></div>
                            </div>
                        </div>
                      
                        <div class="item-content-outer">
                            <div class="row">
                                <div class="col-xs-12">

									@if(!$postsSectionThree->isEmpty())
									@foreach ( $postsSectionThree as $item )
									@if ($item->category->status_home_page == 1)
                                    

                                    <div class="item-small">
                                    <a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
                                        <div class="item-small-image margin-zero">
                                            <img src="{{ asset('img/'.$item->image) }}" alt="{{ $item->title }}">
                                            <div class="image-overlay"></div>
                                        </div>
                                    </a>
                                    
                                        <div class="item-small-content">
                                            <h4><a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ $item->title }}</a></h4>
                                            <div class="author-detail">
                                               
                                                {{ Str::limit($item->post_summary,180) }}
                                            </div>                                             
                                        </div>
                                    </div>
									@endif
									@endforeach
									@else
									no hay contenido
									@endif
                                    
                                  
									
									



								
                                </div>
                            </div>
                        </div>
                        
                    </div>
   </div>
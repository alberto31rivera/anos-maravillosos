<div class="col-md-3 col-md-pull-6 col-sm-12">
    <div class="item-sidebar" data-ref="container-1">
        <div class="sidebar-box">
            <div class="category-sidebar">
                <ul class="post-category">
                    <li class="filter" data-filter=".popular-post">Top</li>
                    <li class="filter" data-filter=".recent-post">Últimos</li>
                    <li class="filter" data-filter=".post-categories">Categorías</li>
                </ul>
            </div>
            <div class="post-categories mix">
                <div class="sidebar-title">
                    <h3>Categorías</h3>
                </div>
                <ul>
                @foreach ($allCategories as $item)
                    <li><a href="{{ route('f.pages.categories', $item->slug ) }}">{{ $item->title }} </a>
                    ({{ $item->posts->count() }})
                    </li>
   
                @endforeach
                </ul>

                
		<!-- <li>
			<a href="">
				<span class="ts-{{ $item->color }}-bg"></span>
			</a>
		</li> -->
        
            </div>
            <div class="recent-post clearfix mix">
                <div class="sidebar-title">
                    <h3>Post recientes</h3>
                </div>
                @foreach ($recentPosts as $item)
                <div class="recent-item">
                    <div class="recent-image">
                        <img src="{{ asset('thumbnail/'.$item->thumbnail) }}" alt="{{ Str::limit($item->title,80) }}">
                    </div>
                    <div class="recent-content">
                        <a href="{{ route('f.pages.categories', $item->category->slug ) }}" class="tag tag-blue">{{ $item->category->title }}</a>
                        <h4><a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ Str::limit($item->title,60) }}</a></h4>
                        <!-- <div class="author-detail">
                            <p><a href="#"><i class="icon-profile-male"></i>{{ config('app.name') }}</a></p>
                            <p><i class="icon-clock"></i> {{ $item->created_at->toFormattedDateString() }}</p>
                        </div> -->
                    </div>
                </div>  
                @endforeach                   
                                           
            </div>
           
            
                @include('front.splits.w-populer-post')  
                 
 
            
        </div>
        <div class="ad1 sidebar-box">
            <div class="sidebar-title">
                <h3>Hosting Wordpress</h3>
            </div>
            <div class="ad1-image">
            <a target="_blank" href="https://clientes.webempresa.com/america/gestion/aff.php?aff=1837&landing=lanzamientohw" > <img src="https://afiliados.webempresa.eu/banner-afiliados-nuevos-planes-hombreverde-250x250px.gif" width="250" height="250" border="0" alt="Hosting WordPress" title="Hosting WordPress español" > 
            </a>

            </div>
        </div>
         
    </div>
</div>
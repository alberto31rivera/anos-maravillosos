<div class="ts-grid-box ts-col-box h-100">
    <div class="item">
        @if (!$postsSectionOne->isEmpty())
            @foreach ($postsSectionOne as $item)
                @if ($item->category->status_home_page == 1)
                    <a class="post-cat ts-{{ $item->category->color }}-bg"
                        href="{{ route('f.pages.categories', $item->category->slug) }}">{{ $item->category->title }}</a>
                    <div class="post-content">
                        <h3 class="post-title md">
                            <a
                                href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2' => $item->slug]) }}">
                                {{ Str::limit($item->title,90) }}
                            </a>
                        </h3>
                        <p>
                            {{ Str::limit($item->post_summary,210) }}
                        </p>
                        <ul class="post-meta-info">
                            <li>
                                <span class="post-date-info">
                                    <i class="fa fa-clock-o"></i>
                                    {{ $item->created_at->toFormattedDateString() }}
                                </span>
                            </li>

                            <li class="active">
                                <i class="icon-fire"></i>
                                @if ($item->visits != null)
                                    {{ $item->visits->counter }}
                                @else
                                    0
                                @endif
                            </li>
                        </ul>
                    </div>
                @endif
            @endforeach
        @else
            no hay datos
        @endif

    </div>
    <!-- item end-->
</div>


 
              
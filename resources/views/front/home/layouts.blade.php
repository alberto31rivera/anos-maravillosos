<!doctype html>
<html lang="es">

<head>
	<!-- Basic Page Needs =====================================-->
	<meta charset="utf-8">

	<!-- Mobile Specific Metas ================================-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Site Title- -->
	<title>@yield('title')</title>
    <meta name="description" content="@yield('description')" />

	<!-- CSS
   ==================================================== -->
	<!-- uikit -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/uikit.min.css')}}">

	<!-- Bootstrap -->
	 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/font-awesome.min.css')}}">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/animate.css')}}">

	<!-- IcoFonts -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/icofonts.css')}}">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/owlcarousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/front/template/css/slick.css')}}">



	<!-- navigation -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/navigation.css')}}">

	<!-- magnific popup -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/magnific-popup.css')}}">

	<!-- Style -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/style.css')}}">
	<!-- color -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/colors/color-1.css')}}">

	<!-- Responsive -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/responsive.css')}}">

 

	<link rel="stylesheet" href="{{asset('assets/front/custom/generic.css')}}">


	<!-- / *** / -->
	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>

  

    @yield('styles')

    @if (env('APP_LOCALE')=='en')
    <style>.ts-breaking-news .breaking-news-content{width:77.5%}</style>
    @endif

    @livewireStyles
</head>

<body class="body-color">
	<!-- top bar start -->
	@include('front.home.top.bar')
	<!-- end top bar-->

	<!-- ad banner start -->
	@include('front.home.top.banner')
	<!-- ad banner end -->

	<!-- header nav start-->
	@include('front.home.top.menu')
	<!-- header nav end-->

	@yield('content')

	<!-- footer area -->
	@include('front.home.bottom.index')
	<!-- footer area end-->

	{{-- Request::route()->getName() != 'pages.advert' || --}}
	@if(
		Request::route()->getName() != 'pages.programs'
		)
		@include('front.splits.ad-pop-up')
	@endif



	<!-- javaScript Files
	=============================================================================-->

	<!-- initialize jQuery Library -->
	 
	<!-- navigation JS -->
	<script src="{{asset('assets/front/template/js/navigation.js')}}"></script>
	<!-- uikit JS -->
	<script src="{{asset('assets/front/template/js/uikit.min.js')}}"></script>

	<script src="{{asset('assets/front/template/js/uikit-icons.js')}}"></script>
	<!-- Popper JS -->
	<script src="{{asset('assets/front/template/js/popper.min.js')}}"></script>

	<!-- magnific popup JS -->
	<script src="{{asset('assets/front/template/js/jquery.magnific-popup.min.js')}}"></script>



	<!-- Bootstrap -->
	 
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

	<!-- Owl Carousel -->
	<script src="{{asset('assets/front/template/js/owl-carousel.2.3.0.min.js')}}"></script>

	<script src="{{asset('assets/front/template/js/slick.min.js')}}"></script>

	<!-- smooth scroling -->
	<script src="{{asset('assets/front/template/js/smoothscroll.js')}}"></script>

	<script src="{{asset('assets/front/template/js/main.js')}}"></script>

	<script src="{{asset('assets/front/custom/generic.js')}}"></script>

    @yield('scripts')
    @livewireScripts
</body>

</html>

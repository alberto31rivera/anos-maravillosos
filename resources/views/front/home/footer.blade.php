

 

    <footer>
        <div class="footer-content">
           
            <div class="footer-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="about">
                                <h3 style="font-size: 14px;color:white;">Wordprecero</h3>
                                <p style="font-size: 14px;">En Wordprecero compartimos conocimiento y experiencias sobre uno de los CMS más usados del mercado: Wordpress. También trabajamos con PHP, Laravel.</p>
                                <ul class="header-social-links">
                                    <li><a target="_blank" href="https://www.facebook.com/wordprecero10"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/wordprecero10/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="https://www.youtube.com/@wordprecero8847"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-2 col-xs-6">
                            <div class="categories">
                                <h3>Categorías</h3>
                                <ul>
                                @foreach ($allCategories as $item)
                                    <li><a href="{{ route('f.pages.categories', $item->slug ) }}">{{ $item->title }} </a></li>
                                     
                                @endforeach
                                </ul>
                                  
   
               
                            </div>
                        </div>
                        <div class="col-sm-2 col-xs-6">
                            <div class="tags">
                                <h3>Populares</h3>
                                <ul>
                                <li><a href="/wordpress">Wordpress</a></li> 
                                <li><a href="/elementor">Elementor</a></li> 
                                <li><a href="/hosting">Hosting</a></li> 
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="newsletter">
                                <h3>¿Buscas Asesoría Wordpress?</h3>
                                <p>Asesoría para migrar sitios Web, para diseñar páginas o tiendas virtuales, creación de portales, asesoría para compra de hosting, dominios, traslados, vps o lo que necesites... </p>
                                <p> No dudes en escribirme <a target="_blank" href="https://wa.link/rbxqvz">a mi Whatsapp</a></p>
                                <!-- <form>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="email1" placeholder="Enter Email">
                                        <a href="#"><span class="search_btn"><i class="fa fa-paper-plane" aria-hidden="true"></i></span></a>
                                    </div>
                                </form> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    <div class="copyright-content text-center">
                        <span>Copyright © 2023 <a href="https://www.wordprecero.com">Wordprecero</a> - All Rights reserved</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="overlay"></div>
    </footer>
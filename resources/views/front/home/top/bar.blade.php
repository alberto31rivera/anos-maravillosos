<section class="top-bar v2">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 align-self-center">
				<div class="ts-breaking-news clearfix">
					<h2 class="breaking-title float-left">
						<i class="fa fa-bolt"></i> Última noticia:</h2>
					<div class="breaking-news-content float-left" id="breaking_slider1">
                        @foreach ($lastNews as $item)
                        <div class="breaking-post-content">
							<p>
								<a href="#">{{ $item->post_summary  }}</a>
							</p>
						</div>
                        @endforeach
					</div>
				</div>
			</div>
			<!-- end col-->

			<div class="col-lg-4 text-right align-self-center">
				<ul class="top-social">
					<li>
						<a href="#">
							<i class="fa fa-twitter"></i>
						</a>
						<a href="#">
							<i class="fa fa-facebook"></i>
						</a>
						<a href="#">
							<i class="fa fa-google-plus"></i>
						</a>
						<a href="#">
							<i class="fa fa-pinterest"></i>
						</a>
						<a href="#">
							<i class="fa fa-vimeo-square"></i>
						</a>
					</li>
					<li class="ts-subscribe">
						<a href="#">subscribe</a>
					</li>
				</ul>
			</div>
			<!--end col -->


		</div>
		<!-- end row -->
	</div>
</section>

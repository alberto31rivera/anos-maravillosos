<section class="header-middle v2">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="logo">
					<a href="index.html">
						<img src="{{asset('assets/front/template/images/logo/logo-v2.png')}}" alt="">
					</a>
				</div>
			</div>
			<div class="col-lg-8 align-self-center">
				<div class="banner-img">
					<a href="index.html">
						<img class="img-fluid" src="{{asset('assets/front/template/images/banner/banner3.jpg')}}" alt="">
					</a>
				</div>
			</div>
			<!-- col end -->
		</div>
		<!-- row  end -->
	</div>
	<!-- container end -->
</section>

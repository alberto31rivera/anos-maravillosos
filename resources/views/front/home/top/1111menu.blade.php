<header class="header-default">
    <div class="container">
        <div class="row">
            <!-- logo end-->
            <div class="col-lg-12">
                <!--nav top end-->
                <nav class="navigation ts-main-menu navigation-landscape">
                    <div class="nav-header">
                        <a class="nav-brand mobile-logo visible-xs" href="{{ route('f.home.index') }}">
                            <img src="{{ asset('assets/front/template/images/logo/footer_logo.png') }}" alt="Logo">
                        </a>
                        <div class="nav-toggle"></div>
                    </div>
                    <!--nav brand end-->

                    <div class="nav-menus-wrapper clearfix">
                        <!--nav right menu start-->
                        <ul class="right-menu align-to-right">
                            <li>
                                @if (!Auth::user())
                                    <a href="{{ route('login') }}">
                                        <i class="fa fa-user-circle-o"></i>
                                    </a>
                                @else
                                    <a href="{{ route('f.panel.user') }}">
                                        <i class="fa fa-user-circle-o"></i>
                                    </a>
                                @endif
                            </li>
                            <li class="header-search">
                                <livewire:manage-search-home />
                            </li>
                        </ul>
                        <!--nav right menu end-->

                                            
                        
                        <!-- NO ES ESTE MENU -->



                        <!-- nav menu start-->
                        <ul class="nav-menu">
                            <li class="active">
                                <a href="{{ route('f.home.index') }}">Inicio</a>
                            </li>

                            @foreach ($allCategories->take(5) as $item)
                            <li>
                                    @if ($item->status_header == 1)
                                    <a href="{{ route('f.pages.categories', $item->slug ) }}">{{ $item->title }}</a>
                                        <div class="megamenu-panel">
                                            <div class="row">

                                                @foreach ($item->posts->take(4) as $item)
                                                <div class="col-12 col-lg-3">
                                                    <div class="item">
                                                        <div class="ts-post-thumb">
                                                            <a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
                                                                <img class="img-fluid" src="{{ asset('img/'.$item->image) }}" alt="{{ $item->title }}">
                                                            </a>
                                                        </div>
                                                        <div class="post-content">
                                                            <h3 class="post-title">
                                                                <a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ $item->title }}</a>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    @endif
                            </li>
                            @endforeach
                        </ul>
                        <!--nav menu end-->
                    </div>
                </nav>
                <!-- nav end-->
            </div>
        </div>
    </div>
</header>

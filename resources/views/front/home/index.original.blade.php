@extends('front.pages.layouts')

@section('content')

<!-- start blog wrapper -->
@include('front.home.slide.index')
<!-- end  blog wrapper -->

@include('front.splits.b-banner')

<!-- start  blog wrapper -->
@include('front.home.zone-1.index')
<!-- end  blog wrapper -->

<!-- footer social list start-->
{{-- @include('front.splits.b-social') --}}
<!-- footer social list end-->

@include('front.home.zone-2.slide.index')
@include('front.splits.b-banner-2')

<!-- blog post wrapper start-->
@include('front.home.zone-2.index')
<!-- blog post wrapper end-->

@include('front.splits.b-banner-3')

@endsection

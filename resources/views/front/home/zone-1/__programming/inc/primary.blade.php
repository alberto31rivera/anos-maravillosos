<div class="col-lg-6 category-post-item1">
	<div class="ts-grid-box ts-grid-content">
        @if(!$postsSectionSix->isEmpty())
        @foreach ( $postsSectionSix as $item )
        @if ($item->category->status_home_page == 1)
		<div class="item">
			<div class="ts-post-thumb">
				<a class="post-cat ts-{{ $item->category->color }}-bg" href="{{ route('f.pages.categories', $item->category->slug ) }}">{{ $item->category->title }}</a>
				<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
					<img class="img-fluid" src="{{ asset('img/'.$item->image) }}" alt="">
				</a>
			</div>
			<div class="post-content">
				<h3 class="post-title md">
					<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ $item->title }}</a>
				</h3>
				<ul class="post-meta-info">
                    <li>
                        <i class="fa fa-clock-o"></i>
                        {{ $item->created_at->toFormattedDateString() }}
                    </li>
                    <li class="active">
                        <i class="icon-fire"></i>
                        @if ($item->visits != null)
                        {{ $item->visits->counter }}
                      @else
                      0
                      @endif
                    </li>
                </ul>
			</div>
		</div>
        @endif
        @endforeach
		<!-- item end-->
        @else
        <div class="item">
			<div class="ts-post-thumb">
				<a class="post-cat ts-orange-bg" href="#">Título de la Categoría</a>
				<a href="#">
					<img class="img-fluid" src="{{ asset('img/image-posts/default.jpg') }}" alt="">
				</a>
			</div>
			<div class="post-content">
				<h3 class="post-title md">
					<a href="#">Título del Post</a>
				</h3>
				<ul class="post-meta-info">
                    <li>
                        <i class="fa fa-clock-o"></i>
                        Mar 30, 2021
                    </li>
                    <li class="active">
                        <i class="icon-fire"></i>
                        0
                    </li>
                </ul>
			</div>
		</div>
        @endif
	</div>
	<!-- ts gird box end-->
</div>

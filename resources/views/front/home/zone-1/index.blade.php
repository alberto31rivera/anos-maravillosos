<section class="block-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-9">
				@include('front.home.zone-1.most-viewed.index')

				<div class="border-top border-top1 mb-30"></div>

				{{-- @include('front.home.zone-1.programming.index') --}}
				{{-- @include('front.home.zone-1.news.index') --}}

				@include('front.home.zone-1.top-music.index')
				<!-- row end-->
			</div>
			<!-- col end-->
			<div class="col-lg-3">
				<div class="right-sidebar">
					@include('front.splits.w-categories', ['mergeClass'=>'mt-30-to-lg'])
					<!-- widgets end-->
					@include('front.splits.w-post-list')
					<!-- widgets end-->
				</div>
			</div>
		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>
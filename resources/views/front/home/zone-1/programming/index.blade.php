<div class="ts-grid-box clearfix ts-category-title">
	<h2 class="ts-title float-left">Programación</h2>
</div>
<div class="row mb-30">
	@include('front.home.zone-1.programming.inc.primary')
	@include('front.home.zone-1.programming.inc.secondary')
</div>
<!-- row end-->
{{-- <div class="row mb-30">
	@include('front.home.zone-1.programming.inc.tertiary')
</div> --}}
<!-- row end-->

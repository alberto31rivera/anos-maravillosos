<div class="ts-grid-box clearfix ts-category-title">
	<h2 class="ts-title float-left">Musica</h2>
</div>

<div class="row">
	<div class="col-lg-8 category-box-item">
		@include('front.home.zone-1.top-music.inc.primary')
		<!-- ts-grid-box end-->
	</div>
	<!-- col end-->
	<div class="col-lg-4">
		@include('front.home.zone-1.top-music.inc.secondary')
		<!-- ts grid box-->
	</div>
	<!-- col end-->

</div>

<div class="ts-grid-box ts-grid-content ts-list-post-box h-100 bx-s--card">

        @if(!$postsSectionFive->isEmpty())
            @foreach ( $postsSectionFive as $item )
                @if ($item->category->status_home_page == 1)

                    @if($loop->first)
                        <div class="item">
                    		<a class="post-cat ts-{{ $item->category->color }}-bg" href="{{ route('f.pages.categories', $item->category->slug ) }}">{{ $item->category->title }}</a>
                    		<div class="ts-post-thumb">
                    			<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
                    				<img class="img-fluid" src="{{ asset('img/'.$item->image) }}" alt="">
                    			</a>
                    		</div>
                    		<div class="post-content">
                    			<h3 class="post-title">
                    				<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ $item->title }}</a>
                    			</h3>
                    			<ul class="post-meta-info">
                                    <li>
                                        <span class="post-date-info">
                                            <i class="fa fa-clock-o"></i>
                                            {{ $item->created_at->toFormattedDateString() }}
                                        </span>
                                    </li>

                                    <li class="active">
                                        <i class="icon-fire"></i>
                                        @if ($item->visits != null)
                                            {{ $item->visits->counter }}
                                        @else
                                            0
                                        @endif
                                    </li>
                                </ul>
                    		</div>
                        </div>
                    @else
                    <div class="item bx-s-none">
                        <div class="post-content">
                            <h3 class="post-title">
                                <a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ $item->title }}</a>
                            </h3>
                            <ul class="post-meta-info">
                                <li>
                                    <span class="post-date-info">
                                        <i class="fa fa-clock-o"></i>
                                        {{ $item->created_at->toFormattedDateString() }}
                                    </span>
                                </li>

                                <li class="active">
                                    <i class="icon-fire"></i>
                                    @if ($item->visits != null)
                                        {{ $item->visits->counter }}
                                    @else
                                        0
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                    @endif

                @endif
            @endforeach
        @else
        <div class="item">
            <a class="post-cat ts-orange-bg" href="#">Título de la Categoría</a>
    		<div class="ts-post-thumb">
    			<a href="#">
    				<img class="img-fluid" src="{{ asset('img/image-posts/default.jpg') }}" alt="">
    			</a>
    		</div>
    		<div class="post-content">
    			<h3 class="post-title">
    				<a href="#">Título del Post</a>
    			</h3>
    			<ul class="post-meta-info">
                    <li>
                        <i class="fa fa-clock-o"></i>
                        Mar 30, 2021
                    </li>
                    <li class="active">
                        <i class="icon-fire"></i>
                        0
                    </li>
                </ul>
    		</div>
        </div>
        @endif

</div>

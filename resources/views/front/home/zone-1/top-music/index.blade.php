<div class="ts-grid-box clearfix ts-category-title" style="background:white">
    @if ($banner != null)
    @if ($banner->title_one != null)
    <h2 class="ts-title float-left">{{ $banner->title_one }}</h2>
    @else
    <h2 class="ts-title float-left">{{ trans('multi-leng.frontend_music_home') }}</h2>
    @endif
    @else
    <h2 class="ts-title float-left">{{ trans('multi-leng.frontend_music_home') }}</h2>
    @endif
</div>

<div class="row">
	<div class="col-lg-8 category-box-item">
		@include('front.home.zone-1.top-music.inc.primary')
		<!-- ts-grid-box end-->
	</div>
	<!-- col end-->
	<div class="col-lg-4">
		@include('front.home.zone-1.top-music.inc.secondary')
		<!-- ts grid box-->
	</div>
	<!-- col end-->

</div>

<div class="ts-grid-box clearfix ts-category-title" style="background:white">
	<h2 class="ts-title float-left">{{ trans('multi-leng.frontend_more_views_home') }}</h2>
</div>
<div class="ts-grid-box category-box-item-3">
	{{-- <a href="#" class="view-all-link float-right">Ver todo</a> --}}
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-lg-6">
			@include('front.home.zone-1.most-viewed.inc.primary')
			<!-- ts-grid-box end-->
		</div>
		<!-- col end-->
		<div class="col-lg-6">
			@include('front.home.zone-1.most-viewed.inc.secondary')
		</div>
	</div>
	<!-- row end-->

</div>

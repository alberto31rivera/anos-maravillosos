<div class="item">
    @if(!$postMoreViewsDay->isEmpty())
    @foreach ( $postMoreViewsDay as $item )
    @if ($loop->first)
    <div class="ts-post-thumb">
		<a class="post-cat ts-{{ $item->post->category->color }}-bg" href="{{ route('f.pages.categories', $item->post->category->slug ) }}">{{ $item->post->category->title }}</a>
		<a href="{{ route('f.pages.post', ['slug1' => $item->post->slug, 'slug2'=> $item->post->slug]) }}">
			<img class="img-fluid" src="{{ asset('img/'.$item->post->image) }}" alt="">
		</a>
	</div>
	<div class="post-content">
		<h3 class="post-title md">
			<a href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2'=> $item->post->slug]) }}">
				{{ Str::limit($item->post->title,60) }}
			</a>
		</h3>
		<ul class="post-meta-info">
			<li>
				<i class="fa fa-clock-o"></i>
                {{ $item->post->created_at->toFormattedDateString() }}
			</li>
			<li class="active">
				<i class="icon-fire"></i>
                @if ($item->post->visits != null)
                          {{ $item->post->visits->counter }}
                        @else
                        0
                        @endif
			</li>
		</ul>
		<p>
			{{ Str::limit($item->post->post_summary,140) }}
		</p>
	</div>
    @else
    @break
    @endif
    @endforeach
    @else
    <div class="ts-post-thumb">
		<a class="post-cat ts-orange-bg" href="#">Título de la Categoría</a>
		<a href="#">
			<img class="img-fluid" src="{{ asset('img/image-posts/default.jpg') }}" alt="">
		</a>
	</div>
	<div class="post-content">
		<h3 class="post-title md">
			<a href="#">Título del Post</a>
		</h3>
		<ul class="post-meta-info">
			<li>
				<i class="fa fa-clock-o"></i>
                Mar 30, 2021
			</li>
			<li class="active">
				<i class="icon-fire"></i>
                    5
			</li>
		</ul>
		<p>
			Resumen del Post
		</p>
	</div>
    @endif
   </div>

<div class="row">
    @if(!$postMoreViewsDay->isEmpty())
    @foreach ( $postMoreViewsDay as $item )
    @if (!$loop->first)
    <div class="col-lg-6">
		<div class="item mb-20">
			<div class="ts-post-thumb">
				<a href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2'=> $item->post->slug]) }}">
					<img class="img-fluid" src="{{ asset('img/'.$item->post->image) }}" alt="">
				</a>
			</div>
			<div class="post-content">
				<h3 class="post-title">
					<a href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2'=> $item->post->slug]) }}">{{ $item->post->title }}</a>
				</h3>
				<ul class="post-meta-info">
                    <li>
                        <i class="fa fa-clock-o"></i>
                        {{ $item->post->created_at->toFormattedDateString() }}
                    </li>
                    <li class="active">
                        <i class="icon-fire"></i>
                        @if ($item->post->visits != null)
                                  {{ $item->post->visits->counter }}
                                @else
                                0
                                @endif
                    </li>
                </ul>

			</div>
		</div>
	</div>
    @endif
    @endforeach
	<!-- col end-->
    @else
    <div class="col-lg-6">
		<div class="item mb-20">
			<div class="ts-post-thumb">
				<a href="#">
			<img class="img-fluid" src="{{ asset('img/image-posts/default.jpg') }}" alt="">
            	</a>
			</div>
			<div class="post-content">
				<h3 class="post-title">
					<a href="#">Título del Post</a>
				</h3>
				<ul class="post-meta-info">
                    <li>
                        <i class="fa fa-clock-o"></i>
                        Mar 30, 2021
                    </li>
                    <li class="active">
                        <i class="icon-fire"></i>
                            5
                    </li>
                </ul>
			</div>
		</div>
	</div>
    @endif
</div>

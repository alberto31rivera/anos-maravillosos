@extends('front.pages.layouts')

@section('styles')
<!-- Swiper CSS -->
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">

<link rel="stylesheet" href="{{asset('assets/front/custom/advert.css')}}">

@php
$widthValue = '170px';
if (env('APP_LOCALE')=='en') {
    $widthValue = '140px';
}
@endphp

<style>
.post-cat--min-w {
	min-width: {{$widthValue}};
}
.card-img-top {
    width: 100%;
    height: calc(100vw * 0.5);
    object-fit: cover;
}
.s-advert {
	/*width: calc(90vw - 30px);*/
	width: inherit;
	/*border-bottom: 1px solid #d4d4d4;*/
}
@media(min-width:575px) and (max-width:767px){ /*sm*/
	.s-advert .card-img-top {
		height: calc( (100vw/2) * 0.5625 );
	}
}
@media(min-width:768px) and (max-width:991px){ /*md*/
	.s-advert .card-img-top {
		height: calc( (100vw/3) * 0.5625 );
	}
}
@media(min-width:992px) and (max-width:1199px){ /*lg*/
	.s-advert .card-img-top {
		height: calc( (100vw/4) * 0.5625 );
	}
}
@media(min-width: 1200px){ /*xl*/
	.s-advert .card-img-top {
		height: calc( (100vw/4) * 0.5625 );
	}
}
/*APIs-Facebook*/
.fb-comments.fb_iframe_widget.fb_iframe_widget_fluid_desktop::-webkit-scrollbar {
    width: 7px;
    background: #F0F0F0;
}
.fb-comments.fb_iframe_widget.fb_iframe_widget_fluid_desktop::-webkit-scrollbar-thumb {
    background: #1877F2;
    /*border-radius: 5px;*/
    border-right: 2px solid #fff;
}
/* sponsors slider */
#swiper-sponsors .swiper-wrapper {
    align-items: center;
}
#swiper-sponsors .swiper-slide img {
    max-height: 85px;
}
</style>
@endsection

@section('content')
{{-- Programas del día:<br><br>

@foreach ($programs as $item)
    <h1>Titlo del programa: {{ $item->title }}</h1>
    <p>Descripción: {{ $item->description }}</p>
@endforeach --}}


<!--JDev-->
<section class="block-wrapper">

<div class="container py-3">
    <div class="row">
    	<div class="col-12 px-3 mb-30">
        	@include('front.home.programs.inc.player')
        </div>

		<div class="col-lg-12 mb-30">
	        @include('front.home.programs.inc.online')
		</div>

		<div class="py-3 mb-4 mx-3  ts-grid-box ts-col-box bg-transparent w-100 mb-30" style="overflow:hidden">
			@include('front.home.programs.inc.sponsors')
		</div>

		<div class="col-lg-12 mb-30">
	        <div class="mx-auto s-advert pb-3 mb-5  ts-grid-box bg-transparent px-0" style="overflow:hidden;box-shadow:none;padding-top:40px;margin-top:30px">
				@include('front.home.programs.inc.slider')
			</div>
		</div>

        <div class="col-lg-9"> <!--col-12 col-md-7 col-lg-8-->
			@include('front.home.programs.inc.facebook-comments')
        </div>

        {{-- <div class="col-12 col-md-5 col-lg-4 text-center">
        	<div class="container-fluid">
        		<div class="row">
        			<div class="col-12 px-0 overflow-auto mb-30">
        				@include('front.home.programs.inc.facebook-banners')
        			</div>
        			<div class="col-12 px-0 mb-30">
        				@include('front.splits.w-contact-us')
        			</div>
        		</div>
        	</div>
        </div> --}}

        <div class="col-lg-3">
			<div class="right-sidebar-1">
				@include('front.splits.w-categories', ['mergeClass'=>'mt-30-to-lg'])
				<!-- widgets end-->
				@include('front.splits.w-post-list')
				<!-- widgets end-->
				@include('front.splits.w-contact-us')
				<!-- widgets end-->
			</div>
		</div>

    </div>
</div>


</section>
<!--JDev-->

@endsection

@section('scripts')

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v11.0&appId=1986500414838971&autoLogAppEvents=1" nonce="vUTwXSLe"></script>

<!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- Initialize Swiper -->
  <script>
  	var swiper = new Swiper('#swiper-sponsors', {
      watchSlidesProgress: true,
        watchSlidesVisibility: true,
        slidesPerView: 1,
        breakpoints: {
            576: {
                slidesPerView: 2
            },
            768: {
                slidesPerView: 3
            },
            992: {
                slidesPerView: 4
            },
        },
        spaceBetween: 30,
        grabCursor: true,
        speed: 20000,
        loop: true,
        keyboard: {
            enabled: true,
            onlyInViewport: true,
        },
        autoplay: {
            delay: 0,
        },
        freeModeMomentumBounce: false,
    });

    $("#swiper-sponsors").hover(function() {
        (this).swiper.autoplay.stop();
    }, function() {
        (this).swiper.autoplay.start();
    });

    var swiper = new Swiper('#swiper-ad-container', {
	  watchSlidesProgress: true,
	    watchSlidesVisibility: true,
	    slidesPerView: 1,
	    breakpoints: {
	    	576: {
	            slidesPerView: 2
	        },
	        768: {
	            slidesPerView: 3
	        },
	        992: {
	            slidesPerView: 4
	        },
	    },
	    spaceBetween: 15,
	    // grabCursor: true,
	    speed: 8000,
	    loop: true,
	    keyboard: {
	        enabled: true,
	        onlyInViewport: true,
	    },
	    autoplay: {
	        delay: 0,
	    },
		freeModeMomentumBounce: false,
	});

	$("#swiper-ad-container").hover(function() {
		(this).swiper.autoplay.stop();
	}, function() {
		(this).swiper.autoplay.start();
	});
  </script>
@endsection

@if ($sponsors != null)
@if (count($sponsors) >= 2)
<div class="item">
    <span class="post-cat post-cat--min-w" href="#" style="background-color:#C20610">{{ trans('multi-leng.frontend_sponsors') }}</span>
</div>
<!-- Swiper -->
<div id="swiper-sponsors">
<div class="swiper-wrapper">
    @foreach ($sponsors as $item)
    <div class="swiper-slide">
        <a href="{{ $item->url }}" target="_blank">
            <img class="img-fluid px-5" src="{{asset('img/sponsors/'.$item->image) }}" alt="">
        </a>
    </div>
    @endforeach
</div>
</div>
@endif
@endif



<div class="ts-grid-box ts-col-box">
	<div class="item">
		<span class="post-cat post-cat--min-w" href="#" style="background-color:#C20610">{{ trans('multi-leng.frontend_current_program') }}</span><!--.ts-blue-light-bg-->
        @if ($programs != null)
        <div class="post-content">

			<div class="d-md-flex justify-content-md-between">
				<h3 class="post-title d-inline-block" style="font-size:1.2rem">
					<!-- Button trigger modal: Greeting -->
					<a href="#" data-toggle="modal" data-target="#program-modal">{{ $programs->title }}</a>
				</h3>
				<div>
					<span class="post-date-info font-weight-bold text-success d-inline-block" style="font-size:.85rem">
						<i class="fa fa-clock-o"></i> {{ trans('multi-leng.frontend_program_start') }}
					</span>
					<span data-toggle="tooltip" data-placement="top" title="Hora de inicio del programa">{{ $programs->start_time }}</span>
					<span class="text-muted font-weight-bold">|</span>
					<span class="post-date-info font-weight-bold text-danger d-inline-block" style="font-size:.85rem">
						<i class="fa fa-clock-o"></i> {{ trans('multi-leng.frontend_program_end') }}
					</span>
					<span  data-toggle="tooltip" data-placement="top" title="Hora del final del programa">{{ $programs->end_time }}</span>
				</div>
			</div>
			<p class="my-2">{{ $programs->description }}</p>
		</div>
        @endif
	</div>
	<!-- item end-->
</div>
<!-- ts gird box end-->

<!-- Modal -->
@if ($programs != null)
<div class="modal fade" id="program-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="width:auto;max-width:100vw">
      <div class="modal-content bg-transparent rounded-0 border-0">

        <div class="modal-body text-center p-2">
			<button type="button" class="close position-absolute bg-white p-2" data-dismiss="modal" aria-label="Close" style="cursor:pointer;z-index:10;top:0;right:0;margin-right:6px">
				<span aria-hidden="true" style="width:54px;height:54px">×</span>
			</button>
            <a href="{{ $programs->url }}" target="_blank">
            <img class="img-fluid border-photo" src="{{asset('img/'.$programs->image)}}" alt="">
            </a>
        </div>

      </div>
    </div>
  </div>
@endif


<!-- blog post wrapper start -->
<section class="block-wrapper pb-0 mb-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if ($banner != null)
                <div class="uk-position-relative uk-visible-toggle uk-light video-slider" uk-slider @if ($flag == 0) style="display:none"  @endif>
                    <ul class="uk-slider-items">
                        @if ($banner->video_one != null && $banner->image_one != null && $banner->description_one != null)
                        <li class="uk-width-3-4">
                            <div class="uk-panel">
                                <div class="post-video">
                                    <img src="{{asset('img/homepage/advertisements/videos/'.$banner->image_one)}}" alt="">
                                    <div class="post-video-content">

                                        <h3>
                                            <a href=""> {{ $banner->description_one }}</a>
                                        </h3>
                                        <span class="post-date-info">
                                            <i class="fa fa-clock-o"></i>
                                            March 21, 2019
                                        </span>
                                    </div>
                                    <a href="{{ $banner->video_one }}" class="ts-play-btn">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        @endif
                        @if ($banner->video_two != null && $banner->image_two != null && $banner->description_two != null)
                        <li class="uk-width-3-4">
                            <div class="uk-panel">
                                <div class="post-video">
                                    <img src="{{asset('img/homepage/advertisements/videos/'.$banner->image_two)}}" alt="">
                                    <div class="post-video-content">

                                        <h3>
                                            <a href=""> {{ $banner->description_two }}</a>
                                        </h3>
                                        <span class="post-date-info">
                                            <i class="fa fa-clock-o"></i>
                                            March 21, 2019
                                        </span>
                                    </div>
                                    <a href="{{ $banner->video_two }}" class="ts-play-btn">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        @endif
                        @if ($banner->video_three != null && $banner->image_three != null && $banner->description_three != null)
                        <li class="uk-width-3-4">
                            <div class="uk-panel">
                                <div class="post-video">
                                    <img src="{{asset('img/homepage/advertisements/videos/'.$banner->image_three)}}" alt="">
                                    <div class="post-video-content">

                                        <h3>
                                            <a href=""> {{ $banner->description_three }}</a>
                                        </h3>
                                        <span class="post-date-info">
                                            <i class="fa fa-clock-o"></i>
                                            March 21, 2019
                                        </span>
                                    </div>
                                    <a href="{{ $banner->video_three }}" class="ts-play-btn">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        @endif
                        @if ($banner->video_four != null && $banner->image_four != null && $banner->description_four != null)
                        <li class="uk-width-3-4">
                            <div class="uk-panel">
                                <div class="post-video">
                                    <img src="{{asset('img/homepage/advertisements/videos/'.$banner->image_four)}}" alt="">
                                    <div class="post-video-content">

                                        <h3>
                                            <a href=""> {{ $banner->description_four }}</a>
                                        </h3>
                                        <span class="post-date-info">
                                            <i class="fa fa-clock-o"></i>
                                            March 21, 2019
                                        </span>
                                    </div>
                                    <a href="{{ $banner->video_four }}" class="ts-play-btn">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        @endif
                        @if ($banner->video_five != null && $banner->image_five != null && $banner->description_five != null)
                        <li class="uk-width-3-4">
                            <div class="uk-panel">
                                <div class="post-video">
                                    <img src="{{asset('img/homepage/advertisements/videos/'.$banner->image_five)}}" alt="">
                                    <div class="post-video-content">

                                        <h3>
                                            <a href=""> {{ $banner->description_five }}</a>
                                        </h3>
                                        <span class="post-date-info">
                                            <i class="fa fa-clock-o"></i>
                                            March 21, 2019
                                        </span>
                                    </div>
                                    <a href="{{ $banner->video_five }}" class="ts-play-btn">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        @endif
                        @if ($banner->video_six != null && $banner->image_six != null && $banner->description_six != null)
                        <li class="uk-width-3-4">
                            <div class="uk-panel">
                                <div class="post-video">
                                    <img src="{{asset('img/homepage/advertisements/videos/'.$banner->image_six)}}" alt="">
                                    <div class="post-video-content">

                                        <h3>
                                            <a href=""> {{ $banner->description_six }}</a>
                                        </h3>
                                        <span class="post-date-info">
                                            <i class="fa fa-clock-o"></i>
                                            March 21, 2019
                                        </span>
                                    </div>
                                    <a href="{{ $banner->video_six }}" class="ts-play-btn">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                        @endif
                    </ul>

                    <a class="uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                    <a class="uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>

                </div>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- blog post wrapper end -->


<div class="ts-grid-box clearfix ts-category-title mt-30-to-lg" style="background:white">
    @if ($banner != null)
    @if ($banner->title_three != null)
    <h2 class="ts-title float-left">{{ $banner->title_three }}</h2>
    @else
    <h2 class="ts-title float-left">{{ trans('multi-leng.frontend_videos_home') }}</h2>
    @endif
    @else
    <h2 class="ts-title float-left">{{ trans('multi-leng.frontend_videos_home') }}</h2>
    @endif
</div>

<div class="ts-grid-box category-item">
	{{-- <h2 class="ts-title">Videos</h2> --}}
	<div class="row">
        @if(!$postsSectionNine->isEmpty())
        @foreach ( $postsSectionNine as $item )
        @if ($item->category->status_home_page == 1)
		<div class="col-lg-3">
			<div class="item">
				<div class="ts-post-thumb">
					<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
						<img class="img-fluid" src="{{ asset('img/'.$item->image) }}" alt="">
					</a>
				</div>
				<div class="post-content">
					<h3 class="post-title">
						<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ $item->title }}</a>
					</h3>
					<ul class="post-meta-info">
                        <li>
                            <span class="post-date-info">
                                <i class="fa fa-clock-o"></i>
                                {{ $item->created_at->toFormattedDateString() }}
                            </span>
                        </li>

                        <li class="active">
                            <i class="icon-fire"></i>
                            @if ($item->visits != null)
                                {{ $item->visits->counter }}
                            @else
                                0
                            @endif
                        </li>
                    </ul>
				</div>
			</div>
		</div>
        @endif
        @endforeach
		<!-- col end-->
        @else
        <div class="col-lg-3">
			<div class="item">
				<div class="ts-post-thumb">
					<a href="#">
						<img class="img-fluid" src="{{ asset('img/image-posts/default.jpg') }}" alt="">
					</a>
				</div>
				<div class="post-content">
					<h3 class="post-title">
						<a href="#">Título del Post</a>
					</h3>
					<ul class="post-meta-info">
                        <li>
                            <i class="fa fa-clock-o"></i>
                            Mar 30, 2021
                        </li>
                        <li class="active">
                            <i class="icon-fire"></i>
                            0
                        </li>
                    </ul>
				</div>
			</div>
		</div>
        @endif
	</div>
	<!-- row end-->
</div>

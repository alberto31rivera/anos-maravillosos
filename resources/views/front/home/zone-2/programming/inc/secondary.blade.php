{{-- <div class="col-lg-6">
	<div class="ts-grid-box ts-col-box">
        @if(!$postsSectionSeven->isEmpty())
        @foreach ( $postsSectionSeven as $item )
        @if ($item->category->status_home_page == 1)
		<div class="item">
			<a class="post-cat ts-{{ $item->category->color }}-light-bg" href="{{ route('f.pages.categories', $item->category->slug ) }}">{{ $item->category->title }}</a>
			<div class="post-content">

				<h3 class="post-title md">
					<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ $item->title }} </a>
				</h3>
                <ul class="post-meta-info">
                    <li>
                        <i class="fa fa-clock-o"></i>
                        {{ $item->created_at->toFormattedDateString() }}
                    </li>
                    <li class="active">
                        <i class="icon-fire"></i>
                        @if ($item->visits != null)
                        {{ $item->visits->counter }}
                      @else
                      0
                      @endif
                    </li>
                </ul>
				<p>
					{{ $item->post_summary }}
				</p>
			</div>
		</div>
        @endif
        @endforeach
		<!-- item end-->
        @else
        <div class="item">
			<a class="post-cat ts-blue-light-bg" href="#">Título de la Categoría</a>
			<div class="post-content">

				<h3 class="post-title md">
					<a href="#">Título del Post</a>
				</h3>
				<ul class="post-meta-info">
                    <li>
                        <i class="fa fa-clock-o"></i>
                        Mar 30, 2021
                    </li>
                    <li class="active">
                        <i class="icon-fire"></i>
                        0
                    </li>
                </ul>
				<p>
					Resumen de la Publicación
				</p>
			</div>
		</div>
        @endif
	</div>
	<!-- ts gird box end-->
</div> --}}


@if(!$postsSectionSeven->isEmpty())
    @foreach ( $postsSectionSeven as $item )
        @if ($item->category->status_home_page == 1)
            <div class="col-lg-3">
                <div class="ts-grid-box ts-col-box h-100">
                    <div class="item">
                        <a class="post-cat ts-{{ $item->category->color }}-light-bg" href="{{ route('f.pages.categories', $item->category->slug ) }}">{{ $item->category->title }}</a>
                        <div class="post-content">

                            <h3 class="post-title md">
                                <a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ Str::limit($item->title,60) }} </a>
                            </h3>
                            <ul class="post-meta-info">
                                <li>
                                    <i class="fa fa-clock-o"></i>
                                    {{ $item->created_at->toFormattedDateString() }}
                                </li>
                                <li class="active">
                                    <i class="icon-fire"></i>
                                    @if ($item->visits != null)
                                    {{ $item->visits->counter }}
                                  @else
                                  0
                                  @endif
                                </li>
                            </ul>
                            <p class="mb-0">
                                {{ Str::limit($item->post_summary,70) }}
                            </p>
                        </div>
                    </div>
                    <!-- item end-->
                </div>
                <!-- ts gird box end-->
            </div>
            <!-- col end-->
        @endif
    @endforeach
@else
    <div class="col-lg-3">
        <div class="ts-grid-box ts-col-box h-100">
            <div class="item">
                <a class="post-cat ts-blue-light-bg" href="#"> Título de la Categoría</a>
                <div class="post-content">

                    <h3 class="post-title md">
                        <a href="#"> Título de la Publicación</a>
                    </h3>
                    <span class="post-date-info">
                        <i class="fa fa-clock-o"></i>
                        Dic 31, 2020
                    </span>
                    <p>
                        Resumen de la Publicación
                    </p>
                </div>
            </div>
            <!-- item end-->
        </div>
        <!-- ts gird box end-->
    </div>
    <!-- col end-->
@endif



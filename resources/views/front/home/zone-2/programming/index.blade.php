<div class="ts-grid-box clearfix ts-category-title" style="background:white">
    @if ($banner != null)
    @if ($banner->title_two != null)
    <h2 class="ts-title float-left">{{ $banner->title_two }}</h2>
    @else
    <h2 class="ts-title float-left">{{ trans('multi-leng.frontend_programming_home') }}</h2>
    @endif
    @else
    <h2 class="ts-title float-left">{{ trans('multi-leng.frontend_programming_home') }}</h2>
    @endif
</div>
<div class="row mb-30">
	@include('front.home.zone-2.programming.inc.primary')
	@include('front.home.zone-2.programming.inc.secondary')
</div>
<!-- row end-->
<div class="row mb-30">
	@include('front.home.zone-2.programming.inc.tertiary')
</div>
<!-- row end-->

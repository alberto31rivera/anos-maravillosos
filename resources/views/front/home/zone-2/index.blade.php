<section class="block-wrapper mb-20  pt-0">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 ts-content-box-item">
				@include('front.home.zone-2.programming.index')
				@include('front.home.zone-2.videos.index')
				{{-- @include('front.home.zone-2.top-music.index') --}}
			</div>
			<!-- col end-->
			<div class="col-lg-3">
				@include('front.splits.w-categories')
				<!-- widgets end-->
				@include('front.splits.w-post-list')
				<!-- widgets end-->
			</div>
			<!-- col end-->
		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>

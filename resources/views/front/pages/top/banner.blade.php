@if( $layout === 'silver' )
<section class="block-wrapper d-none d-lg-block">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="banner-img text-center">
                    @if ($banner != null)
                    @if ($banner->advertising_one !=null && $banner->url_advertising_one != null)
                    <a target="_blank" href="{{ $banner->url_advertising_one }}">
						<img class="img-fluid img-banner-ad" src="{{asset('img/homepage/advertisements/'.$banner->advertising_one)}}" alt="">
					</a>
                    @endif
                    @endif
				</div>
			</div>
			<!-- col end -->
		</div>
		<!-- row  end -->
	</div>
	<!-- container end -->
</section>

@else {{--$layout === 'white'--}}

<section class="header-middle d-none d-lg-block">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="banner-img">
					<a href="{{route('f.home.index')}}">
						<img class="img-fluid img-banner-ad" src="{{asset('assets/front/template/images/banner/banner3.jpg')}}" alt="">
					</a>
				</div>
			</div>
			<!-- col end -->
		</div>
		<!-- row  end -->
	</div>
	<!-- container end -->
</section>

@endif

<!-- Modal -->
<div class="modal fade" id="greeting-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content rounded-0 border-0">
      <div class="modal-header border-bottom-0 pb-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="cursor:pointer">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body">
        <img class="img-fluid" src="{{asset('assets/front/template/images/news/travel/travel1.jpg')}}" alt="">
      </div>

    </div>
  </div>
</div>

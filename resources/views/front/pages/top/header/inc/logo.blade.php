<div class="logo">
	<a href="{{route('f.home.index')}}">
        @if ($admin->logo == '')
        <img class="img-fluid" src="{{asset('assets/front/template/images/logo/logo-v2.png')}}" alt="logo">
        @else
        <img class="img-fluid" src="{{asset('img/logos-users/'.$admin->logo)}}" alt="logo">
        @endif
	</a>
</div>

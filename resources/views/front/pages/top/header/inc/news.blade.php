 
<div class="upper-head clearfix">
            <div class="container">
                <div class="header-date">
                    
                    <p style="color:black;font-weight:400;font-size: 10px;"><i style="color:white;" class="icon-cloud"></i> Publicaciones:
					 
					@foreach ($lastNews as $item)						 
							<a style="color:white;" href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ $item->title }}</a>
				    @endforeach	
							
					</p>

</div>

				

                <ul class="header-social-links pull-right">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
            </div>
</div>
<nav class="navigation ts-main-menu navigation-landscape">

     
	<!--nav brand end-->

	<div class="nav-menus-wrapper clearfix" style="background:@if($themeSettings != null) @if ($themeSettings->color_header != null) {{ $themeSettings->color_header }}
        @else #ef1b1b @endif  @endif">
		<!--nav right menu start-->
		<ul class="right-menu align-to-right list-icon--action">
            @if (!Auth::user())
                <li @if (Request::url() == route('login')) class="active" @endif>
                    <a href="{{route('login')}}">
                        <i class="fa fa-user-circle-o"></i>
                    </a></li>
                @else
                <li @if (Request::url() == route('f.panel.user')) class="active" @endif>
                    <a href="{{route('f.panel.user')}}">
                        <i class="fa fa-user-circle-o"></i>
                    </a></li>
                @endif
			@if (Request::url() != route('f.pages.searches'))
            <li>
				<a role="button" href="{{ route('f.pages.searches') }}">
					<i class="icon icon-search"></i>
				</a>
			</li>
            @endif
		</ul>
		<!--nav right menu end-->

		<!--nav pre right menu start-->
            <!--only screens xs|sm|md -->
        <ul class="right-menu d-lg-none list-icon--social">
            @if ($admin->facebook != '')
                <li>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                </li>
            @endif
            @if ($admin->instagram != '')
                <li>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                </li>
            @endif
            @if ($admin->twitter != '')
                <li>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                </li>
            @endif
            @if ($admin->linkedin != '')
                <li>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                </li>
            @endif
            @if ($admin->youtube != '')
                <li>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                </li>
            @endif
        </ul>
            <!--only screens lg|xl -->
		 
		<!--nav pre right menu end-->

		<!-- nav menu start-->
		
		 
	</div>
</nav>

    <div class="navigation" style="border-bottom: 1px solid #a4a0a0;">  
        <div class="container">
            <div class="navigation-content">
                <div class="header_menu">
                    
                    <nav class="navbar navbar-default navbar-sticky-function navbar-arrow">
                        <div class="logo pull-left">
                            <a href="{{ route('f.home.index') }}">
                                <img alt="Image" src="{{ route('f.home.index') }}/images/logo1.png">
                            </a>
                        </div>
                        <div id="navbar" class="navbar-nav-wrapper pull-right">

                            <ul class="nav navbar-nav" id="responsive-menu">

                            <li @if (Request::url() == route('f.home.index')) class="active has-child" @endif>
                            <a href="{{ route('f.home.index') }}"> 
                                
                               <button> {{ trans('multi-leng.frontend_title_home') }}</button>
                            </a>
                                    
                            </li>

                                @php
                                $categoriesMenu = App\Models\Category::orderBy('display_order', 'ASC')->where('status_header',1)->whereNotNull('display_order')->get()->take(5);
                                @endphp
                                @foreach ($categoriesMenu as $item)
                                <li @if ($item->slug == Request::path()) class="has-child" @endif>
                                        <a href="#">
                                            <button class="text-uppercase" 
                                                onclick="window.location.href='{{ route('f.pages.categories', $item->slug ) }}'">
                                                {{ $item->title }}
                                            </button>
                                        </a>
                                            
                                </li>
                                @endforeach       

                              <!--   <li @if (Request::url() == route('pages.contanct-us')) class="has-child" @endif>
                                      <a href="{{ route('pages.contanct-us') }}"><button class="text-uppercase">Contacto</button> </a>
                                    </li> -->

                                    <li class="has-child">
                                      <a target="_blank" href="https://wa.link/rbxqvz"><button class="text-uppercase">Contacto</button> </a>
                                    </li>

                                 
                            </ul>
                            <a href="{{ route ('f.pages.searches') }}" id="searchtoggl" class="searchtoggle">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </a>
                        </div> 

                        <div id="slicknav-mobile"></div>

                    </nav>
                </div>
                
            </div>
        </div>
    </div>




<section class="top-bar v2 d-none d-lg-block" style="background:@if($themeSettings != null) @if ($themeSettings->color_regards != null) {{ $themeSettings->color_regards }}
    @else #c2211c @endif  @endif">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 align-self-center">
                {{-- <div class="ts-breaking-news clearfix"> --}}
                    {{-- <h2 class="breaking-title float-left">
						<i class="fa fa-bolt"></i> Última noticia:</h2> --}}
                    {{-- <div class="breaking-news-content float-left" id="breaking_slider1"> --}}
                        {{-- @foreach ($lastNews as $item) --}}
                        {{-- <div class="breaking-post-content"> --}}
                            <p class="wspace-nrap mb-0" style="font-size:13px;overflow:hidden">
                                {{-- <a href="#">{{ $item->post_summary  }}</a> --}}
                                <!-- Button trigger modal: Greeting -->

                                <a class="font-weight-bold" style="color:@if($themeSettings != null) @if ($themeSettings->color_text_regards != null) {{ $themeSettings->color_text_regards }}
                                    @else white @endif @else white  @endif" href="#" data-toggle="modal"
                                    data-target="#regards-modal">@if ($msg == 'Buenos días') {{ trans('multi-leng.frontend_good_morning') }}
                                    @elseif ($msg == 'Buenas tardes') {{ trans('multi-leng.frontend_good_afternoon') }} @else  {{ trans('multi-leng.frontend_good_night') }}  @endif @if (Auth::user())
                                        {{ Auth::user()->name }}@endif, {{ trans('multi-leng.frontend_today') }}
                                        @if ($day->dayName == 'lunes'){{ trans('multi-leng.frontend_monday') }}
                                        @elseif ($day->dayName == 'martes'){{ trans('multi-leng.frontend_tuesday') }}
                                        @elseif ($day->dayName == 'miércoles'){{ trans('multi-leng.frontend_wednesday') }}
                                        @elseif ($day->dayName == 'jueves'){{ trans('multi-leng.frontend_thursday') }}
                                        @elseif ($day->dayName == 'viernes'){{ trans('multi-leng.frontend_friday') }}
                                        @elseif ($day->dayName == 'sábado'){{ trans('multi-leng.frontend_saturday') }}
                                        @else
                                        {{ trans('multi-leng.frontend_sunday') }}
                                        @endif
                                        @if ($regards != null) {{ $regards->title }}
                                        @endif</a>
                            </p>
                        {{-- </div> --}}
                        {{-- @endforeach --}}
                    {{-- </div> --}}
                {{-- </div> --}}
            </div>
            <!-- end col-->

            <!-- Modal -->
            @if ($regards != null)
                <div class="modal fade" id="regards-modal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="width:auto;max-width:100vw">
                        <div class="modal-content bg-transparent rounded-0 border-0">

                            <div class="modal-body text-center p-2">
                                <button type="button" class="close position-absolute bg-white p-2" data-dismiss="modal" aria-label="Close" style="cursor:pointer;z-index:10;top:0;right:0;margin-right:6px">
                                    <span aria-hidden="true" style="width:54px;height:54px">×</span>
                                </button>

                                <a href="{{ $regards->url }}" target="_blank">
                                    <img class="img-fluid border-photo" src="{{ asset('img/' . $regards->image) }}" alt="" style="max-width:90vw;max-height:90vh">
                                </a>

                            </div>

                        </div>
                    </div>
                </div>
            @endif

            <div class="col-lg-5 text-right align-self-center">
                <ul class="top-social">
                    <li>
                        @if ($admin->facebook != '')
                    <li>
                        <a href="{{ $admin->facebook }}" target="_blank"><i class="fa fa-facebook fa"
                                aria-hidden="true"></i></a>
                    </li>
                    @endif
                    @if ($admin->instagram != '')
                        <li>
                            <a href="{{ $admin->instagram }}" target="_blank"><i class="fa fa-instagram fa"
                                    aria-hidden="true"></i></a>
                        </li>
                    @endif
                    @if ($admin->twitter != '')
                        <li>
                            <a href="{{ $admin->twitter }}" target="_blank"><i class="fa fa-twitter fa"
                                    aria-hidden="true"></i></a>
                        </li>
                    @endif
                    @if ($admin->linkedin != '')
                        <li>
                            <a href="{{ $admin->linkedin }}" target="_blank"><i class="fa fa-linkedin fa"
                                    aria-hidden="true"></i></a>
                        </li>
                    @endif
                    @if ($admin->youtube != '')
                        <li>
                            <a href="{{ $admin->youtube }}" target="_blank"><i class="fa fa-youtube fa"
                                    aria-hidden="true"></i></a>
                        </li>
                    @endif
                    </li>
                    <li class="ts-subscribe" style="background:#000">
                        @if ($banner != null)
                        @if ($banner->title_seven != null)
                        <a href="{{ route('pages.programs') }}">{{ $banner->title_seven }}</a>
                        @else
                        <a href="{{ route('pages.programs') }}">{{ trans('multi-leng.frontend_radio_online') }}</a>
                        @endif
                        @else
                        <a href="{{ route('pages.programs') }}">{{ trans('multi-leng.frontend_radio_online') }}</a>
                        @endif
                    </li>
                </ul>
            </div>
            <!--end col -->


        </div>
        <!-- end row -->
    </div>
</section>

@include('front.pages.top.inc.modal-greeting')


{{-- <section class="top-bar v3">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 align-self-center">
				<div class="ts-date">
					<i class="fa fa-clock-o"></i>
					Sunday, August 24
				</div>
				<div class="ts-temperature">
					<i class="icon-weather"></i>
					<span>25.8
						<b>c</b>
					</span>
					<span>Dubai</span>
				</div>
			</div>
			<!-- end col-->

			<div class="col-lg-6 text-right align-self-center">
				<ul class="top-social">
					<li>
						<a href="#">
							<i class="fa fa-twitter"></i>
						</a>
						<a href="#">
							<i class="fa fa-facebook"></i>
						</a>
						<a href="#">
							<i class="fa fa-google-plus"></i>
						</a>
						<a href="#">
							<i class="fa fa-pinterest"></i>
						</a>
						<a href="#">
							<i class="fa fa-vimeo-square"></i>
						</a>
					</li>
					<li class="ts-subscribe">
						<a href="#">subscribe</a>
					</li>
				</ul>
			</div>
			<!--end col -->

		</div>
		<!-- end row -->
	</div>
</section> --}}

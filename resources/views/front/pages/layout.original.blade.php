<!doctype html>
<html lang="en">

<head>
	<!-- Basic Page Needs =====================================-->
	<meta charset="utf-8">

	<!-- Mobile Specific Metas ================================-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>

	<!-- Site Title- -->
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')" />
    <meta name="robots" content="index, follow"/>
    <meta name="author" content="{{ env('APP_NAME') }}" />

    @yield('meta')

	<!-- CSS
   ==================================================== -->
   <!-- uikit -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/uikit.min.css')}}">

	<!-- Bootstrap -->
	{{-- <link rel="stylesheet" href="{{asset('assets/front/template/css/bootstrap.min.css')}}"> --}}
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/font-awesome.min.css')}}">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/animate.css')}}">

	<!-- IcoFonts -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/icofonts.css')}}">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/owlcarousel.min.css')}}">
		<link rel="stylesheet" href="{{asset('assets/front/template/css/slick.css')}}">


	<!-- navigation -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/navigation.css')}}">

	<!-- magnific popup -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/magnific-popup.css')}}">




	<!-- Style -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/style.css')}}">
	<!-- color -->
	{{-- <link rel="stylesheet" href="{{asset('assets/front/template/css/colors/color-1.css')}}"> --}}

	<!-- Responsive -->
	<link rel="stylesheet" href="{{asset('assets/front/template/css/responsive.css')}}">


	 

	<link rel="stylesheet" href="{{asset('assets/front/custom/generic.css')}}">


	<!-- / *** / -->
	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>

	<!-- Include Unite Gallery core files -->
	<script src='{{asset('assets/front/custom/unitegallery/dist/js/unitegallery.min.js')}}' type='text/javascript'  ></script>
	<link  href='{{asset('assets/front/custom/unitegallery/dist/css/unite-gallery.css')}}' rel='stylesheet' type='text/css' />

	<!-- include Unite Gallery Theme Files -->
	{{-- <script src='{{asset('assets/front/custom/unitegallery/dist/themes/tiles/ug-theme-tiles.js')}}' type='text/javascript'></script> --}}

	<script src='{{asset('assets/front/custom/unitegallery/dist/themes/compact/ug-theme-compact.js')}}' type='text/javascript'></script>
	<!-- / *** / -->

    @php
       $google = App\Models\ManageGoogle::first();
    @endphp

    @if ($google->recaptcha_key != null || $google->recaptcha_secret_key)
    {!! htmlScriptTagJsApi() !!}
    @endif

    @if ($google->google_analytics != null)
    {{ $google->google_analytics }}
    @endif

    @yield('styles')

    @if (env('APP_LOCALE')=='en')
    <style>.ts-breaking-news .breaking-news-content{width:77.5%}</style>
    @endif

    @php
    // JDev ------------------------------------------------------
    $OUT_HEIGHT = [
    	'movil' => 134,
    	'no-movil' => 260,
    ];

    if ($banner != null && $banner->advertising_one !=null && $banner->url_advertising_one != null) {
    	$OUT_HEIGHT['no-movil']+=120;
    }
    if ($services != null) {
    	$OUT_HEIGHT['movil']+=38;$OUT_HEIGHT['no-movil']+=38;
    }
    // -----------------------------------------------------------
    if ($themeSettings != null) {
        if ($themeSettings->color_text_header != null) {
            $color_text_header = $themeSettings->color_text_header;
        }else{
            $color_text_header = 'white';
        }
    }else {
        $color_text_header = 'white';
    }
    @endphp

    @php
    if ($themeSettings != null) {
        if ($themeSettings->color_highlight_header != null) {
            $color_highlight_header = $themeSettings->color_highlight_header;
        }else{
            $color_highlight_header = '#d72924;';
        }
    }else {
        $color_highlight_header = '#d72924;';
    }
    @endphp

    <style>

    .content-min-h {
	    min-height: calc(100vh - {{$OUT_HEIGHT['movil']}}px);
	}
	@media(min-width:992px) {
	    .content-min-h {
	        min-height: calc(100vh - {{$OUT_HEIGHT['no-movil']}}px);
	    }
	}

        .nav-menu > li > a, .right-menu li a {
            color: {{ $color_text_header }}
        }

        .nav-menu > li.active > a, .right-menu li.active a {
        background: {{ $color_highlight_header }};
        color: {{ $color_text_header }}
        }

        .nav-menu > li > a:hover, .right-menu li a:hover {
        background: {{ $color_highlight_header }};
        color: {{ $color_text_header }} !important
        }

    </style>

    @livewireStyles
</head>


<body class="{{ $layout=='silver'?'body-color':'' }}">

@if( $layout=='white' )
	<div class="body-inner-content category-layout-6">
@endif

	<!-- top bar start -->
	@include('front.pages.top.bar')
	<!-- end top bar-->

	<!-- ad banner start -->
	@include('front.pages.top.banner')
	<!-- ad banner end -->

	<!-- header nav start-->
	@include('front.pages.top.header.index')
	<!-- header nav end-->

	<div class="span-menu d-lg-none"></div>

	<!-- single post start -->
	@yield('content')
	<!-- single post end-->

	@include('front.pages.bottom.index')


@if( $layout=='white' )
	</div>
@endif

	@if(
		(Request::route()->getName() != "pages.advert") &&
		(Request::route()->getName() != "pages.programs") &&
		(Request::route()->getName() != "f.panel.user")
	)
		@include('front.splits.ad-pop-up')
	@endif

	<!-- javaScript Files
	=============================================================================-->

	<!-- initialize jQuery Library -->
	{{-- <script src="{{asset('assets/front/template/js/jquery.min.js')}}"></script> --}}
	<!-- navigation JS -->
	<script src="{{asset('assets/front/template/js/navigation.js')}}"></script>

		<!-- uikit JS -->
		<script src="{{asset('assets/front/template/js/uikit.min.js')}}"></script>
		<script src="{{asset('assets/front/template/js/uikit-icons.js')}}"></script>

	<!-- Popper JS -->
	<script src="{{asset('assets/front/template/js/popper.min.js')}}"></script>

	<!-- magnific popup JS -->
	<script src="{{asset('assets/front/template/js/jquery.magnific-popup.min.js')}}"></script>



	<!-- Bootstrap jQuery -->
	{{-- <script src="{{asset('assets/front/template/js/bootstrap.min.js')}}"></script> --}}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

	<!-- Owl Carousel -->
	<script src="{{asset('assets/front/template/js/owl-carousel.2.3.0.min.js')}}"></script>

	<script src="{{asset('assets/front/template/js/slick.min.js')}}"></script>

	{{-- <script src="{{asset('assets/front/template/js/main.js')}}"></script> --}}
    	<!-- smooth scroling -->
	<script src="{{asset('assets/front/template/js/smoothscroll.js')}}"></script>

	<script src="{{asset('assets/front/template/js/main.js')}}"></script>

	<script src="{{asset('assets/front/custom/generic.js')}}"></script>

    @yield('scripts')
    @livewireScripts

</body>

</html>


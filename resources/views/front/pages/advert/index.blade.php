@extends('front.pages.layouts')
@section('title') 107.5 | Publicidad @endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/front/custom/advert.css')}}">
    <style>
    .body-color{background-color:#fff;}

    @media(min-width:768px) and (max-width:991px) { /*md*/
        .s-advert .card-img-top {
            height: calc((100vw/2) * 0.5);
        }
    }
    @media(min-width:992px) and (max-width:1199px) { /*lg*/
        .s-advert .card-img-top {
            height: calc((100vw/3) * 0.5625);
        }
    }
    @media(min-width: 1200px) { /*xl*/
        .s-advert .card-img-top {
            height: calc((100vw/3) * 0.5625);
        }
    }
    .s-nav-advert>.col-12 h2 {
        border-left: 3px solid #ef1b1b;
    }
    .s-nav-advert>.col a:hover {
        color: #fff !important;
    }
    .s-nav-advert>.col a.active:hover {
        color: #333 !important;
    }
    @media (max-width:575px) {
        .s-nav-advert>.col a {
            padding: 6px 12px;
            margin-bottom: 4px;
        }
    }
</style>
@endsection

@section('content')

    <livewire:manage-advertisements-component />

@endsection

 


@foreach ($postsCategory as $item)
					<div class="list-item">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="list-image">
                                    <a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
									    <img src="{{ asset('img/'.$item->image) }}" alt="{{ Str::limit($item->title, 80) }}">
									</a>
                                    <div class="image-overlay"></div>
                                </div>
                            </div>
                            <div class="col-xs-8">
                                <div class="list-content">
                                    <h3><a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
									{{ Str::limit($item->title, 150) }}</a>
									</h3>
                                    
                                    <!-- <a href="#" class="tag tag-green">#Sports</a>
                                    <a href="#" class="tag tag-gold">#Trending</a> -->
                                    <p style="padding-bottom:10px;"> {{ Str::limit($item->post_summary, 180) }}</p>
									<a href="#" class="tag tag-blue"> 

                                    {{ Str::limit($item->tags,60) }}

                                    </a>
                                   <!--  <div class="author-detail">
                                        <p><a href="#"><i class="icon-profile-male"></i> {{ config('app.name') }}</a></p>
                                        <p><i class="icon-clock"></i> {{ $item->created_at->toFormattedDateString() }}</p>
                                        <p><a href="#"><i class="fa fa-eye"></i> 
									
										@if ($item->visits != null)
										{{ $item->visits->counter }}
										@else
											0
										@endif

									</a></p>
                                    </div> -->
                                </div>
                            </div>
                        </div>                        
                    </div>
@endforeach

@extends('front.pages.layouts')

@section('content')


<div class="breadcrumb_wrapper">
        <div class="container">
            <div class="breadcrumb-content">
                <nav aria-label="breadcrumb">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('f.home.index') }}">Inicio</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('f.pages.categories', $category->slug ) }}">{{ $category->title }}</a></li>
                        
                    </ul>
                </nav>
            </div>
        </div>
    </div>

	<section class="blog-list">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                     
                                        
                    <!-- <div class="list-ad">
                        <img src="images/ad-header.jpg" alt="Image">
                    </div> -->
					
					@include('front.pages.categories.inc.list')


                    
                </div>
                <div id="sidebar" class="col-md-4 col-sm-12">
                    <div class="item-sidebar">
                         
                        <div class="post-categories sidebar-box">
                            <div class="sidebar-title">
                                <h3>{{ trans('multi-leng.frontend_categories') }}</h3>
                            </div>
                            <ul>
							@foreach ($allCategories as $item)
                                <li><a href="{{ route('f.pages.categories', $item->slug ) }}">{{ $item->title }} ({{ $item->posts->count() }})</a></li>
							@endforeach
                                 
                            </ul>

							 
							

                        </div>
                        <div class="recent-post clearfix sidebar-box">
                            <div class="sidebar-title">
                                <h3>Últimos Posts</h3>
                            </div>

                            @foreach ($recentPosts as $item)
                            <div class="recent-item">
                                <div class="recent-image">
                                    <img src="{{ asset('thumbnail/'.$item->thumbnail) }}" alt="{{ Str::limit($item->title,80) }}">
                                </div>
                                <div class="recent-content">
                                    <a href="{{ route('f.pages.categories', $item->category->slug ) }}" class="tag tag-blue">{{ $item->category->title }}</a>
                                    <h4><a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ Str::limit($item->title,60) }}</a></h4>
                                    
                                </div>
                            </div>  
                            @endforeach 
                            
                        </div> 


                        <div class="ad1 sidebar-box">
                            <div class="sidebar-title">
                                <h3>Hosting Recomendado</h3>
                            </div>
                            <div class="ad1-image">
                                <!-- <img src="{{ asset('images/footer1.jpg')}}" alt="Image"> -->
                                
                                <a target="_blank" href="https://clientes.webempresa.com/america/gestion/aff.php?aff=1837&landing=lanzamientohw" > <img src="https://afiliados.webempresa.eu/banner-afiliados-nuevos-planes-mujernavidad-300x250px.png" width="300" height="250" border="0" alt="hosting WordPress" title="Hosting WordPress en español"> </a>


                            </div>
                        </div>
                        <div class="popular-post sidebar-box">
                            <div class="sidebar-title">
                                <h3>Popular Posts</h3>
                            </div>
							@foreach ( $postMoreViewsWeek as $item )
							 
      					
                            <div class="popular-item">
                                <div class="popular-content">
                                    <span class="item-no">{{ $loop->iteration }}</span>
                                    <h4><a href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2'=> $item->post->slug]) }}">
									{{  $item->post->title }}</a>
									</h4>
									<!-- aqui la imagen: {{ asset('thumbnail/'.$item->post->thumbnail) }} -->
                                    <div class="author-detail">
                                         
                                        
                                        <p style="line-height: 1.2;">{{  Str::limit($item->post->post_summary,100) }}  
                                        </p>
                                    </div>
                                </div>
                            </div>
							@endforeach
                             
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="pagination">
                        <ul class="pagination">
                             
							{{    $postsCategory->links('pagination::bootstrap-4') }}
                        </ul>
                    </div>  
                </div>
            </div>
        </div>
    </section>

 

@endsection

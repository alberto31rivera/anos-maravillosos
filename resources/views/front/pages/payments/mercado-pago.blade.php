@extends('front.pages.layouts')

@section('content')

<!-- post wraper start-->
<section class="block-wrapper mt-15  mb-md-4">
	<div class="container">
		<div class="row">
			<div class="col-lg-9">
				@include('front.pages.payments.inc.mercado-pago-content')
			</div>
			<!-- col end-->
			<div class="col-lg-3">
				<div class="right-sidebar">
					@include('front.splits.w-contact-us')
				</div>
				<!-- right sidebar end-->
			</div>
			<!-- col end-->
		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>
<!-- post wraper end-->

@endsection

@section('scripts')
    <script src="https://www.mercadopago.com/v2/security.js" view="item"></script>
@endsection

@extends('front.pages.layouts')

@section('content')

<!-- post wraper start-->
<section class="block-wrapper content-min-h mt-15 mb-md-4">
	<div class="container">
		<div class="row">
			<div class="col-lg-9">
                <div class="contact-box ts-grid-box">
                    <div class="row justify-content-center text-center">
                        <div class="col-12 text-center py-3">
                            <h3 class="mt-3">{{ trans('multi-leng.frontend_paypal_billing_data') }}</h3>
                        </div>
                        @if ($image != null)
                        <div class="col-8 col-sm-6 col-md-4 d-flex justify-content-center pt-2 pb-4">
                            <img class="img-fluid" src="{{ asset('img/payments/paypal/'.$image) }}" alt="">
                        </div>
                        @else
                        <div class="col-8 col-sm-6 col-md-4 d-flex justify-content-center pt-2 pb-4">
                            <img class="img-fluid" src="https://www.inversionesjiraco.com/images/paypal-logo.png" alt="">
                        </div>
                        @endif
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-10">
                            <form id="contact-form" action="{{ route('paypal.billing.information') }}" method="post" role="form">
                                @csrf
                                <div class="error-container"></div>
                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_paypal_name') }}</label>
                                        <input class="form-control form-control-plan" name="name" type="text" required value="@if (Auth::check()){{ Auth::user()->name }}@endif">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_paypal_cellphone') }}</label>
                                            <input class="form-control form-control-cost" name="cellphone" required value="@if (Auth::check()){{ Auth::user()->cellphone }}@endif">
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" value="{{ $plan->id }}" name="id_plan">

                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_paypal_email') }}</label>
                                        <input class="form-control form-control-plan" name="email" type="email" required value="@if (Auth::check()){{ Auth::user()->email }}@endif">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_paypal_type_document') }}</label>
                                            <select name="type_document" class="form-control" required>
                                                <option selected disabled >{{ trans('multi-leng.frontend_paypal_select') }}</option>
                                                <option value="dni" @if (Auth::check()) {{  Auth::user()->type_document == 'dni' ? 'selected' : '' }} @endif>{{ trans('multi-leng.frontend_paypal_dni') }}</option>
                                                <option value="ruc" @if (Auth::check()) {{  Auth::user()->type_document == 'ruc' ? 'selected' : '' }} @endif>{{ trans('multi-leng.frontend_paypal_ruc') }}</option>
                                                <option value="ce" @if (Auth::check()) {{  Auth::user()->type_document == 'ce' ? 'selected' : '' }} @endif>{{ trans('multi-leng.frontend_paypal_ce') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_paypal_number_document') }}</label>
                                        <input class="form-control form-control-plan" name="number_document" type="text" required value="@if (Auth::check()){{ Auth::user()->number_document }}@endif">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_paypal_address') }}</label>
                                            <input class="form-control form-control-cost" name="address" required value="@if (Auth::check()){{ Auth::user()->address }}@endif">
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_paypal_city') }}</label>
                                        <input class="form-control form-control-plan" name="city" type="text" required value="@if (Auth::check()){{ Auth::user()->city }}@endif">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_paypal_country') }}</label>
                                            <input class="form-control form-control-cost" name="country" required value="@if (Auth::check()){{ Auth::user()->country }}@endif">
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center my-4">
                                    <button class="btn btn-primary solid blank text-capitalize" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
                                        @else #e91e63 @endif  @endif" type="submit">{{ trans('multi-leng.frontend_paypal_next') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- grid box end -->

			</div>
			<!-- col end-->
			<div class="col-lg-3">
				<div class="right-sidebar">
					@include('front.splits.w-contact-us')
				</div>
				<!-- right sidebar end-->
			</div>
			<!-- col end-->
		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>
<!-- post wraper end-->

@endsection

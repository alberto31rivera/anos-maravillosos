@extends('front.pages.layouts')

@section('content')

<!-- post wraper start-->
<section class="block-wrapper content-min-h mt-15 mb-md-4 d-flex align-items-center">
	<div class="container">
		<div class="row">
            <div class="col-11 col-sm-8 col-md-6 mx-auto">
                <div class="row">

                   <div class="col-lg-12">
                      <h5 class="title">
                         <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">{{ trans('multi-leng.frontend_paypal_well_done') }}</h4>
                            <p>{{ trans('multi-leng.frontend_paypal_message_one') }} {{ $admin->cellphone }} {{ trans('multi-leng.frontend_paypal_message_two') }} {{ $admin->email }}</p>
                            <hr>
                            <p class="mb-0">{{ trans('multi-leng.frontend_paypal_regards') }} {{ $admin->name }}</p>
                         </div>
                      </h5>

                      <a href="{{ route('f.home.index') }}"><button class="btn btn-primary" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
                        @else #e91e63 @endif  @endif">{{ trans('multi-leng.frontend_paypal_go_home') }}</button></a>
                   </div>

                </div>
             </div>
		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>
<!-- post wraper end-->

@endsection


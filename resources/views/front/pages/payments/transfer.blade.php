@extends('front.pages.layouts')

@section('styles')
<link rel="stylesheet" href="{{asset('assets/front/custom/bootstrap-datepicker/css/bootstrap-datepicker3.standalone.css')}}">
@endsection

@section('content')

<!-- post wraper start-->
<section class="block-wrapper mt-15 content-min-h mb-md-4">
	<div class="container">
		<div class="row">
			<div class="col-lg-9">
				@include('front.pages.payments.inc.transfer-content')
			</div>
			<!-- col end-->
			<div class="col-lg-3">
				<div class="right-sidebar">
					@include('front.splits.w-contact-us')
				</div>
				<!-- right sidebar end-->
			</div>
			<!-- col end-->
		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>
<!-- post wraper end-->

@endsection

@section('scripts')
<script src="{{asset('assets/front/custom/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/front/custom/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

<script>
$(document).ready(function() {
	$('#transfer_date').datepicker({
        language: "es",
        todayBtn: "linked",
        clearBtn: true,
        format: "yyyy-dd-mm",
        multidate: false,
        todayHighlight: true
    });
})
</script>

<script>
    function loadPreview(input, id) {
      id = id || '#preview_img';
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $(id)
                      .attr('src', e.target.result)
                      // .width(200)
                      // .height(150)
                      ;
          };

          reader.readAsDataURL(input.files[0]);
      }
   }
  </script>

@endsection

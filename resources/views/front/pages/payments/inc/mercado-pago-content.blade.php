<div class="contact-box ts-grid-box">
    <div class="row justify-content-center text-center">
        <div class="col-10 text-center py-3">
            <h3 class="mt-3">{{ trans('multi-leng.frontend_mp_summary') }}</h3>
        </div>
        @if ($image != null)
            <div class="col-md-4 d-flex justify-content-center py-3">
                <img class="img-fluid" src="{{ asset('img/payments/mercado-pago/' . $image) }}" alt="">
            </div>
        @else
            <div class="col-md-4 d-flex justify-content-center py-3">
                <img class="img-fluid" src="https://www.inversionesjiraco.com/images/mercadopago-logo.png" alt="">
            </div>
        @endif
    </div>
    <div class="row justify-content-center">
        <div class="col">
            <form id="contact-form" action="" method="post" role="form">
                <div class="error-container"></div>
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_mp_plan') }}</label>
                            <input class="form-control form-control-plan" name="plan" id="plan"
                                value="{{ $plan->title }}" placeholder="" type="text" readonly="">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_mp_cost') }}</label>
                            <input class="form-control form-control-cost" name="cost" id="cost"
                                value="{{ $plan->cost }} {{ $currencyLocal->name }}" placeholder="" readonly="">
                        </div>
                    </div>
                </div>
                <div class="text-center my-4">
                    <a class="btn btn-primary solid blank text-capitalize" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
                        @else #e91e63 @endif  @endif"
                        href="{{ $paymentConfiguration->sandbox == '1' ? $preference->sandbox_init_point : $preference->init_point }}">{{ trans('multi-leng.frontend_mp_checkout') }}</a>
                </div>
            </form>
        </div>
    </div>
</div><!-- grid box end -->

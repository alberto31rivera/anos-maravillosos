<div class="contact-box ts-grid-box">
    <div class="row justify-content-center text-center">
        <div class="col-12 text-center py-3">
            <h3 class="mt-3">{{ trans('multi-leng.frontend_transfer_summary') }}</h3>
        </div>
        <div class="col-md-12">
            @if (session()->has('sucess-message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('sucess-message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-10">
            <h4 class="py-3">{{ trans('multi-leng.frontend_transfer_bank_data') }}</h4>
            @if ($paymentsTransfer != null)
                @if ($paymentsTransfer->bank_details != null)
                    <div class="border rounded p-3" style="box-shadow: 0px 5px 14px rgb(0 0 0 / 8%)">
                        {!! $paymentsTransfer->bank_details !!}
                    </div>
                @endif
            @endif
            <br>
        </div>
        <div class="col-10">
            <form id="contact-form" action="{{ route('pay.transfer', $paymentsTransfer->payment_method_id) }}"
                method="post" enctype="multipart/form-data">
                @csrf

                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_plan') }}</label>
                        <input class="form-control form-control-plan" name="plan" id="plan" value="{{ $plan->title }}"
                            placeholder="" type="text" readonly="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_cost') }}</label>
                        <input class="form-control form-control-cost" name="cost" id="cost" value="{{ $plan->cost }} {{ $currencyLocal->name }}"
                            placeholder="" readonly="">
                    </div>
                </div>
                <input type="hidden" value="{{ $plan->cost_dolar}}" name="cost_dolar">
                <input type="hidden" value="{{ $plan->description}}" name="description">
                <input type="hidden" value="{{ $plan->duration}}" name="duration">
            </div>
                <div class="error-container"></div>
                <div class="row">
                    <div class="col-10">
                        <h4 class="pt-4 pb-3">{{ trans('multi-leng.frontend_transfer_billing_data') }}</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_name') }}</label>
                            @error('name ')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <input class="form-control form-control-name" name="name" id="name" placeholder=""
                                type="text" required value="@if (Auth::check()){{ Auth::user()->name }}@endif">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_cellphone') }}</label>
                            @error('cellphone')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <input class="form-control form-control-cellphone" name="cellphone" id="cellphone"
                                placeholder="" required value="@if (Auth::check()){{ Auth::user()->cellphone }}@endif">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_email') }}</label>
                            @error('email')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <input type="email" class="form-control form-control-email" name="email" id="email"
                                placeholder="" required value="@if (Auth::check()){{ Auth::user()->email }}@endif">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_type_document') }}</label>
                            @error('type_document')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <select name="type_document" class="form-control" required>
                                <option selected disabled >{{ trans('multi-leng.frontend_transfer_select') }}</option>
                                <option value="dni" @if (Auth::check()) {{  Auth::user()->type_document == 'dni' ? 'selected' : '' }} @endif>{{ trans('multi-leng.frontend_transfer_dni') }}</option>
                                <option value="ruc" @if (Auth::check()) {{  Auth::user()->type_document == 'ruc' ? 'selected' : ''  }} @endif>{{ trans('multi-leng.frontend_transfer_ruc') }}</option>
                                <option value="ce" @if (Auth::check()) {{  Auth::user()->type_document == 'ce' ? 'selected' : '' }} @endif>{{ trans('multi-leng.frontend_transfer_ce') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_number_document') }}</label>
                            @error('number_document')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <input class="form-control form-control-email" name="number_document" id="number_document"
                                placeholder="" required value="@if (Auth::check()){{ Auth::user()->number_document }}@endif">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_address') }}</label>
                            @error('address')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <input class="form-control form-control-email" name="address" id="address"
                                placeholder="" required value="@if (Auth::check()){{ Auth::user()->address }}@endif">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_city') }}</label>
                            @error('city')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <input class="form-control form-control-email" name="city" id="city"
                                placeholder="" required value="@if (Auth::check()){{ Auth::user()->city }}@endif">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_country') }}</label>
                            @error('country')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <input class="form-control form-control-email" name="country" id="country"
                                placeholder="" required value="@if (Auth::check()){{ Auth::user()->country }}@endif">
                        </div>
                    </div>
                    <div class="col-10">
                        <h4 class="pt-4 pb-3">{{ trans('multi-leng.frontend_transfer_proof') }}</h4>
                    </div>
                    <div class="col-md-6 mb-4">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_file') }}</label>
                            @error('proof_transfer')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <div class="custom-file" style="margin:1.5px 0">
                                <input type="file" name="proof_transfer" class="custom-file-input" id="file-src" onchange="loadPreview(this);"
                                    required="">
                                <label class="custom-file-label" for="file-src">{{ trans('multi-leng.frontend_transfer_choose_file') }}</label>

                                <div class="contains-img-payments">
                                <img id="preview_img" src="https://iamindore.com/w3newdesign/w3a/wp-content/uploads/2019/09/No_Image-128.png"/>
                                {{-- <div class="invalid-feedback">Example invalid custom file feedback</div> --}}
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-4">
                        <div class="form-group">
                            <label class="font-weight-bold">{{ trans('multi-leng.frontend_transfer_date') }}</label>
                            @error('transfer_date')
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ $message }}
                                </div>
                            @enderror
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <input id="transfer_date" type="text" required name="transfer_date" data-date-format='yyyy-mm-dd'
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mt-md-4 mb-4">
                    <button class="btn btn-primary solid blank text-capitalize" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
                        @else #e91e63 @endif  @endif" type="submit">{{ trans('multi-leng.frontend_transfer_checkout') }}</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- grid box end -->

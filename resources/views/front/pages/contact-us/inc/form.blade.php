<div class="contact-box ts-grid-box">
    <div class="row justify-content-center">
        <div class="col-10 col-sm-12">

            @if ($banner != null)
                @if ($banner->title_four != null)
                    <h3 class="mb-30">{{ $banner->title_four }}</h3>
                @else
                    <h3 class="mb-30">{{ trans('multi-leng.frontend_contact_advertise_here') }}</h3>
                @endif
            @else
                <h3 class="mb-30">{{ trans('multi-leng.frontend_contact_advertise_here') }}</h3>
            @endif
            <p class="mb-30">{{ $admin->description_contact }}</p>
            @if ($banner != null)
                @if ($banner->title_five != null)
                    <p class="font-weight-bold mb-30"><a href="{{ route('pages.ad-plans') }}">{{ $banner->title_five }}</a>
                    </p>
                @else
                    <p class="font-weight-bold mb-30"><a href="{{ route('pages.ad-plans') }}">{{ trans('multi-leng.frontend_contact_view_advertising_plans') }}</a></p>
                @endif
            @else
                <p class="font-weight-bold mb-30"><a href="{{ route('pages.ad-plans') }}">{{ trans('multi-leng.frontend_contact_view_advertising_plans') }}</a></p>
            @endif
            @if (Session::has('success-message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('success-message') }}
                </div>
            @endif
            <form id="contact-form" action="{{ route('contact.us') }}" method="post" role="form">
                @csrf
                <div class="error-container"></div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{ trans('multi-leng.frontend_contact_name') }}</label>
                            <input class="form-control form-control-name" name="name" id="name" placeholder="" type="text"
                                required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{ trans('multi-leng.frontend_contact_email') }}</label>
                            <input class="form-control form-control-email" name="email" id="email" placeholder="" type="email"
                                required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{ trans('multi-leng.frontend_contact_cellphone') }}</label>
                            <input class="form-control form-control-cellphone" name="cellphone" id="cellphone" placeholder=""
                                required>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-30">
                    <label>{{ trans('multi-leng.frontend_contact_message') }}</label>
                    <textarea class="form-control form-control-message" name="message" id="message" placeholder="" rows="10"
                        required></textarea>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary solid blank" type="submit" style="background:@if ($themeSettings !=null)  @if ($themeSettings->color_buttons !=null) {{ $themeSettings->color_buttons }}
                        @else #e91e63 @endif @endif">{{ trans('multi-leng.frontend_contact_send_message') }}</button>
                </div>
            </form>

        </div>
    </div>
</div><!-- grid box end -->

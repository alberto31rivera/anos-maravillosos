<div class="ts-grid-box pb-0">
	<div class="author-box author-box-item">
        @if ($admin->avatar != '')
		<img class="author-img" src="{{asset('img/avatar-users/'. $admin->avatar)}}" alt="">
        @else
        <img class="author-img" src="{{asset('assets/front/template/images/news/user1.png')}}" alt="">
        @endif
		<div class="author-info">
			<div class="author-name">
				<h4>{{ $admin->name }}</h4>
				<a href="{{ $admin->web_site }}">{{ $admin->web_site }}</a>
			</div>

			<div class="authors-social mb-2 mb-md-0">
                @if ($admin->facebook != '')
                <a href="{{ $admin->facebook }}" target="_blank" class="ts-facebook">
					<i class="fa fa-facebook"></i>
				</a>
                @endif
                @if ($admin->instagram != '')
                <a href="{{ $admin->instagram }}" target="_blank" class="ts-instagram">
					<i class="fa fa-instagram"></i>
				</a>
                @endif
                @if ($admin->twitter != '')
				<a href="{{ $admin->twitter }}" target="_blank" class="ts-twitter">
					<i class="fa fa-twitter"></i>
				</a>
                @endif
                @if ($admin->linkedin != '')
				<a href="{{ $admin->linkedin }}" target="_blank" class="ts-linkedin">
					<i class="fa fa-linkedin"></i>
				</a>
                @endif
                @if ($admin->youtube != '')
                <a href="{{ $admin->youtube }}" target="_blank" class="ts-youtube">
					<i class="fa fa-youtube"></i>
				</a>
                @endif
			</div>
			<div class="clearfix"></div>

			<div class="authors-social" style="float:none">
                @if ($admin->cellphone != '')
				<a href="tel:{{ $admin->cellphone }}" class="ts-envelope">
					<i class="fa fa-phone fa-lg"></i>
				</a>
                @endif
                @if ($admin->whatsapp != '')
                <a href="https://api.whatsapp.com/send?phone={{ $admin->whatsapp }}" target="_blank" class="ts-whatsapp">
					<i class="fa fa-whatsapp fa-lg"></i>
				</a>
                @endif
                @if ($admin->email != '')
                <a href="mailto:{{ $admin->email }}" class="ts-phone">
					<i class="fa fa-envelope fa-lg"></i>
				</a>
                @endif
			</div>
			<p class="d-parragraph">{{ $admin->description }}</p>

		</div>
	</div>
</div>

@extends('front.pages.layouts')

@section('styles')
<style>
.author-box .authors-social a.ts-phone {
    color: #EF1B1B;
    transition: all ease 500ms;
}
.author-box .authors-social a.ts-whatsapp {
    color: #1BD741;
    transition: all ease 500ms;
}
.author-box .authors-social a.ts-envelope {
    color: #EF1B1B;
    transition: all ease 500ms;
}
.author-box .authors-social a.ts-phone:hover,
.author-box .authors-social a.ts-whatsapp:hover,
.author-box .authors-social a.ts-envelope:hover {
	color: #d72924;
}
.author-box .authors-social a.ts-whatsapp i {
	transform: scale(1.2);
}
.author-box.author-box-item {
	padding: 20px 0 20px 100px;
}
.author-info > .author-name,
.author-info > .author-name h4 {
	margin-bottom: 10px;
}
.author-box-item .d-parragraph {
	margin-top: 10px !important;
}
</style>
@endsection

@section('content')

<!-- post wraper start-->
<section class="block-wrapper mt-15  mb-md-4">
	<div class="container">
		<div class="row">
			<div class="col-lg-9">

				<!--AUTHOR-->
				@include('front.pages.contact-us.inc.author')
				<!--FORM-->
				@include('front.pages.contact-us.inc.form')

			</div>
			<!-- col end-->
			<div class="col-lg-3">
				<div class="right-sidebar">

				</div>
				<!-- right sidebar end-->
			</div>
			<!-- col end-->
		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>
<!-- post wraper end-->

@endsection
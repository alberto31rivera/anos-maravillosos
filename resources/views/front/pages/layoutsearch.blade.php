<!doctype html>
<html lang="es">

<head>
	<!-- Basic Page Needs =====================================-->
	<meta charset="utf-8">

	<!-- Mobile Specific Metas ================================-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>

	<!-- Site Title- -->
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')" />
    <meta name="robots" content="index, follow"/>
    <meta name="author" content="{{ env('APP_NAME') }}" />

    @yield('meta')


	<!-- CSS blogmag -->
    
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <!--Custom CSS-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <!--Plugin CSS-->
    <link href="{{asset('css/plugin.css')}}" rel="stylesheet" type="text/css">
    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- CSS
   ==================================================== -->
   

    @php
       $google = App\Models\ManageGoogle::first();
    @endphp

    <!-- @if ($google->recaptcha_key != null || $google->recaptcha_secret_key)
    {!! htmlScriptTagJsApi() !!}
    @endif -->
 
    @if ($google->google_analytics != null)
    {!! $google->google_analytics !!}
    @endif

    @yield('styles')

    @if (env('APP_LOCALE')=='en')
    <style>.ts-breaking-news .breaking-news-content{width:77.5%}</style>
    @endif

      
     

    @livewireStyles
</head>

 
	<!-- header nav start-->
	@include('front.pages.top.header.index')
	<!-- header nav end-->

	 

	<!-- single post start -->
	@yield('content')
	<!-- single post end-->

	 


 
	<!-- javaScript Files
	=============================================================================-->
	
	 

	<!-- Scriptsblogmag-->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
<!--     <script src="{{asset('js/plugin.js')}}"></script> -->
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/custom-mixitup.js')}}"></script>


    @yield('scripts')
    @livewireScripts

    @include('front.home.footer')

</body>

</html>


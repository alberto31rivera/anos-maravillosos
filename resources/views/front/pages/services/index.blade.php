@extends('front.pages.layouts')

@section('styles')
<style>
.ts-grid-box.content-wrapper.single-post{
    padding-left: 30px;
    padding-right: 30px;
}
.wrapper-service {
    overflow-x: auto;
}
.wrapper-service::-webkit-scrollbar {
    height: 8.5px;
    background: rgba(255,255,255, .1);
}
.wrapper-service::-webkit-scrollbar-thumb {
    background: #F0F0F0;
    border-radius: 5px;
    border-left: 1px solid #CECECE;
    border-right: 1px solid #CECECE;
    border-top: 1px solid #CECECE;
    border-bottom: 2px solid #CECECE;
}
</style>
@endsection

@section('content')

    <!-- post wraper start-->
    <section class="block-wrapper mt-15 content-min-h mb-md-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    @if ($getService->slug_one == $slug)
                        <div class="ts-grid-box clearfix ts-category-title">
                            <h2 class="ts-title text-center">{{ $getService->title_one }}</h2>
                        </div>
                        <div class="ts-grid-box content-wrapper single-post">
                            <div class="wrapper-service">
                            {!! $getService->description_one !!}
                            </div>
                        </div>

                    @elseif ($getService->slug_two == $slug)
                        <div class="ts-grid-box clearfix ts-category-title">
                            <h2 class="ts-title text-center">{{ $getService->title_two }}</h2>
                        </div>
                        <div class="ts-grid-box content-wrapper single-post">
                            <div class="wrapper-service">
                            {!! $getService->description_two !!}
                            </div>
                        </div>

                    @elseif ($getService->slug_three == $slug)
                        <div class="ts-grid-box clearfix ts-category-title">
                            <h2 class="ts-title text-center">{{ $getService->title_three }}</h2>
                        </div>
                        <div class="ts-grid-box content-wrapper single-post">
                            <div class="wrapper-service">
                            {!! $getService->description_three !!}
                            </div>
                        </div>

                    @elseif ($getService->slug_four == $slug)
                        <div class="ts-grid-box clearfix ts-category-title">
                            <h2 class="ts-title text-center">{{ $getService->title_four }}</h2>
                        </div>
                        <div class="ts-grid-box content-wrapper single-post">
                            <div class="wrapper-service">
                            {!! $getService->description_four !!}
                            </div>
                        </div>

                    @elseif ($getService->slug_five == $slug)
                        <div class="ts-grid-box clearfix ts-category-title">
                            <h2 class="ts-title text-center">{{ $getService->title_five }}</h2>
                        </div>
                        <div class="ts-grid-box content-wrapper single-post">
                            <div class="wrapper-service">
                            {!! $getService->description_five !!}
                            </div>
                        </div>

                    @else
                        <div class="ts-grid-box clearfix ts-category-title">
                            <h2 class="ts-title text-center">{{ $getService->title_six }}</h2>
                        </div>
                        <div class="ts-grid-box content-wrapper single-post">
                            <div class="wrapper-service">
                            {!! $getService->description_six !!}
                            </div>
                        </div>

                    @endif

                </div>
                <!-- col end-->
            </div>
            <!-- row end-->
        </div>
        <!-- container end-->
    </section>
    <!-- post wraper end-->

@endsection

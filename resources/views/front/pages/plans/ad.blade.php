@extends('front.pages.layouts')

@section('styles')
<style>
	.plans-card .cicle {
		height: 130px;
		width: 130px;
		border-radius: 50%;
		background-color: #D72924;
		display: flex;
		justify-content: center;
		align-items: center;
		color: #fff;
		box-shadow: 0px 5px 14px rgb(0 0 0 / 15%);
		/*transition: transform 300ms ease;*/
	}
	.plans-card .cicle:hover {
		/*transform: scale(1.05);*/
	}
	.values {
		letter-spacing: .8px;
	}
	/*.plans-card .values sup {
		top: -18px;
    	font-size: 20px;
	}*/
	.plans-card .values span.money {
    	font-size: .8rem;
	}
	.plans-card .values span.money-local {
    	font-size: 1.1rem;
	}
	.plans-card .price {
		font-size: 1.1rem;
	}
	.plans-card .price-local {
		font-size: 1.8rem;
		font-weight: 600;
	}
	.plans-card .month {
		font-size: 1.3rem;
	}
	.plans-card .paragraph {
		box-shadow: 0px 5px 14px rgb(0 0 0 / 7%);
		margin-bottom: 0px !important;
	}

	.btn-group-paymants img {
		width: 54px;
		height: 54px;
		border-radius: 50%;
		object-fit: cover;
		padding: 5px;
		box-shadow: 0px 5px 14px rgb(0 0 0 / 7%);
		transition: transform 300ms ease;
	}
	.btn-group-paymants img:hover {
		transform: scale(1.1);
		width: 54px;
		height: 54px;
	}

</style>
@endsection

@section('content')

<!-- post wraper start-->
<section class="block-wrapper mt-15  mb-md-4  content-min-h">
	<div class="container">
		<div class="row">
			<div class="col-lg-9">
				@include('front.pages.plans.inc.details')
			</div>
			<!-- col end-->
			<div class="col-lg-3">
				<div class="right-sidebar">
					BIENVENIDOS
				</div>
				<!-- right sidebar end-->
			</div>
			<!-- col end-->
		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>
<!-- post wraper end-->

@endsection

<div class="ts-grid-box clearfix ts-category-title">
    @if ($banner != null)
        @if ($banner->title_five != null)
            <h2 class="ts-title float-left">{{ $banner->title_five }}</h2>
        @else
            <h2 class="ts-title float-left">{{ trans('multi-leng.frontend_advertising_plans') }}</h2>
        @endif
    @else
        <h2 class="ts-title float-left">{{ trans('multi-leng.frontend_advertising_plans') }}</h2>
    @endif

</div>

@foreach ($plans as $item)
    <div class="contact-box ts-grid-box plans-card p-3 p-md-5">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <h3 class="post-title md">{{ $item->title }}</h3>
                </div>
                <hr class="container mt-0 mx-3" style="height:.05rem;background-color:#D72924">
                <div class="col-12 col-md-auto text-center">
                    <div class="cicle mx-auto mb-2">
                        <span class="values text-center mt-2">
                            <span class="price-local">{{ $item->cost }}</span>
                            <span class="money-local">{{ $currencyLocal->name }}</span>
                            <br>
                            <span class="price">{{ $item->cost_dolar }}</span>
                            <span class="money">{{ $currencyDolar->name }}</span>
                            <br>
                            <span class="month">{{ $item->duration }} <span
                                    class="font-weight-bold">{{ trans('multi-leng.frontend_plans_day') }}</span></span>
                        </span>
                    </div>
                    <div class="btn-group-paymants d-inline">
                        @if ($paymentsPaypal != null)
                            @if ($paymentsPaypal->status == 1 && $paymentsPaypal->payment_method_id == 1 && $paymentsPaypal->email != null)
                                <a href="{{ route('pages.paypal', $item->id) }}"
                                    style="border:none; background: rgba(29, 49, 42, 0.0);">
                                    <img src="{{ asset('assets/front/custom/paypal.png') }}" alt="">
                                </a>
                            @endif
                        @endif
                        @if ($paymentsMercadoPago != null)
                            @if ($paymentsMercadoPago->status == 1 && $paymentsMercadoPago->payment_method_id == 2 && $paymentsMercadoPago->public_key != null && $paymentsMercadoPago->access_token != null && $paymentsMercadoPago->client_id != null && $paymentsMercadoPago->client_secret != null)
                                <a href="{{ route('pages.mercado-pago', $item->id) }}" data-toggle="tooltip"
                                    data-placement="top" title="Pagar con Mercado Pago">
                                    <img src="{{ asset('assets/front/custom/mercadopago.jpg') }}" alt="">
                                </a>
                            @endif
                        @endif
                        @if ($paymentsTransfer != null)
                            @if ($paymentsTransfer->status == 1 && $paymentsTransfer->payment_method_id == 3)
                                <a href="{{ route('pages.transfer', $item->id) }}" data-toggle="tooltip"
                                    data-placement="top" title="Pagar con Depósito o Transferencia">
                                    <img src="{{ asset('assets/front/custom/transfer.png') }}" alt="">
                                </a>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="col">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 mb-3 mt-3 mt-md-0 pt-3 paragraph">
                                <!--Begin: Texto enriquecido-->
                                {!! $item->description !!}
                                <!--End: Texto enriquecido-->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div><!-- grid box end -->
@endforeach

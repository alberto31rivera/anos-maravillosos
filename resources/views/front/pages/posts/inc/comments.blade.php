
<div class="comments-form">
    
    <!-- contado de comentarios -->
    <h3 class="comments-counter">{{ count(App\Models\Comment::where('post_id', $post->id)->get()) }} {{ trans('multi-leng.frontend_post_comments') }}</h3>

    @include('front.pages.comments.list', ['comments' => $post->comments])

    @include('front.pages.comments.form')

    @if (session('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        {{ session('error') }} <a href="{{ route('register') }}">Registro</a> o <a href="{{ route('login') }}">iniciar sesión</a>   
         
    </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
              
        </div>
    @endif  
    
</div>





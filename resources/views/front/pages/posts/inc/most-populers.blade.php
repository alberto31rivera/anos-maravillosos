<div class="ts-grid-box mb-30">
    <h2 class="ts-title">{{ trans('multi-leng.frontend_post_most_popular') }}</h2>
    <div class="most-populers owl-carousel">

        @if ($postMorePopular->count() >= 3)
            @foreach ($postMorePopular as $item)
                <div class="item">
                    <a class="post-cat ts-{{ $item->post->category->color }}-light-bg"
                        href="{{ route('f.pages.categories', $item->post->category->slug) }}">{{ $item->post->category->title }}</a>
                    <div class="ts-post-thumb">
                        <a href="#">
                            <img class="img-fluid" src="{{ asset('img/' . $item->post->image) }}" alt="">
                        </a>
                    </div>
                    <div class="post-content">
                        <h3 class="post-title">
                            <a
                                href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2' => $item->post->slug]) }}">{{ $item->post->title }}</a>
                        </h3>
                        <ul class="post-meta-info">
                            <li>
                                <span class="post-date-info">
                                    <i class="fa fa-clock-o"></i>
                                    {{ $item->created_at->toFormattedDateString() }}
                                </span>
                            </li>

                            <li class="active">
                                <i class="icon-fire"></i>
                                @if ($item->visits != null)
                                    {{ $item->visits->counter }}
                                @else
                                    0
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            @endforeach
        @endif
        <!-- ts-grid-box end-->
    </div>
    <!-- most-populers end-->
</div>

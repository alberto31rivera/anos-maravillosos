<div class="ts-grid-box content-wrapper single-post">
	<div class="entry-header">
		<h2 class="post-title lg">{{ $post->title }}</h2>
		<ul class="post-meta-info">
			<li>
				<a href="" class="post-cat ts-{{ $post->category->color }}-bg">{{ $post->category->title }}</a>
			</li>
			<li>
				<i class="fa fa-clock-o"></i>
				{{ $post->created_at->toFormattedDateString() }}
			</li>
			<li class="active">
				<i class="icon-fire"></i>
				@if ($post->visits != null)
                    {{ $post->visits->counter }}
                  @else
                  0
                  @endif
			</li>

		</ul>
	</div>
	<!-- single post header end-->
	<div class="post-content-area">
		<div class="post-media post-featured-image">
			<a href="{{ asset('img/'.$post->image) }}" class="gallery-popup">
				<img src="{{ asset('img/'.$post->image) }}" class="img-fluid" alt="" style="max-height:500px;object-fit:cover">
			</a>
		</div>
		<div class="entry-content">

			<p style="margin-top:30px">
				{{-- <span class="tie-dropcap">A</span> {{ $post->post_summary }} --}}

				<span class="tie-dropcap">
					{{ Str::of($post->post_summary)->substr(0, 1) }}
				</span>
				{{ Str::of($post->post_summary)->substr(1) }}
			</p>

            {!! $post->description !!}

			<blockquote>
				{{ $post->sentence }}
				{{-- <cite>Name Author</cite> --}}
			</blockquote>
            <a class="btn btn-primary" style="background-color: #3b5998;" href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank" role="button"
            ><i class="fa fa-facebook me-2"></i> Facebook</a>
            <a class="btn btn-primary" style="background-color: #4dc247;" href="
            ><i class="fa fa-whatsapp me-2"></i> Whatsapp</a>
            <a class="btn btn-primary" style="background-color: #55acee;" href="https://twitter.com/intent/tweet?url={{url()->current()}}" target="_blank"role="button"
            ><i class="fa fa-twitter me-2"></i> Twitter</a>
            <a class="btn btn-primary" style="background-color: #32afed;" href="https://telegram.me/share/url?url={{url()->current()}}" target="_blank"role="button"
                ><i class="fa fa-telegram me-2"></i> Telegram</a>

		</div>
		<!-- entry content end-->
	</div>

	<div class="post-navigation clearfix  mt-5">
        @if ($previusPost != null)
        <div class="post-previous float-left">
			<a href="{{ route('f.pages.post', ['slug1' => $previusPost->category->slug, 'slug2'=> $previusPost->slug]) }}">
				<img src="{{asset('img/'.$previusPost->image)}}" alt="">
				<span>{{ trans('multi-leng.frontend_post_previous') }}</span>
				<p>
					{{ $previusPost->title }}
				</p>
			</a>
		</div>
        @endif
        @if ($nextPost != null)
		<div class="post-next float-right">
			<a href="{{ route('f.pages.post', ['slug1' => $nextPost->category->slug, 'slug2'=> $nextPost->slug]) }}">
				<img src="{{asset('img/'.$nextPost->image)}}" alt="">
				<span>{{ trans('multi-leng.frontend_post_next') }}</span>
				<p>
                    {{ $nextPost->title }}
				</p>
			</a>
		</div>
        @endif
	</div>
	<!-- post navigation end-->
</div>

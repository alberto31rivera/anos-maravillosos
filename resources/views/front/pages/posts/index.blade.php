@extends('front.pages.layouts')
@section('title'){{ $post->title }}@endsection
@section('description'){!! $post->post_summary!!}@endsection

@section('meta')
<!-- Contents for Post -->
<meta name="keywords" content="{{ $post->tags }}"/>
<meta property="og:title" content="{{ $post->title }}">
<meta property="og:site_name" content="{{ env('APP_NAME') }}">
<meta property="og:url" content="{{url()->current()}}">
<link rel="canonical" href="{{url()->current()}}" />
<meta property="og:description" content="{!! $post->post_summary!!}">
<meta property="og:type" content="website">
<meta property="og:image" content="{{ asset('img/'.$post->image) }}">
<meta property="og:image" content="{{route('f.home.index')}}/medium/{!! $post->medium !!}">
<meta property="og:image" content="{{route('f.home.index')}}/thumbnail/{!! $post->thumbnail !!}">

<link href="{{route('f.home.index')}}/medium/{!! $post->medium !!}" rel="{{ $post->title }}" />

@endsection

@section('styles')
<style>
	@media(min-width:992px)
	{
		.comments-list .comments-reply {
    		margin: 0px 0 0 110px;
		}
	}
	.ts-newslatter .newsletter-form .email-form-group .form-control {
	    padding: 5px 0;
	    font-size: 14px;
	}
</style>
@endsection

@section('content')

					 


	<!-- Breadcrumb -->
    <div class="page-cover">
        <div class="container">
            <div class="row">
                <div class="col-xs-5">
                    <div class="cover-content">
                        <div class="author-detail">
                            <a href="{{ route('f.pages.categories', $post->category->slug ) }}" class="tag tag-blue">{{ $post->category->title }}</a>
                            <a href="#" class="tag tag-blue"><i class="fa fa-eye"></i> 
                            @if ($post->visits != null)
                                {{ $post->visits->counter }}
                            @else
                            0
                            @endif
                        </a>
                        </div>
                        <h1>{{ $post->title }}</h1>
                        <div class="author-detail">
                            <p><i class="icon-clock"></i> Publicado: {{ $post->created_at->toFormattedDateString() }}</p> 
                            
                            <p><a href="#"><i class="icon-profile-male"></i> {{ config('app.name') }}</a></p>
                            
                        </div>
                        <p>{{ $post->post_summary }}</p> 
                    </div>
                </div>
                <div class="col-xs-7">
                    <img src="{{ asset('img/'.$post->image) }}" alt="{{ $post->title }}">
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Ends -->

    <!-- Detail -->
    <section class="item-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8" style="box-shadow: 0 0 15px #b6b8bb;padding:15px;">
                    <div class="item-wrapper">
                        <div class="item-detail">
                            <p class="articlepara">{{ Str::of($post->sentence)->substr(0, 1) }}</p>
                            
                            {{ Str::of($post->sentence)->substr(1) }}
                            <p> {!! $post->description !!} </p>
                        </div>                      

                         
                        <div class="author-profile">
                            <div class="profile-image">
                                <img src="{{ asset('images/profile1.jpg')}}" alt="Imagen perfil {{ config('app.name') }}">
                            </div>
                            <div class="profile-content">
                                <h3>{{ config('app.name') }}</h3>
                                <p>La idea de crear Wordprecero, fue para compartir conocimiento y para ofrecer ayuda a personas que trabajan con Wordpress. Si necesitas una asesoría para escoger un buen hosting, sobre Wordpress o páginas Web no dudes en escribirme. Te dejo mi whatsapp aquí</p>
                                <ul style="padding-top:10px;" class="profile-link">
                                    <li style="padding:10px;"><a target="_blank" href="https://www.facebook.com/wordprecero10/"><i class="fa fa-facebook"></i></a></li>
                                    <li style="padding:10px;"><a target="_blank" href="https://wa.link/brlfz7"><i class="fa fa-whatsapp"></i></a></li>
                              <!--       <li><a href="#"><i class="fa fa-tumblr"></i></a></li>   -->
                                </ul>
                               
                            </div>
                        </div>
                        <div class="share-buttons">
                            <a role="button" href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank" class="btn-large btn-facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Comparte en Facebook</a>
                             
                            <p> <span>@if ($post->visits != null)
                                {{ $post->visits->counter }}
                            @else
                            0
                            @endif</span> vistas</p>
                            <a href="https://api.whatsapp.com/send?text={{url()->current()}}" data-action="share/whatsapp/share" target="_blank" role="button" class="btn-large btn-twitter"><i class="fa fa-whatsapp" aria-hidden="true"></i> Comparte en Whatsapp</a>
                        </div>
                        <div class="item-tags">
                            <a href="#" class="tag-blue tag">{{ $post->tags }}</a>
                             
                        </div>
                        

                        <!-- titulo del comentario-->
                         
                    </div>

                    @include('front.pages.posts.inc.comments')
                </div>


                <div class="col-md-4 col-sm-12">
                    <div class="item-sidebar">
                        <div class="author sidebar-box">
                            <div class="sidebar-title">
                                <h3>Autor</h3>
                            </div>
                            <div class="author-image">
                                <img src="{{ asset('images/profile1.jpg')}}" alt="{{ config('app.name') }}">
                            </div>
                            <div class="author-content">
                                <h4><a href="#">{{ config('app.name') }}</a></h4>
                                <p>En Wordprecero disfruto compartir el contenido. Sobre todo lo veo como un pasatiempo y el mejor escape de la realidad. Es tan sólo una forma de escribir para el recuerdo. 
                                    </p>
                                <ul class="header-social-links">
                                    <li><a target="_blank" href="https://www.facebook.com/wordprecero10/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="https://wa.link/brlfz7"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/wordprecero10/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
              
                        <div class="recent-post clearfix sidebar-box">
                            <div class="sidebar-title">
                                <h3>Últimos Posts</h3>
                            </div>  

                            @foreach ($recentPosts as $item)
                            <div class="recent-item">
                                <div class="recent-image">
                                    <img src="{{ asset('thumbnail/'.$item->thumbnail) }}" alt="{{ Str::limit($item->title,80) }}">
                                </div>
                                <div class="recent-content">
                                    <a href="{{ route('f.pages.categories', $item->category->slug ) }}" class="tag tag-blue">{{ $item->category->title }}</a>
                                    <h4><a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">{{ Str::limit($item->title,60) }}</a></h4>
                                    
                                </div>
                            </div>  
                            @endforeach             
                            
                            
                            
                        </div>

                        <div class="post-categories sidebar-box">
                            <div class="sidebar-title">
                                <h3>Categorías</h3>
                            </div>    
                            <ul>@foreach ($allCategories as $item)
                            <li><a href="{{ route('f.pages.categories', $item->slug ) }}">{{ $item->title }} </a>({{ $item->posts->count() }})</li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="ad1 sidebar-box">
                            <div class="sidebar-title">
                                <h3>Hosting Wordpress</h3>
                            </div>
                            <div class="ad1-image">
                                <!-- <img src="{{ asset('images/footer1.jpg')}}" alt="Image"> -->
                                
                                <a target="_blank" href="https://clientes.webempresa.com/america/gestion/aff.php?aff=1837&landing=lanzamientohw" > <img src="https://afiliados.webempresa.eu/banner-afiliados-nuevos-planes-mujernavidad-300x250px.png" width="300" height="250" border="0" alt="hosting WordPress" title="Hosting WordPress en español"> </a>


                            </div>
                        </div>
                        <div class="popular-post sidebar-box">
                            <div class="sidebar-title">
                                <h3>Popular Posts</h3>
                            </div>

                        @if ($postMorePopular->count() >= 3)
                        @foreach ($postMorePopular as $item)
                            <div class="popular-item">
                                <div class="popular-content">
                                    <span class="item-no">{{ $loop->iteration }}</span>
                                    <h4><a href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2' => $item->post->slug]) }}">
                                    {{ $item->post->title }}
                                        </a></h4>
                                    <div class="author-detail">
                                       <!--  <p><a href="#"><i class="icon-profile-male"></i> {{ config('app.name') }}</a></p> -->
                                       <p><a href="#"><i class="fa fa-eye">
                                        @if ($item->visits != null)
										{{ $item->visits->counter }}
										@else
											0
										@endif
                                        </i>  
                                        
                                        </a></p>
                                        <p><i class="icon-clock"></i> {{ $item->created_at->toFormattedDateString() }}</p>
                                        
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @endif                

                        </div>
                    </div>
                </div>

                
            </div>
        </div>


    </section>


    
	
@endsection


    


@section('scripts')
<script>
$(".link-reply").click(function()
{
    $(".msg-reply").toggle('.d-none');
})
</script>
@endsection



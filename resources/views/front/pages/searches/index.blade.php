@extends('front.pages.layoutsearch')

@section('content')

<section class="block-wrapper mt-15">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">

				@include('front.pages.searches.inc.searcher')

			</div>
			 
		</div>
		<!-- row end-->
	</div>
	<!-- container end-->
</section>

@endsection

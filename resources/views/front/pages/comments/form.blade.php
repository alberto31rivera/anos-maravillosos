
 


                         
                        <form role="form" class="ts-form" action="{{ route('comment.user', $post->id) }}" method="POST" style="padding: 10px 0;">
                                @csrf
                                    @if (isset($comment->id))
                                    <input type="hidden" name="parent_id" value="{{$comment->id}}">
                                @endif
                                <h3 style="padding-bottom: 0px;">Envíar comentario:</h3>
                                <div class="row">
                                    <div class="textarea col-sm-12">
                                        <!-- <label for="Name">Your Comment:</label> -->
                                        <textarea style="height: 80px!important;margin-bottom:0;" class="form-control msg-box" id="title" name="title" placeholder="Escribe aquí tu comentario" rows="10" required=""></textarea>
                                    </div>
                                     
                                    <div class="col-sm-12">
                                        <!-- publicar -->
                                        <div class="comment-btn">
                                            
                                            <button class="btn btn-primary" type="submit" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
                                             @else #e91e63 @endif  @endif">{{ trans('multi-leng.frontend_post_publish') }}</button>
                                        </div>
                                    </div>
                                </div>
                        </form>
                         
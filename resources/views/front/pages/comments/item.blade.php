
<div class="comment-box">
    <ul class="comments-list" style="box-shadow: 0 0 15px #b6b8bb;">

        <li>
            <div class="comment-item">
                <div class="comment-image">
                    @if ($comment->users->avatar == null)
                    <img alt="{{ $comment->title }}" src="{{asset('img/avatar-users/profile.png')}}">
                    @else
                    <img alt="{{ $comment->title }}" src="{{asset('img/avatar-users/'. $comment->users->avatar )}}">
                    @endif
                </div>
                <div class="comment-content">
                    <h4>{{ $comment->users->name }}</h4>
                    <p class="date"><i class="icon-clock"></i> {{ $comment->date }}</p>
                    <p>{{ $comment->title }}</p> 
                    
                    
                <button class="btn-white btn-red" type="button" data-toggle="collapse" data-target="#reply-{{$comment->id}}" aria-expanded="false" aria-controls="reply-{{$comment->id}}">
                    responder  
                </button>

                    <div class="collapse comment-content" id="reply-{{$comment->id}}">        
             
                        @include('front.pages.comments.form', ['comment' => $comment])
                                
 
                        <ul class="reply"  style="padding-top:20px;">
                                @if ($comment->replies)
                                    @include('front.pages.comments.list', ['comments' => $comment->replies])
                                @endif
                            </ul>
                        </div>

                    </div> 
                

        </li>
    </ul>

    
</div>

    



            








 
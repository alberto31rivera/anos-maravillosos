<div class="ts-grid-box widgets category-list-item {{ $mergeClass ?? ''}}" style="background:white">
	<h2 class="widget-title">{{ trans('multi-leng.frontend_categories') }}</h2>
	<ul class="category-list  pr-2">
        @foreach ($allCategories as $item)
		<li>
			<a href="{{ route('f.pages.categories', $item->slug ) }}">{{ $item->title }}
				<span class="ts-{{ $item->color }}-bg">{{ $item->posts->count() }}</span>
			</a>
		</li>
        @endforeach
	</ul>
</div>

<div class="widgets post-list-item">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist" style="background:white">
		<li role="presentation">
			<a class="active" href="#home" aria-controls="home" role="tab" data-toggle="tab">
				<i class="fa fa-clock-o"></i>
				{{ trans('multi-leng.frontend_recent') }}
			</a>
		</li>
		<li role="presentation">
			<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
				<i class="fa fa-star"></i>
                {{ trans('multi-leng.frontend_top_post') }}
			</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active ts-grid-box post-tab-list" id="home">
            @foreach ($recentPosts as $item)
			<div class="post-content media">
				<img class="d-flex sidebar-img" src="{{ asset('thumbnail/'.$item->thumbnail) }}" alt="{{ Str::limit($item->title,80) }}">
				<div class="media-body">
					<span class="post-tag">
						<a href="{{ route('f.pages.categories', $item->category->slug ) }}" class="{{ $item->category->color }}-color"> {{ $item->category->title }}</a>
					</span>
					<h4 class="post-title">
						<a href="{{ route('f.pages.post', ['slug1' => $item->category->slug, 'slug2'=> $item->slug]) }}">
							{{ Str::limit($item->title,30) }}
						</a>
					</h4>
				</div>
			</div>
            @endforeach
			<!--post-content end-->
		</div>
		<!--ts-grid-box end -->

		<div role="tabpanel" class="tab-pane ts-grid-box post-tab-list" id="profile">
            @foreach ( $postMoreViewsWeek as $item )
			<div class="post-content media">
				<img class="d-flex sidebar-img" src="{{ asset('thumbnail/'.$item->post->thumbnail) }}" alt="">
				<div class="media-body">
					<span class="post-tag">
						<a href="{{ route('f.pages.categories', $item->post->category->slug ) }}" class="{{ $item->post->category->color }}-color">{{  $item->post->category->title }}</a>
					</span>
					<h4 class="post-title">
						<a href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2'=> $item->post->slug]) }}">{{  $item->post->title }} </a>
					</h4>
				</div>
			</div>
            @endforeach
			<!--post-content end-->

		</div>
		<!--ts-grid-box end -->
	</div>
	<!-- tab content end-->
</div>

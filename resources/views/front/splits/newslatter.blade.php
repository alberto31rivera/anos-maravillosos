<section class="ts-newslatter">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-lg-7">
				<div class="ts-newslatter-content">
					<h2>
						Sign up for the Newsletter
					</h2>
				</div>
			</div>
			<!-- col end-->

			<div class="col-md-8 col-lg-7 align-self-center">
				<div class="newsletter-form">
					<form action="#" method="post" class="media align-items-end">
						<div class="email-form-group media-body">
							<i class="fa fa-paper-plane" aria-hidden="true"></i>
							<input type="email" name="email" id="newsletter-form-email" class="form-control" placeholder="Enter Your Email" autocomplete="off">
						</div>
						<div class="d-flex ts-submit-btn">
							<button class="btn btn-primary">Subscribe</button>
						</div>
					</form>
				</div>
				<p class="mt-4 mb-0">
					Join our newsletter and get updates in your inbox. We won’t spam you and we respect your privacy.
				</p>
			</div>
		</div>
	</div>
</section>

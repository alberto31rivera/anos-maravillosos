<div id="content-banner-2" class="block-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="ad-banner text-center mb-15 pb-3">
					@if ($banner != null)
                    @if ($banner->advertising_three !=null && $banner->url_advertising_three != null)
                    <a target="_blank" href="{{ $banner->url_advertising_three }}">
						<img class="img-fluid img-banner-ad" src="{{asset('img/homepage/advertisements/'.$banner->advertising_three)}}" alt="">
					</a>
                    @endif
                    @endif
				</div>
			</div>
			<!-- col end -->
		</div>
		<!-- row  end -->


	</div>
	<!-- container end -->
</div>

<div class="ts-grid-box widgets category-list-item">
	<h2 class="widget-title">{{ trans('multi-leng.user_control_panel') }}</h2>

	<div class="card list-menu--selected">
	  <ul class="list-group list-group-flush rounded-0">
	  	<a href="{{ route('f.panel.user') }}">
		    <li class="list-group-item @if ('f.panel.user' == Request::route()->getName()) active @endif">
	            <i class="fa fa-user-circle fa-lg mr-1"></i>
	            {{ trans('multi-leng.user_edit_profile') }}
			</li>
		</a>
		<a href="{{route('pages.ad-plans')}}">
		    <li class="list-group-item">
	            <i class="fa fa-info-circle fa-lg mr-1"></i>
	            {{ trans('multi-leng.user_advertising_plans') }}
			</li>
		</a>
		<a href="{{route('f.shopping.user')}}">
		    <li class="list-group-item @if ('f.shopping.user' == Request::route()->getName()) active @endif">
	            <i class="fa fa-shopping-bag fa-lg mr-1"></i>
	            {{ trans('multi-leng.user_shopping') }}
			</li>
		</a>
		<form method="POST" action="{{ route('logout') }}">
		    @csrf
			<a href="{{ route('logout') }}" onclick="event.preventDefault();
		        this.closest('form').submit();">
				<li class="list-group-item">
		        	<i class="fa fa-sign-out fa-lg mr-1"></i> {{ trans('multi-leng.user_logout') }}
				</li>
			</a>
		</form>
	  </ul>
	</div>


	{{-- <ul class="category-list">
		<li>
			<a href="{{ route('f.panel.user') }}">
                <i class="fa fa-user-circle"></i>
                {{ trans('multi-leng.user_edit_profile') }}
			</a>
		</li>
        <li>
			<a href="{{route('pages.ad-plans')}}">
                <i class="fa fa-info-circle"></i>
                {{ trans('multi-leng.user_advertising_plans') }}
			</a>
		</li>
        <li>
			<a href="{{route('f.shopping.user')}}">
                <i class="fa fa-info-circle"></i>
                {{ trans('multi-leng.user_shopping') }}
			</a>
		</li>
		<!--
		<li>
			<a href="#"><i class="fa fa-lock"></i> Cambiar Contraseña</a>
		</li>
		-->
		<li>
            <form method="POST" action="{{ route('logout') }}">
                @csrf
			<a href="{{ route('logout') }}" onclick="event.preventDefault();
            this.closest('form').submit();"><i class="fa fa-sign-out"></i> {{ trans('multi-leng.user_logout') }}
			</a>
           </form>
		</li>

	</ul> --}}
</div>
<!-- widgets end-->

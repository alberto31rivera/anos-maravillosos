<footer class="ts-footer" style="background:@if($themeSettings != null) @if ($themeSettings->color_footer != null) {{ $themeSettings->color_footer }}
    @else black @endif  @endif">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
                @if ($services != null)
				<div class="footer-menu text-center">
					<ul>
                        @if ($services->title_one != null && $services->status_one == 1)
                        <li>
							<a style="color:@if($themeSettings != null) @if ($themeSettings->color_text_footer!= null) {{ $themeSettings->color_text_footer}}
                                @else black @endif  @endif" href="{{ route('service.footer.one', $services->slug_one) }}">{{ $services->title_one }}</a>
						</li>
                        @endif
                        @if ($services->title_two != null && $services->status_two == 1)
                        <li>
							<a style="color:@if($themeSettings != null) @if ($themeSettings->color_text_footer!= null) {{ $themeSettings->color_text_footer}}
                                @else black @endif  @endif" href="{{ route('service.footer.two', $services->slug_two) }}">{{ $services->title_two }}</a>
						</li>
                        @endif
						@if ($services->title_three != null && $services->status_three == 1)
                        <li>
							<a style="color:@if($themeSettings != null) @if ($themeSettings->color_text_footer!= null) {{ $themeSettings->color_text_footer}}
                                @else black @endif  @endif" href="{{ route('service.footer.three', $services->slug_three) }}">{{ $services->title_three }}</a>
						</li>
                        @endif
						@if ($services->title_four != null && $services->status_four == 1)
                        <li>
							<a style="color:@if($themeSettings != null) @if ($themeSettings->color_text_footer!= null) {{ $themeSettings->color_text_footer}}
                                @else black @endif  @endif" href="{{ route('service.footer.four', $services->slug_four) }}">{{ $services->title_four }}</a>
						</li>
                        @endif
						@if ($services->title_five != null && $services->status_five == 1)
                        <li>
							<a style="color:@if($themeSettings != null) @if ($themeSettings->color_text_footer!= null) {{ $themeSettings->color_text_footer}}
                                @else black @endif  @endif" href="{{ route('service.footer.five', $services->slug_five) }}">{{ $services->title_five }}</a>
						</li>
                        @endif
						@if ($services->title_six != null && $services->status_six == 1)
                        <li>
							<a style="color:@if($themeSettings != null) @if ($themeSettings->color_text_footer!= null) {{ $themeSettings->color_text_footer}}
                                @else black @endif  @endif" href="{{ route('service.footer.six', $services->slug_six) }}">{{ $services->title_six }}</a>
						</li>
                        @endif
					</ul>
				</div>
                @endif
				<div class="copyright-text text-center">
                    @if ($banner != null)
                    @if ($banner->title_eight != null)
                    <p style="color:@if($themeSettings != null) @if ($themeSettings->color_text_footer!= null) {{ $themeSettings->color_text_footer}}
                        @else white @endif  @endif">&copy; {{ $banner->title_eight }}</p>
                    @else
                    <p style="color:@if($themeSettings != null) @if ($themeSettings->color_text_footer!= null) {{ $themeSettings->color_text_footer}}
                        @else white @endif  @endif">&copy; 2018. Jiraco. All rights reserved</p>
                    @endif
                    @else
                    <p style="color:@if($themeSettings != null) @if ($themeSettings->color_text_footer!= null) {{ $themeSettings->color_text_footer}}
                        @else white @endif  @endif">&copy; 2018. Jiraco. All rights reserved</p>
                    @endif
				</div>
			</div>
		</div>
		<div id="back-to-top" class="back-to-top">
			<button class="btn btn-primary" title="Back to Top" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
            @else #e91e63 @endif  @endif">
				<i class="fa fa-angle-up"></i>
			</button>
		</div>
	</div><!-- Container end-->
</footer>

<div id="content-banner-3" class="block-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="ad-banner text-center mb-15 pb-3">
					@if ($banner != null)
                    @if ($banner->advertising_four !=null && $banner->url_advertising_four != null)
                    <a target="_blank" href="{{ $banner->url_advertising_four }}">
						<img class="img-fluid img-banner-ad" src="{{asset('img/homepage/advertisements/'.$banner->advertising_four)}}" alt="">
					</a>
                    @endif
                    @endif
				</div>
			</div>
			<!-- col end -->
		</div>
		<!-- row  end -->


	</div>
	<!-- container end -->
</div>

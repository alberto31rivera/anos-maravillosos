<div class="widgets ts-grid-box  widgets-populer-post">
	<h3 class="widget-title">Populer Post</h3>
	<div class="ts-overlay-style">
		<div class="item">
            @foreach ( $postMoreViewsMonth as $item )
            @if ($loop->first)
			<div class="ts-post-thumb">
				<a href="{{ route('f.pages.post', ['slug1' => $item->post->slug, 'slug2'=> $item->post->slug]) }}">
					<img class="img-fluid" src="{{ asset('img/'.$item->post->image) }}" alt="">
				</a>
			</div>
			<div class="overlay-post-content">
				<div class="post-content">
					<h3 class="post-title">
						<a href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2'=> $item->post->slug]) }}">{{ $item->post->title }}</a>
					</h3>
					<ul class="post-meta-info">
						<li>
							<i class="fa fa-clock-o"></i>
							March 21, 2019
						</li>
					</ul>
				</div>
			</div>
            @else
            @break
            @endif
            @endforeach
		</div>
	</div>
	<!-- ts-overlay-style  end-->
    @foreach ( $postMoreViewsMonth as $item )
    @if (!$loop->first)
	<div class="post-content media">
		<img class="d-flex sidebar-img" src="{{ asset('img/'.$item->post->image) }}" alt="">
		<div class="media-body align-self-center">
			<h4 class="post-title">
				<a href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2'=> $item->post->slug]) }}">{{ $item->post->title }}</a>
			</h4>
		</div>
	</div>
	<!-- post content end-->
    @endif
    @endforeach
</div>

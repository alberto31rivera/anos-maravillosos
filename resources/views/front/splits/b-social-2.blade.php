<section class="ts-footer-social-list">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="footer-logo">
					<a href="#">
						<img src="{{asset('assets/front/template/images/logo/footer_logo.png')}}" alt="">
					</a>
				</div>
				<!-- footer logo end-->
			</div>
			<!-- col end-->

			<div class="col-lg-8 align-self-center">
				<ul class="footer-social">
					<li class="ts-facebook">
						<a href="#">
							<i class="fa fa-facebook"></i>
							<span>Facebook</span>
						</a>
					</li>
					<li class="ts-google-plus">
						<a href="#">
							<i class="fa fa-google-plus"></i>
							<span>Google Plus</span>
						</a>
					</li>
					<li class="ts-twitter">
						<a href="#">
							<i class="fa fa-twitter"></i>
							<span>Twitter</span>
						</a>
					</li>
					<li class="ts-pinterest">
						<a href="#">
							<i class="fa fa-pinterest-p"></i>
							<span>pinterest</span>
						</a>
					</li>
					<li class="ts-linkedin">
						<a href="#">
							<i class="fa fa-linkedin"></i>
							<span>Linkedin</span>
						</a>

					</li>
				</ul>
			</div>
			<!-- col end-->

		</div>
	</div>
</section>
<div class="widgets widgets-item {{ $margeClass ?? '' }}">
    <div class="ts-widget-newsletter" style="background:@if ($themeSettings !=null)  @if ($themeSettings->color_contact !=null)
        {{ $themeSettings->color_contact }}
        @else #d72924 @endif @endif">
        <div class="newsletter-introtext">
            <h4>{{ trans('multi-leng.frontend_contact_us_floating') }}</h4>
            <p>{{ trans('multi-leng.frontend_consultations_suggestions') }}</p>
        </div>

        <div class="newsletter-form">
            @if (Session::has('success-message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('success-message') }}
                </div>
            @endif
            <form action="{{ route('contact.us') }}" method="post"  role="form">
                @csrf
                <div class="form-group">

                    <input type="text" name="name" id="newsletter-form-name" class="form-control form-control-lg"
                        required placeholder="{{ trans('multi-leng.frontend_contact_us_floating_name') }}" autocomplete="off">

                    <input type="text" name="cellphone" id="newsletter-form-cellphone"
                        class="form-control form-control-lg" required placeholder="{{ trans('multi-leng.frontend_contact_us_floating_cellphone') }}" autocomplete="off">

                    <input type="email" name="email" id="newsletter-form-email" class="form-control form-control-lg"
                        required placeholder="{{ trans('multi-leng.frontend_contact_us_floating_email') }}" autocomplete="off">

                    <textarea name="message" id="" class="form-control form-control-lg" placeholder="{{ trans('multi-leng.frontend_contact_us_floating_message') }}" required
                        style="height:100px"></textarea>

                    <button class="btn btn-primary" style="background:@if ($themeSettings
                        !=null)  @if ($themeSettings->color_buttons
                        !=null) {{ $themeSettings->color_buttons }}
                        @else #e91e63 @endif @endif">{{ trans('multi-leng.frontend_contact_us_floating_send') }}</button>

                </div>
            </form>
        </div>
    </div>
</div>

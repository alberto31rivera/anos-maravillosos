<div class="ts-grid-box widgets ts-social-list-item">
	<h3 class="ts-title">Síguenos:</h3>
	<ul>
		<li class="ts-facebook">
			<a href="#">
				<i class="fa fa-facebook"></i>
				<b>5.5 k </b>
				<span>Likes</span>
			</a>

		</li>
		<li class="ts-google-plus">
			<a href="#">
				<i class="fa fa-google-plus"></i>
				<b>5.5 k </b>
				<span>Follwers</span>
			</a>

		</li>
		<li class="ts-twitter">
			<a href="#">
				<i class="fa fa-twitter"></i>
				<b>12.5 k </b>
				<span>Follwers</span>
			</a>
		</li>
		<li class="ts-pinterest">
			<a href="#">
				<i class="fa fa-pinterest-p"></i>
				<b>5.5 k </b>
				<span>Photos</span>
			</a>
		</li>
		<li class="ts-linkedin">
			<a href="#">
				<i class="fa fa-linkedin"></i>
				<b>5.5 k </b>
				<span>Follwers</span>
			</a>
		</li>
		<li class="ts-youtube">
			<a href="#">
				<i class="fa fa-youtube"></i>
				<b>5.5 k </b>
				<span>Follwers</span>
			</a>
		</li>
		<li class="ts-instragram">
			<a href="#">
				<i class="fa fa-instagram"></i>
				<b>5.5 k </b>
				<span>Follwers</span>
			</a>
		</li>
		<li class="ts-dribble">
			<a href="#">
				<i class="fa fa-dribbble"></i>
				<b>5.5 k </b>
				<span>Follwers</span>
			</a>
		</li>
		<li class="ts-behance">
			<a href="#">
				<i class="fa fa-behance"></i>
				<b>5.5 k </b>
				<span>Follwers</span>
			</a>
		</li>
	</ul>
</div>

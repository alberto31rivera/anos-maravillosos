<!-- Modal -->
<div class="modal fade modal--radios" id="receiptModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content rounded-0 border-0" style="background-color:#E91E63">
      <div class="modal-header pt-1 pb-1 rounded-0 border-0 bg-white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pt-1 pb-0 px-0">

        <!--Content-->
        <div class="card rounded-0 border-0 ts-grid-box">
          <div class="card-header bg-white d-flex justify-content-md-between align-items-end">
             @if ($admin->logo == '')
              <img class="img-fluid" src="{{asset('assets/front/template/images/logo/logo-v2.png')}}" alt="">
             @else
              <img class="img-fluid" src="{{asset('img/logos-users/'.$admin->logo)}}" alt="">
             @endif
             <span class="text-muted">Date: 28 Ene, 2021</span>
          </div>
          <div class="card-body">
            <div class="contaner">
              <div class="row">
                <div class="col-md-4">
                  <h3 class="mb-0">Jiraco</h3>
                  <p>
                    <span class="font-weight-bold">RUC 2058957855</span>
                    <span>Av. Primavera 120 Chacarilla Surco</span>
                    <span>Lima</span>
                    <span>Peru</span>
                    <span>Email. info@fmsanfracisco.net</span>
                  </p>
                </div>
                <div class="col-md-4">
                  <p>
                    <span><strong>Usuario 2</strong> - Empresa 1</span>
                    <span>Nro. Doc o Registro</span>
                    <span>Dirección 1</span>
                    <span>Ciudad</span>
                    <span>Argentina</span>
                    <span>Email: 2@jiraco.com</span>
                  </p>
                </div>
                <div class="col-md-4">
                  <p>
                    <span><strong>Factura</strong> #0006</span>
                    <span><strong>Fecha de pago</strong> 28 Ene, 2021</span>
                  </p>
                </div>
              </div>
              <div class="row">
                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Descripción</th>
                        <th scope="col" class="text-right">Subtotal</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur voluptatibus animi ipsa nulla minus, perspiciatis quis doloremque repellendus.</td>
                        <td class="wspace-nrap text-right">$20.000 USD</td>
                      </tr>
                      <tr>
                        <td colspan="3" class="text-right">
                          <span class="font-weight-bold">Total:</span> 20.000 USD
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <button type="button" class="btn btn-primary"><i class="fa fa-print"></i> Imprimir</button>
          </div>
        </div> <!--/card-->
        <!--/Content-->

      </div>
    </div>
  </div>
</div>

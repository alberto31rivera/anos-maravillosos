 
 
    
 

				<div class="popular-post mix">
                                <div class="sidebar-title">
                                    <h3>Popular Posts</h3>
                                </div>
								@foreach ( $postMoreViewsMonth as $item )
    							@if (!$loop->first)
                                <div class="popular-item">
                                    <div class="popular-content">
                                        <span class="item-no">{{ $loop->index }} </span>
                                        <h4><a href="{{ route('f.pages.post', ['slug1' => $item->post->category->slug, 'slug2'=> $item->post->slug]) }}">
										{{ $item->post->title }}</a></h4>
                                        <div class="author-detail">
                                            
                                            <p><i class="icon-clock"></i> {{ $item->created_at->toFormattedDateString() }}</p>
                                           
                                        </div>
                                    </div>
                                </div>
                                @endif
    							@endforeach 
                                 
                </div>

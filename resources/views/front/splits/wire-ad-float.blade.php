<section id="s-float-ad"
    class="float-ad container-fluid fixed-top d-flex justify-content-center align-items-center py-0 element-hidden"
    wire:ignore.self>
    @if ($advertising != null)

    <div class="row wrapper">
        <!--RESOURCES-->
        <div
         class="col-sm-6  col-md-8 col-lg-9 box-resource bg-deep-dark position-relative px-0  d-flex justify-content-center align-items-center"> {{--{{ $advertising->type_advertising != 'Audio' ? 'col-md-8 col-lg-9' : '' }}--}}

            @if ($advertising->type_advertising == 'Foto')
                {{-- <div id="gallery" style="display:none;">
                    <img alt="" src="{{ asset('img/advertisements/'.$advertising->image) }}"
                        data-image="{{ asset('img/advertisements/'.$advertising->image)  }}"
                        data-description="">
                </div> --}}
                <div>
                    <img alt="" src="{{ asset('img/advertisements/'.$advertising->image) }}"
                        data-image=""
                        data-description=""
                        class="img-fluid">
                </div>

            @elseif( $advertising->type_advertising  == 'Video' )
                <div class="embed-responsive embed-responsive-16by9">

                    <iframe src="https://www.youtube.com/embed/{{str_replace('https://www.youtube.com/watch?v=','',$advertising->video)}}" frameborder="0" allowfullscreen></iframe>

                </div>

            @elseif( $advertising->type_advertising  == 'Audio' )
                <div class="content-audio text-center">
                    <audio controls style="height:revert">
                        <source src="{{ asset('audio/advertisements/' . $advertising->audio) }}"
                            type="audio/mp3">
                        Your browser does not support the audio tag.
                    </audio>
                </div>
            @endif

            <a role="button" onclick="switchEventAd({{ $action }})" class="btn btn-light btn--close-ad">
                <i class="fa fa-times fa-lg" aria-hidden="true"></i>
            </a>
            <a href="#" wire:click="previous()" class="btn btn-light btn--prev-ad trigger-prev">
                <i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>
            </a>
            <a href="#" wire:click="next()" class="btn btn-light btn--next-ad trigger-next">
                <i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>
            </a>
        </div>

        <!--DETAILS-->
        <div class="col-sm-6 col-md-4 col-lg-3 box-data mt-3 mt-sm-0">
            <!--Card Footer-->
            <div class="ts-grid-box d-flex align-items-center card-footer py-3 mb-3">
                <div class="widget contact-info w-100">
                    <h3 class="ts-title text-uppercase mb-0 d-none d-sm-block">{{ trans('multi-leng.frontend_advertising_advertiser') }}</h3>

                    {{-- Only xs --}}
                    <div class="row no-gutters">
                        <h3 class="ts-title text-uppercase mb-0 d-inline d-sm-none col-auto d-flex align-items-center">{{ trans('multi-leng.frontend_advertising_follow') }}</h3>
                        <ul class="list-unstyled mb-0 d-inline-flex d-sm-none col-auto">
                            @if ($advertising->vendor->facebook != '')
                            <li>
                                <a href="{{ $advertising->vendor->facebook }}" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->web_site != '')
                            <li>
                                <a href="{{ $advertising->vendor->web_site }}" target="_blank"><i class="fa fa-globe fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->instagram != '')
                            <li>
                                <a href="{{ $advertising->vendor->instagram  }}" target="_blank"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->twitter != '')
                            <li>
                                <a href="{{ $advertising->vendor->twitter  }}" target="_blank"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->linkedin != '')
                            <li>
                                <a href="{{ $advertising->vendor->linkedin  }}" target="_blank"><i class="fa fa-linkedin fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->youtube != '')
                            <li>
                                <a href="{{ $advertising->vendor->youtube  }}" target="_blank"><i class="fa fa-youtube fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                        </ul>
                    </div>

                    <hr class="my-2">
                    <div class="contact-info-box">
                        <div class="contact-info-box-content">
                            <h4 class="mb-0">{{ trans('multi-leng.frontend_advertising_name') }}</h4>
                            <p class="mb-2">{{ $advertising->vendor->name }}</p>
                        </div>
                    </div>
                    <div class="contact-info-box">
                        <div class="contact-info-box-content">
                            <h4 class="mb-0">{{ trans('multi-leng.frontend_advertising_dni') }}</h4>
                            <p class="mb-2">{{ $advertising->vendor->dni }}</p>
                        </div>
                    </div>
                    <div class="contact-info-box">
                        <div class="contact-info-box-content">
                            <h4 class="mb-0">{{ trans('multi-leng.frontend_advertising_address') }}</h4>
                            <p class="mb-2">{{ $advertising->vendor->address }}</p>
                        </div>
                    </div>
                    <hr class="my-2 d-block d-sm-none d-xl-block">
                    <div class="contact-info-box">
                        <div class="contact-info-box-content">
                            {{-- Only xs --}}
                            <h3 class="ts-title text-uppercase mb-0 d-sm-none">{{ trans('multi-leng.frontend_advertising_contact') }}</h3>
                            {{-- for: sm|md|lg|xl --}}
                            <h4 class="mb-0 mr-1 d-none d-sm-block">{{ trans('multi-leng.frontend_advertising_contact') }}</h4>

                            <ul class="list-unstyled mb-0 d-inline-flex">
                                @if ($advertising->vendor->cellphone != '')
                                <li class="mr-lg-1">
                                    <a href="tel:{{ $advertising->vendor->cellphone }}"><i
                                        class="fa fa-phone fa-lg" aria-hidden="true"></i></a>
                                </li>
                                @endif
                                @if ($advertising->vendor->whatsapp != '')
                                <li class="mr-lg-1">
                                    <a href="https://api.whatsapp.com/send?phone={{ $advertising->vendor->whatsapp }}" target="_blank"><i
                                        class="fa fa-whatsapp fa-lg" aria-hidden="true"></i></a>
                                </li>
                                @endif
                                @if ($advertising->vendor->email != '')
                                <li class="mr-lg-1">
                                    <a href="mailto:{{ $advertising->vendor->email }}"><i
                                        class="fa fa-envelope fa-lg" aria-hidden="true"></i></a>
                                </li>
                                @endif
                                {{-- <li class="mr-lg-1">
                                    <a href="#"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i></a>
                                </li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--Card Header-->
            <div class="ts-grid-box align-items-center card-header py-3 mb-0 d-none d-sm-flex">
                <img class="img-fluid rounded-circle mr-3" src="{{ asset('img/avatar-users/profile.png') }}" alt="">
                <h3 class="post-title md mb-0">{{ $advertising->title }}</h3>
            </div>
            <!--Card Body-->
            <div class="ts-grid-box flex-column card-body py-3 mb-0 d-none d-sm-flex">
                {{-- <span class="post-date-info post-cat bg-dark text-white" style="right:0">
                    <i class="fa fa-clock-o"></i> Apr 16, 2021
                </span> --}}
                <div class="rich-text-ad">
                    <!--BEGIN: TEXTO ENRIQUECIDO-->
                    {!! $advertising->description !!}
                    <!--END: TEXTO ENRIQUECIDO-->
                </div>
                <hr class="mb-2">

                <div class="row no-gutters">
                        <h4 class="text-muted mb-0 mr-1 col-auto d-flex align-items-center" style="font-weight:500;">{{ trans('multi-leng.frontend_advertising_follow') }}</h4>
                        <ul class="list-unstyled mb-0 d-inline-flex col-auto">
                            {{-- <li class="d-flex align-items-center">
                                <h4 class="text-muted mb-0 mr-1" style="font-weight:500;">{{ trans('multi-leng.frontend_advertising_follow') }}</h4>
                            </li> --}}
                            @if ($advertising->vendor->facebook != '')
                            <li>
                                <a href="{{ $advertising->vendor->facebook }}" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->web_site != '')
                            <li>
                                <a href="{{ $advertising->vendor->web_site }}" target="_blank"><i class="fa fa-globe fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->instagram != '')
                            <li>
                                <a href="{{ $advertising->vendor->instagram  }}" target="_blank"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->twitter != '')
                            <li>
                                <a href="{{ $advertising->vendor->twitter  }}" target="_blank"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->linkedin != '')
                            <li>
                                <a href="{{ $advertising->vendor->linkedin  }}" target="_blank"><i class="fa fa-linkedin fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                            @if ($advertising->vendor->youtube != '')
                            <li>
                                <a href="{{ $advertising->vendor->youtube  }}" target="_blank"><i class="fa fa-youtube fa-lg" aria-hidden="true"></i></a>
                            </li>
                            @endif
                        </ul>
                </div>

            </div>
        </div>
    </div>
    <!--/.row.wrapper-->
    @endif
</section>


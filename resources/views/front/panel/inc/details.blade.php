<div class="ts-grid-box content-wrapper single-post pb-lg-0">
    <div class="entry-header">
        <h3 class="post-title lg"> {{ trans('multi-leng.user_welcome') }} {{ Auth::user()->name }}</h3>
        <ul class="post-meta-info  mb-0">
            <li class="mb-0">
                <i class="fa fa-clock-o"></i>
                {{ Auth::user()->created_at->toFormattedDateString() }}
            </li>
        </ul>
    </div>
    <!-- single post header end-->
</div>
<!-- cambiar logo  -->

<form action="{{ route('update.user', Auth::user()->id) }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="col-12 col-md-6 ts-grid-box pb-5 change-img  pt-lg-4">
        <h2 class="widget-title" style="margin-bottom: 5px;">{{ trans('multi-leng.user_change_photo') }}</h2>
        <div class="author-box author-box-item pb-3">
            @if (Auth::user()->is_provider == 1)
               @if(file_exists( public_path().'/img/avatar-users/'.Auth::User()->avatar ))
               <img id="img-author" src="{{asset('img/'.Auth::user()->avatar)}}" alt="">
               @else
               <img id="img-author" src="{{asset(Auth::user()->avatar)}}" alt="">
               @endif
            @endif
            @if (Auth::user()->is_provider == 0)
            @if (Auth::user()->avatar == null)
            <img id="img-author" class="author-img" src="{{asset('img/avatar-users/profile.png')}}" alt="">
            @else
            <img id="img-author" class="author-img" src="{{asset('img/'.Auth::user()->avatar)}}" alt="">
            @endif
            @endif
            <div class="author-info">
                <div class="author-name pt-4">
                    <h4>{{ trans('multi-leng.user_select_photo') }}</h4>
                    <input type="file" class="form-control-file" id="avatar" name="avatar">
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
    <div class="col-md-6"></div>

    <div class="col-md-12 ts-grid-box pb-5 details-account  mb-3" style="padding-top:30px;">
        @if (session('notification'))
        <div class="alert alert-success">
            {{ session('notification') }}
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        <h2 class="widget-title pl-xs-30" style="margin-bottom: 5px;">{{ trans('multi-leng.user_account_details') }}</h2>
        <div class="row justify-content-center">
            <div class="col-10 col-sm-12">
        <div class="author-box author-box-item px-0">
            <div class="ts-grid-box  px-0 pb-0" style="box-shadow:none">
                <div class="reg-page">
                        <div class="form-row">
                            <div class="col form-group">
                                <label>{{ trans('multi-leng.user_name') }} *</label>
                                    <input type="text" value="{{Auth::user() != null ? Auth::user()->name: ''}}" name="name" required class="form-control" placeholder="">
                            </div> <!-- form-group end.// -->
                            <div class="form-group col-md-6">
                                <label>{{ trans('multi-leng.user_cellphone') }} *</label>
                                <input type="number" name="cellphone" value="{{Auth::user() != null ? Auth::user()->cellphone: ''}}" required class="form-control" placeholder="">
                            </div> <!-- form-group end.// -->
                        </div> <!-- form-row end.// -->
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>{{ trans('multi-leng.user_email') }}</label>
                                <input type="email" name="email" readonly value="{{Auth::user() != null ? Auth::user()->email: ''}}" class="form-control" placeholder="">
                            </div> <!-- form-group end.// -->
                            <div class="form-group col-md-6">
                                <label>{{ trans('multi-leng.user_type_document') }}</label>
                                <select name="type_document" class="form-control">
                                    <option selected disabled >{{ trans('multi-leng.user_select_type_document') }}</option>
                                    <option value="dni" {{ Auth::user() != null ?  ( Auth::user()->type_document == 'dni' ? 'selected' : '') : false  }}>{{ trans('multi-leng.user_number_dni') }}</option>
                                    <option value="ruc" {{ Auth::user() != null ?  ( Auth::user()->type_document == 'ruc' ? 'selected' : '') : false  }}>{{ trans('multi-leng.user_number_ruc') }}</option>
                                    <option value="ce" {{ Auth::user() != null ?  ( Auth::user()->type_document == 'ce' ? 'selected' : '') : false  }}>{{ trans('multi-leng.user_number_ce') }}</option>
                                </select>
                            </div> <!-- form-group end.// -->
                        </div> <!-- form-row.// -->
                        <div class="form-row">
                            <div class="col-md-6 form-group">
                                <label>{{ trans('multi-leng.user_number_document') }}</label>
                                    <input type="text" value="{{Auth::user() != null ? Auth::user()->number_document: ''}}" name="number_document" class="form-control" placeholder="">
                            </div> <!-- form-group end.// -->
                            <div class="form-group col-md-6">
                                <label>{{ trans('multi-leng.user_address') }}</label>
                                <input type="text" name="address" value="{{Auth::user() != null ? Auth::user()->address: ''}}" class="form-control" placeholder="">
                            </div> <!-- form-group end.// -->
                        </div> <!-- form-row end.// -->
                        <div class="form-row">
                            <div class="col-md-6 form-group">
                                <label>{{ trans('multi-leng.user_city') }}</label>
                                    <input type="text" value="{{Auth::user() != null ? Auth::user()->city: ''}}" name="city" class="form-control" placeholder="">
                            </div> <!-- form-group end.// -->
                            <div class="form-group col-md-6">
                                <label>{{ trans('multi-leng.user_country') }}</label>
                                <input type="text" name="country" value="{{Auth::user() != null ? Auth::user()->country: ''}}" class="form-control" placeholder="">
                            </div> <!-- form-group end.// -->
                        </div> <!-- form-row end.// -->
                        <div class="form-row">
                            <div class="col-sm-6 form-group">
                                <label>{{ trans('multi-leng.user_previous_password') }}</label>
                                <input class="form-control" name="password" type="password">
                            </div> <!-- form-group end.// -->
                            <div class="col-sm-6 form-group">
                                <label>{{ trans('multi-leng.user_new_password') }}</label>
                                <input class="form-control" name="confirm_password" type="password">
                            </div> <!-- form-group end.// -->
                        </div> <!-- form-row.// -->
                        <div class="form-group my-4">
                            <button type="submit" class="btn btn-primary btn-block pt-2" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
                                @else #e91e63 @endif  @endif">{{ trans('multi-leng.user_save') }}</button>
                        </div> <!-- form-group// -->
                    </form>

                </div> <!-- card.// -->

            </div><!-- grid box end -->

        </div>

            </div>
        </div>

    </div>


</form>

@extends('front.pages.layouts')

@section('styles')
<style>
	.widgets.widgets-item {
		padding-bottom: 30px
	}

	.ts-grid-box.change-img,
	.ts-grid-box.details-account {
		padding-left: 15px;
		padding-right: 15px;
	}
	.ts-grid-box.change-img .widget-title:before,
	.ts-grid-box.details-account .widget-title:before {
		left: -15px;
	}
	@media(min-width:768px) and (max-width:991px) {
		.ts-grid-box.change-img,
		.ts-grid-box.details-account {
			padding-left: 40px;
			padding-right: 40px;
		}
		.ts-grid-box.change-img .widget-title:before,
		.ts-grid-box.details-account .widget-title:before {
			left: -40px;
		}
	}
	@media(min-width:992px) and (max-width:1199px) {
		.ts-grid-box.change-img,
		.ts-grid-box.details-account {
			padding-left: 80px;
			padding-right: 80px;
		}
		.ts-grid-box.change-img .widget-title:before,
		.ts-grid-box.details-account .widget-title:before {
			left: -80px;
		}
	}
	@media(min-width:1200px) {
		.ts-grid-box.change-img,
		.ts-grid-box.details-account {
			padding-left: 80px;
			padding-right: 80px;
		}
		.ts-grid-box.change-img .widget-title:before,
		.ts-grid-box.details-account .widget-title:before {
			left: -80px;
		}
	}

	.card-body.table-responsive::-webkit-scrollbar {
	  height: 8px;
	  /*background: #F0F0F0;*/
	}
	.card-body.table-responsive::-webkit-scrollbar-thumb {
	  background: rgba(0,0,0,.125);
	  border-radius: 5px;
	  border-bottom: 2px solid #e91e63;
	}
</style>
@endsection

@section('content')

<section class="block-wrapper mt-15">
	<div class="container">
		<div class="row">

			<div class="col-lg-3">
				<div class="right-sidebar">
					@include('front.splits.w-control')

					@include('front.splits.w-contact-us', ['margeClass'=>'d-none d-lg-block'])
				</div>
				<!-- right sidebar end-->
			</div>

			<div class="col-lg-9">
        <livewire:manage-table-shoppings/>
			</div>
			<!-- col end-->

			<div class="col-lg-12 d-lg-none" style="margin-top:14px">
				<div class="right-sidebar">
					@include('front.splits.w-contact-us')
				</div>
				<!-- *movil-button* sidebar end-->
			</div>
			<!-- col end-->

		</div>
		<!-- row end-->
	</div>
	<!-- container end-->

</section>

@endsection

@section('scripts')
<script>
	/* preview: author photo */
document.getElementById("avatar").onchange = function(e) {
  let reader = new FileReader();

  reader.readAsDataURL(e.target.files[0]);

  reader.onload = function(){
	let img = document.getElementById('img-author')
    img.src = reader.result;
  };
}

// $('#[idModal]').modal('show')
</script>
@endsection

@extends('front.pages.layouts')

@section('styles')
<style>
	.widgets.widgets-item {
		padding-bottom: 30px
	}

	.ts-grid-box.change-img,
	.ts-grid-box.details-account {
		padding-left: 15px;
		padding-right: 15px;
	}
	.ts-grid-box.change-img .widget-title:before,
	.ts-grid-box.details-account .widget-title:before {
		left: -15px;
	}
	@media(min-width:768px) and (max-width:991px) {
		.ts-grid-box.change-img,
		.ts-grid-box.details-account {
			padding-left: 40px;
			padding-right: 40px;
		}
		.ts-grid-box.change-img .widget-title:before,
		.ts-grid-box.details-account .widget-title:before {
			left: -40px;
		}
	}
	@media(min-width:992px) and (max-width:1199px) {
		.ts-grid-box.change-img,
		.ts-grid-box.details-account {
			padding-left: 80px;
			padding-right: 80px;
		}
		.ts-grid-box.change-img .widget-title:before,
		.ts-grid-box.details-account .widget-title:before {
			left: -80px;
		}
	}
	@media(min-width:1200px) {
		.ts-grid-box.change-img,
		.ts-grid-box.details-account {
			padding-left: 80px;
			padding-right: 80px;
		}
		.ts-grid-box.change-img .widget-title:before,
		.ts-grid-box.details-account .widget-title:before {
			left: -80px;
		}
	}
</style>
@endsection

@section('content')

<section class="block-wrapper mt-15">
	<div class="container">
		<div class="row">

			<div class="col-lg-3">
				<div class="right-sidebar">
					@include('front.splits.w-control')

				 
				</div>
				<!-- right sidebar end-->
			</div>

			<div class="col-lg-9">
				@include('front.panel.inc.details')
			</div>
		 

		</div>
		<!-- row end-->
	</div>
	<!-- container end-->

</section>

@endsection

@section('scripts')
<script>
	/* preview: author photo */
document.getElementById("avatar").onchange = function(e) {
  let reader = new FileReader();

  reader.readAsDataURL(e.target.files[0]);

  reader.onload = function(){
	let img = document.getElementById('img-author')
    img.src = reader.result;
  };
}

// $('#[idModal]').modal('show')
</script>
@endsection

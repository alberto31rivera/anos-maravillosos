@extends('front.pages.layouts')

@section('content')

<section id="account" class="account">
    <div class="container">
        <div class="row">

			<div class="col-md-4 col-xs-12">
			</div>
             
            <div class="col-md-4 col-xs-12 bg-primary" style="padding:30px 10px;">
                <div class="account-inner">
                    <div class="section-title">
                        <h3 style="color:white;">{{ trans('multi-leng.frontend_please_signup') }}</h3>
                    </div>

                    <!-- alerts -->
                    @if (session('notification'))
                    <div class="alert alert-success">
                        {{ session('notification') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                    <!-- fin alerts -->

                    <form action="{{ route('register.user') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label style="color:white;">Nombre:</label>
                            <input type="text" name="name" required class="form-control" placeholder="">
                        </div>
                        <div class="form-group">
                            <label style="color:white;">{{ trans('multi-leng.frontend_register_email') }}</label>
                            <input type="email" name="email" required class="form-control" placeholder="">
                        </div>
                        <div class="form-group">
                            <label style="color:white;">{{ trans('multi-leng.frontend_register_cellphone') }}</label>
                            <input type="number" name="cellphone" required class="form-control" placeholder="">
                        </div> 
                        <div class="form-group">
                            <label style="color:white;">{{ trans('multi-leng.frontend_register_password') }}</label>
                            <input class="form-control" required name="password" type="password">
                        </div>
                        <div class="form-group">
                            <label style="color:white;">{{ trans('multi-leng.frontend_register_confirm_password') }}</label>
                            <input class="form-control" required name="confirm_password" type="password">
                        </div> 
                        <div class="form-group">
                             
                            <button type="submit" class="btn-red btn-block" style="background:@if($themeSettings != null) @if ($themeSettings->color_buttons != null) {{ $themeSettings->color_buttons }}
                                @else #e91e63 @endif  @endif"> {{ trans('multi-leng.frontend_register') }}  </button>
                                <small style="color:black;" class="text-muted">{{ trans('multi-leng.frontend_register_policy') }}</small>
                        </div>    
                    </form>
                    <div class="border-top card-body text-center" style="padding-top:15px;">{{ trans('multi-leng.frontend_register_have_account') }} <a style="color:red;" href="{{ route('login') }}">{{ trans('multi-leng.frontend_register_log_in') }} </a></div>
                    
                </div>
            </div>

			<div class="col-md-4 col-xs-12">
			</div>

        </div>
    </div>
</section>

@endsection

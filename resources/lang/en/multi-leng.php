<?php

return [

    /*-------------- Start Admin Translate ----------------*/

    /**
     * Left Sidebar Translate
     * @author Sebastian Cabarcas
     */

    'general_settings' => 'General Settings',
    'homepage' => 'Homepage',
    'themes' => 'Themes',
    'emails' => 'Emails',
    'language' => 'Language',
    'social_links' => 'Social Links',
    'google' => 'Google',
    'documents' => 'Documents',
    'currencies' => 'Currencies',
    'manage_admin' => 'Manage Admin',
    'profile' => 'Profile',
    'regards' => 'Regards',
    'manage_users' => 'Manage Users',
    'users' => 'Users',
    'comments' => 'Comments',
    'manage_publications' => 'Manage Publications',
    'categories' => 'Categories',
    'posts' => 'Posts',
    'manage_programs' => 'Manage Programs',
    'programs' => 'Programs',
    'sponsors' => 'Sponsors',
    'manage_advertising' => 'Manage Advertising',
    'categories_advertising' => 'Categories',
    'plans' => 'Plans',
    'vendors' => 'Vendors',
    'advertisements' => 'Advertisements',
    'manage_payments' => 'Manage Payments',
    'paypal' => 'Paypal',
    'mercado_pago' => 'Mercado Pago',
    'transfer' => 'Transfer',
    'payments' => 'Payments',
    'exit' => 'Exit',


    /**
     * Dashboard Translate
     * @author Sebastian Cabarcas
     */

    'dashborad_advertisements' => 'Advertisements',
    'see_more' => 'See more',
    'net_earnings' => 'Net Earnings',
    'dashborad_vendors' => 'Vendors',
    'dashborad_posts' => 'Posts',
    'monthly_statistics' => 'Statistics for the month',
    'today_income' => 'Today income',
    'this_week_income' => 'This week income',
    'this_month_income' => 'This month income',
    'current_year_earnings' => 'Earnings in the current year',
    'month' => 'Month',
    'january' => 'January',
    'february' => 'February',
    'march' => 'March',
    'april' => 'April',
    'may' => 'May',
    'june' => 'June',
    'july' => 'July',
    'august' => 'August',
    'september' => 'September',
    'october' => 'October',
    'november' => 'November',
    'december' => 'December',

    /**
     * Manage Homepage Translate
     * @author Sebastian Cabarcas
     */

    'manage_homepage' => 'Manage Homepage',
    'homepage_announcements' => 'Homepage Announcements',
    'advertisement_one' => 'Advertisement 1',
    'advertisement_two' => 'Advertisement 2',
    'advertisement_three' => 'Advertisement 3',
    'advertisement_four' => 'Advertisement 4',
    'link_one' => 'Link 1',
    'link_two' => 'Link 2',
    'link_three' => 'Link 3',
    'link_four' => 'Link 4',
    'homepage_videos' => 'Homepage Videos',
    'url_video_one' => 'URL Video 1',
    'url_video_two' => 'URL Video 2',
    'url_video_three' => 'URL Video 3',
    'url_video_four' => 'URL Video 4',
    'url_video_five' => 'URL Video 5',
    'url_video_six' => 'URL Video 6',
    'description_one' => 'Description 1',
    'description_two' => 'Description 2',
    'description_three' => 'Description 3',
    'description_four' => 'Description 4',
    'description_five' => 'Description 5',
    'description_six' => 'Description 6',
    'image_one' => 'Image 1',
    'image_two' => 'Image 2',
    'image_three' => 'Image 3',
    'image_four' => 'Image 4',
    'image_five' => 'Image 5',
    'image_six' => 'Image 6',
    'web_titles' => 'Web Titles',
    'title_one' => 'Title 1',
    'title_two' => 'Title 2',
    'title_three' => 'Title 3',
    'title_four' => 'Title 4',
    'title_five' => 'Title 5',
    'title_six' => 'Title 6',
    'title_seven' => 'Title 7',
    'title_eight' => 'Title 8',
    'homepage_save' => 'Save',

    /**
     * Manage Themes Translate
     * @author Sebastian Cabarcas
     */

    'manage_themes' => 'Manage Themes',
    'web_colors' => 'Web Colors',
    'greeting_bar_color' => 'Greeting bar color',
    'color_text_regards' => 'Greeting bar text color',
    'color_text_header' => 'Header bar text color',
    'color_highlight_header' => 'Header bar highlight color',
    'color_text_footer' => 'Footer text color',
    'header_bar_color' => 'Header bar color',
    'footer_bar_color' => 'Footer bar color',
    'contact_us_color' => 'Contact Us Color',
    'button_color' => 'Button Color',
    'theme_save' => 'Save',

    /**
     * Manage Emails Translate
     * @author Sebastian Cabarcas
     */

    'manage_emails' => 'Manage Emails',
    'emails' => 'Emails',
    'email_driver' => 'Email Driver',
    'host' => 'Host',
    'port' => 'Port',
    'encryption' => 'Encryption',
    'smtp_username' => 'SMTP Username',
    'smtp_password' => 'SMTP Password',
    'from_email' => 'From Email',
    'from_name' => 'From Name',
    'email_save' => 'Save',

    /**
     * Manage Language Translate
     * @author Sebastian Cabarcas
     */

    'manage_language' => 'Manage Language',
    'language' => 'Language',
    'manage_language_text_one' => 'To configure your language you must first download the language file, after modifying the downloaded file you must replace it in the following path: resources/lang/new',
    'manage_language_text_two' => 'If for some reason there was an error on the site you can revert to the default language.',
    'manage_language_btn_one' => 'Download language file',
    'manage_language_btn_two' => 'Back to default language',
    'manage_language_btn_three' => 'Set configured language',

    /**
     * Manage Social Links Translate
     * @author Sebastian Cabarcas
     */

    'manage_social_links' => 'Manage Social Links',
    'social_links' => 'Social Links',
    'facebook_client_id' => 'Facebook Client ID',
    'facebook_client_secret' => 'Facebook Client Secret',
    'facebook_login' => 'Facebook Login',
    'facebook_login_on' => 'On',
    'facebook_login_off' => 'Off',
    'google_client_id' => 'Google Client ID',
    'google_client_secret' => 'Google Client Secret',
    'google_login' => 'Google Login',
    'google_login_on' => 'On',
    'google_login_off' => 'Off',
    'social_links_save' => 'Save',

     /**
     * Manage Transmissions Translate
     * @author Sebastian Cabarcas
     */

    'manage_transmissions' => 'Manage Transmissions',
    'transmissions' => 'Transmissions',
    'online_transmission' => 'Online Transmission',
    'online_transmission_placeholder' => 'Place the url of your website linked to facebook',
    'online_chat' => 'Online Chat',
    'online_chat_placeholder' => 'Enter the address of the web page associated with your facebook web page',
    'transmissions_save' => 'Save',

    /**
     * Manage Google Translate
     * @author Sebastian Cabarcas
     */

    'manage_google' => 'Manage Google',
    'manage_google_title' => 'Google',
    'recaptcha_key' => 'reCAPTCHA Key (V3)',
    'recaptcha_secret' => 'reCAPTCHA Secret Key (V3)',
    'google_analytics' => 'Google Analytics',
    'google_save' => 'Save',

    /**
     * Manage Services Translate
     * @author Sebastian Cabarcas
     */

    'manage_documents' => 'Manage Documents',
    'footer_documents' => 'Footer Documents',
    'service_title_one' => 'Title 1',
    'service_title_two' => 'Title 2',
    'service_title_three' => 'Title 3',
    'service_title_four' => 'Title 4',
    'service_title_five' => 'Title 5',
    'service_title_six' => 'Title 6',
    'service_description_one' => 'Description',
    'service_description_two' => 'Description',
    'service_description_three' => 'Description',
    'service_description_four' => 'Description',
    'service_description_five' => 'Description',
    'service_description_six' => 'Description',
    'service_show_in_footer_one' => 'Show in Footer',
    'service_show_in_footer_two' => 'Show in Footer',
    'service_show_in_footer_three' => 'Show in Footer',
    'service_show_in_footer_four' => 'Show in Footer',
    'service_show_in_footer_five' => 'Show in Footer',
    'service_show_in_footer_six' => 'Show in Footer',
    'service_status_yes' => 'Yes',
    'service_status_no' => 'No',
    'service_save' => 'Save',

    /**
     * Manage Currencies Translate
     * @author Sebastian Cabarcas
     */

    'manage_currencies' => 'Manage Currencies',
    'payment_details' => 'Payment Details',
    'symbol' => 'Symbol',
    'manage_currencies_save' => 'Save',
    'manage_currencies_cancel' => 'Cancel',
    'list_currencies' => 'List of Currencies',
    'currency' => 'Currency',
    'actions' => 'Actions',

    /**
     * Profile Translate
     * @author Sebastian Cabarcas
     */

    'profile_admin' => 'Admin Profile',
    'profile_data' => 'Profile Data',
    'profile_avatar' => 'Avatar',
    'profile_logo' => 'Logo',
    'profile_name' => 'Name',
    'profile_email' => 'Email',
    'profile_number_document' => 'N° of Document',
    'profile_address' => 'Address',
    'profile_city' => 'City',
    'profile_country' => 'Country',
    'profile_description' => 'Description',
    'profile_contact_description' => 'Contact Description',
    'contact_information' => 'Contact Information',
    'profile_phone' => 'Phone',
    'profile_cellphone' => 'Cellphone',
    'profile_whatsapp' => 'Whatsapp',
    'social_networks' => 'Social Networks',
    'web_site' => 'Web Site',
    'profile_facebook' => 'Facebook',
    'profile_instagram' => 'Instagram',
    'profile_twitter' => 'Twitter',
    'profile_linkedin' => 'Linkedin',
    'profile_youtube' => 'Youtube',
    'profile_change_password' => 'Change Password',
    'previous_password' => 'Previous Password',
    'new_password' => 'New Password',
    'profile_save' => 'Save',

    /**
     * Manage Regards Translate
     * @author Sebastian Cabarcas
     */

    'manage_regards' => 'Manage Regards',
    'new_regard' => 'New Regard',
    'regards_save' => 'Save',
    'edit_regard' => 'Edit Regard',
    'regards_update' => 'Update',
    'regards_cancel' => 'Cancel',
    'regards_featured_image' => 'Featured Image',
    'regards_title' => 'Title',
    'regards_url' => 'Link',
    'regards_day' => 'Day',
    'regards_select_day' => 'Select Day',
    'regards_monday' => 'Monday',
    'regards_tuesday' => 'Tuesday',
    'regards_wednesday' => 'Wednesday',
    'regards_thursday' => 'Thursday',
    'regards_friday' => 'Friday',
    'regards_saturday' => 'Saturday',
    'regards_sunday' => 'Sunday',
    'regards_start_time' => 'Start Time',
    'regards_end_time' => 'End Time',
    'regards_actions' => 'Actions',
    'list_regards' => 'Regards List',

    /**
     * Manage Users Translate
     * @author Sebastian Cabarcas
     */

    'manage_users' => 'Manage Users',
    'users_avatar' => 'Avatar',
    'users_name' => 'Name or Company Name',
    'users_last_names' => 'Last Names',
    'users_email' => 'Email',
    'users_type_document' => 'Type Document',
    'users_number_dni' => 'Identity Document',
    'users_number_ruc' => 'RUC',
    'users_number_ce' => 'Alien Registration Card',
    'users_number_document' => 'N° of Document',
    'users_address' => 'Address',
    'users_city' => 'City',
    'users_country' => 'Country',
    'users_cellphone' => 'Cellphone',
    'users_banned' => 'Banned',
    'users_banned_no' => 'No',
    'users_banned_yes' => 'Yes',
    'users_new_password' => 'New Password',
    'users_confirm_password' => 'Confirm Password',
    'users_update' => 'Update',
    'users_cancel' => 'Cancel',
    'users_search' => 'Search by Name or Company Name',
    'users_edit' => 'Edit User',
    'users_list' => 'List of Users',
    'users_actions' => 'Actions',
    'users_title_delete' => 'Do you want to delete this User?',
    'users_text_delete' => 'This operation is irreversible!',
    'users_swal_delete' => 'Record successfully deleted!',

    /**
     * Manage Comments Translate
     * @author Sebastian Cabarcas
     */

    'manage_comments' => 'Manage Comments',
    'comments_list' => 'Comments List',
    'comments_search_name' => 'Search by Name or Company Name',
    'comments_user' => 'Name or Company',
    'comments_title' => 'Title',
    'comments_date' => 'Date',
    'comments_actions' => 'Actions',
    'comments_title_delete' => 'Do you want to delete this Comment?',
    'comments_text_delete' => 'This operation is irreversible!',
    'comments_swal_delete' => 'Record successfully deleted!',

    /**
     * Manage Categories Translate
     * @author Sebastian Cabarcas
     */

    'manage_categories' => 'Manage Categories',
    'new_category' => 'New Category',
    'categories_save' => 'Save',
    'categories_edit' => 'Edit Category',
    'categories_update' => 'Update',
    'categories_cancel' => 'Cancel',
    'categories_title' => 'Title',
    'categories_location' => 'Location',
    'categories_show_header' => 'Show in Header',
    'categories_select_location' => 'Select Location',
    'categories_section_one' => 'Section 1',
    'categories_section_two' => 'Section 2',
    'categories_section_three' => 'Section 3',
    'categories_section_four' => 'Section 4',
    'categories_section_five' => 'Section 5',
    'categories_section_six' => 'Section 6',
    'categories_section_seven' => 'Section 7',
    'categories_section_eight' => 'Section 8',
    'categories_section_nine' => 'Section 9',
    'categories_status_header_yes' => 'Yes',
    'categories_status_header_no' => 'No',
    'categories_select_order' => 'Select Order',
    'display_order' => 'Display Order',
    'categories_status_homepage_yes' => 'Yes',
    'categories_status_homepage_no' => 'No',
    'categories_list' => 'Categories List',
    'categories_actions' => 'Actions',
    'categories_search_title' => 'Search by Title',
    'categories_title_confirm' => 'Confirm',
    'categories_title_cancel' => 'Cancel',
    'categories_text_error' => 'This category cannot be deleted because it has associated publications.',
    'categories_title_delete' => 'Do you want to delete this Category?',
    'categories_text_delete' => 'This operation is irreversible!',
    'categories_swal_delete' => 'Record successfully deleted!',

     /**
     * Manage Posts Translate
     * @author Sebastian Cabarcas
     */

    'manage_posts' => 'Manage Posts',
    'posts_title_delete' => 'Do you want to delete this Post?',
    'posts_text_delete' => 'This operation is irreversible!',
    'new_post' => 'New Post',
    'posts_save' => 'Save',
    'posts_edit' => 'Edit Post',
    'posts_update' => 'Update',
    'posts_cancel' => 'Cancel',
    'posts_featured_image' => 'Featured Image',
    'posts_title' => 'Title',
    'posts_category' => 'Category',
    'posts_select_category' => 'Select Category',
    'posts_description' => 'Description',
    'posts_seo' => 'SEO',
    'posts_tags' => 'Tags',
    'posts_summary' => 'Summary of the post',
    'posts_sentence' => 'Phrase',
    'posts_list' => 'Post List',
    'posts_search_title' => 'Search by Title',
    'posts_actions' => 'Actions',

    /**
     * Manage Programs Translate
     * @author Sebastian Cabarcas
     */

    'manage_programs' => 'Manage Programs',
    'new_program' => 'New Program',
    'programs_save' => 'Save',
    'programs_edit' => 'Edit Program',
    'programs_update' => 'Update',
    'programs_cancel' => 'Cancel',
    'programs_title' => 'Title',
    'programs_url' => 'Link',
    'programs_featured_image' => 'Featured Image',
    'programs_description' => 'Description',
    'programs_day' => 'Day',
    'programs_select_day' => 'Select Day',
    'programs_monday' => 'Monday',
    'programs_tuesday' => 'Tuesday',
    'programs_wednesday' => 'Wednesday',
    'programs_thursday' => 'Thursday',
    'programs_friday' => 'Friday',
    'programs_saturday' => 'Saturday',
    'programs_sunday' => 'Sunday',
    'programs_start_time' => 'Start Time',
    'programs_end_time' => 'End Time',
    'programs_list' => 'List of Programs',
    'programs_search_title' => 'Search by Title',
    'programs_actions' => 'Actions',
    'programs_text_error' => 'This program cannot be eliminated because it has associated sponsors.',
    'programs_title_delete' => 'Do you want to delete this Program?',
    'programs_text_delete' => 'This operation is irreversible!',
    'programs_swal_delete' => 'Record successfully deleted!',

    /**
     * Manage Sponsors Translate
     * @author Sebastian Cabarcas
     */

    'manage_sponsors' => 'Manage Sponsors',
    'new_sponsor' => 'New Sponsor',
    'sponsors_save' => 'Save',
    'sponsors_edit' => 'Edit Sponsor',
    'sponsors_update' => 'Update',
    'sponsors_cancel' => 'Cancel',
    'sponsors_title' => 'Title',
    'sponsors_url' => 'Link',
    'sponsors_featured_image' => 'Featured Image',
    'sponsors_program' => 'Program',
    'sponsors_select_program' => 'Select Program',
    'sponsors_list' => 'List of Sponsors',
    'sponsors_search_title' => 'Search by Program',
    'sponsors_actions' => 'Actions',
    'sponsors_title_delete' => 'Do you want to delete this Sponsor?',
    'sponsors_text_delete' => 'This operation is irreversible!',
    'sponsors_swal_delete' => 'Record successfully deleted!',

    /**
     * Manage Advertising Categories Translate
     * @author Sebastian Cabarcas
     */

    'manage_advertising_categories' => 'Manage Advertising Categories',
    'new_advertising_category' => 'New Category',
    'manage_advertising_save' => 'Save',
    'manage_advertising_edit' => 'Edit Category',
    'manage_advertising_update' => 'Update',
    'manage_advertising_cancel' => 'Cancel',
    'manage_advertising_title' => 'Title',
    'manage_advertising_list' => 'List of Categories',
    'manage_advertising_search_title' => 'Search by Title',
    'manage_advertising_actions' => 'Actions',
    'manage_advertising_text_error' => 'This category cannot be deleted because it has associated advertisements.',
    'manage_advertising_title_delete' => 'Do you want to delete this Category?',
    'manage_advertising_text_delete' => 'This operation is irreversible!',
    'manage_advertising_swal_delete' => 'Record successfully deleted!',

    /**
     * Manage Plans Translate
     * @author Sebastian Cabarcas
     */

    'manage_plans' => 'Manage Plans',
    'plans_title_delete' => 'Do you want to delete this Plan?',
    'plans_text_delete' => 'This operation is irreversible!',
    'plans_text_error' => 'This plan cannot be deleted because it has associated advertisements.',
    'new_plan' => 'New Plan',
    'plans_save' => 'Save',
    'plans_title' => 'Title',
    'plans_local_cost' => 'Local Cost',
    'plans_dolar_cost' => 'Dolar Cost',
    'plans_time' => 'Time (Days)',
    'plans_description' => 'Description',
    'plans_edit' => 'Edit Plan',
    'plans_update' => 'Update',
    'plans_cancel' => 'Cancel',
    'plans_list' => 'List of Plans',
    'plans_search_title' => 'Search by Title',
    'plans_actions' => 'Actions',

     /**
     * Manage Vendors Translate
     * @author Sebastian Cabarcas
     */

    'manage_vendors' => 'Manage Vendors',
    'new_vendor' => 'New Seller',
    'vendors_save' => 'Save',
    'vendors_edit' => 'Edit Seller',
    'vendors_update' => 'Update',
    'vendors_cancel' => 'Cancel',
    'vendors_avatar' => 'Avatar',
    'profile_data' => 'Profile Data',
    'vendors_document_number' => 'N° of Document',
    'vendors_name' => 'Name or Company Name',
    'vendors_address' => 'Address',
    'vendors_email' => 'Email',
    'vendors_city' => 'City',
    'vendors_country' => 'Country',
    'vendors_contact_information' => 'Contact Information',
    'vendors_phone' => 'Phone',
    'vendors_cellphone' => 'Cellphone',
    'vendors_whatsapp' => 'Whatsapp',
    'vendors_social_networks' => 'Social Networks',
    'vendors_web_site' => 'Web Site',
    'vendors_facebook' => 'Facebook',
    'vendors_instagram' => 'Instagram',
    'vendors_twitter' => 'Twitter',
    'vendors_linkedin' => 'Linkedin',
    'vendors_youtube' => 'Youtube',
    'vendors_list' => 'List of Vendors',
    'vendors_search_name' => 'Search by Name or Company Name',
    'vendors_actions' => 'Actions',
    'vendors_text_error' => 'This seller cannot be removed because it has associated advertisements.',
    'vendors_title_delete' => 'Do you want to delete this Seller?',
    'vendors_text_delete' => 'This operation is irreversible!',
    'vendors_swal_delete' => 'Record successfully deleted!',

    /**
     * Manage Advertisements Translate
     * @author Sebastian Cabarcas
     */

    'manage_advertisements' => 'Manage Advertisements',
    'manage_advertisements_title_delete' => 'Do you want to delete this Advertisement?',
    'manage_advertisements_text_delete' => 'This operation is irreversible!',
    'new_advertising' => 'New Advertising',
    'manage_advertisements_save' => 'Save',
    'manage_advertisements_title' => 'Title',
    'manage_advertisements_seller' => 'Seller',
    'manage_advertisements_select_seller' => 'Select Seller',
    'manage_advertisements_category' => 'Category',
    'manage_advertisements_select_category' => 'Select Category',
    'manage_advertisements_description' => 'Description',
    'manage_advertisements_advertising_type' => 'Type of Advertising',
    'manage_advertisements_select_advertising_type' => 'Select Type of Advertising',
    'manage_advertisements_photo' => 'Photo',
    'manage_advertisements_audio' => 'Audio',
    'manage_advertisements_video' => 'Video',
    'manage_advertisements_payment' => 'Payment Id',
    'manage_advertisements_start_date' => 'Start Date',
    'manage_advertisements_end_date' => 'End Date',
    'manage_advertisements_plan' => 'Plan',
    'manage_advertisements_select_plan' => 'Select Plan',
    'manage_advertisements_local_cost' => 'Local Cost',
    'manage_advertisements_dolar_cost' => 'Dolar Cost',
    'manage_advertisements_status' => 'Status',
    'manage_advertisements_url_video' => 'URL Video',
    'manage_advertisements_edit' => 'Edit Advertising',
    'manage_advertisements_update' => 'Update',
    'manage_advertisements_cancel' => 'Cancel',
    'manage_advertisements_list' => 'List of Advertisements',
    'manage_advertisements_search_title' => 'Search by Title',
    'manage_advertisements_actions' => 'Actions',

    /**
     * Manage Payments - Paypal Translate
     * @author Sebastian Cabarcas
     */

    'manage_payments' => 'Manage Payments',
    'manage_payments_paypal' => 'Paypal',
    'manage_payments_paypal_account' => 'Paypal Account',
    'manage_payments_image' => 'Image',
    'manage_payments_status' => 'Status',
    'manage_payments_status_on' => 'On',
    'manage_payments_status_off' => 'Off',
    'manage_payments_paypal_sandbox' => 'Paypal Sandbox',
    'manage_payments_save' => 'Save',
    'manage_payments_cancel' => 'Cancel',

    /**
     * Manage Payments - Mercado Pago Translate
     * @author Sebastian Cabarcas
     */

    'manage_payments_mp' => 'Mercado Pago',
    'manage_payments_mp_public_key' => 'Public Key',
    'manage_payments_mp_access_token' => 'Access Token',
    'manage_payments_mp_client_id' => 'Client ID',
    'manage_payments_mp_client_secret' => 'Client Secret',
    'manage_payments_mp_image' => 'Image',
    'manage_payments_mp_status' => 'Status',
    'manage_payments_mp_status_on' => 'On',
    'manage_payments_mp_status_off' => 'Off',
    'manage_payments_mp_sandbox' => 'Mercado Pago Sandbox',
    'manage_payments_mp_save' => 'Save',

     /**
     * Manage Payments - Transfer Translate
     * @author Sebastian Cabarcas
     */

    'manage_payments_transfer' => 'Transfer',
    'manage_payments_bank_details' => 'Bank Details',
    'manage_payments_transfer_status' => 'Status',
    'manage_payments_transfer_status_on' => 'On',
    'manage_payments_transfer_status_off' => 'Off',
    'manage_payments_transfer_save' => 'Save',

    /**
     * Manage Payments Translate
     * @author Sebastian Cabarcas
     */

    'manage_payments_payment_details' => 'Payment Details',
    'manage_payments_id' => 'ID',
    'manage_payments_transaction_id' => 'Transaction ID',
    'manage_payments_payment_gateway' => 'Payment Gateway',
    'manage_payments_plan' => 'Plan',
    'manage_payments_days' => 'Days',
    'manage_payments_local_cost' => 'Local Cost',
    'manage_payments_dolar_cost' => 'Dolar Cost',
    'manage_payments_name' => 'Name or Company Name',
    'manage_payments_document_number' => 'N° of Document',
    'manage_payments_type_document' => 'Type Document',
    'manage_payments_cellphone' => 'Cellphone',
    'manage_payments_email' => 'Email',
    'manage_payments_address' => 'Address',
    'manage_payments_city' => 'City',
    'manage_payments_country' => 'Country',
    'manage_payments_transaction_date' => 'Transaction Date',
    'manage_payments_transfer_payment_details' => 'Transfer Payment Details',
    'manage_payments_proof_transfer' => 'Proof of Transfer',
    'manage_payments_view_file' => 'See Voucher',
    'manage_payments_transfer_date' => 'Transfer Date',
    'manage_payments_transfer_status' => 'Status',
    'manage_payments_transfer_select_status' => 'Select Status',
    'manage_payments_transfer_status_pending' => 'Pending',
    'manage_payments_transfer_select_approved' => 'Approved',
    'manage_payments_transfer_select_rejected' => 'Rejected',
    'manage_payments_close' => 'Close',
    'manage_payments_save' => 'Save',
    'manage_payments_list' => 'Transaction List',
    'manage_payments_search_id' => 'Search by ID',
    'manage_payments_actions' => 'Actions',
    'manage_payments_title_delete' => 'Do you want to delete this Transaction?',
    'manage_payments_text_delete' => 'This operation is irreversible!',
    'manage_payments_swal_delete' => 'Record successfully deleted!',

    /*-------------- End Admin Translate ----------------*/



     /*-------------- Start User Translate ----------------*/

    /**
     * User Panel Translate
     * @author Sebastian Cabarcas
     */

    'user_control_panel' => 'Control Panel',
    'user_edit_profile' => ' Edit Profile',
    'user_advertising_plans' => 'Advertising Plans',
    'user_shopping' => 'Shopping',
    'user_logout' => 'Logout',
    'user_welcome' => 'Welcome: ',
    'user_change_photo' => 'Change Photo:',
    'user_select_photo' => 'Select Photo',
    'user_account_details' => 'Account details:',
    'user_transactions_list' => 'Transaction List',
    'user_transactions_id' => 'ID',
    'user_transactions_payment_gateway' => 'Payment Gateway',
    'user_transactions_payment_plan' => 'Plan',
    'user_transactions_cost' => 'Cost',
    'user_transactions_time' => 'Time (Days)',
    'user_transactions_date' => 'Transaction Date',
    'user_transactions_status' => 'Status',
    'user_transactions_details' => 'Actions',
    'user_transactions_status_pending' => 'Pending',
    'user_transactions_status_approved' => 'Approved',
    'user_transactions_status_rejected' => 'Rejected',
    'user_transactions_transfer_date' => 'Transfer Date',
    'user_transactions_invoice' => 'Receipt',
    'user_transactions_date_two' => 'Date',
    'user_transactions_quantity' => 'Quantity',
    'user_transactions_description' => 'Description',
    'user_transactions_subtotal' => 'Subtotal',
    'user_transactions_total' => 'Total',
    'user_transactions_print' => 'Download ',
    'user_search_id' => 'Search by ID',
    'user_shopping_details' => 'Detail your purchases:',
    'user_name' => 'Name or Company Name',
    'user_last_names' => 'Last Names',
    'user_email' => 'Email',
    'user_type_document' => 'Type Document',
    'user_select_type_document' => 'Select',
    'user_number_dni' => 'Identity Document',
    'user_number_ruc' => 'RUC',
    'user_number_ce' => 'Alien Registration Card',
    'user_number_document' => 'N° of Document',
    'user_address' => 'Address',
    'user_city' => 'City',
    'user_country' => 'Country',
    'user_cellphone' => 'Cellphone',
    'user_previous_password' => 'Previous Password',
    'user_new_password' => 'New Password',
    'user_save' => 'Save Changes',

    /*-------------- End User Translate ----------------*/



    /*-------------- Frontend Translate ----------------*/

    /**
     * Top Bar Translate
     * @author Sebastian Cabarcas
     */

    'frontend_today' => 'today is',
    'frontend_good_morning' => 'Good morning',
    'frontend_good_afternoon' => 'Good afternoon',
    'frontend_good_night' => 'Good night',
    'frontend_monday' => 'Monday',
    'frontend_tuesday' => 'Tuesday',
    'frontend_wednesday' => 'Wednesday',
    'frontend_thursday' => 'Thursday',
    'frontend_friday' => 'Friday',
    'frontend_saturday' => 'Saturday',
    'frontend_sunday' => 'Sunday',
    'frontend_radio_online' => 'RADIO ONLINE',

    /**
     * Top Header Translate
     * @author Sebastian Cabarcas
     */

    'frontend_last_publications' => 'Last Publications:',
    'frontend_advertise_here' => 'ADVERTISE HERE',
    'frontend_title_home' => 'HOME',

    /**
     * Home section titles Translate
     * @author Sebastian Cabarcas
     */

    'frontend_more_views_home' => 'More Views',
    'frontend_music_home' => 'Music',
    'frontend_programming_home' => 'Programming',
    'frontend_videos_home' => 'Videos',

    /**
     * Right Sidebar Translate
     * @author Sebastian Cabarcas
     */

    'frontend_categories' => 'Categories',
    'frontend_recent' => 'Recent',
    'frontend_top_post' => 'Top Post',

     /**
     * Post Translate
     * @author Sebastian Cabarcas
     */

    'frontend_post_most_popular' => 'Most Popular',
    'frontend_post_comments' => 'Comments',
    'frontend_post_publish' => 'PUBLISH',
    'frontend_post_your_comment' => 'Your Comment',
    'frontend_post_reply' => 'Reply',

    /**
     * Contact Floating Translate
     * @author Sebastian Cabarcas
     */

    'frontend_contact_us_floating' => 'Contact Us',
    'frontend_consultations_suggestions' => 'Consultations, suggestions',
    'frontend_contact_us_floating_name' => 'Name',
    'frontend_contact_us_floating_cellphone' => 'Cellphone',
    'frontend_contact_us_floating_email' => 'Email',
    'frontend_contact_us_floating_message' => 'Message',
    'frontend_contact_us_floating_send' => 'Send',

     /**
     * Register Translate
     * @author Sebastian Cabarcas
     */

    'frontend_please_signup' => 'Please Signup',
    'frontend_register_names' => 'Name or Company Name',
    'frontend_register_last_names' => 'Last Names',
    'frontend_register_email' => 'Email',
    'frontend_register_cellphone' => 'Cellphone',
    'frontend_register_password' => 'Password',
    'frontend_register_confirm_password' => 'Confirm password',
    'frontend_register' => 'REGISTER ME',
    'frontend_register_policy' => 'By clicking the "Sign Up" button, you confirm that you accept our Terms of use and Privacy Policy.',
    'frontend_register_have_account' => '¿Have an account?',
    'frontend_register_log_in' => 'Log In',

    /**
     * Log in Translate
     * @author Sebastian Cabarcas
     */

    'frontend_please_login' => 'Please Log In, or ',
    'frontend_login_sign_up' => 'Sign Up',
    'frontend_login_or' => 'or',
    'frontend_login_facebook' => 'Facebook',
    'frontend_login_google' => 'Google',
    'frontend_login_email' => 'Email',
    'frontend_login_forgot_password' => 'Forgot password?',
    'frontend_login_password' => 'Password',
    'frontend_login_remember' => 'Remember me',
    'frontend_login' => 'LOG IN',

     /**
     * Reset Password Translate
     * @author Sebastian Cabarcas
     */

    'frontend_reset_password' => 'Reset Password, or ',
    'frontend_reset_login' => 'Log In',
    'frontend_reset_email' => 'Email',
    'frontend_reset_send_link' => 'Send Password Reset Link',

     /**
     * Recovery Password Translate
     * @author Sebastian Cabarcas
     */

    'frontend_recovery_password' => 'Reset password',
    'frontend_recovery_email' => 'Email',
    'frontend_recovery_pass' => 'Password',
    'frontend_recovery_confirm_password' => 'Confirm Password',
    'frontend_recovery_pass_btn' => 'Reset password',

    /**
     * Contact Us Translate
     * @author Sebastian Cabarcas
     */

    'frontend_contact_advertise_here' => 'Advertise here',
    'frontend_contact_view_advertising_plans' => 'View Advertising Plans',
    'frontend_contact_name' => 'Name',
    'frontend_contact_email' => 'Email',
    'frontend_contact_cellphone' => 'Cellphone',
    'frontend_contact_message' => 'Message',
    'frontend_contact_send_message' => 'Send Message',

    /**
     * Plans Translate
     * @author Sebastian Cabarcas
     */

    'frontend_advertising_plans' => 'Advertising Plans',
    'frontend_plans_day' => 'Days',

    /**
     * Paypal Translate
     * @author Sebastian Cabarcas
     */

    'frontend_paypal_billing_data' => 'Billing Data',
    'frontend_paypal_name' => 'Name or Company Name *',
    'frontend_paypal_cellphone' => 'Cellphone *',
    'frontend_paypal_email' => 'Email *',
    'frontend_paypal_type_document' => 'Type Document *',
    'frontend_paypal_select' => 'Select',
    'frontend_paypal_dni' => 'Identity Document',
    'frontend_paypal_ruc' => 'RUC',
    'frontend_paypal_ce' => 'Alien Registration Card',
    'frontend_paypal_number_document' => 'N° of Document',
    'frontend_paypal_address' => 'Address',
    'frontend_paypal_city' => 'City',
    'frontend_paypal_country' => 'Country',
    'frontend_paypal_next' => 'Next',
    'frontend_paypal_summary' => 'Summary of your purchase',
    'frontend_paypal_plan' => 'Plan',
    'frontend_paypal_cost' => 'Cost',
    'frontend_paypal_checkout' => 'Checkout',
    'frontend_paypal_well_done' => 'Well done!',
    'frontend_paypal_message_one' => 'Your payment has been accepted, you will soon have your advertisements on the site. Our staff will contact you as soon as possible to finalize details but you can also contact us at',
    'frontend_paypal_message_two' => 'or write to email',
    'frontend_paypal_sorry' => 'Sorry!',
    'frontend_paypal_message_fail_one' => 'Your payment has been declined, therefore you will not be able to have advertisements on this site. You can contact us at',
    'frontend_paypal_message_fail_two' => 'or write to email',
    'frontend_paypal_message_fail_three' => 'if you believe this is an error so that our staff can provide you with the corresponding details.',
    'frontend_paypal_regards' => 'Regards,',
    'frontend_paypal_go_home' => 'GO TO HOME',

    /**
     * Transfer Translate
     * @author Sebastian Cabarcas
     */

    'frontend_transfer_summary' => 'Summary of your purchase',
    'frontend_transfer_bank_data' => 'Bank Data',
    'frontend_transfer_plan' => 'Plan',
    'frontend_transfer_cost' => 'Cost',
    'frontend_transfer_billing_data' => 'Billing Data',
    'frontend_transfer_name' => 'Name or Company Name *',
    'frontend_transfer_cellphone' => 'Cellphone *',
    'frontend_transfer_email' => 'Email *',
    'frontend_transfer_type_document' => 'Type Document *',
    'frontend_transfer_select' => 'Select',
    'frontend_transfer_dni' => 'Identity Document',
    'frontend_transfer_ruc' => 'RUC',
    'frontend_transfer_ce' => 'Alien Registration Card',
    'frontend_transfer_number_document' => 'N° of Document *',
    'frontend_transfer_address' => 'Address *',
    'frontend_transfer_city' => 'City *',
    'frontend_transfer_country' => 'Country *',
    'frontend_transfer_proof' => 'Proof of Transfer',
    'frontend_transfer_file' => 'Photo, scan or screenshot *',
    'frontend_transfer_choose_file' => 'Choose a file...',
    'frontend_transfer_date' => 'Date of transfer *',
    'frontend_transfer_checkout' => 'Checkout',
    'frontend_transfer_pending' => 'Hello!',
    'frontend_transfer_message_pending_one' => 'Your payment is in pending status, as soon as it is confirmed you will be able to have advertisements on this site. Our staff will contact you as soon as possible to finalize details but you can also contact us at',
    'frontend_transfer_message_pending_two' => 'or write to email',
    'frontend_transfer_regards' => 'Regards,',
    'frontend_transfer_go_home' => 'GO TO HOME',

    /**
     * Mercado Pago Translate
     * @author Sebastian Cabarcas
     */

    'frontend_mp_billing_data' => 'Billing Data',
    'frontend_mp_name' => 'Name or Company Name *',
    'frontend_mp_cellphone' => 'Cellphone *',
    'frontend_mp_email' => 'Email *',
    'frontend_mp_type_document' => 'Type Document *',
    'frontend_mp_select' => 'Select',
    'frontend_mp_dni' => 'Identity Document',
    'frontend_mp_ruc' => 'RUC',
    'frontend_mp_ce' => 'Alien Registration Card',
    'frontend_mp_number_document' => 'N° of Document *',
    'frontend_mp_address' => 'Address *',
    'frontend_mp_city' => 'City *',
    'frontend_mp_country' => 'Country *',
    'frontend_mp_next' => 'Next',
    'frontend_mp_summary' => 'Summary of your purchase',
    'frontend_mp_plan' => 'Plan',
    'frontend_mp_cost' => 'Cost',
    'frontend_mp_checkout' => 'Checkout',
    'frontend_mp_well_done' => 'Well done!',
    'frontend_mp_message_one' => 'Your payment has been accepted, you will soon have your advertisements on the site. Our staff will contact you as soon as possible to finalize details but you can also contact us at',
    'frontend_mp_message_two' => 'or write to email',
    'frontend_mp_pending' => 'Hello!',
    'frontend_mp_message_pending_one' => 'Your payment is in pending status, as soon as it is confirmed you will be able to have advertisements on this site. Our staff will contact you as soon as possible to finalize details but you can also contact us at',
    'frontend_mp_message_pending_two' => 'or write to email',
    'frontend_mp_sorry' => 'Sorry!',
    'frontend_mp_message_fail_one' => 'Your payment has been declined, therefore you will not be able to have advertisements on this site. You can contact us at',
    'frontend_mp_message_fail_two' => 'or write to email',
    'frontend_mp_message_fail_three' => 'if you believe this is an error so that our staff can provide you with the corresponding details.',
    'frontend_mp_regards' => 'Regards,',
    'frontend_mp_go_home' => 'GO TO HOME',

     /**
     * Advertising Translate
     * @author Sebastian Cabarcas
     */

    'frontend_advertising' => 'Advertising',
    'frontend_advertising_all' => 'All',

    /**
     * Advertising Floating Translate
     * @author Sebastian Cabarcas
     */

    'frontend_advertising_advertiser' => 'Advertiser:',
    'frontend_advertising_name' => 'Name',
    'frontend_advertising_dni' => 'DNI or RUC',
    'frontend_advertising_address' => 'Address',
    'frontend_advertising_contact' => 'Contact:',
    'frontend_advertising_follow' => 'Follow us:',

     /**
     * Advertising Random Translate
     * @author Sebastian Cabarcas
     */

     'frontend_more_ads' => ' More Ads',

    /**
     * Programs Translate
     * @author Sebastian Cabarcas
     */

    'frontend_current_program' => 'Current Program',
    'frontend_interact_us' => 'Interact with us',
    'frontend_program_start' => 'Start:',
    'frontend_program_end' => 'End:',
    'frontend_sponsors' => 'Sponsors',

    /**
     * Search Translate
     * @author Sebastian Cabarcas
     */

    'frontend_search' => 'Search Publication',
    'frontend_result' => 'Search Result',

    /**
     * Post Translate
     * @author Sebastian Cabarcas
     */

    'frontend_post_previous' => 'Previous',
    'frontend_post_next' => 'Next',

    /*-------------- End Frontend Translate ----------------*/
];

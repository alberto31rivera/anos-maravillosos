<?php

return [

    /*-------------- Start Admin Translate ----------------*/

    /**
     * Left Sidebar Translate
     * @author Sebastian Cabarcas
     */

    'general_settings' => 'Ajustes Generales',
    'homepage' => 'Homepage',
    'themes' => 'Temas',
    'emails' => 'Correos',
    'language' => 'Idioma',
    'social_links' => 'Social Links',
    'google' => 'Google',
    'documents' => 'Documentos',
    'currencies' => 'Monedas',
    'manage_admin' => 'Gestionar Admin',
    'profile' => 'Perfil',
    'regards' => 'Saludos',
    'manage_users' => 'Gestionar Usuarios',
    'users' => 'Usuarios',
    'comments' => 'Comentarios',
    'manage_publications' => 'Gestionar Publicaciones',
    'categories' => 'Categorías',
    'posts' => 'Publicaciones',
    'manage_programs' => 'Gestionar Programas',
    'programs' => 'Programas',
    'sponsors' => 'Auspiciadores',
    'manage_advertising' => 'Gestionar Publicidad',
    'categories_advertising' => 'Categorías',
    'plans' => 'Planes',
    'vendors' => 'Vendedores',
    'advertisements' => 'Publicidades',
    'manage_payments' => 'Gestionar Pagos',
    'paypal' => 'Paypal',
    'mercado_pago' => 'Mercado Pago',
    'transfer' => 'Transferencia',
    'payments' => 'Pagos',
    'exit' => 'Salir',


    /**
     * Dashboard Translate
     * @author Sebastian Cabarcas
     */

    'dashborad_advertisements' => 'Publicidades',
    'see_more' => 'Ver más',
    'net_earnings' => 'Ganancias Netas',
    'dashborad_vendors' => 'Vendedores',
    'dashborad_posts' => 'Publicaciones',
    'monthly_statistics' => 'Estadísticas del mes',
    'today_income' => 'Ingresos de hoy',
    'this_week_income' => 'Ingresos de esta semana',
    'this_month_income' => 'Ingresos de este mes',
    'current_year_earnings' => 'Ganancias en el presente año',
    'month' => 'Mes',
    'january' => 'Enero',
    'february' => 'Febrero',
    'march' => 'Marzo',
    'april' => 'Abril',
    'may' => 'Mayo',
    'june' => 'Junio',
    'july' => 'Julio',
    'august' => 'Agosto',
    'september' => 'Septiembre',
    'october' => 'Octubre',
    'november' => 'Noviembre',
    'december' => 'Deciembre',

    /**
     * Manage Homepage Translate
     * @author Sebastian Cabarcas
     */

    'manage_homepage' => 'Administrar Homepage',
    'homepage_announcements' => 'Anuncios del Homepage',
    'advertisement_one' => 'Anuncio 1',
    'advertisement_two' => 'Anuncio 2',
    'advertisement_three' => 'Anuncio 3',
    'advertisement_four' => 'Anuncio 4',
    'link_one' => 'Enlace 1',
    'link_two' => 'Enlace 2',
    'link_three' => 'Enlace 3',
    'link_four' => 'Enlace 4',
    'homepage_videos' => 'Videos del Homepage',
    'url_video_one' => 'URL Video 1',
    'url_video_two' => 'URL Video 2',
    'url_video_three' => 'URL Video 3',
    'url_video_four' => 'URL Video 4',
    'url_video_five' => 'URL Video 5',
    'url_video_six' => 'URL Video 6',
    'description_one' => 'Descripción 1',
    'description_two' => 'Descripción 2',
    'description_three' => 'Descripción 3',
    'description_four' => 'Descripción 4',
    'description_five' => 'Descripción 5',
    'description_six' => 'Descripción 6',
    'image_one' => 'Imágen 1',
    'image_two' => 'Imágen 2',
    'image_three' => 'Imágen 3',
    'image_four' => 'Imágen 4',
    'image_five' => 'Imágen 5',
    'image_six' => 'Imágen 6',
    'web_titles' => 'Títulos de la Web',
    'title_one' => 'Título 1',
    'title_two' => 'Título 2',
    'title_three' => 'Título 3',
    'title_four' => 'Título 4',
    'title_five' => 'Título 5',
    'title_six' => 'Título 6',
    'title_seven' => 'Título 7',
    'title_eight' => 'Título 8',
    'homepage_save' => 'Guardar',

    /**
     * Manage Themes Translate
     * @author Sebastian Cabarcas
     */

    'manage_themes' => 'Administrar Temas',
    'web_colors' => 'Colores de la Web',
    'greeting_bar_color' => 'Color de barra de Saludo',
    'color_text_regards' => 'Color de texto de barra de saludo',
    'color_text_header' => 'Color de texto de barra de cabecera',
    'color_highlight_header' => 'Color de resalte de barra de cabecera',
    'color_text_footer' => 'Color de texto de pie de página',
    'header_bar_color' => 'Color de barra de cabecera',
    'footer_bar_color' => 'Color de barra de pie de página',
    'contact_us_color' => 'Color de Contáctanos',
    'button_color' => 'Color de Botones',
    'theme_save' => 'Guardar',

    /**
     * Manage Emails Translate
     * @author Sebastian Cabarcas
     */

    'manage_emails' => 'Administrar Emails',
    'emails' => 'Emails',
    'email_driver' => 'Email Driver',
    'host' => 'Host',
    'port' => 'Puerto',
    'encryption' => 'Encriptación',
    'smtp_username' => 'Usuario SMTP',
    'smtp_password' => 'Contraseña SMTP',
    'from_email' => 'From Email',
    'from_name' => 'From Name',
    'email_save' => 'Guardar',

    /**
     * Manage Language Translate
     * @author Sebastian Cabarcas
     */

    'manage_language' => 'Administrar Idioma',
    'language' => 'Idioma',
    'manage_language_text_one' => 'Para configurar su idioma primero debe descargar el archivo de idioma, luego de modificar el archivo descargado debe reemplazarlo en la siguiente ruta: resources/lang/new',
    'manage_language_text_two' => 'Si por algún motivo hubo un error en el sitio puede volver al idioma por defecto.',
    'manage_language_btn_one' => 'Descargar archivo de idioma',
    'manage_language_btn_two' => 'Volver al idioma por defecto',
    'manage_language_btn_three' => 'Establecer idioma configurado',

    /**
     * Manage Social Links Translate
     * @author Sebastian Cabarcas
     */

    'manage_social_links' => 'Administrar Redes Sociales',
    'social_links' => 'Redes Sociales',
    'facebook_client_id' => 'Facebook Client ID',
    'facebook_client_secret' => 'Facebook Client Secret',
    'facebook_login' => 'Inicio de sesión con Facebook',
    'facebook_login_on' => 'Activado',
    'facebook_login_off' => 'Desactivado',
    'google_client_id' => 'Google Client ID',
    'google_client_secret' => 'Google Client Secret',
    'google_login' => 'Inicio de sesión con Google',
    'google_login_on' => 'Activado',
    'google_login_off' => 'Desactivado',
    'social_links_save' => 'Guardar',

    /**
     * Manage Transmissions Translate
     * @author Sebastian Cabarcas
     */

    'manage_transmissions' => 'Administrar Transmisiones',
    'transmissions' => 'Transmisiones',
    'online_transmission' => 'Transmisión en Línea',
    'online_transmission_placeholder' => 'Colocar código iframe',
    'online_chat' => 'Chat en Línea',
    'online_chat_placeholder' => 'Colocar la dirección de la página web asociada a su página web de facebook',
    'transmissions_save' => 'Guardar',

    /**
     * Manage Google Translate
     * @author Sebastian Cabarcas
     */

    'manage_google' => 'Administrar Google',
    'manage_google_title' => 'Google',
    'recaptcha_key' => 'reCAPTCHA Key (V3)',
    'recaptcha_secret' => 'reCAPTCHA Secret Key (V3)',
    'google_analytics' => 'Google Analytics',
    'google_save' => 'Guardar',

    /**
     * Manage Services Translate
     * @author Sebastian Cabarcas
     */

    'manage_documents' => 'Administrar Documentos',
    'footer_documents' => 'Documentos del Footer',
    'service_title_one' => 'Título 1',
    'service_title_two' => 'Título 2',
    'service_title_three' => 'Título 3',
    'service_title_four' => 'Título 4',
    'service_title_five' => 'Título 5',
    'service_title_six' => 'Título 6',
    'service_description_one' => 'Descripción',
    'service_description_two' => 'Descripción',
    'service_description_three' => 'Descripción',
    'service_description_four' => 'Descripción',
    'service_description_five' => 'Descripción',
    'service_description_six' => 'Descripción',
    'service_show_in_footer_one' => 'Mostrar en el Footer',
    'service_show_in_footer_two' => 'Mostrar en el Footer',
    'service_show_in_footer_three' => 'Mostrar en el Footer',
    'service_show_in_footer_four' => 'Mostrar en el Footer',
    'service_show_in_footer_five' => 'Mostrar en el Footer',
    'service_show_in_footer_six' => 'Mostrar en el Footer',
    'service_status_yes' => 'Si',
    'service_status_no' => 'No',
    'service_save' => 'Guardar',

    /**
     * Manage Currencies Translate
     * @author Sebastian Cabarcas
     */

    'manage_currencies' => 'Administrar Monedas',
    'payment_details' => 'Detalle de Pago',
    'symbol' => 'Símbolo',
    'manage_currencies_save' => 'Guardar',
    'manage_currencies_cancel' => 'Cancelar',
    'list_currencies' => 'Lista de Monedas',
    'currency' => 'Moneda',
    'actions' => 'Acciones',

    /**
     * Profile Translate
     * @author Sebastian Cabarcas
     */

    'profile_admin' => 'Perfil Admin',
    'profile_data' => 'Datos de Perfil',
    'profile_avatar' => 'Avatar',
    'profile_logo' => 'Logo',
    'profile_name' => 'Nombre',
    'profile_email' => 'Email',
    'profile_number_document' => 'N° de Documento',
    'profile_address' => 'Dirección',
    'profile_city' => 'Ciudad',
    'profile_country' => 'País',
    'profile_description' => 'Descripción',
    'profile_contact_description' => 'Descripción de Contacto',
    'contact_information' => 'Datos de Contacto',
    'profile_phone' => 'Teléfono',
    'profile_cellphone' => 'Celular',
    'profile_whatsapp' => 'Whatsapp',
    'social_networks' => 'Redes Sociales',
    'web_site' => 'Sitio Web',
    'profile_facebook' => 'Facebook',
    'profile_instagram' => 'Instagram',
    'profile_twitter' => 'Twitter',
    'profile_linkedin' => 'Linkedin',
    'profile_youtube' => 'Youtube',
    'profile_change_password' => 'Cambiar Contraseña',
    'previous_password' => 'Contraseña Anterior',
    'new_password' => 'Nueva Contraseña',
    'profile_save' => 'Guardar',

    /**
     * Manage Regards Translate
     * @author Sebastian Cabarcas
     */

    'manage_regards' => 'Gestionar Saludos',
    'new_regard' => 'Nuevo Saludo',
    'regards_save' => 'Guardar',
    'edit_regard' => 'Editar Saludo',
    'regards_update' => 'Actualizar',
    'regards_cancel' => 'Cancelar',
    'regards_featured_image' => 'Imágen Destacada',
    'regards_title' => 'Título',
    'regards_url' => 'Enlace',
    'regards_day' => 'Día',
    'regards_select_day' => 'Seleccionar Día',
    'regards_monday' => 'Lunes',
    'regards_tuesday' => 'Martes',
    'regards_wednesday' => 'Miércoles',
    'regards_thursday' => 'Jueves',
    'regards_friday' => 'Viernes',
    'regards_saturday' => 'Sábado',
    'regards_sunday' => 'Domingo',
    'regards_start_time' => 'Hora Inicio',
    'regards_end_time' => 'Hora Fin',
    'regards_actions' => 'Acciones',
    'list_regards' => 'Lista de Saludos',

    /**
     * Manage Users Translate
     * @author Sebastian Cabarcas
     */

    'manage_users' => 'Gestionar Usuarios',
    'users_avatar' => 'Avatar',
    'users_name' => 'Nombre o Empresa',
    'users_last_names' => 'Apellidos',
    'users_email' => 'Email',
    'users_type_document' => 'Tipo de Documento',
    'users_number_dni' => 'Documento de Identidad',
    'users_number_ruc' => 'RUC',
    'users_number_ce' => 'Carnet de Extranjería',
    'users_number_document' => 'N° de Documento',
    'users_address' => 'Direción',
    'users_city' => 'Ciudad',
    'users_country' => 'País',
    'users_cellphone' => 'Celular',
    'users_banned' => 'Baneado',
    'users_banned_no' => 'No',
    'users_banned_yes' => 'Si',
    'users_new_password' => 'Nueva Contraseña',
    'users_confirm_password' => 'Confirmar Contraseña',
    'users_update' => 'Actualizar',
    'users_cancel' => 'Cancelar',
    'users_search' => 'Buscar por Nombre o Empresa',
    'users_edit' => 'Editar Usuario',
    'users_list' => 'Lista de Usuarios',
    'users_actions' => 'Acciones',
    'users_title_delete' => 'Desea borrar este Usuario?',
    'users_text_delete' => 'Esta operación es irreversible!',
    'users_swal_delete' => 'Registro eliminado correctamente!',

    /**
     * Manage Comments Translate
     * @author Sebastian Cabarcas
     */

    'manage_comments' => 'Gestionar Comentarios',
    'comments_list' => 'Lista de Comentarios',
    'comments_search_name' => 'Buscar por Nombre o Empresa',
    'comments_user' => 'Nombre o Empresa',
    'comments_title' => 'Título',
    'comments_date' => 'Fecha',
    'comments_actions' => 'Acciones',
    'comments_title_delete' => 'Desea borrar este Comentario?',
    'comments_text_delete' => 'Esta operación es irreversible!',
    'comments_swal_delete' => 'Registro eliminado correctamente!',

    /**
     * Manage Categories Translate
     * @author Sebastian Cabarcas
     */

    'manage_categories' => 'Gestionar Categorías',
    'new_category' => 'Nueva Categoría',
    'categories_save' => 'Guardar',
    'categories_edit' => 'Editar Categoría',
    'categories_update' => 'Actualizar',
    'categories_cancel' => 'Cancelar',
    'categories_title' => 'Título',
    'categories_location' => 'Ubicación',
    'categories_show_header' => 'Mostrar en la Cabecera',
    'categories_select_location' => 'Seleccionar Ubicación',
    'categories_section_one' => 'Sección 1',
    'categories_section_two' => 'Sección 2',
    'categories_section_three' => 'Sección 3',
    'categories_section_four' => 'Sección 4',
    'categories_section_five' => 'Sección 5',
    'categories_section_six' => 'Sección 6',
    'categories_section_seven' => 'Sección 7',
    'categories_section_eight' => 'Sección 8',
    'categories_section_nine' => 'Sección 9',
    'categories_status_header_yes' => 'Si',
    'categories_status_header_no' => 'No',
    'categories_select_order' => 'Seleccionar Orden',
    'display_order' => 'Orden de Visualización',
    'categories_status_homepage_yes' => 'Si',
    'categories_status_homepage_no' => 'No',
    'categories_list' => 'Lista de Categorías',
    'categories_actions' => 'Acciones',
    'categories_search_title' => 'Buscar por Título',
    'categories_title_confirm' => 'Confirmar',
    'categories_title_cancel' => 'Cancelar',
    'categories_text_error' => 'No se puede eliminar esta categoría porque tiene publicaciones asociadas.',
    'categories_title_delete' => 'Desea borrar esta Categoría?',
    'categories_text_delete' => 'Esta operación es irreversible!',
    'categories_swal_delete' => 'Registro eliminado correctamente!',

    /**
     * Manage Posts Translate
     * @author Sebastian Cabarcas
     */

    'manage_posts' => 'Gestionar Publicaciones',
    'posts_title_delete' => 'Desea borrar esta Publicación?',
    'posts_text_delete' => 'Esta operación es irreversible!',
    'new_post' => 'Nueva Publicación',
    'posts_save' => 'Guardar',
    'posts_edit' => 'Editar Publicación',
    'posts_update' => 'Actualizar',
    'posts_cancel' => 'Cancelar',
    'posts_featured_image' => 'Imágen Destacada',
    'posts_title' => 'Título',
    'posts_category' => 'Categoría',
    'posts_select_category' => 'Seleccionar Categoría',
    'posts_description' => 'Descripción',
    'posts_seo' => 'SEO',
    'posts_tags' => 'Etiquetas',
    'posts_summary' => 'Resumen de la publicación',
    'posts_sentence' => 'Frase',
    'posts_list' => 'Lista de Publicaciones',
    'posts_search_title' => 'Buscar por Título',
    'posts_actions' => 'Acciones',

    /**
     * Manage Programs Translate
     * @author Sebastian Cabarcas
     */

    'manage_programs' => 'Gestionar Programas',
    'new_program' => 'Nuevo Programa',
    'programs_save' => 'Guardar',
    'programs_edit' => 'Editar Programa',
    'programs_update' => 'Actualizar',
    'programs_cancel' => 'Cancelar',
    'programs_title' => 'Título',
    'programs_url' => 'Enlace',
    'programs_featured_image' => 'Imágen Destacada',
    'programs_description' => 'Descripción',
    'programs_day' => 'Día',
    'programs_select_day' => 'Seleccionar Día',
    'programs_monday' => 'Lunes',
    'programs_tuesday' => 'Martes',
    'programs_wednesday' => 'Miércoles',
    'programs_thursday' => 'Jueves',
    'programs_friday' => 'Viernes',
    'programs_saturday' => 'Sábado',
    'programs_sunday' => 'Domingo',
    'programs_start_time' => 'Hora Inicio',
    'programs_end_time' => 'Hora Fin',
    'programs_list' => 'Lista de Programas',
    'programs_search_title' => 'Buscar por Título',
    'programs_actions' => 'Acciones',
    'programs_text_error' => 'No se puede eliminar este programa porque tiene auspiciadores asociados.',
    'programs_title_delete' => 'Desea borrar esta Programa?',
    'programs_text_delete' => 'Esta operación es irreversible!',
    'programs_swal_delete' => 'Registro eliminado correctamente!',

    /**
     * Manage Sponsors Translate
     * @author Sebastian Cabarcas
     */

    'manage_sponsors' => 'Gestionar Auspiciadores',
    'new_sponsor' => 'Nuevo Auspiciador',
    'sponsors_save' => 'Guardar',
    'sponsors_edit' => 'Editar Auspiciador',
    'sponsors_update' => 'Actualizar',
    'sponsors_cancel' => 'Cancelar',
    'sponsors_title' => 'Título',
    'sponsors_url' => 'Enlace',
    'sponsors_featured_image' => 'Imágen Destacada',
    'sponsors_program' => 'Programa',
    'sponsors_select_program' => 'Seleccionar Programa',
    'sponsors_list' => 'Lista de Auspiciadores',
    'sponsors_search_title' => 'Buscar por Programa',
    'sponsors_actions' => 'Acciones',
    'sponsors_title_delete' => 'Desea borrar esta Auspiciador?',
    'sponsors_text_delete' => 'Esta operación es irreversible!',
    'sponsors_swal_delete' => 'Registro eliminado correctamente!',

     /**
     * Manage Advertising Categories Translate
     * @author Sebastian Cabarcas
     */

    'manage_advertising_categories' => 'Gestionar Categorías de Publicidades',
    'new_advertising_category' => 'Nueva Categoría',
    'manage_advertising_save' => 'Guardar',
    'manage_advertising_edit' => 'Editar Categoría',
    'manage_advertising_update' => 'Actualizar',
    'manage_advertising_cancel' => 'Cancelar',
    'manage_advertising_title' => 'Título',
    'manage_advertising_list' => 'Lista de Categorías',
    'manage_advertising_search_title' => 'Buscar por Título',
    'manage_advertising_actions' => 'Acciones',
    'manage_advertising_text_error' => 'No se puede eliminar esta categoría porque tiene publicidades asociadas.',
    'manage_advertising_title_delete' => 'Desea borrar esta Categoría?',
    'manage_advertising_text_delete' => 'Esta operación es irreversible!',
    'manage_advertising_swal_delete' => 'Registro eliminado correctamente!',

    /**
     * Manage Plans Translate
     * @author Sebastian Cabarcas
     */

    'manage_plans' => 'Gestionar Planes',
    'plans_title_delete' => 'Desea borrar esta Plan?',
    'plans_text_delete' => 'Esta operación es irreversible!',
    'plans_text_error' => 'No se puede eliminar este plan porque tiene publicidades asociadas.',
    'new_plan' => 'Nuevo Plan',
    'plans_save' => 'Guardar',
    'plans_title' => 'Título',
    'plans_local_cost' => 'Costo Local',
    'plans_dolar_cost' => 'Costo Dolar',
    'plans_time' => 'Tiempo (Días)',
    'plans_description' => 'Descripción',
    'plans_edit' => 'Editar Plan',
    'plans_update' => 'Actualizar',
    'plans_cancel' => 'Cancelar',
    'plans_list' => 'Lista de Planes',
    'plans_search_title' => 'Buscar por Título',
    'plans_actions' => 'Acciones',

    /**
     * Manage Vendors Translate
     * @author Sebastian Cabarcas
     */

    'manage_vendors' => 'Gestionar Vendedores',
    'new_vendor' => 'Nuevo Vendedor',
    'vendors_save' => 'Guardar',
    'vendors_edit' => 'Editar Vendedor',
    'vendors_update' => 'Actualizar',
    'vendors_cancel' => 'Cancelar',
    'vendors_avatar' => 'Avatar',
    'profile_data' => 'Datos del Perfil',
    'vendors_document_number' => 'N° de Documento',
    'vendors_name' => 'Nombre o Empresa',
    'vendors_address' => 'Dirección',
    'vendors_email' => 'Email',
    'vendors_city' => 'Ciudad',
    'vendors_country' => 'País',
    'vendors_contact_information' => 'Información de Contacto',
    'vendors_phone' => 'Teléfono',
    'vendors_cellphone' => 'Celular',
    'vendors_whatsapp' => 'Whatsapp',
    'vendors_social_networks' => 'Redes Sociales',
    'vendors_web_site' => 'Sitio Web',
    'vendors_facebook' => 'Facebook',
    'vendors_instagram' => 'Instagram',
    'vendors_twitter' => 'Twitter',
    'vendors_linkedin' => 'Linkedin',
    'vendors_youtube' => 'Youtube',
    'vendors_list' => 'Lista de Vendedores',
    'vendors_search_name' => 'Buscar por Nombre o Empresa',
    'vendors_actions' => 'Acciones',
    'vendors_text_error' => 'No se puede eliminar este vendedor porque tiene publicidades asociadas.',
    'vendors_title_delete' => 'Desea borrar este Vendedor?',
    'vendors_text_delete' => 'Esta operación es irreversible!',
    'vendors_swal_delete' => 'Registro eliminado correctamente!',

    /**
     * Manage Advertisements Translate
     * @author Sebastian Cabarcas
     */

    'manage_advertisements' => 'Gestionar Publicidades',
    'manage_advertisements_title_delete' => 'Desea borrar esta Publicidad?',
    'manage_advertisements_text_delete' => 'Esta operación es irreversible!',
    'new_advertising' => 'Nueva Publicidad',
    'manage_advertisements_save' => 'Guardar',
    'manage_advertisements_title' => 'Título',
    'manage_advertisements_seller' => 'Vendedor',
    'manage_advertisements_select_seller' => 'Seleccionar Vendedor',
    'manage_advertisements_category' => 'Categoría',
    'manage_advertisements_select_category' => 'Seleccionar Categoría',
    'manage_advertisements_description' => 'Descripción',
    'manage_advertisements_advertising_type' => 'Tipo de Publicidad',
    'manage_advertisements_select_advertising_type' => 'Seleccionar Tipo de Publicidad',
    'manage_advertisements_photo' => 'Foto',
    'manage_advertisements_audio' => 'Audio',
    'manage_advertisements_video' => 'Video',
    'manage_advertisements_payment' => 'Id de Pago',
    'manage_advertisements_start_date' => 'Fecha Inicio',
    'manage_advertisements_end_date' => 'Fecha Fin',
    'manage_advertisements_plan' => 'Plan',
    'manage_advertisements_select_plan' => 'Seleccionar Plan',
    'manage_advertisements_local_cost' => 'Costo Local',
    'manage_advertisements_dolar_cost' => 'Costo Dolar',
    'manage_advertisements_status' => 'Estado',
    'manage_advertisements_url_video' => 'URL Video',
    'manage_advertisements_edit' => 'Editar Publicidad',
    'manage_advertisements_update' => 'Actualizar',
    'manage_advertisements_cancel' => 'Cancelar',
    'manage_advertisements_list' => 'Lista de Publicidades',
    'manage_advertisements_search_title' => 'Buscar por Título',
    'manage_advertisements_actions' => 'Acciones',

     /**
     * Manage Payments - Paypal Translate
     * @author Sebastian Cabarcas
     */

    'manage_payments' => 'Gestionar Pagos',
    'manage_payments_paypal' => 'Paypal',
    'manage_payments_paypal_account' => 'Cuenta de Paypal',
    'manage_payments_image' => 'Imágen',
    'manage_payments_status' => 'Estado',
    'manage_payments_status_on' => 'Activo',
    'manage_payments_status_off' => 'Inactivo',
    'manage_payments_paypal_sandbox' => 'Paypal Sandbox',
    'manage_payments_save' => 'Guardar',
    'manage_payments_cancel' => 'Cancelar',

    /**
     * Manage Payments - Mercado Pago Translate
     * @author Sebastian Cabarcas
     */

    'manage_payments_mp' => 'Mercado Pago',
    'manage_payments_mp_public_key' => 'Public Key',
    'manage_payments_mp_access_token' => 'Access Token',
    'manage_payments_mp_client_id' => 'Client ID',
    'manage_payments_mp_client_secret' => 'Client Secret',
    'manage_payments_mp_image' => 'Imágen',
    'manage_payments_mp_status' => 'Estado',
    'manage_payments_mp_status_on' => 'Activo',
    'manage_payments_mp_status_off' => 'Inactivo',
    'manage_payments_mp_sandbox' => 'Mercado Pago Sandbox',
    'manage_payments_mp_save' => 'Guardar',

    /**
     * Manage Payments - Transfer Translate
     * @author Sebastian Cabarcas
     */

    'manage_payments_transfer' => 'Transferencia',
    'manage_payments_bank_details' => 'Detalles Bancarios',
    'manage_payments_transfer_status' => 'Estado',
    'manage_payments_transfer_status_on' => 'Activo',
    'manage_payments_transfer_status_off' => 'Inactivo',
    'manage_payments_transfer_save' => 'Guardar',

    /**
     * Manage Payments Translate
     * @author Sebastian Cabarcas
     */

    'manage_payments_payment_details' => 'Detalle de Pago',
    'manage_payments_id' => 'ID',
    'manage_payments_transaction_id' => 'ID de Transacción',
    'manage_payments_payment_gateway' => 'Pasarela de Pago',
    'manage_payments_plan' => 'Plan',
    'manage_payments_days' => 'Dias',
    'manage_payments_local_cost' => 'Costo Local',
    'manage_payments_dolar_cost' => 'Costo Dolar',
    'manage_payments_name' => 'Nombre o Empresa',
    'manage_payments_document_number' => 'N° de Documento',
    'manage_payments_type_document' => 'Tipo de Documento',
    'manage_payments_cellphone' => 'Celular',
    'manage_payments_email' => 'Email',
    'manage_payments_address' => 'Dirección',
    'manage_payments_city' => 'Ciudad',
    'manage_payments_country' => 'País',
    'manage_payments_transaction_date' => 'Fecha de Transacción',
    'manage_payments_transfer_payment_details' => 'Datos de Pago por Transferencia',
    'manage_payments_proof_transfer' => 'Constancia de Transferencia',
    'manage_payments_view_file' => 'Ver Voucher',
    'manage_payments_transfer_date' => 'Fecha de Transferencia',
    'manage_payments_transfer_status' => 'Estado',
    'manage_payments_transfer_select_status' => 'Seleccionar Estado',
    'manage_payments_transfer_status_pending' => 'Pendiente',
    'manage_payments_transfer_select_approved' => 'Aprobado',
    'manage_payments_transfer_select_rejected' => 'Rechazado',
    'manage_payments_close' => 'Cerrar',
    'manage_payments_save' => 'Guardar',
    'manage_payments_list' => 'Lista de Transacciones',
    'manage_payments_search_id' => 'Buscar por ID',
    'manage_payments_actions' => 'Acciones',
    'manage_payments_title_delete' => 'Desea borrar esta Transacción?',
    'manage_payments_text_delete' => 'Esta operación es irreversible!',
    'manage_payments_swal_delete' => 'Registro eliminado correctamente!',

    /*-------------- End Admin Translate ----------------*/



     /*-------------- Start User Translate ----------------*/

    /**
     * User Panel Translate
     * @author Sebastian Cabarcas
     */

    'user_control_panel' => 'Panel de Control',
    'user_edit_profile' => 'Editar Perfil',
    'user_advertising_plans' => 'Planes Publicitarios',
    'user_shopping' => 'Compras',
    'user_logout' => 'Cerrar Sesión',
    'user_welcome' => 'Bienvenido(a): ',
    'user_change_photo' => 'Cambiar Foto:',
    'user_select_photo' => 'Seleccionar Foto',
    'user_account_details' => 'Detalles de la cuenta:',
    'user_transactions_list' => 'Lista de Transacciones',
    'user_transactions_id' => 'ID',
    'user_transactions_payment_gateway' => 'Pasarela de Pago',
    'user_transactions_payment_plan' => 'Plan',
    'user_transactions_cost' => 'Costo',
    'user_transactions_time' => 'Tiempo (Días)',
    'user_transactions_date' => 'Fecha de Transacción',
    'user_transactions_status' => 'Estado',
    'user_transactions_details' => 'Acciones',
    'user_transactions_status_pending' => 'Pendiente',
    'user_transactions_status_approved' => 'Aprobado',
    'user_transactions_status_rejected' => 'Rechazado',
    'user_transactions_transfer_date' => 'Fecha de Transferencia',
    'user_transactions_invoice' => 'Recibo',
    'user_transactions_date_two' => 'Fecha',
    'user_transactions_quantity' => 'Cantidad',
    'user_transactions_description' => 'Descripción',
    'user_transactions_subtotal' => 'Subtotal',
    'user_transactions_total' => 'Total',
    'user_transactions_print' => 'Descargar',
    'user_search_id' => 'Buscar por ID',
    'user_shopping_details' => 'Detalle de sus compras: ',
    'user_name' => 'Nombre o Empresa',
    'user_last_names' => 'Apellidos',
    'user_email' => 'Email',
    'user_type_document' => 'Tipo de Documento',
    'user_select_type_document' => 'Seleccionar',
    'user_number_dni' => 'Documento de Identidad',
    'user_number_ruc' => 'RUC',
    'user_number_ce' => 'Carnet de Extranjería',
    'user_number_document' => 'N° de Documento',
    'user_address' => 'Direción',
    'user_city' => 'Ciudad',
    'user_country' => 'País',
    'user_cellphone' => 'Celular',
    'user_previous_password' => 'Contraseña Anterior',
    'user_new_password' => 'Nueva Contraseña',
    'user_save' => 'Guardar Cambios',

    /*-------------- End User Translate ----------------*/



    /*-------------- Frontend Translate ----------------*/

    /**
     * Top Bar Translate
     * @author Sebastian Cabarcas
     */

    'frontend_today' => 'hoy es',
    'frontend_good_morning' => 'Buenos días',
    'frontend_good_afternoon' => 'Buenas tardes',
    'frontend_good_night' => 'Buenas noches',
    'frontend_monday' => 'Lunes',
    'frontend_tuesday' => 'Martes',
    'frontend_wednesday' => 'Miércoles',
    'frontend_thursday' => 'Jueves',
    'frontend_friday' => 'Viernes',
    'frontend_saturday' => 'Sábado',
    'frontend_sunday' => 'Domingo',
    'frontend_radio_online' => 'TRANSMISIÓN',

     /**
     * Top Header Translate
     * @author Sebastian Cabarcas
     */

    'frontend_last_publications' => 'Últimas Publicaciones:',
    'frontend_advertise_here' => 'ANUNCIE AQUÍ',
    'frontend_title_home' => 'INICIO',

    /**
     * Home section titles Translate
     * @author Sebastian Cabarcas
     */

    'frontend_more_views_home' => 'Más Vistas',
    'frontend_music_home' => 'Música',
    'frontend_programming_home' => 'Programación',
    'frontend_videos_home' => 'Videos',

    /**
     * Right Sidebar Translate
     * @author Sebastian Cabarcas
     */

    'frontend_categories' => 'Categorías',
    'frontend_recent' => 'Recientes',
    'frontend_top_post' => 'Top Post',

    /**
     * Post Translate
     * @author Sebastian Cabarcas
     */

    'frontend_post_most_popular' => 'Más Populares',
    'frontend_post_comments' => 'Comentarios',
    'frontend_post_publish' => 'PUBLICAR',
    'frontend_post_your_comment' => 'Tu Comentario',
    'frontend_post_reply' => 'Responder',

    /**
     * Contact Floating Translate
     * @author Sebastian Cabarcas
     */

    'frontend_contact_us_floating' => 'Contáctanos',
    'frontend_consultations_suggestions' => 'Consultas, sugerencias',
    'frontend_contact_us_floating_name' => 'Nombre',
    'frontend_contact_us_floating_cellphone' => 'Celular',
    'frontend_contact_us_floating_email' => 'Email',
    'frontend_contact_us_floating_message' => 'Mensaje',
    'frontend_contact_us_floating_send' => 'Enviar',

    /**
     * Register Translate
     * @author Sebastian Cabarcas
     */

    'frontend_please_signup' => 'Por favor, regístrate',
    'frontend_register_names' => 'Nombre o Empresa *',
    'frontend_register_last_names' => 'Apellidos *',
    'frontend_register_email' => 'Email *',
    'frontend_register_cellphone' => 'Celular *',
    'frontend_register_password' => 'Contraseña *',
    'frontend_register_confirm_password' => 'Confirmar contraseña *',
    'frontend_register' => 'REGISTRARME',
    'frontend_register_policy' => 'Al hacer clic en el botón "Registrarse", confirma que acepta nuestras condiciones de uso y nuestra política de privacidad.',
    'frontend_register_have_account' => '¿Tiene una cuenta?',
    'frontend_register_log_in' => 'Iniciar Sesión',

    /**
     * Log in Translate
     * @author Sebastian Cabarcas
     */

    'frontend_please_login' => 'Por favor, inicie sesión, o ',
    'frontend_login_sign_up' => 'Regístrate',
    'frontend_login_or' => 'o',
    'frontend_login_facebook' => 'Facebook',
    'frontend_login_google' => 'Google',
    'frontend_login_email' => 'Email',
    'frontend_login_forgot_password' => '¿Olvidaste tu contraseña?',
    'frontend_login_password' => 'Contraseña',
    'frontend_login_remember' => 'Recordarme',
    'frontend_login' => 'INICIAR SESIÓN',

    /**
     * Reset Password Translate
     * @author Sebastian Cabarcas
     */

    'frontend_reset_password' => 'Restablecer contraseña, o ',
    'frontend_reset_login' => 'Iniciar Sesión',
    'frontend_reset_email' => 'Email',
    'frontend_reset_send_link' => 'Enviar enlace de restablecimiento de contraseña',

     /**
     * Recovery Password Translate
     * @author Sebastian Cabarcas
     */

    'frontend_recovery_password' => 'Restablecer contraseña',
    'frontend_recovery_email' => 'Email',
    'frontend_recovery_pass' => 'Contraseña',
    'frontend_recovery_confirm_password' => 'Confirmar Contraseña',
    'frontend_recovery_pass_btn' => 'Restablecer contraseña',

    /**
     * Contact Us Translate
     * @author Sebastian Cabarcas
     */

    'frontend_contact_advertise_here' => 'Anuncia aquí',
    'frontend_contact_view_advertising_plans' => 'Ver Planes Publicitarios',
    'frontend_contact_name' => 'Nombre',
    'frontend_contact_email' => 'Email',
    'frontend_contact_cellphone' => 'Celular',
    'frontend_contact_message' => 'Mensaje',
    'frontend_contact_send_message' => 'Enviar Mensaje',

    /**
     * Plans Translate
     * @author Sebastian Cabarcas
     */

    'frontend_advertising_plans' => 'Planes Publicitarios',
    'frontend_plans_day' => 'Días',

    /**
     * Paypal Translate
     * @author Sebastian Cabarcas
     */

    'frontend_paypal_billing_data' => 'Datos de Facturación',
    'frontend_paypal_name' => 'Nombre o Empresa *',
    'frontend_paypal_cellphone' => 'Celular *',
    'frontend_paypal_email' => 'Email *',
    'frontend_paypal_type_document' => 'Tipo de Documento *',
    'frontend_paypal_select' => 'Seleccionar',
    'frontend_paypal_dni' => 'Documento de Identidad',
    'frontend_paypal_ruc' => 'RUC',
    'frontend_paypal_ce' => 'Carnet de Extranjería',
    'frontend_paypal_number_document' => 'N° de Documento *',
    'frontend_paypal_address' => 'Dirección *',
    'frontend_paypal_city' => 'Ciudad *',
    'frontend_paypal_country' => 'País *',
    'frontend_paypal_next' => 'Siguiente',
    'frontend_paypal_summary' => 'Resumen de su compra',
    'frontend_paypal_plan' => 'Plan',
    'frontend_paypal_cost' => 'Costo',
    'frontend_paypal_checkout' => 'Realizar Pago',
    'frontend_paypal_well_done' => '¡Bien hecho!',
    'frontend_paypal_message_one' => 'Su pago ha sido aceptado, pronto tendrá sus publicidades en el sitio. Nuestro personal se comunicará con usted lo antes posible para ultimar detalles pero también puede comunicarse con nosotros al',
    'frontend_paypal_message_two' => 'o escribir al email',
    'frontend_paypal_sorry' => '¡Lo sentimos!',
    'frontend_paypal_message_fail_one' => 'Su pago ha sido rechazado, por lo tanto no podrá tener publicidades en este sitio. Puede comunicarse con nosotros al',
    'frontend_paypal_message_fail_two' => 'o escribir al email',
    'frontend_paypal_message_fail_three' => 'si cree que esto es un error para que nuestro personal le brinde los detalles correspondientes.',
    'frontend_paypal_regards' => 'Saludos,',
    'frontend_paypal_go_home' => 'IR AL INICIO',

    /**
     * Transfer Translate
     * @author Sebastian Cabarcas
     */

    'frontend_transfer_summary' => 'Resumen de su compra',
    'frontend_transfer_bank_data' => 'Datos Bancarios',
    'frontend_transfer_plan' => 'Plan',
    'frontend_transfer_cost' => 'Costo',
    'frontend_transfer_billing_data' => 'Datos de Facturación',
    'frontend_transfer_name' => 'Nombre o Empresa *',
    'frontend_transfer_cellphone' => 'Celular *',
    'frontend_transfer_email' => 'Email *',
    'frontend_transfer_type_document' => 'Tipo de Documento *',
    'frontend_transfer_select' => 'Seleccionar',
    'frontend_transfer_dni' => 'Documento de Identidad',
    'frontend_transfer_ruc' => 'RUC',
    'frontend_transfer_ce' => 'Carnet de Extranjería',
    'frontend_transfer_number_document' => 'N° de Documento *',
    'frontend_transfer_address' => 'Dirección *',
    'frontend_transfer_city' => 'Ciudad *',
    'frontend_transfer_country' => 'País *',
    'frontend_transfer_proof' => 'Comprobante de Transferencia',
    'frontend_transfer_file' => 'Foto, escaneo o captura de pantalla *',
    'frontend_transfer_choose_file' => 'Elija un archivo...',
    'frontend_transfer_date' => 'Fecha de transferencia *',
    'frontend_transfer_checkout' => 'Realizar Pago',
    'frontend_transfer_pending' => '¡Hola!',
    'frontend_transfer_message_pending_one' => 'Tu pago esta en estado pendiente, en cuanto se confirme podrás tener publicidades en este sitio. Nuestro personal se comunicará con usted lo antes posible para ultimar detalles pero también puede comunicarse con nosotros al',
    'frontend_transfer_message_pending_two' => 'o escribir al email',
    'frontend_transfer_regards' => 'Saludos,',
    'frontend_transfer_go_home' => 'IR AL INICIO',

    /**
     * Mercado Pago Translate
     * @author Sebastian Cabarcas
     */

    'frontend_mp_billing_data' => 'Datos de Facturación',
    'frontend_mp_name' => 'Nombre o Empresa *',
    'frontend_mp_cellphone' => 'Celular *',
    'frontend_mp_email' => 'Email *',
    'frontend_mp_type_document' => 'Tipo de Documento *',
    'frontend_mp_select' => 'Seleccionar',
    'frontend_mp_dni' => 'Documento de Identidad',
    'frontend_mp_ruc' => 'RUC',
    'frontend_mp_ce' => 'Carnet de Extranjería',
    'frontend_mp_number_document' => 'N° de Documento *',
    'frontend_mp_address' => 'Dirección *',
    'frontend_mp_city' => 'Ciudad *',
    'frontend_mp_country' => 'País *',
    'frontend_mp_next' => 'Siguiente',
    'frontend_mp_summary' => 'Resumen de su compra',
    'frontend_mp_plan' => 'Plan',
    'frontend_mp_cost' => 'Costo',
    'frontend_mp_checkout' => 'Realizar Pago',
    'frontend_mp_well_done' => '¡Bien hecho!',
    'frontend_mp_message_one' => 'Su pago ha sido aceptado, pronto tendrá sus publicidades en el sitio. Nuestro personal se comunicará con usted lo antes posible para ultimar detalles pero también puede comunicarse con nosotros al',
    'frontend_mp_message_two' => 'o escribir al email',
    'frontend_mp_pending' => '¡Hola!',
    'frontend_mp_message_pending_one' => 'Tu pago esta en estado pendiente, en cuanto se confirme podrás tener publicidades en este sitio. Nuestro personal se comunicará con usted lo antes posible para ultimar detalles pero también puede comunicarse con nosotros al',
    'frontend_mp_message_pending_two' => 'o escribir al email',
    'frontend_mp_sorry' => '¡Lo sentimos!',
    'frontend_mp_message_fail_one' => 'Su pago ha sido rechazado, por lo tanto no podrá tener publicidades en este sitio. Puede comunicarse con nosotros al',
    'frontend_mp_message_fail_two' => 'o escribir al email',
    'frontend_mp_message_fail_three' => 'si cree que esto es un error para que nuestro personal le brinde los detalles correspondientes.',
    'frontend_mp_regards' => 'Saludos,',
    'frontend_mp_go_home' => 'IR AL INICIO',

    /**
     * Advertising Translate
     * @author Sebastian Cabarcas
     */

    'frontend_advertising' => 'Anuncios Publicitarios',
    'frontend_advertising_all' => 'Todas',

    /**
     * Advertising Floating Translate
     * @author Sebastian Cabarcas
     */

    'frontend_advertising_advertiser' => 'Anunciante:',
    'frontend_advertising_name' => 'Nombre',
    'frontend_advertising_dni' => 'DNI o RUC',
    'frontend_advertising_address' => 'Dirección',
    'frontend_advertising_contact' => 'Contacto:',
    'frontend_advertising_follow' => 'Síguenos:',

    /**
     * Advertising Random Translate
     * @author Sebastian Cabarcas
     */

    'frontend_more_ads' => ' Más anuncios',

    /**
     * Programs Translate
     * @author Sebastian Cabarcas
     */

    'frontend_current_program' => 'Programa Actual',
    'frontend_interact_us' => 'Interactúa con Nosotros',
    'frontend_program_start' => 'Inicio:',
    'frontend_program_end' => 'Fin:',
    'frontend_sponsors' => 'Auspiciadores',

    /**
     * Search Translate
     * @author Sebastian Cabarcas
     */

     'frontend_search' => 'Buscar Publicación',
     'frontend_result' => 'Resultado de la Búsqueda',

     /**
     * Post Translate
     * @author Sebastian Cabarcas
     */

    'frontend_post_previous' => 'Anterior',
    'frontend_post_next' => 'Siguiente',

    /*-------------- End Frontend Translate ----------------*/

];

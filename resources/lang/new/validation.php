<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'El :atributo debe ser aceptado.',
    'active_url' => 'El :atributo no es una dirección web válida.',
    'after' => 'El :atributo debe ser una fecha despues de :date.',
    'after_or_equal' => 'El :atributo debe ser una fecha igual o superior a :date.',
    'alpha' => 'El :atributo sólo puede contener letras.',
    'alpha_dash' => 'El :atributo sólo puede contener letras, múmeros, guiones y guiones bajos',
    'alpha_num' => 'El :atributo sólo puede contener letras y números.',
    'array' => 'El :atributo debe ser una matríz.',
    'before' => 'El :atributo debe ser una fecha anterior :date.',
    'before_or_equal' => 'El :atributo debe ser una fecha igual o superior a :date.',
    'between' => [
        'numeric' => 'El :atributo debe estar entre :min y :max.',
        'file' => 'El :atributo debe estar entre :min y :max kilobytes.',
        'string' => 'El :atributo debe estar entre :min y :max characters.',
        'array' => 'El :atributo debe estar entre :min y :max items.',
    ],
    'boolean' => 'El :atributo debe ser verdadero o falso.',
    'confirmed' => 'El :atributo confirmación no coinciden.',
    'date' => 'El :atributo no es una fecha válida.',
    'date_equals' => 'El :atributo debe ser una fecha igual a :date.',
    'date_format' => 'El :atributo no coincide con el formato :format.',
    'different' => 'El :atributo y :other debe ser diferente.',
    'digits' => 'El :atributo debe tener :digits dígitos.',
    'digits_between' => 'El :atributo debe estar entre :min y :max digits.',
    'dimensions' => 'El :atributo tiene dimensiones inválidas.',
    'distinct' => 'El :atributo tiene un valor duplicado.',
    'email' => 'El :atributo debe ser un email válido.',
    'ends_with' => 'El :atributo debe terminar con lo siguiente: :values.',
    'exists' => 'El :atributo seleccionado es inválido.',
    'file' => 'El :atributo debe ser un archivo.',
    'filled' => 'El :atributo debe tener un valor.',
    'gt' => [
        'numeric' => 'El :atributo debe ser mayor que :value.',
        'file' => 'El :atributo debe ser mayor que :value kilobytes.',
        'string' => 'El :atributo debe ser mayor que :value characters.',
        'array' => 'El :atributo debe tener mas que :value items.',
    ],
    'gte' => [
        'numeric' => 'El :atributo debe ser mayor o igual a :value.',
        'file' => 'El :atributo debe ser mayor o igual a :value kilobytes.',
        'string' => 'El :atributo debe ser mayor o igual a :value characters.',
        'array' => 'El :atributo debe tener :value items o más.',
    ],
    'image' => 'El :atributo debe ser una imagen.',
    'in' => 'El :atributo seleccionado es inválido.',
    'in_array' => 'El :atributo no existe en :other.',
    'integer' => 'El :atributo debe ser un número entero.',
    'ip' => 'El atributo :debe ser una dirección IP válida',
    'ipv4' => 'El atributo :debe ser una dirección IPv4 válida',
    'ipv6' => 'El atributo :debe ser una dirección IPv6 válida',
    'json' => 'El atributo :debe ser una cadena JSON válida',
    'lt' => [
        'numeric' => 'El :atributo debe ser menor a :value.',
        'file' => 'El :atributo debe ser menor a :value kilobytes.',
        'string' => 'El :atributo debe ser menor a :value characters.',
        'array' => 'El :atributo debe tener menos de :value items.',
    ],
    'lte' => [
        'numeric' => 'El :atributo debe ser menor o igual a :value.',
        'file' => 'El :atributo debe ser menor o igual a :value kilobytes.',
        'string' => 'El :atributo debe ser menor o igual a :value characters.',
        'array' => 'El :atributo debe tener menos de :value items.',
    ],
    'max' => [
        'numeric' => 'El :atributo no puede ser mayor que :max.',
        'file' => 'El :atributo no puede ser mayor que :max kilobytes.',
        'string' => 'El :atributo no puede ser mayor que :max characters.',
        'array' => 'El :atributo no debe tener más de :max items.',
    ],
    'mimes' => 'El :atributo debe ser un archivo del tipo: :values.',
    'mimetypes' => 'El :atributo debe ser un archio del tipo: :values.',
    'min' => [
        'numeric' => 'El :atributo debe ser por lo menos :min.',
        'file' => 'El :atributo debe ser al menos :min kilobytes.',
        'string' => 'El :atributo debe ser al menos :min characters.',
        'array' => 'El :atributo debe tener al menos :min items.',
    ],
    'multiple_of' => 'El atributo :debe ser un múltiplo de :value.',
    'not_in' => 'El :atributo seleccionado es inválido.',
    'not_regex' => 'El :atributo formato es inválido.',
    'numeric' => 'El :atributo debe ser un número.',
    'password' => 'La contraseña es incorrecta.',
    'present' => 'El :atributo debe estar presente.',
    'regex' => 'El :atributo formato es inválido.',
    'required' => 'El :atributo es requerido.',
    'required_if' => 'El :atributo es requerido cuando :other es :value.',
    'required_unless' => 'El :atributo es requerido a menos que :other es en :values.',
    'required_with' => 'El :atributo es requerido cuando :values esta presente.',
    'required_with_all' => 'El :atributo es requerido cuando :values están presentes.',
    'required_without' => 'El :atributo es requerido cuando :values no está presente.',
    'required_without_all' => 'El :atributo es requerido cuando ninguno de los :values están presentes.',
    'same' => 'El :atributo y :other deben coincidir.',
    'size' => [
        'numeric' => 'El :atributo debe ser :size.',
        'file' => 'El :atributo debe ser :size kilobytes.',
        'string' => 'El :atributo debe ser :size characters.',
        'array' => 'El :atributo debe contener :size items.',
    ],
    'starts_with' => 'El :atributo debe iniciar con uno de lo siguientes: :values.',
    'string' => 'El :atributo debe ser una cuerda.',
    'timezone' => 'El :atributo debe ser una zona válida.',
    'unique' => 'El :atributo ya se encuentra registrado.',
    'uploaded' => 'El :atributo fallo al cargarse.',
    'url' => 'El :atributo formato es invalido.',
    'uuid' => 'El :atributo debe ser un UUID valido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],
    'recaptcha' => 'Hey!!! :Debes demostrar que no eres un robot',

];

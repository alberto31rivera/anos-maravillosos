<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomepageSettings extends Model
{
    use HasFactory;

    protected $fillable = [
        'advertising_one',
        'url_advertising_one',
        'advertising_two',
        'url_advertising_two',
        'advertising_three',
        'url_advertising_three',
        'advertising_four',
        'url_advertising_four',
        'video_one',
        'video_two',
        'video_three',
        'video_four',
        'video_five',
        'video_six',
        'description_one',
        'description_two',
        'description_three',
        'description_four',
        'description_five',
        'description_six',
        'image_one',
        'image_two',
        'image_three',
        'image_four',
        'image_five',
        'image_six',
        'title_one',
        'title_two',
        'title_three',
        'title_four',
        'title_five',
        'title_six',
        'title_seven',
        'title_eight',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionPlan extends Model
{
    use HasFactory;


    protected $fillable = [
        'id_transaction',
        'payment_method_id ',
        'plan_id ',
        'user_id',
        'payment_method',
        'plan',
        'cost',
        'cost_dolar',
        'description',
        'duration',
        'payment_date',
        'name',
        'cellphone',
        'number_document',
        'address',
        'city',
        'country',
        'type_document',
        'email',
        'proof_transfer',
        'transfer_date',
        'status',
    ];

    public function paymentMethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id', 'id');
    }

    public function plan()
    {
        return $this->belongsTo('App\Models\Plan', 'plan_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}

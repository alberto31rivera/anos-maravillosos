<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentConfiguration extends Model
{
    use HasFactory;

    protected $fillable = [
        'payment_method_id',
        'email',
        'public_key',
        'access_token',
        'client_id',
        'client_secret',
        'image',
        'status',
        'sandbox',
    ];

    public function paymentMethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialLink extends Model
{
    use HasFactory;

    protected $table = 'social_links';

    protected $fillable = [
        'facebook_client_id',
        'facebook_client_secret',
        'facebook_status',
        'google_client_id',
        'google_client_secret',
        'google_status',
    ];
}

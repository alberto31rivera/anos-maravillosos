<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    use HasFactory;

    protected $fillable = [
      'program_id',
      'title',
      'image',
      'url',
    ];

    public function program()
    {
        return $this->belongsTo('App\Models\Program', 'program_id', 'id');
    }

}

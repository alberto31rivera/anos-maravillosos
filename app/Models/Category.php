<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image_location_id',
        'title',
        'slug',
        'description',
        'image',
        'color',
        'display_order',
        'status_home_page',
        'status_header',
    ];

    public function imageLocation(){
        return $this->belongsTo('App\Models\ImageLocation', 'image_location_id', 'id');
    }

    public function posts() {
        return $this->hasMany('App\Models\Post');
    }
}

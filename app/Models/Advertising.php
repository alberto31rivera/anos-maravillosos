<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Advertising extends Model
{
    use HasFactory;

    protected $table = 'advertisements';

    protected $fillable = [
        'vendor_id',
        'category_id',
        'plan_id',
        'cost',
        'cost_dolar',
        'status',
        'title',
        'description',
        'type_advertising',
        'image',
        'pago',
        'video',
        'audio',
        'start_date',
        'end_date',
    ];

    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor', 'vendor_id', 'id');
    }

    public function plan()
    {
        return $this->belongsTo('App\Models\Plan', 'plan_id', 'id');
    }

}

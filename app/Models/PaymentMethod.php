<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class PaymentMethod extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
    ];

    /**
     * Get the PaymentConfiguration associated with the PaymentMethod
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function paymentConfiguration(): HasOne
    {
        return $this->hasOne(PaymentConfiguration::class, 'payment_method_id', 'id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManageGoogle extends Model
{
    use HasFactory;

    protected $table = 'manage_google';

    protected $fillable = [
        'recaptcha_key',
        'recaptcha_secret_key',
        'google_analytics',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;

    protected $fillable = [
        'avatar',
        'dni',
        'name',
        'address',
        'phone',
        'cellphone',
        'whatsapp',
        'email',
        'city',
        'country',
        'web_site',
        'facebook',
        'instagram',
        'twitter',
        'linkedin',
        'youtube',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryAdvertising extends Model
{
    use HasFactory;

    protected $table = 'categories_advertising';

    protected $fillable = [
        'title',
        'slug',
    ];
}

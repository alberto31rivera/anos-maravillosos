<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThemeSetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'color_regards',
        'color_text_regards',
        'color_header',
        'color_text_header',
        'color_highlight_header',
        'color_footer',
        'color_text_footer',
        'color_contact',
        'color_buttons',
    ];
}

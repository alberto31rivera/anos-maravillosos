<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [

        'title_one',
        'slug_one',
        'description_one',
        'status_one',
        'title_two',
        'slug_two',
        'description_two',
        'status_two',
        'title_three',
        'slug_three',
        'description_three',
        'status_three',
        'title_four',
        'slug_four',
        'description_four',
        'status_four',
        'title_five',
        'slug_five',
        'description_five',
        'status_five',
        'title_six',
        'slug_six',
        'description_six',
        'status_six'
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    use HasFactory;

    protected $fillable = [
        'post_id',
        'counter',
        'date',
    ];


    public function post(){
        return $this->belongsTo('App\Models\Post', 'post_id', 'id');
    }
}

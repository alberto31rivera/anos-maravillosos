<?php

namespace App\Http\Livewire;

use Livewire\WithPagination;
use App\Models\Post;
use Livewire\Component;

class ManageTablePosts extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search;

    public function render()
    {
        return view('livewire.manage-table-posts', [
        'posts' => Post::where('title', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
        ]);
    }
}

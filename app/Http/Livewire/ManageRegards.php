<?php

namespace App\Http\Livewire;

use App\Models\Regard;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

class ManageRegards extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $view = 'create';
    protected $paginationTheme = 'bootstrap';

    public $title;
    public $url;
    public $image;
    public $search;
    public $day;
    public $start_time;
    public $end_time;
    public $regard_id;
    public $srcEditImage = "";

    public function render()
    {
        return view('livewire.manage-regards', [
            'regards' => Regard::where('title', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
        ]);
    }

    protected $rules = [
        'title' => 'required',
        'day' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'end_time' => 'after:start_time',
    ];

    public function store()
    {
        $this->validate();

        $regard = new Regard();
        $regard->title = $this->title;
        $regard->url = $this->url;
        $regard->day = $this->day;
        $regard->start_time = $this->start_time;
        $regard->end_time = $this->end_time;

        if(!empty($this->image)){
            $regard->image = $this->image->store('image-regards');
            }

        $regard->save();

        session()->flash('success-message', ' Nuevo Saludo creado con éxito.');

        $this->reset();
    }

    public function
    default()
    {
        $this->reset();
    }

    public function edit($id)
    {
        $regard = Regard::find($id);
        $srcEditImage = $regard->image;
        $this->title = $regard->title;
        $this->url = $regard->url;
        $this->regard_id = $regard->id;
        $this->day = $regard->day;
        $this->start_time = $regard->start_time;
        $this->end_time = $regard->end_time;
        $this->srcEditImage = $srcEditImage;
        $this->view = 'edit';
    }

    public function update()
    {
        $this->validate();
        $regard = Regard::find($this->regard_id);
        $regard->title = $this->title;
        $regard->url = $this->url;
        $regard->day = $this->day;
        $regard->start_time = $this->start_time;
        $regard->end_time = $this->end_time;

        if(!empty($this->image)){
            $regard->image = $this->image->store('image-regards');
            }

        $regard->save();

        session()->flash('success-message', 'Saludo actualizado con éxito.');
    }

    public function destroy($id)
    {
        Regard::destroy($id);
    }
}

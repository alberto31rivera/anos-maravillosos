<?php

namespace App\Http\Livewire;

use App\Models\Advertising;
use App\Models\CategoryAdvertising;
use Livewire\Component;

class ManageAdvertisementsComponent extends Component
{
    public $advertising, $categoryId, $indiceCollecction = 0, $maxLength = 0, $collectionAdvertisingIds = [];

    public function render()
    {
        $categoriesAdvertising = CategoryAdvertising::all();

        $query = Advertising::orderBy('id', 'ASC');

        if($this->categoryId != 0){
            $query = $query->where('category_id', $this->categoryId);
        }


        return view('livewire.manage-advertisements-component', [
            'categoriesAdvertising' => $categoriesAdvertising,
            'advertisements' => $query->get(),
        ]);
    }

    public function mount(){
        $this->categoryId = 0;
        $this->stablishAdvertisingIds();
    }

    public function setItemValue($id)
    {
        $this->advertising = Advertising::find($id);
    }


    public function stablishAdvertisingIds()
    {
        $this->collectionAdvertisingIds = Advertising::where('status','Activo')->get()->pluck('id');
        $this->maxLength = count($this->collectionAdvertisingIds);
        $this->indiceCollecction = 0;
    }

    public function previous()
    {

        $this->indiceCollecction --;
        if($this->indiceCollecction < 0){
            $this->indiceCollecction = $this->maxLength - 1;
        }
        $this->advertising = Advertising::find($this->collectionAdvertisingIds[$this->indiceCollecction]);

    }

    public function next()
    {
        $this->indiceCollecction ++;
        if($this->indiceCollecction > $this->maxLength - 1){
            $this->indiceCollecction = 0;
        }
        $this->advertising = Advertising::find($this->collectionAdvertisingIds[$this->indiceCollecction]);
    }

    public function setCategoryValue($id)
    {
        $this->categoryId = $id;
    }
}

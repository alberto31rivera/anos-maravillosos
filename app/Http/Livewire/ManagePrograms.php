<?php

namespace App\Http\Livewire;

use App\Models\Program;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

class ManagePrograms extends Component
{
    use WithPagination;
    use WithFileUploads;
    public $view = 'create';
    protected $paginationTheme = 'bootstrap';

    public $search;
    public $title;
    public $url;
    public $description;
    public $image;
    public $day;
    public $start_time;
    public $end_time;
    public $program_id;
    public $srcEditImage = "";

    public function render()
    {
        return view('livewire.manage-programs', [
            'programs' => Program::where('title', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
        ]);
    }

    protected $rules = [
        'title' => 'required',
        'day' => 'required',
        'description' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'end_time' => 'after:start_time',
    ];

    public function store()
    {
        $this->validate();

        $program = new Program();
        $program->title = $this->title;
        $program->url = $this->url;
        $program->description = $this->description;
        $program->day = $this->day;
        $program->start_time = $this->start_time;
        $program->end_time = $this->end_time;

        if(!empty($this->image)){
            $program->image = $this->image->store('image-programs');
            }

        $program->save();

        session()->flash('success-message', ' Nuevo Programa creado con éxito.');

        $this->reset();
    }

    public function
    default()
    {
        $this->reset();
    }

    public function edit($id)
    {
        $program = Program::find($id);
        $srcEditImage = $program->image;
        $this->title = $program->title;
        $this->url = $program->url;
        $this->program_id = $program->id;
        $this->description = $program->description;
        $this->day = $program->day;
        $this->start_time = $program->start_time;
        $this->end_time = $program->end_time;
        $this->srcEditImage = $srcEditImage;
        $this->view = 'edit';
    }

    public function update()
    {
        $program = Program::find($this->program_id);
        $program->title = $this->title;
        $program->url = $this->url;
        $program->description = $this->description;
        $program->day = $this->day;
        $program->start_time = $this->start_time;
        $program->end_time = $this->end_time;

        if(!empty($this->image)){
            $program->image = $this->image->store('image-programs');
            }

        $this->validate();
        $program->save();

        session()->flash('success-message', 'Programa actualizado con éxito.');
    }

    public function destroy($id)
    {
        Program::destroy($id);
    }
}

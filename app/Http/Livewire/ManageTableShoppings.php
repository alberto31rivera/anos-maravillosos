<?php

namespace App\Http\Livewire;

use App\Models\Currency;
use App\Models\TransactionPlan;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;
use Barryvdh\DomPDF\Facade as PDF;

class ManageTableShoppings extends Component
{
    use WithPagination;
    const ADMIN = 1;

    protected $paginationTheme = 'bootstrap';
    public $search;
    public $document;

    public function render()
    {
        $transactions = TransactionPlan::where('user_id', Auth::user()->id)->where('id', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10);
        $admin = User::where('role_id', $this::ADMIN)->first();

        return view('livewire.manage-table-shoppings', [
            'transactions' => $transactions,
            'admin' => $admin,
            'currencyLocal' => Currency::where('id', 2)->first(),
            'currencyDolar' => Currency::where('id', 1)->first(),
            ]);
    }

    public function setValue($id)
    {
        $this->document = TransactionPlan::find($id);
    }

    /*
    public function dowloandPdf($id)
    {
        $admin = User::where('role_id', $this::ADMIN)->first();
        $currencyDolar = Currency::where('id', 1)->first();
        $currencyLocal = Currency::where('id', 2)->first();
        $document = TransactionPlan::find($id);

        $pdf = PDF::loadView('admin.print.index', ['admin' => $admin, 'document'=> $document, 'currencyDolar' => $currencyDolar, 'currencyLocal' => $currencyLocal])->output();
        return response()->streamDownload(
            fn () => print($pdf),
            "recibo.pdf"
       );
    }
    */

}

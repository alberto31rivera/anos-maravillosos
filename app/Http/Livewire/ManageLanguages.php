<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Artisan;
use Jackiedo\DotenvEditor\Facades\DotenvEditor;
use Livewire\Component;

class ManageLanguages extends Component
{
    public $view = 'create';
    public $language;

    public function render()
    {
        return view('livewire.manage-languages');
    }

    public function downloadFile()
    {
        $path = resource_path('lang/new/multi-leng.php');

        return response()->download($path);
    }

    function setLanguage($language)
    {
        $file = DotenvEditor::setKeys([
            [
                'key'     => 'APP_LOCALE',
                'value'   => $language,
            ],
        ]);

        $file = DotenvEditor::save();

        Artisan::call('config:clear');

        session()->flash('success-message', 'Idioma configurado.');

        $this->dispatchBrowserEvent('refresh-page');

    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Sponsor;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class ManageTableSponsors extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search = '';

    public function render()
    {
        $query = Sponsor::orderBy('created_at', 'DESC');

        $query = $query->with('program')->whereHas(
            'program',
            function (Builder $subQuery) {
                $subQuery->where('title', 'like', '%' . $this->search . '%');
            }
        );

        $query = $query->paginate(10);

        return view('livewire.manage-table-sponsors', [
            'sponsors' => $query,
        ]);
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithPagination;

class ManageSearchHome extends Component
{
    use WithPagination;

    public $search;
    public $result;
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        if (!empty($this->search)) {
            $post = Post::where('title', 'like', '%' . $this->search . '%')->get();
        } else {
            $post = '';
        }
        return view('livewire.manage-search-home', [
            'post' => $post,
        ]);
    }

    public function search()
    {
        if (!empty($this->search)) {
            $this->result = Post::where('title', 'like', '%' . $this->search . '%')->get();
        } else {
            $this->result = '';
        }
    }
}

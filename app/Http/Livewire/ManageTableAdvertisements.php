<?php

namespace App\Http\Livewire;

use App\Models\Advertising;
use Livewire\Component;
use Livewire\WithPagination;

class ManageTableAdvertisements extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search;
    public function render()
    {
        return view('livewire.manage-table-advertisements', [
        'advertisements' => Advertising::where('title', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
        ]);
    }

}

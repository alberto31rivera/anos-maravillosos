<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class ManageComments extends Component
{
    use WithPagination;

    public $search = '';
    public $query;

    public function render()
    {
        $query = Comment::orderBy('created_at', 'DESC');

        $query = $query->with('users')->whereHas('users', function(Builder $subQuery){
            $subQuery->where('name', 'like', '%' . $this->search . '%');
        }
        );

        $query = $query->paginate(10);

        return view('livewire.manage-comments', [
            'comments' => $query,
        ]);
    }

    public function destroy($id)
    {
        Comment::destroy($id);
    }
}

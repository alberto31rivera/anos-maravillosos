<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\ImageLocation;
use App\Models\Post;
use Illuminate\Http\Request;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class ManageCategories extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $view = 'create';
    protected $paginationTheme = 'bootstrap';

    public $image_location_id;
    public $title;
    public $slug;
    public $description;
    public $display_order;
    public $status_header = 0;
    public $search;
    public $category_id;
    public $srcEditImage = "";

    public function render()
    {
        return view('livewire.manage-categories', [
            'categories' => Category::where('title', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
            'image_locations' => ImageLocation::all(),
        ]);
    }

    protected $rules = [
        'image_location_id' => 'required',
        'title' => 'required',
        'slug' => 'unique:categories',
    ];

    public function store()
    {
        $this->validate();

        $collection = collect([
            'blue', 'green', 'orange', 'yellow', 'pink', 'purple', 'dark', 'teal', 'teal-dark', 'purple-wine',
            'maroon', 'fuchsia', 'crimson', 'red', 'olive', 'dark-green'
        ]);
        $colors = $collection->shuffle();

        $category = new Category();
        $category->image_location_id = $this->image_location_id;
        $category->title = $this->title;
        $category->slug = Str::slug($this->title, '-');
        $category->color = $colors->first();

        if (Category::where('slug', $category->slug)->exists()) {
            return view('livewire.manage-categories', [
                session()->flash('error-message', 'Ya existe una categoría con este título, por favor cambie el título de la categoría.'),
            ]);
        }

        if (Category::where('status_header', 1)->get()->count() >= 4 && $this->status_header == 1) {
            return view('livewire.manage-categories', [
                session()->flash('error-message', 'No puede poner mas de 4 categorias en el header.'),
            ]);
        }

        if (Category::whereNotNull('display_order')->where('display_order', $category->display_order)->exists()) {
            return view('livewire.manage-categories', [
                session()->flash('error-message', 'Ya existe una categoría en esta posición.'),
            ]);
        }

        $category->display_order = $this->display_order;
        $category->status_header = $this->status_header;
        $category->save();

        session()->flash('success-message', 'Nueva Categoría creada con éxito.');

        $this->reset();
    }

    public function
    default()
    {
        $this->reset();
    }

    public function edit($id)
    {
        $category = Category::find($id);
        $srcEditImage = $category->image;
        $this->image_location_id = $category->image_location_id;
        $this->category_id = $category->id;
        $this->title = $category->title;
        $this->slug = $category->slug;
        $this->display_order = $category->display_order;
        $this->status_header = $category->status_header;
        $this->srcEditImage = $srcEditImage;
        $this->view = 'edit';
    }

    public function update()
    {
        $this->validate([
            'title' => 'required',
            ]);

        $category = Category::find($this->category_id);
        $category->image_location_id = $this->image_location_id;
        $category->title = $this->title;

        if ($category->slug !=  Str::slug($this->title, '-')) {
            if (Category::where('slug', Str::slug($this->title, '-'))->exists()) {
                return view('livewire.manage-categories', [
                    session()->flash('error-message', 'Ya existe una categoría con este título, por favor cambie el título de la categoría.'),
                ]);
            }
        }

        if ($category->status_header != $this->status_header) {
            if (Category::where('status_header', 1)->get()->count() >= 4 && $this->status_header == 1) {
                return view('livewire.manage-categories', [
                    session()->flash('error-message', 'No puede poner mas de 4 categorias en el header.'),
                ]);
            }
        }

        if ($category->display_order != $this->display_order) {

            if (Category::whereNotNull('display_order')->where('display_order', $this->display_order)->exists()) {
                return view('livewire.manage-categories', [
                    session()->flash('error-message', 'Ya existe una categoría en esta posición.'),
                ]);
            }
        }

        $category->slug = Str::slug($this->title, '-');
        $category->status_header = $this->status_header;
        if($category->status_header == 0){
            $category->display_order = '';
        }else{
            $category->display_order = $this->display_order;
        }

        $category->save();

        session()->flash('success-message', 'Categoría actualizada con éxito.');
    }


}

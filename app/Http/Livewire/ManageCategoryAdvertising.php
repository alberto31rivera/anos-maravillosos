<?php

namespace App\Http\Livewire;

use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Livewire\Component;
use App\Models\CategoryAdvertising;
use Illuminate\Support\Str;

class ManageCategoryAdvertising extends Component
{
    use WithFileUploads;
    use WithPagination;
    public $view = 'create';
    protected $paginationTheme = 'bootstrap';

    public $search;
    public $title;
    public $slug;
    public $categoryAdvertising_id;

    public function render()
    {
        return view('livewire.manage-category-advertising', [
            'categoriesAdvertising' => CategoryAdvertising::where('title', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
        ]);
    }

    protected $rules = [
        'title' => 'required',
        'slug' => 'unique:posts',
    ];

    public function store()
    {
        $this->validate();

        $categoryAdvertising = new CategoryAdvertising();
        $categoryAdvertising->title = $this->title;
        $categoryAdvertising->slug = Str::slug($this->title, '-');


        if (CategoryAdvertising::where('slug', $categoryAdvertising->slug)->exists()) {

            return view('livewire.manage-category-advertising', [
                session()->flash('error-message', 'Ya existe una categoría con este título, por favor cambie el título de la categoría.'),
            ]);
        }

        $categoryAdvertising->save();

        session()->flash('success-message', ' Nueva Categoría creada con éxito.');

        $this->reset();
    }

    public function
    default()
    {
        $this->reset();
    }

    public function edit($id)
    {
        $categoryAdvertising = CategoryAdvertising::find($id);
        $this->categoryAdvertising_id = $categoryAdvertising->id;
        $this->title = $categoryAdvertising->title;
        $this->slug = $categoryAdvertising->slug;
        $this->view = 'edit';
    }

    public function update()
    {
        $this->validate();

        $categoryAdvertising = CategoryAdvertising::find($this->categoryAdvertising_id);
        $categoryAdvertising->title = $this->title;

        if ($categoryAdvertising->slug !=  Str::slug($this->title, '-')) {
            if (CategoryAdvertising::where('slug', Str::slug($this->title, '-'))->exists()) {
                return view('livewire.manage-category-advertising', [
                    session()->flash('error-message', 'Ya existe una categoría con este título, por favor cambie el título de la categoría.'),
                ]);
            }
        }

        $categoryAdvertising->slug = Str::slug($this->title, '-');

        $categoryAdvertising->save();

        session()->flash('success-message', 'Categoría actualizada con éxito.');
    }

    public function destroy($id)
    {
        CategoryAdvertising::destroy($id);
    }
}

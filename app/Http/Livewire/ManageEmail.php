<?php

namespace App\Http\Livewire;

use Jackiedo\DotenvEditor\Facades\DotenvEditor;
use Livewire\Component;

class ManageEmail extends Component
{
    public $view = 'create';
    public $email_driver, $host, $port, $encryption, $username, $password, $email, $name, $timezone;

    public function mount()
    {
        $this->email_driver = DotenvEditor::getValue('MAIL_MAILER');
        $this->host = DotenvEditor::getValue('MAIL_HOST');
        $this->port = DotenvEditor::getValue('MAIL_PORT');
        $this->encryption = DotenvEditor::getValue('MAIL_ENCRYPTION');
        $this->username = DotenvEditor::getValue('MAIL_USERNAME');
        $this->password = DotenvEditor::getValue('MAIL_PASSWORD');
        $this->email = DotenvEditor::getValue('MAIL_FROM_ADDRESS');
        $this->name = DotenvEditor::getValue('MAIL_FROM_NAME');
        $this->timezone = DotenvEditor::getValue('TIMEZONE');

    }

    public function render()
    {
        return view('livewire.manage-email');
    }

    protected $rules = [
        'host' => 'required',
        'port' => 'required',
        'encryption' => 'required',
        'username' => 'required',
        'password' => 'required',
        'email' => 'required',
        'name' => 'required',
    ];

    function store()
    {
        $this->validate();

        $file = DotenvEditor::setKeys([
            [
                'key'     => 'MAIL_MAILER',
                'value'   => $this->email_driver,
            ],
            [
                'key'     => 'MAIL_HOST',
                'value'   => $this->host,
            ],
            [
                'key'     => 'MAIL_PORT',
                'value'   => $this->port,
            ],
            [
                'key'     => 'MAIL_ENCRYPTION',
                'value'   => $this->encryption,
            ],
            [
                'key'     => 'MAIL_USERNAME',
                'value'   => $this->username,
            ],
            [
                'key'     => 'MAIL_PASSWORD',
                'value'   => $this->password,
            ],
            [
                'key'     => 'MAIL_FROM_ADDRESS',
                'value'   => $this->email,
            ],
            [
                'key'     => 'MAIL_FROM_NAME',
                'value'   => $this->name,
            ],
            [
            'key'     => 'APP_NAME',
            'value'   => $this->name,
             ],
            [
                'key'     => 'TIMEZONE',
                'value'   => $this->timezone,
            ],
        ]);

        $file = DotenvEditor::save();

        session()->flash('success-message', 'Datos actualizados con éxito.');

        $this->dispatchBrowserEvent('refresh-page');
    }
}

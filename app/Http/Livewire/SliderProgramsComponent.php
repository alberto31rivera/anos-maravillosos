<?php

namespace App\Http\Livewire;

use App\Models\Advertising;
use Livewire\Component;

class SliderProgramsComponent extends Component
{
    public $advertising, $indiceCollecction = 0, $maxLength = 0, $collectionAdvertisingIds = [];

    public function render()
    {
        return view('livewire.slider-programs-component', [
            'advertisements' => Advertising::all(),
        ]);
    }

    public function mount(){
        $this->stablishAdvertisingIds();
    }

    public function setItemValue($id)
    {
        $this->advertising = Advertising::find($id);
    }

    public function stablishAdvertisingIds()
    {
        $this->collectionAdvertisingIds = Advertising::where('status','Activo')->get()->pluck('id');
        $this->maxLength = count($this->collectionAdvertisingIds);
        $this->indiceCollecction = 0;
    }

    public function previous()
    {

        $this->indiceCollecction --;
        if($this->indiceCollecction < 0){
            $this->indiceCollecction = $this->maxLength - 1;
        }
        $this->advertising = Advertising::find($this->collectionAdvertisingIds[$this->indiceCollecction]);

    }

    public function next()
    {
        $this->indiceCollecction ++;
        if($this->indiceCollecction > $this->maxLength - 1){
            $this->indiceCollecction = 0;
        }
        $this->advertising = Advertising::find($this->collectionAdvertisingIds[$this->indiceCollecction]);
    }
}

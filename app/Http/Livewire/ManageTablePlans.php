<?php

namespace App\Http\Livewire;

use App\Models\Currency;
use App\Models\Plan;
use Livewire\WithPagination;
use Livewire\Component;

class ManageTablePlans extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search;

    public function render()
    {
        return view('livewire.manage-table-plans', [
            'plans' => Plan::where('title', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
            'currencyDolar' => Currency::where('id', 1)->first(),
            'currencyLocal' => Currency::where('id', 2)->first(),
            ]);
    }
}

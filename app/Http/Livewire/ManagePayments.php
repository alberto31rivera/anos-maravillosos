<?php

namespace App\Http\Livewire;

use App\Models\Currency;
use App\Models\TransactionPlan;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Barryvdh\DomPDF\Facade as PDF;


class ManagePayments extends Component
{
    use WithPagination;
    const ADMIN = 1;

    protected $paginationTheme = 'bootstrap';

    public $search;
    public $id_transfer;
    public $id_transaction;
    public $payment_method;
    public $plan;
    public $days;
    public $cost;
    public $cost_dolar;
    public $payment_date;
    public $name;
    public $number_document;
    public $type_document;
    public $cellphone;
    public $email;
    public $address;
    public $city;
    public $country;
    public $proof_transfer;
    public $status;
    public $transfer_date;
    public $refStatus;
    public $flag = 0;
    public $document;

    public function render()
    {
        return view('livewire.manage-payments', [
            'transactions' => TransactionPlan::where('id', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
            'currencyDolar' => Currency::where('id', 1)->first(),
            'currencyLocal' => Currency::where('id', 2)->first(),
            'admin' =>  User::where('role_id', $this::ADMIN)->first(),
        ]);
    }

    public function store()
    {
       if($this->status == null){
        session()->flash('error-message', 'Debe elegir una transacción.');
       }else{
        $transaction = TransactionPlan::find($this->id_transfer);
        $transaction->status = $this->status;
        if($this->status == ''){
            session()->flash('error-message', 'Debe seleccionar un estado válido.');
        }else{
            $transaction->save();
            session()->flash('success-message', 'Estado actualizado con éxito.');
            $this->reset();
        }
       }

    }

    public function edit($id)
    {
        $currencyDolar = Currency::where('id', 1)->first();
        $currencyLocal = Currency::where('id', 2)->first();
        $transaction = TransactionPlan::find($id);
        $this->id_transfer = $transaction->id;
        $this->id_transaction = $transaction->id_transaction;
        $this->payment_method  = $transaction->payment_method;
        $this->plan = $transaction->plan;
        $this->days = $transaction->duration;
        $this->cost = $transaction->cost.' '.$currencyLocal->name;
        $this->cost_dolar = $transaction->cost_dolar.' '.$currencyDolar->name;
        $this->payment_date = $transaction->payment_date;
        $this->name = $transaction->name;
        $this->number_document = $transaction->number_document;
        if($transaction->type_document == 'ce')
        $this->type_document = 'Carnet de Extranjería';
        elseif($transaction->type_document == 'dni')
        $this->type_document = 'Documento de Identidad';
        else
        $this->type_document = 'RUC';
        $this->cellphone = $transaction->cellphone;
        $this->email = $transaction->email;
        $this->address = $transaction->address;
        $this->city = $transaction->city;
        $this->country = $transaction->country;
        $this->proof_transfer = $transaction->proof_transfer;
        $this->transfer_date = $transaction->transfer_date;
        $this->status = $transaction->status;
        $this->refStatus = $transaction;
        $this->flag = 1;
    }

    public function destroy($id)
    {
        TransactionPlan::destroy($id);
    }

    public function setValue($id)
    {
     $this->document = TransactionPlan::find($id);
    }

    /*
    public function dowloandPdf($id)
    {
        $admin = User::where('role_id', $this::ADMIN)->first();
        $currencyDolar = Currency::where('id', 1)->first();
        $currencyLocal = Currency::where('id', 2)->first();
        $document = TransactionPlan::find($id);

        $pdf = PDF::loadView('admin.print.index', ['admin' => $admin, 'document'=> $document, 'currencyDolar' => $currencyDolar, 'currencyLocal' => $currencyLocal])->output();
        return response()->streamDownload(
            fn () => print($pdf),
            "recibo.pdf"
       );
    }
    */


    public function
    default()
    {
        $this->reset();
    }
}

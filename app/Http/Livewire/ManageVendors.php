<?php

namespace App\Http\Livewire;

use App\Models\Vendor;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Livewire\Component;

class ManageVendors extends Component
{
    use WithFileUploads;
    use WithPagination;
    public $view = 'create';
    protected $paginationTheme = 'bootstrap';

    public $search;
    public $vendor_id;
    public $avatar;
    public $dni;
    public $name;
    public $email;
    public $city;
    public $country;
    public $address;
    public $phone;
    public $cellphone;
    public $web_site;
    public $whatsapp;
    public $facebook;
    public $instagram;
    public $twitter;
    public $linkedin;
    public $youtube;
    public $srcEditAvatar = "";

    public function render()
    {
        return view('livewire.manage-vendors', [
            'vendors' => Vendor::where('name', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
        ]);
    }

    public function store()
    {
        $this->validate([
        'dni' => 'required',
        'name' => 'required',
        'email' => 'required',
        'cellphone' => 'required',
        'whatsapp' => 'required',
        'avatar' => 'mimes:jpg,jpeg,bmp,png',
        ]);

        $vendor = new Vendor();
        $vendor->dni = $this->dni;
        $vendor->name = $this->name;
        $vendor->email = $this->email;
        $vendor->city = $this->city;
        $vendor->country = $this->country;
        $vendor->address = $this->address;
        $vendor->phone = $this->phone;
        $vendor->cellphone = $this->cellphone;
        $vendor->web_site = $this->web_site;
        $vendor->whatsapp = $this->whatsapp;
        $vendor->facebook = $this->facebook;
        $vendor->instagram = $this->instagram;
        $vendor->twitter = $this->twitter;
        $vendor->linkedin = $this->linkedin;
        $vendor->youtube = $this->youtube;

        if (!empty($this->avatar)) {
            $vendor->avatar = $this->avatar->store('avatar-vendors');
        }

        $vendor->save();

        session()->flash('success-message', ' Nuevo Vendedor creado con éxito.');

        $this->reset();
    }

    public function
    default()
    {
        $this->reset();
    }

    public function edit($id)
    {

        $vendor = Vendor::find($id);
        $srcEditAvatar = $vendor->avatar;
        $this->vendor_id = $vendor->id;
        $this->dni = $vendor->dni;
        $this->name = $vendor->name;
        $this->email = $vendor->email;
        $this->city = $vendor->city;
        $this->country = $vendor->country;
        $this->address = $vendor->address;
        $this->phone = $vendor->phone;
        $this->cellphone = $vendor->cellphone;
        $this->web_site = $vendor->web_site;
        $this->whatsapp = $vendor->whatsapp;
        $this->facebook = $vendor->facebook;
        $this->instagram = $vendor->instagram;
        $this->twitter = $vendor->twitter;
        $this->linkedin = $vendor->linkedin;
        $this->youtube = $vendor->youtube;
        $this->srcEditAvatar = $srcEditAvatar;
        $this->view = 'edit';
    }

    public function update()
    {
        $this->validate([
            'dni' => 'required',
            'name' => 'required',
            'email' => 'required',
            'cellphone' => 'required',
            'whatsapp' => 'required',
            ]);

        $vendor = Vendor::find($this->vendor_id);
        $vendor->dni = $this->dni;
        $vendor->name = $this->name;
        $vendor->email = $this->email;
        $vendor->city = $this->city;
        $vendor->country = $this->country;
        $vendor->address = $this->address;
        $vendor->phone = $this->phone;
        $vendor->cellphone = $this->cellphone;
        $vendor->web_site = $this->web_site;
        $vendor->whatsapp = $this->whatsapp;
        $vendor->facebook = $this->facebook;
        $vendor->instagram = $this->instagram;
        $vendor->twitter = $this->twitter;
        $vendor->linkedin = $this->linkedin;
        $vendor->youtube = $this->youtube;

        if (!empty($this->avatar)) {
            $vendor->avatar = $this->avatar->store('avatar-vendors');
        }

        $vendor->save();

        session()->flash('success-message', 'Vendedor actualizado con éxito.');

    }

    public function destroy($id)
    {
        Vendor::destroy($id);
    }

}

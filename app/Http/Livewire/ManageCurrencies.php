<?php

namespace App\Http\Livewire;

use App\Models\Currency;
use Livewire\Component;

class ManageCurrencies extends Component
{
    public $id_currency;
    public $name;
    public $flag = 0;

    public function render()
    {
        return view('livewire.manage-currencies', [
            'currencies' => Currency::all(),
        ]);
    }

    public function store()
    {
        if($this->name != ''){
            $currency = Currency::find($this->id_currency);
            $currency->name = $this->name;
            $currency->save();
            session()->flash('success-message', 'Nombre de Moneda actualizada con éxito.');
        }else{
            session()->flash('error-message', 'Debe colocar un nombre a su moneda');
        }

    }

    public function edit($id)
    {
        $currency = Currency::find($id);
        $this->id_currency = $currency->id;
        $this->flag = 1;
        $this->name = $currency->name;
    }

    public function
    default()
    {
        $this->reset();
    }
}

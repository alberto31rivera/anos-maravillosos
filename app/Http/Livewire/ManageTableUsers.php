<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class ManageTableUsers extends Component
{
    const USER = 2;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search;

    public function render()
    {
        return view('livewire.manage-table-users', [
            'users' => User::where('role_id', $this::USER)->where('name', 'like', '%' . $this->search . '%')->orderBy('created_at', 'DESC')->paginate(10),
            ]);
    }
}

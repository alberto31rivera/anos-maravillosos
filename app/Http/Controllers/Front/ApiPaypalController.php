<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\HomepageSettings;
use App\Models\Plan;
use App\Models\Post;
use App\Models\Regard;
use App\Models\Service;
use App\Models\ThemeSetting;
use App\Models\TransactionPlan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Traits\ApiPaypal;
use App\Models\Currency;
use App\Models\PaymentMethod;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ApiPaypalController extends Controller
{
    use ApiPaypal;

    public function __construct()
    {
        //Start information header
        $this->now = Carbon::now();
        $this->day = $this->now->locale('es');
        $hour = $this->now->isoFormat('H:mm');
        $this->msg = '';

        $startMorning = Carbon::createFromTimeString('06:00');
        $endMorning = Carbon::createFromTimeString('11:59');
        $startEvening = Carbon::createFromTimeString('12:00');
        $endEvening = Carbon::createFromTimeString('17:59');
        $startNigth = Carbon::createFromTimeString('18:00');
        $endNigth = Carbon::createFromTimeString('05:59')->addDay();

        $this->regards = Regard::where('day', $this->day->dayName)->whereTime('start_time', '<=',  $hour)->whereTime('end_time', '>=',  $hour)->inRandomOrder()->take(1)->first();


        if ($this->now->between($startMorning, $endMorning)) {
            $this->msg = 'Buenos días';
        }

        if ($this->now->between($startEvening, $endEvening)) {
            $this->msg = 'Buenas tardes';
        }

        if ($this->now->between($startNigth, $endNigth)) {
            $this->msg = 'Buenas noches';
        }
        //End information header


        $this->recentPosts = Post::orderBy('created_at', 'DESC')->take(4)->get();
        $this->allCategories = Category::orderBy('created_at', 'DESC')->get();

        $this->admin = User::where('role_id', 1)->first();

        $this->banner = HomepageSettings::all()->first();

        $this->themeSettings = ThemeSetting::all()->first();

        $this->services = Service::all()->first();
    }

    public function successResponse(Request $params)
    {

        if (Session::get('id_plan') == null) {
            return redirect()->route('f.home.index');
        }

        $now = Carbon::now();

        $transactionPaypal = new TransactionPlan();
        $transactionPaypal->id_transaction = $params->paymentId;
        $transactionPaypal->payment_method = 'Paypal';
        $transactionPaypal->plan = Plan::find(Session::get('id_plan'))->title;
        $transactionPaypal->cost = Plan::find(Session::get('id_plan'))->cost;
        $transactionPaypal->cost_dolar = Plan::find(Session::get('id_plan'))->cost_dolar;
        $transactionPaypal->duration = Plan::find(Session::get('id_plan'))->duration;
        $transactionPaypal->description = Plan::find(Session::get('id_plan'))->description;
        $transactionPaypal->payment_date = $now->format('Y-m-d');
        $transactionPaypal->name = Session::get('name');
        $transactionPaypal->cellphone = Session::get('cellphone');
        $transactionPaypal->email = Session::get('email');
        $transactionPaypal->type_document = Session::get('type_document');
        $transactionPaypal->number_document = Session::get('number_document');
        $transactionPaypal->address = Session::get('address');
        $transactionPaypal->city = Session::get('city');
        $transactionPaypal->country = Session::get('country');
        $transactionPaypal->transfer_date = $now->format('Y-m-d');
        $transactionPaypal->status = 1;
        if(Auth::check()){
            $transactionPaypal->user_id = Auth::user()->id;
        }
        $transactionPaypal->save();

        Session::forget('image');
        Session::forget('id_plan');
        Session::forget('name');
        Session::forget('cellphone');
        Session::forget('email');
        Session::forget('type_document');
        Session::forget('city');
        Session::forget('country');
        Session::forget('number_document');
        Session::forget('address');

        return view('front.pages.payments.paypal.paypal-succes-response', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }

    public function failedResponse(Request $params)
    {

        if (Session::get('id_plan') == null) {
            return redirect()->route('f.home.index');
        }

        $now = Carbon::now();

        $transactionPaypal = new TransactionPlan();
        $transactionPaypal->id_transaction = $params->paymentId;
        $transactionPaypal->payment_method = 'Paypal';
        $transactionPaypal->plan = Plan::find(Session::get('id_plan'))->title;
        $transactionPaypal->cost = Plan::find(Session::get('id_plan'))->cost;
        $transactionPaypal->cost_dolar = Plan::find(Session::get('id_plan'))->cost_dolar;
        $transactionPaypal->duration = Plan::find(Session::get('id_plan'))->duration;
        $transactionPaypal->description = Plan::find(Session::get('id_plan'))->description;
        $transactionPaypal->payment_date = $now->format('Y-m-d');
        $transactionPaypal->name = Session::get('name');
        $transactionPaypal->cellphone = Session::get('cellphone');
        $transactionPaypal->email = Session::get('email');
        $transactionPaypal->type_document = Session::get('type_document');
        $transactionPaypal->number_document = Session::get('number_document');
        $transactionPaypal->address = Session::get('address');
        $transactionPaypal->city = Session::get('city');
        $transactionPaypal->country = Session::get('country');
        $transactionPaypal->transfer_date = $now->format('Y-m-d');
        $transactionPaypal->status = 0;
        if(Auth::check()){
            $transactionPaypal->user_id = Auth::user()->id;
        }
        $transactionPaypal->save();

        Session::forget('image');
        Session::forget('id_plan');
        Session::forget('name');
        Session::forget('cellphone');
        Session::forget('email');
        Session::forget('type_document');
        Session::forget('city');
        Session::forget('country');
        Session::forget('number_document');
        Session::forget('address');

        return view('front.pages.payments.paypal.paypal-failed-response', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }


    public function paymentsPaypal(Plan $plan)
    {
        $image = PaymentMethod::find(1)->paymentConfiguration->image;

        return view('front.pages.payments.paypal.paypal-billing-information', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
            'plan' => $plan,
            'image' => $image,
        ]);
    }

    public function SendBillingInformation(Request $request)
    {
        $image = PaymentMethod::find(1)->paymentConfiguration->image;

        Session::put('image', $image);
        Session::put('name', $request->name);
        Session::put('cellphone', $request->cellphone);
        Session::put('email', $request->email);
        Session::put('type_document', $request->type_document);
        Session::put('number_document', $request->number_document);
        Session::put('address', $request->address);
        Session::put('city', $request->city);
        Session::put('country', $request->country);
        Session::put('id_plan', $request->id_plan);

        $plan = Plan::find($request->id_plan);

        return $this->makePaypalForm($plan);
    }
}

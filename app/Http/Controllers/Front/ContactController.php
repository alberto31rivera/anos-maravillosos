<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Jackiedo\DotenvEditor\Facades\DotenvEditor;

class ContactController extends Controller
{

    public function contactUs(Request $request)
    {
        $data = [];

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'cellphone' => 'required',
            'message' => 'required',
        ]);

        $data['name'] = $request->name;
        $data['cellphone'] = $request->cellphone;
        $data['email'] = $request->email;
        $data['messages'] = $request->message;

        Mail::send('emails.contact_us', $data, function ($message) use ($data) {
            $message->to(DotenvEditor::getValue('MAIL_SUPPORT'), $data['name'], $data['cellphone'], $data['email'], $data['messages'])->subject('Una persona está interesada.');
        });

        return redirect()->back()->with('success-message', 'Información enviada con éxito.');
    }
}

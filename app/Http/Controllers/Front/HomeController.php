<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Post;
use App\Models\Visit;
use App\Models\Comment;
use App\Models\HomepageSettings;
use App\Models\PaymentConfiguration;
use App\Models\Plan;
use App\Models\Program;
use App\Models\Regard;
use App\Models\Service;
use App\Models\SocialLink;
use App\Models\ThemeSetting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use App\Http\Traits\ApiPaypal;
use App\Models\Currency;
use App\Models\ManageGoogle;
use App\Models\PaymentMethod;
use App\Models\Sponsor;
use App\Models\Transmission;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{

    use ApiPaypal;

    const SECTION_ONE = 1, SECTION_TWO = 2, SECTION_THREE = 3, SECTION_FOUR = 4, SECTION_FIVE = 5,
        SECTION_SIX = 6, SECTION_SEVEN = 7, SECTION_EIGHT = 8, SECTION_NINE = 9;

    const ADMIN = 1;

    const PAYPAL = 1, MERCADOPAGO = 2, TRANSFER = 3;

    public function boot()
    {
        Paginator::useBootstrap();
    }
    public function __construct()
    {
        $credentials = ManageGoogle::findOrFail(1);
        Config::set('recaptcha.api_site_key', $credentials->recaptcha_key);
        Config::set('recaptcha.api_secret_key', $credentials->recaptcha_secret_key);

        //Start information header
        $this->now = Carbon::now();
        $this->day = $this->now->locale('es');
        $hour = $this->now->isoFormat('H:mm');
        $this->msg = '';

        $startMorning = Carbon::createFromTimeString('06:00');
        $endMorning = Carbon::createFromTimeString('11:59');
        $startEvening = Carbon::createFromTimeString('12:00');
        $endEvening = Carbon::createFromTimeString('17:59');
        $startNigth = Carbon::createFromTimeString('18:00');
        $endNigth = Carbon::createFromTimeString('05:59')->addDay();

        $this->regards = Regard::where('day', $this->day->dayName)->whereTime('start_time', '<=',  $hour)->whereTime('end_time', '>=',  $hour)->inRandomOrder()->take(1)->first();
        if ($this->now->between($startMorning, $endMorning)) {
            $this->msg = 'Buenos días';
        }

        if ($this->now->between($startEvening, $endEvening)) {
            $this->msg = 'Buenas tardes';
        }

        if ($this->now->between($startNigth, $endNigth)) {
            $this->msg = 'Buenas noches';
        }
        //End information header

        $startDay = Carbon::now()->startOfDay();
        $endDay   = Carbon::now()->endOfDay();
        $startWeek = Carbon::now()->startOfWeek();
        $endWeek  = Carbon::now()->endOfWeek();
        $startMonth = Carbon::now()->startOfMonth();
        $endMonth  = Carbon::now()->endOfMonth();

        $this->postMoreViewsDay = Visit::whereBetween('date', [$startDay, $endDay])->orderBy('counter', 'DESC')->take(5)->get();
        $this->postMoreViewsWeek = Visit::whereBetween('date', [$startWeek, $endWeek])->orderBy('counter', 'DESC')->take(4)->get();
        $this->postMoreViewsMonth = Visit::whereBetween('date', [$startMonth, $endMonth])->orderBy('counter', 'DESC')->take(4)->get();
        $this->postMorePopular = Visit::whereBetween('date', [$startMonth, $endMonth])->orderBy('counter', 'DESC')->take(5)->get();
        $this->recentPosts = Post::orderBy('created_at', 'DESC')->take(4)->get(); 
        $this->allCategories = Category::orderBy('created_at', 'DESC')->take(5)->get();
        $this->oldPosts = Post::orderBy('created_at', 'ASC')->take(3)->get();
        $this->oldCategories = Category::orderBy('created_at', 'ASC')->take(5)->get();
        $this->lastNews = Post::orderBy('created_at', 'DESC')->take(1)->get();

        $this->postsSectionNine = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_NINE);
        })->orderBy('created_at', 'DESC')->take(4)->get();

        $this->postsSectionTwo = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_TWO);
        })->orderBy('created_at', 'DESC')->take(3)->get();

        $this->admin = User::where('role_id', $this::ADMIN)->first();

        $this->banner = HomepageSettings::all()->first();

        $this->flag = '';

        if($this->banner != null){
            if($this->banner->video_one != null && $this->banner->image_one != null && $this->banner->description_one != null)
            $this->flag = 1;
            elseif($this->banner->video_two != null && $this->banner->image_two != null && $this->banner->description_two != null)
            $this->flag = 1;
            elseif($this->banner->video_three != null && $this->banner->image_three != null && $this->banner->description_three != null)
            $this->flag = 1;
            elseif($this->banner->video_four!= null && $this->banner->image_four!= null && $this->banner->description_four!= null)
            $this->flag = 1;
            elseif($this->banner->video_five!= null && $this->banner->image_five!= null && $this->banner->description_five!= null)
            $this->flag = 1;
            elseif($this->banner->video_six != null && $this->banner->image_six != null && $this->banner->description_six != null)
            $this->flag = 1;
            else{
                $this->flag = 0;
            }
        }

        $this->themeSettings = ThemeSetting::all()->first();

        $this->services = Service::all()->first();

        $this->paymentsPaypal = PaymentConfiguration::where('payment_method_id', $this::PAYPAL)->first();
        $this->paymentsMercadoPago = PaymentConfiguration::where('payment_method_id', $this::MERCADOPAGO)->first();
        $this->paymentsTransfer = PaymentConfiguration::where('payment_method_id', $this::TRANSFER)->first();
    }

    public function index()
    {
        $layout = 'silver';

        $postsSectionOne = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_ONE);
        })->orderBy('created_at', 'DESC')->take(3)->get();

        $postsSectionThree = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_THREE);
        })->orderBy('created_at', 'DESC')->take(3)->get();

        $postsSectionFour = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_FOUR);
        })->orderBy('created_at', 'DESC')->take(3)->get();

        $postsSectionFive = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_FIVE);
        })->orderBy('created_at', 'DESC')->take(3)->get();

        $postsSectionSix = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_SIX);
        })->orderBy('created_at', 'DESC')->take(1)->get();

        $postsSectionSeven = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_SEVEN);
        })->orderBy('created_at', 'DESC')->take(2)->get();

        $postsSectionEight = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_EIGHT);
        })->orderBy('created_at', 'DESC')->take(3)->get();


        return view('front.home.index', [
            'postMoreViewsDay' => $this->postMoreViewsDay,
            'postMoreViewsWeek' => $this->postMoreViewsWeek,
            'postMoreViewsMonth' => $this->postMoreViewsMonth,
            'postsSectionOne' => $postsSectionOne,
            'postsSectionTwo' => $this->postsSectionTwo,
            'postsSectionThree' => $postsSectionThree,
            'postsSectionFour' => $postsSectionFour,
            'postsSectionFive' => $postsSectionFive,
            'postsSectionSix' => $postsSectionSix,
            'postsSectionSeven' => $postsSectionSeven,
            'postsSectionEight' => $postsSectionEight,
            'postsSectionNine' => $this->postsSectionNine,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'oldPosts' => $this->oldPosts,
            'oldCategories' => $this->oldCategories,
            'lastNews' => $this->lastNews,
            'layout' => $layout,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'banner' => $this->banner,
            'flag' => $this->flag,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,

        ]);
    }

    public function login()
    {
        $layout = 'silver';
        $socialLinks = SocialLink::all()->first();

        return view('login', [
            'layout' => $layout,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'socialLinks' => $socialLinks,
            'lastNews' => $this->lastNews,
            'services' => $this->services,
        ]);
    }

    public function register()
    {
        $layout = 'silver';

        return view('register', [
            'layout' => $layout,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'lastNews' => $this->lastNews,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }

    public function resetPassword()
    {
        $layout = 'silver';

        return view('reset-password', [
            'layout' => $layout,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'lastNews' => $this->lastNews,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }

    public function categories($slug)
    {
        $layout = 'silver';

        $category = Category::where('slug', $slug)->firstOrFail();

        $postsCategory = Post::where('category_id', $category->id)->latest()->paginate(20);

        return view('front.pages.categories.index', [
            'layout' => $layout,
            'lastNews' => $this->lastNews,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'category' => $category,
            'postsCategory' => $postsCategory,
            'postsSectionTwo' => $this->postsSectionTwo,
            'postsSectionNine' => $this->postsSectionNine,
            'postMoreViewsWeek' => $this->postMoreViewsWeek,
            'postMoreViewsMonth' => $this->postMoreViewsMonth,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }


    public function posts($slug1, $slug2)
    {
        $layout = 'silver';

        $post = Post::where('slug', $slug2)->firstOrFail();
        $nextPost = Post::where('id', $post->id + 1)->first();
        $previusPost = Post::where('id', $post->id - 1)->first();

        $visit = Visit::where('post_id', $post->id)->first();

        if ($visit == null) {
            $visit = new Visit();
            $visit->post_id = $post->id;
            $visit->date = Carbon::now();
            $visit->counter += 1;
            $visit->save();
        } else {
            $visit->date = Carbon::now();
            $visit->counter += 1;
            $visit->save();
        }

        return view('front.pages.posts.index', [
            'post' => $post,
            'layout' => $layout,
            'lastNews' => $this->lastNews,
            'postsSectionTwo' => $this->postsSectionTwo,
            'postsSectionNine' => $this->postsSectionNine,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'postMoreViewsWeek' => $this->postMoreViewsWeek,
            'postMoreViewsMonth' => $this->postMoreViewsMonth,
            'postMorePopular' => $this->postMorePopular,
            'nextPost' => $nextPost,
            'previusPost' => $previusPost,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }

    public function postsRef($slug1, $slug2, $id)
    {
        $post = Post::find($id);
        if($post == null){
            return redirect('/');
        }

        return view('front.pages.posts.reference', [
            'slug1' => $slug1,
            'slug2' => $slug2,
            'post' => $post,
        ]);
    }

    public function addComment(Request $request, $idPost)
    {
        if (Auth::guest()) {
            return back()->with([
                'error' => 'Para comentar debe registrarse.',
            ]);
        }

        if(Auth::user()->is_banned == 1){
            return back()->with([
                'error' => 'Su cuenta a sido bloqueada para enviar comentarios por infringir nuestras políticas de uso.
                            Comuníquese a '.env('MAIL_USERNAME').' para poder activarla nuevamente.',
            ]);
        }

        $comment = new Comment();
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $idPost;
        $comment->parent_id = $request->parent_id;
        $comment->title = $request->title;
        $comment->date = Carbon::now();
        $comment->save();

        return back()->withErrors([
            'msg' => 'Comentario realizado correctamente.',
        ]);
    }

    public function programs()
    {
        $sponsors = '';
        $layout = 'silver';
        $now = Carbon::now();
        $day = $now->locale('es');
        $hour = $now->isoFormat('H:mm');
        $transmission = Transmission::all()->first();

        $programs = Program::where('day', $day->dayName)->whereTime('start_time', '<=',  $hour)->whereTime('end_time', '>=',  $hour)->inRandomOrder()->take(1)->first();
        if($programs != null){
            $sponsors = Sponsor::where('program_id', $programs->id)->get();
        }


        return view('front.home.programs.index', [
            'lastNews' => $this->lastNews,
            'postsSectionTwo' => $this->postsSectionTwo,
            'postsSectionNine' => $this->postsSectionNine,
            'allCategories' => $this->allCategories,
            'recentPosts' => $this->recentPosts,
            'programs' => $programs,
            'transmission' => $transmission,
            'sponsors' => $sponsors,
            'layout' => $layout,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'postMoreViewsWeek' => $this->postMoreViewsWeek,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }

    public function advert()
    {
        return view('front.pages.advert.index', [
            'layout' => 'silver',
            'lastNews' => $this->lastNews,
            'postsSectionTwo' => $this->postsSectionTwo,
            'postsSectionNine' => $this->postsSectionNine,
            'allCategories' => $this->allCategories,
            'recentPosts' => $this->recentPosts,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }

    public function contanctUs()
    {
        return view('front.pages.contact-us.index', [
            'layout' => 'silver',
            'lastNews' => $this->lastNews,
            'postsSectionTwo' => $this->postsSectionTwo,
            'postsSectionNine' => $this->postsSectionNine,
            'allCategories' => $this->allCategories,
            'recentPosts' => $this->recentPosts,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }

    public function paymentsMercadoPago(Plan $plan)
    {
        $image = PaymentMethod::find(2)->paymentConfiguration->image;

        return view('front.pages.payments.mercadopago.mp-billing-information', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'lastNews' => $this->lastNews,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
            'plan' => $plan,
            'image' => $image,
        ]);
    }

    public function paymentsTransfer($id)
    {
        $plan = Plan::find($id);

        return view('front.pages.payments.transfer', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'lastNews' => $this->lastNews,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
            'plan' => $plan,
            'paymentsTransfer' => $this->paymentsTransfer,
            'currencyLocal' => Currency::where('id', 2)->first(),

        ]);
    }

    public function adPlans()
    {
        $plans = Plan::all();

        return view('front.pages.plans.ad', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
            'lastNews' => $this->lastNews,
            'plans' => $plans,
            'paymentsTransfer' => $this->paymentsTransfer,
            'paymentsPaypal' => $this->paymentsPaypal,
            'paymentsMercadoPago' => $this->paymentsMercadoPago,
            'currencyDolar' => Currency::where('id', 1)->first(),
            'currencyLocal' => Currency::where('id', 2)->first(),
        ]);
    }

    public function serviceOne($slug)
    {
        $getService = Service::where('slug_one', $slug)->first();

        if ($getService->status_one == 1) {
            return view('front.pages.services.index', [
                'layout' => 'silver',
                'lastNews' => $this->lastNews,
                'postsSectionTwo' => $this->postsSectionTwo,
                'postsSectionNine' => $this->postsSectionNine,
                'allCategories' => $this->allCategories,
                'recentPosts' => $this->recentPosts,
                'day' => $this->day,
                'msg' => $this->msg,
                'regards' => $this->regards,
                'admin' => $this->admin,
                'banner' => $this->banner,
                'themeSettings' => $this->themeSettings,
                'services' => $this->services,
                'getService' => $getService,
                'slug' => $slug,
            ]);
        } else {
            return redirect()->route('f.home.index');
        }
    }

    public function serviceTwo($slug)
    {
        $getService = Service::where('slug_two', $slug)->first();

        if ($getService->status_two == 1) {
            return view('front.pages.services.index', [
                'layout' => 'silver',
                'lastNews' => $this->lastNews,
                'postsSectionTwo' => $this->postsSectionTwo,
                'postsSectionNine' => $this->postsSectionNine,
                'allCategories' => $this->allCategories,
                'recentPosts' => $this->recentPosts,
                'day' => $this->day,
                'msg' => $this->msg,
                'regards' => $this->regards,
                'admin' => $this->admin,
                'banner' => $this->banner,
                'themeSettings' => $this->themeSettings,
                'services' => $this->services,
                'getService' => $getService,
                'slug' => $slug,
            ]);
        } else {
            return redirect()->route('f.home.index');
        }
    }

    public function serviceThree($slug)
    {
        $getService = Service::where('slug_three', $slug)->first();

        if ($getService->status_three == 1) {
            return view('front.pages.services.index', [
                'layout' => 'silver',
                'lastNews' => $this->lastNews,
                'postsSectionTwo' => $this->postsSectionTwo,
                'postsSectionNine' => $this->postsSectionNine,
                'allCategories' => $this->allCategories,
                'recentPosts' => $this->recentPosts,
                'day' => $this->day,
                'msg' => $this->msg,
                'regards' => $this->regards,
                'admin' => $this->admin,
                'banner' => $this->banner,
                'themeSettings' => $this->themeSettings,
                'services' => $this->services,
                'getService' => $getService,
                'slug' => $slug,
            ]);
        } else {
            return redirect()->route('f.home.index');
        }
    }

    public function serviceFour($slug)
    {
        $getService = Service::where('slug_four', $slug)->first();

        if ($getService->status_four == 1) {
            return view('front.pages.services.index', [
                'layout' => 'silver',
                'lastNews' => $this->lastNews,
                'postsSectionTwo' => $this->postsSectionTwo,
                'postsSectionNine' => $this->postsSectionNine,
                'allCategories' => $this->allCategories,
                'recentPosts' => $this->recentPosts,
                'day' => $this->day,
                'msg' => $this->msg,
                'regards' => $this->regards,
                'admin' => $this->admin,
                'banner' => $this->banner,
                'themeSettings' => $this->themeSettings,
                'services' => $this->services,
                'getService' => $getService,
                'slug' => $slug,
            ]);
        } else {
            return redirect()->route('f.home.index');
        }
    }

    public function serviceFive($slug)
    {
        $getService = Service::where('slug_five', $slug)->first();

        if ($getService->status_five == 1) {
            return view('front.pages.services.index', [
                'layout' => 'silver',
                'lastNews' => $this->lastNews,
                'postsSectionTwo' => $this->postsSectionTwo,
                'postsSectionNine' => $this->postsSectionNine,
                'allCategories' => $this->allCategories,
                'recentPosts' => $this->recentPosts,
                'day' => $this->day,
                'msg' => $this->msg,
                'regards' => $this->regards,
                'admin' => $this->admin,
                'banner' => $this->banner,
                'themeSettings' => $this->themeSettings,
                'services' => $this->services,
                'getService' => $getService,
                'slug' => $slug,
            ]);
        } else {
            return redirect()->route('f.home.index');
        }
    }

    public function serviceSix($slug)
    {
        $getService = Service::where('slug_six', $slug)->first();

        if ($getService->status_six == 1) {
            return view('front.pages.services.index', [
                'layout' => 'silver',
                'lastNews' => $this->lastNews,
                'postsSectionTwo' => $this->postsSectionTwo,
                'postsSectionNine' => $this->postsSectionNine,
                'allCategories' => $this->allCategories,
                'recentPosts' => $this->recentPosts,
                'day' => $this->day,
                'msg' => $this->msg,
                'regards' => $this->regards,
                'admin' => $this->admin,
                'banner' => $this->banner,
                'themeSettings' => $this->themeSettings,
                'services' => $this->services,
                'getService' => $getService,
                'slug' => $slug,
            ]);
        } else {
            return redirect()->route('f.home.index');
        }
    }

    public function search()
    {
        $layout = 'silver';
        return view('front.pages.searches.index', [
            'layout' => $layout,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'postMoreViewsWeek' => $this->postMoreViewsWeek,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'banner' => $this->banner,
            'lastNews' => $this->lastNews,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }

    // Temp Method:
    public function paymentsTransferResponse($e=-1)
    {
        $layout = 'silver';
        $srcLayout = [
            'layout' => $layout,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'postMoreViewsWeek' => $this->postMoreViewsWeek,
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ];
        if ($e==1)
        return view('front.pages.payments.transfer.success-response', $srcLayout);
            elseif ($e==0)
            return view('front.pages.payments.transfer.pending-response', $srcLayout);
                elseif ($e==-1)
                return view('front.pages.payments.transfer.failed-response', $srcLayout);
    }

}

<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Traits\ApiMercadoPago;
use App\Models\Category;
use App\Models\Currency;
use App\Models\HomepageSettings;
use App\Models\PaymentMethod;
use App\Models\Plan;
use App\Models\Post;
use App\Models\Regard;
use App\Models\Service;
use App\Models\ThemeSetting;
use App\Models\TransactionPlan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ApiMercadoPagoController extends Controller
{

    use ApiMercadoPago;

    public function __construct()
    {
        //Start information header
        $this->now = Carbon::now();
        $this->day = $this->now->locale('es');
        $hour = $this->now->isoFormat('H:mm');
        $this->msg = '';

        $startMorning = Carbon::createFromTimeString('06:00');
        $endMorning = Carbon::createFromTimeString('11:59');
        $startEvening = Carbon::createFromTimeString('12:00');
        $endEvening = Carbon::createFromTimeString('17:59');
        $startNigth = Carbon::createFromTimeString('18:00');
        $endNigth = Carbon::createFromTimeString('05:59')->addDay();

        $this->regards = Regard::where('day', $this->day->dayName)->whereTime('start_time', '<=',  $hour)->whereTime('end_time', '>=',  $hour)->inRandomOrder()->take(1)->first();


        if ($this->now->between($startMorning, $endMorning)) {
            $this->msg = 'Buenos días';
        }

        if ($this->now->between($startEvening, $endEvening)) {
            $this->msg = 'Buenas tardes';
        }

        if ($this->now->between($startNigth, $endNigth)) {
            $this->msg = 'Buenas noches';
        }
        //End information header


        $this->recentPosts = Post::orderBy('created_at', 'DESC')->take(4)->get();
        $this->allCategories = Category::orderBy('created_at', 'DESC')->get();

        $this->admin = User::where('role_id', 1)->first();

        $this->banner = HomepageSettings::all()->first();

        $this->themeSettings = ThemeSetting::all()->first();

        $this->services = Service::all()->first();
    }


    public function sendBillingInformation(Request $request)
    {
        $image = PaymentMethod::find(2)->paymentConfiguration->image;

        Session::put('image', $image);
        Session::put('name', $request->name);
        Session::put('cellphone', $request->cellphone);
        Session::put('email', $request->email);
        Session::put('type_document', $request->type_document);
        Session::put('number_document', $request->number_document);
        Session::put('address', $request->address);
        Session::put('city', $request->city);
        Session::put('country', $request->country);
        Session::put('id_plan', $request->id_plan);
        $plan = Plan::find($request->id_plan);
        $preference = $this->makePreference($plan);

        return view('front.pages.payments.mercado-pago', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
            'plan' => $plan,
            'preference' => $preference,
            'paymentConfiguration' => PaymentMethod::find(2)->paymentConfiguration,
            'image' => $image,
            'currencyLocal' => Currency::where('id', 2)->first(),
        ]);

    }


    public function success()
    {
        if (Session::get('id_plan') == null) {
            return redirect()->route('f.home.index');
        }

        $now = Carbon::now();

        $transactionMercadoPago = new TransactionPlan();
        $transactionMercadoPago->id_transaction = $_REQUEST['payment_id'];
        $transactionMercadoPago->payment_method = 'Mercado Pago';
        $transactionMercadoPago->plan = Plan::find(Session::get('id_plan'))->title;
        $transactionMercadoPago->cost = Plan::find(Session::get('id_plan'))->cost;
        $transactionMercadoPago->cost_dolar = Plan::find(Session::get('id_plan'))->cost_dolar;
        $transactionMercadoPago->description = Plan::find(Session::get('id_plan'))->description;
        $transactionMercadoPago->duration = Plan::find(Session::get('id_plan'))->duration;
        $transactionMercadoPago->payment_date = $now->format('Y-m-d');
        $transactionMercadoPago->name = Session::get('name');
        $transactionMercadoPago->cellphone = Session::get('cellphone');
        $transactionMercadoPago->email = Session::get('email');
        $transactionMercadoPago->type_document = Session::get('type_document');
        $transactionMercadoPago->number_document = Session::get('number_document');
        $transactionMercadoPago->address = Session::get('address');
        $transactionMercadoPago->city = Session::get('city');
        $transactionMercadoPago->country = Session::get('country');
        $transactionMercadoPago->transfer_date = $now->format('Y-m-d');
        $transactionMercadoPago->status = 1;
        if(Auth::check()){
            $transactionMercadoPago->user_id = Auth::user()->id;
        }
        $transactionMercadoPago->save();

        Session::forget('image');
        Session::forget('id_plan');
        Session::forget('name');
        Session::forget('cellphone');
        Session::forget('email');
        Session::forget('type_document');
        Session::forget('city');
        Session::forget('country');
        Session::forget('number_document');
        Session::forget('address');

        return view('front.pages.payments.mercadopago.mp-succes-response', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);

    }

    public function pending()
    {
        if (Session::get('id_plan') == null) {
            return redirect()->route('f.home.index');
        }

        $now = Carbon::now();

        $transactionMercadoPago = new TransactionPlan();
        $transactionMercadoPago->id_transaction = $_REQUEST['payment_id'];
        $transactionMercadoPago->payment_method = 'Mercado Pago';
        $transactionMercadoPago->plan = Plan::find(Session::get('id_plan'))->title;
        $transactionMercadoPago->cost = Plan::find(Session::get('id_plan'))->cost;
        $transactionMercadoPago->cost_dolar = Plan::find(Session::get('id_plan'))->cost_dolar;
        $transactionMercadoPago->description = Plan::find(Session::get('id_plan'))->description;
        $transactionMercadoPago->duration = Plan::find(Session::get('id_plan'))->duration;
        $transactionMercadoPago->payment_date = $now->format('Y-m-d');
        $transactionMercadoPago->name = Session::get('name');
        $transactionMercadoPago->cellphone = Session::get('cellphone');
        $transactionMercadoPago->email = Session::get('email');
        $transactionMercadoPago->type_document = Session::get('type_document');
        $transactionMercadoPago->number_document = Session::get('number_document');
        $transactionMercadoPago->address = Session::get('address');
        $transactionMercadoPago->city = Session::get('city');
        $transactionMercadoPago->country = Session::get('country');
        $transactionMercadoPago->transfer_date = $now->format('Y-m-d');
        $transactionMercadoPago->status = 0;
        if(Auth::check()){
            $transactionMercadoPago->user_id = Auth::user()->id;
        }
        $transactionMercadoPago->save();

        Session::forget('image');
        Session::forget('id_plan');
        Session::forget('name');
        Session::forget('cellphone');
        Session::forget('email');
        Session::forget('type_document');
        Session::forget('city');
        Session::forget('country');
        Session::forget('number_document');
        Session::forget('address');

        return view('front.pages.payments.mercadopago.mp-pending-response', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);

    }

    public function failure()
    {
        if (Session::get('id_plan') == null) {
            return redirect()->route('f.home.index');
        }

        $now = Carbon::now();

        $transactionMercadoPago = new TransactionPlan();
        $transactionMercadoPago->id_transaction = $_REQUEST['payment_id'];
        $transactionMercadoPago->payment_method = 'Mercado Pago';
        $transactionMercadoPago->plan = Plan::find(Session::get('id_plan'))->title;
        $transactionMercadoPago->cost = Plan::find(Session::get('id_plan'))->cost;
        $transactionMercadoPago->cost_dolar = Plan::find(Session::get('id_plan'))->cost_dolar;
        $transactionMercadoPago->description = Plan::find(Session::get('id_plan'))->description;
        $transactionMercadoPago->duration = Plan::find(Session::get('id_plan'))->duration;
        $transactionMercadoPago->payment_date = $now->format('Y-m-d');
        $transactionMercadoPago->name = Session::get('name');
        $transactionMercadoPago->cellphone = Session::get('cellphone');
        $transactionMercadoPago->email = Session::get('email');
        $transactionMercadoPago->type_document = Session::get('type_document');
        $transactionMercadoPago->number_document = Session::get('number_document');
        $transactionMercadoPago->address = Session::get('address');
        $transactionMercadoPago->city = Session::get('city');
        $transactionMercadoPago->country = Session::get('country');
        $transactionMercadoPago->transfer_date = $now->format('Y-m-d');
        $transactionMercadoPago->status = 2;
        if(Auth::check()){
            $transactionMercadoPago->user_id = Auth::user()->id;
        }
        $transactionMercadoPago->save();

        Session::forget('image');
        Session::forget('id_plan');
        Session::forget('name');
        Session::forget('cellphone');
        Session::forget('email');
        Session::forget('type_document');
        Session::forget('city');
        Session::forget('country');
        Session::forget('number_document');
        Session::forget('address');

        return view('front.pages.payments.mercadopago.mp-failed-response', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);

    }

}

<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\HomepageSettings;
use App\Models\PaymentMethod;
use App\Models\Post;
use App\Models\Regard;
use App\Models\Service;
use App\Models\ThemeSetting;
use App\Models\TransactionPlan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class TransactionTransferController extends Controller
{

    public function __construct()
    {
        //Start information header
        $this->now = Carbon::now();
        $this->day = $this->now->locale('es');
        $hour = $this->now->isoFormat('H:mm');
        $this->msg = '';

        $startMorning = Carbon::createFromTimeString('06:00');
        $endMorning = Carbon::createFromTimeString('11:59');
        $startEvening = Carbon::createFromTimeString('12:00');
        $endEvening = Carbon::createFromTimeString('17:59');
        $startNigth = Carbon::createFromTimeString('18:00');
        $endNigth = Carbon::createFromTimeString('05:59')->addDay();

        $this->regards = Regard::where('day', $this->day->dayName)->whereTime('start_time', '<=',  $hour)->whereTime('end_time', '>=',  $hour)->inRandomOrder()->take(1)->first();


        if ($this->now->between($startMorning, $endMorning)) {
            $this->msg = 'Buenos días';
        }

        if ($this->now->between($startEvening, $endEvening)) {
            $this->msg = 'Buenas tardes';
        }

        if ($this->now->between($startNigth, $endNigth)) {
            $this->msg = 'Buenas noches';
        }
        //End information header

        $this->recentPosts = Post::orderBy('created_at', 'DESC')->take(4)->get();
        $this->allCategories = Category::orderBy('created_at', 'DESC')->get();

        $this->admin = User::where('role_id', 1)->first();

        $this->banner = HomepageSettings::all()->first();

        $this->themeSettings = ThemeSetting::all()->first();

        $this->services = Service::all()->first();
    }

    public function payTransfer(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'cellphone' => 'required',
            'number_document' => 'required',
            'email' => 'required',
            'transfer_date' => 'required',
            'proof_transfer' => 'required|mimes:jpg,jpeg,png',
        ]);

        $status = 0;
        $now = Carbon::now();
        $payment_method = PaymentMethod::find($id);

        $transactionTransfer = new TransactionPlan();
        $transactionTransfer->payment_method = $payment_method->title;
        $transactionTransfer->plan = $request->plan;
        $transactionTransfer->cost = Str::of($request->cost)->explode(' ')[0];
        $transactionTransfer->cost_dolar = $request->cost_dolar;
        $transactionTransfer->description = $request->description;
        $transactionTransfer->duration = $request->duration;
        $transactionTransfer->payment_date = $now->format('Y-m-d');
        $transactionTransfer->name = $request->name;
        $transactionTransfer->cellphone = $request->cellphone;
        $transactionTransfer->number_document = $request->number_document;
        $transactionTransfer->address = $request->address;
        $transactionTransfer->city = $request->city;
        $transactionTransfer->country = $request->country;
        $transactionTransfer->type_document = $request->type_document;
        $transactionTransfer->email = $request->email;
        $transactionTransfer->transfer_date = $request->transfer_date;
        $transactionTransfer->status = $status;

        if (!empty($request->proof_transfer)) {

            $file = $request->file('proof_transfer');

            //obtenemos el nombre del archivo
            $nameDocument =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameDocument = str_replace(' ', '-', $nameDocument);

            //movemos el archivo a la ruta indicada
            $file->move('documents/voucher-transfer', $nameDocument);

            //guardamos el nombre de la ruta en base de datos
            $transactionTransfer->proof_transfer = $nameDocument;
        }

        if(Auth::check()){
            $transactionTransfer->user_id = Auth::user()->id;
        }

        $transactionTransfer->save();

        return view('front.pages.payments.transfer.pending-response', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
        ]);
    }
}

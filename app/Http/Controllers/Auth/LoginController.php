<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    const ROLE_USER = 2;
    const ROLE_ADMIN = 1;

    public function authenticateAdmin(Request $request)
    {
        $credentials = $request->only('email', 'password');

         /**
         * PpshareKey validationby USER::ROLE
         */
        $ppshareKeyRole = User::where('email', $request->email)->first();

        if($ppshareKeyRole != null){
            if($ppshareKeyRole->role_id == $this::ROLE_USER){
                return back()->withErrors([
                    'email' => 'Las credenciales proporcionadas no coinciden con nuestros registros.',
                ]);
            }
        }

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            $user = Auth::user();
            if ($user->role_id == $this::ROLE_ADMIN) {
                return redirect()->intended('admin/dashboard');
            }
        }

        return back()->withErrors([
            'email' => 'Las credenciales proporcionadas no coinciden con nuestros registros.',
        ]);
    }

    public function authenticateUser(Request $request)
    {
         $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        /**
         * PpshareKey validationby USER::ROLE
         */
        $ppshareKeyRole = User::where('email', $request->email)->first();

        if($ppshareKeyRole != null){
            if($ppshareKeyRole->role_id == $this::ROLE_ADMIN){
                return back()->withErrors([
                    'email' => 'Las credenciales proporcionadas no coinciden con nuestros registros.',
                ]);
            }
        }

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            $user = Auth::user();
            if ($user->role_id == $this::ROLE_USER && $user->confirmed == 1) {

                return redirect()->intended('panel');
            }

            return back()->withErrors([
                'email' => 'Su email no ha sido verificado.',
            ]);
        }

        return back()->withErrors([
            'email' => 'Las credenciales proporcionadas no coinciden con nuestros registros.',
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $user = Auth::user();

        switch ($user->role_id) {

            case $this::ROLE_ADMIN:

                Auth::logout();

                $request->session()->invalidate();

                $request->session()->regenerateToken();

                return redirect('/admin');

                break;

            case $this::ROLE_USER:

                Auth::logout();

                $request->session()->invalidate();

                $request->session()->regenerateToken();

                return redirect('/iniciar-sesion');

                break;
        }
    }
}

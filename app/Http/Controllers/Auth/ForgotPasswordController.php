<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\HomepageSettings;
use App\Models\Post;
use App\Models\Regard;
use App\Models\Service;
use App\Models\ThemeSetting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    const ADMIN = 1;

    public function __construct()
    {
        //Start information header
        $this->now = Carbon::now();
        $this->day = $this->now->locale('es');
        $hour = $this->now->isoFormat('H:mm');
        $this->msg = '';
        $this->layout = 'silver';

        $startMorning = Carbon::createFromTimeString('06:00');
        $endMorning = Carbon::createFromTimeString('11:59');
        $startEvening = Carbon::createFromTimeString('12:00');
        $endEvening = Carbon::createFromTimeString('17:59');
        $startNigth = Carbon::createFromTimeString('18:00');
        $endNigth = Carbon::createFromTimeString('05:59')->addDay();

        if ($this->now->between($startMorning, $endMorning)) {
            $this->msg = 'Buenos días';
        }

        if ($this->now->between($startEvening, $endEvening)) {
            $this->msg = 'Buenas tardes';
        }

        if ($this->now->between($startNigth, $endNigth)) {
            $this->msg = 'Buenas noches';
        }

        $this->regards = Regard::where('day', $this->day->dayName)->whereTime('start_time', '<=',  $hour)->whereTime('end_time', '>=',  $hour)->inRandomOrder()->take(1)->first();
        $this->admin = User::where('role_id', $this::ADMIN)->first();
        $this->recentPosts = Post::orderBy('created_at', 'DESC')->take(4)->get();
        $this->allCategories = Category::orderBy('created_at', 'DESC')->get();
        $this->themeSettings = ThemeSetting::all()->first();
        $this->banner = HomepageSettings::all()->first();
        $this->services = Service::all()->first();
    }


    public function forgot(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user != null) {

            if ($user->role_id == 2 && $user->confirmed == 1) {

                //create a new token to be sent to the user.
                DB::table('password_resets')->insert([
                    'email' => $request->email,
                    'token' => Str::random(60), //change 60 to any length you want
                    'created_at' => Carbon::now()
                ]);

                $tokenData = DB::table('password_resets')
                    ->where('email', $request->email)->orderBy('created_at', 'DESC')->first();

                $token = $tokenData->token;

                $data = [];

                $data['name'] = $user->name;
                $data['email'] = $request->email;
                $data['token'] = $token;

                Mail::send('emails.recovery_password', $data, function ($message) use ($data) {
                    $message->to($data['email'], $data['name'])->subject('Restablecer Contraseña');
                });

                return redirect()->back()->with('notification', 'Por favor revisar su bandeja de entrada.');
            } else {
                return redirect()->back()->with('error', 'Su email no ha sido verificado');
            }
        } else {
            return redirect()->back()->with('error', 'El email no existe en nuestra base de datos.');
        }
    }

    public function recovery($token)
    {

        $token = DB::table('password_resets')
            ->where('token', $token)->first();

        if ($token && $token->expirate == 0) {

            return view('recovery-password', [
                'layout' => $this->layout,
                'day' => $this->day,
                'msg' => $this->msg,
                'regards' => $this->regards,
                'admin' => $this->admin,
                'recentPosts' => $this->recentPosts,
                'allCategories' => $this->allCategories,
                'themeSettings' => $this->themeSettings,
                'banner' => $this->banner,
                'token' => $token->token,
                'services' => $this->services,
            ]);
        } else {
            return redirect('/restablecer-clave')->with('error', 'Token Expirado');
        }
    }

    public function reset(Request $request)
    {
        $token = DB::table('password_resets')
            ->where('token', $request->token)->first();

        if ($token && $token->expirate == 0) {

            if($request->email == $token->email){
                if($request->password == $request->confirmed_password){

                    $user = User::where('email', $request->email)->first();
                    $user->password = Hash::make($request->password);
                    $user->save();

                    DB::table('password_resets')->where('email', $request->email)->update(['expirate' => 1]);

                    return redirect('/iniciar-sesion')->with('notification', 'Su contraseña ha sido restablecida satisfactoriamente!');
                }else{
                    return redirect()->back()->with('error', 'Las contraseñas no coinciden.');
                }

            }else{
                return redirect()->back()->with('error', 'El email no corresponde a este token.');
            }

        } else {
            return redirect('/restablecer-clave')->with('error', 'Token Expirado');
        }
    }
}

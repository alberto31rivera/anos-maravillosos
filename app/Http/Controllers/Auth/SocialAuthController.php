<?php

namespace App\Http\Controllers\Auth;

use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\SocialLink;
use App\Models\User;
use Illuminate\Support\Facades\Config;

class SocialAuthController extends Controller
{
    const ROL_USER = 2;
    const USER_CONFIRM = 1;
    const IS_PROVIDER = 1;

    public function __construct()
    {
      $link = SocialLink::findOrFail(1);
      Config::set('services.google.client_id', $link->google_client_id);
      Config::set('services.google.client_secret', $link->google_client_secret);
      Config::set('services.google.redirect', url('/auth/google/callback'));
      Config::set('services.facebook.client_id', $link->facebook_client_id);
      Config::set('services.facebook.client_secret', $link->facebook_client_secret);
      $url = url('/auth/facebook/callback');
      $url = preg_replace("/^http:/i", "https:", $url);
      Config::set('services.facebook.redirect', $url);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    // Metodo encargado de obtener la información del usuario
    public function handleProviderCallback($provider)
    {
        try
        {
                    // Obtenemos los datos del usuario
        $social_user = Socialite::driver($provider)->user();
        }
        catch(\Exception $e)
        {
            return redirect('/iniciar-sesion');
        }
        // Comprobamos si el usuario ya existe
        if ($user = User::where('email', $social_user->email)->first()) {
            return $this->authAndRedirect($user); // Login y redirección
        } else {
            // En caso de que no exista creamos un nuevo usuario con sus datos.
            if($social_user->email != null){
                $user = new User();
                $user->is_provider = $this::IS_PROVIDER;
                $user->role_id = $this::ROL_USER;
                $user->confirmed = $this::USER_CONFIRM;
                $user->name = $social_user->name;
                $user->email = $social_user->email;
                $user->avatar = $social_user->avatar;
                $user->save();

                return $this->authAndRedirect($user); // Login y redirección
            }

            return redirect('/iniciar-sesion')->with('error', 'Debe registrar un email y su Facebook no lo tiene.');
        }
    }
    // Login y redirección
    public function authAndRedirect($user)
    {
        Auth::login($user);
        return redirect()->to('/panel');
    }
}

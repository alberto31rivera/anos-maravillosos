<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    const ROL_USER = 2;

    public function registerUser(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'cellphone' => 'required',
            'password' => 'required',
            'confirm_password' => 'required',
        ]);

        if (User::where('email', $request->email)->count() > 0) {
            return redirect('/registro')->with('error', 'El email ya se encuentra registrado');
        }

        if ($request->password != $request->confirm_password) {
            return redirect('/registro')->with('error', 'Las contraseñas no coinciden');
        }

        $data = [];
        $confirmation_code = Str::random(25);

        $user = new User();
        $user->role_id = $this::ROL_USER;
        $user->name = $request->name;
        $user->last_names = $request->last_names;
        $user->email = $request->email;
        $user->cellphone = $request->cellphone;
        $user->password = Hash::make($request->password);
        $user->confirmation_code = $confirmation_code;
        $user->save();


        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['confirmation_code'] = $confirmation_code;

        Mail::send('emails.confirmation_code', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['name'])->subject('Por favor confirma tu correo');
        });

        return redirect('/registro')->with('notification', 'Revisa tu bandeja de entrada');
    }

    public function verify($code)
    {
        $user = User::where('confirmation_code', $code)->first();

        if (!$user)
            return redirect('/');

        $user->confirmed = true;
        $user->confirmation_code = null;
        $user->save();

        return redirect('/iniciar-sesion')->with('notification', 'Has confirmado correctamente tu correo!');
    }
}

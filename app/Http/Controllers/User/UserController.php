<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Advertising;
use App\Models\Category;
use App\Models\HomepageSettings;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Post;
use App\Models\Regard;
use App\Models\Service;
use App\Models\ThemeSetting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    const SECTION_ELEVEN = 11, SECTION_TWO = 2, ROL_USER = 2;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user.authenticate');

        $this->allCategories = Category::orderBy('created_at', 'DESC')->get();

        $this->randomAdvertising = Advertising::inRandomOrder()->take(1)->get();

        $this->themeSettings = ThemeSetting::all()->first();

        $this->banner = HomepageSettings::all()->first();


        $this->oldPosts = Post::orderBy('created_at', 'ASC')->take(3)->get();
        $this->lastNews = Post::orderBy('created_at', 'DESC')->take(1)->get();

        $this->postsSectionNine = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_ELEVEN);
        })->orderBy('created_at', 'DESC')->take(4)->get();

        $this->postsSectionTwo = Post::with('category')->with('category.imageLocation')->whereHas('category', function (Builder $query) {
            $query->where('image_location_id', $this::SECTION_TWO);
        })->orderBy('created_at', 'DESC')->take(3)->get();

         //Start information header
         $this->now = Carbon::now();
         $this->day = $this->now->locale('es');
         $hour = $this->now->isoFormat('H:mm');
         $this->msg = '';

         $startMorning = Carbon::createFromTimeString('06:00');
         $endMorning = Carbon::createFromTimeString('11:59');
         $startEvening = Carbon::createFromTimeString('12:00');
         $endEvening = Carbon::createFromTimeString('17:59');
         $startNigth = Carbon::createFromTimeString('18:00');
         $endNigth = Carbon::createFromTimeString('05:59')->addDay();

         $this->regards = Regard::where('day', $this->day->dayName)->whereTime('start_time', '<=',  $hour)->whereTime('end_time', '>=',  $hour)->inRandomOrder()->take(1)->first();

         if ($this->now->between($startMorning, $endMorning)) {
             $this->msg = 'Buenos días';
         }

         if ($this->now->between($startEvening, $endEvening)) {
             $this->msg = 'Buenas tardes';
         }

         if ($this->now->between($startNigth, $endNigth)) {
             $this->msg = 'Buenas noches';
         }

    }

    public function index()
    {
        return view('front.panel.index', [
            'oldPosts' => $this->oldPosts,
            'postsSectionNine' => $this->postsSectionNine,
            'postsSectionTwo' => $this->postsSectionTwo,
            'lastNews' => $this->lastNews,
            'allCategories' => $this->allCategories,
            'randomAdvertising' => $this->randomAdvertising,
            'layout' => 'silver',
            'msg' => $this->msg,
            'day' => \Carbon\Carbon::now()->locale('es'),
            'regards' => $this->regards,
            'admin' => User::where('role_id', 1)->first(),
            'recentPosts' => Post::orderBy('created_at', 'DESC')->take(4)->get(),
            'themeSettings' => $this->themeSettings,
            'banner' => $this->banner,
            'services' => Service::all()->first(),
        ]);
    }

    public function updateUser(Request $request, $id)
    {

        $user = User::find($id);
        $user->role_id = $this::ROL_USER;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->cellphone = $request->cellphone;
        $user->type_document = $request->type_document;
        $user->number_document = $request->number_document;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->country = $request->country;

        if (!empty($request->avatar)) {
            $user->avatar = $request->avatar->store('avatar-users');
        }

        if (!empty($request->password && $request->confirm_password)) {

            if (Hash::check($request->password, $user->password)) {
                $user->password = Hash::make($request->confirm_password);
            } else {
                return redirect('/panel')->with('error', 'La contraseña actual no es correcta.');
            }
        }

        $user->save();

        return redirect('/panel')->with('notification', 'Perfil actualizado con exito.');
    }

    public function shopping()
    {
        return view('front.panel.shopping', [
            'oldPosts' => $this->oldPosts,
            'postsSectionNine' => $this->postsSectionNine,
            'postsSectionTwo' => $this->postsSectionTwo,
            'lastNews' => $this->lastNews,
            'allCategories' => $this->allCategories,
            'randomAdvertising' => $this->randomAdvertising,
            'layout' => 'silver',
            'msg' => $this->msg,
            'day' => \Carbon\Carbon::now()->locale('es'),
            'regards' => $this->regards,
            'admin' => User::where('role_id', 1)->first(),
            'recentPosts' => Post::orderBy('created_at', 'DESC')->take(4)->get(),
            'themeSettings' => $this->themeSettings,
            'banner' => $this->banner,
            'services' => Service::all()->first(),
        ]);
    }
}

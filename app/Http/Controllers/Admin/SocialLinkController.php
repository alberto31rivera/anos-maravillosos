<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SocialLink;

class SocialLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }


    public function index()
    {
        $socialLinks = SocialLink::first();

        return view('admin.social-links.index', [
            'socialLinks' => $socialLinks,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = SocialLink::all();

        if (count($query)) {
            //Actualizar registros
            $query = $query->first();
            $socialLinks = SocialLink::find($query->id);
            $socialLinks->facebook_client_id = $request->facebook_client_id;
            $socialLinks->facebook_client_secret = $request->facebook_client_secret;
            $socialLinks->facebook_status = $request->facebook_status;
            $socialLinks->google_client_id = $request->google_client_id;
            $socialLinks->google_client_secret = $request->google_client_secret;
            $socialLinks->google_status = $request->google_status;

            $socialLinks->save();

            return redirect()->back()->with('success-message', 'Datos Actualizados con éxito.');

        } else {
            $socialLinks = new SocialLink();
            $socialLinks->facebook_client_id = $request->facebook_client_id;
            $socialLinks->facebook_client_secret = $request->facebook_client_secret;
            $socialLinks->facebook_status = $request->facebook_status;
            $socialLinks->google_client_id = $request->google_client_id;
            $socialLinks->google_client_secret = $request->google_client_secret;
            $socialLinks->google_status = $request->google_status;

            $socialLinks->save();

            return redirect()->back()->with('success-message', 'Datos Guardados con éxito.');
        }
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    public function index(){
        return view('admin.categories.index');
    }

    public function destroy($id)
    {
        $post = Post::where('category_id', $id)->first();

        if($post != null){
            return response()->json(['error' => 'error', 'code' => 422], 422);
        }else{
            Category::destroy($id);
            return response()->json(['data' => 'success', 'code' => 200], 200);
        }
    }
}

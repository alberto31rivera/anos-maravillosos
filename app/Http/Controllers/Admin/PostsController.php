<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{
    public $view = 'create';

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }


    public function index()
    {
        return view('admin.posts.index', [
            'view' => $this->view,
            'categories' => Category::all(),
        ]);
    }


    public function store(Request $request)
    {
        $request->validate([
        'category_id' => 'required',
        'title' => 'required',
        'post_summary' => 'required',
        'sentence' => 'required',
        'description' => 'required',
        'slug' => 'unique:posts',
        'image' => 'required|mimes:jpg,jpeg,bmp,png',
        ]);

        $post = new Post();
        $post->user_id = Auth::user()->id;
        $post->category_id = $request->category_id;
        $post->title = $request->title;
        $post->slug = Str::slug($request->title, '-');
        $post->description = $request->description;
        $post->tags = Str::replace(',', ',', $request->tags);
        $post->post_summary = $request->post_summary;
        $post->sentence = $request->sentence;
        $post->video = $request->video;
        $post->audio = $request->audio;


        if (!empty($request->image)) {
            //Guardo la imagen que sube el usuario
            $post->image = $request->image->store('image-posts');


            $destinationPaththumbnail = public_path('/thumbnail');
            $destinationPathMedium = public_path('/medium');

            $image = $request->image;
            $input['imagenamethumbnail'] = time() . '.' . $image->extension();
            $input['imagenamemedium'] = time() . '.' . $image->extension();

            $imgThumbnail = Image::make($image->path());
            $imgMedium = Image::make($image->path());

            //tratamiento de imagen para miniatura
            $imgThumbnail->resize(75, 50, function ($constraint) {

                $constraint->aspectRatio();
            })->save($destinationPaththumbnail . '/' . $input['imagenamethumbnail']);


            //tratamiento de imagen para media
            $imgMedium->resize(300, 150, function ($constraint) {

                $constraint->aspectRatio();
            })->save($destinationPathMedium . '/' . $input['imagenamemedium']);

            $post->thumbnail = $input['imagenamethumbnail'];
            $post->medium = $input['imagenamemedium'];
        }

        if (Post::where('slug', $post->slug)->exists()) {
            return redirect()->route('admin.posts')
                        ->with('error-message','Ya existe una publicación con este título, por favor cambie el título de la publicación.');
        }

        $post->save();

        return redirect()->route('admin.posts')
                        ->with('success-message','Nuevo Post creado con éxito.');

    }

    public function edit($id)
    {
        $post = Post::find($id);

        return view('admin.posts.index', [
            'post' => $post,
            'view' => 'edit',
            'categories' => Category::all(),
        ]);

    }

    public function update(Request $request, $id)
    {
        $request->validate([
        'category_id' => 'required',
        'title' => 'required',
        'post_summary' => 'required',
        'sentence' => 'required',
        'description' => 'required',
        ]);

        $post = Post::find($id);
        $post->user_id = Auth::user()->id;
        $post->category_id = $request->category_id;
        $post->title = $request->title;
        $post->description = $request->description;
        $post->tags = Str::replace(',', ', ', $request->tags);
        $post->post_summary = $request->post_summary;
        $post->sentence = $request->sentence;
        $post->video = $request->video;
        $post->audio = $request->audio;

        if (!empty($request->image)) {
            //Guardo la imagen que sube el usuario
            $post->image = $request->image->store('image-posts');


            $destinationPaththumbnail = public_path('/thumbnail');
            $destinationPathMedium = public_path('/medium');

            $image = $request->image;
            $input['imagenamethumbnail'] = time() . '.' . $image->extension();
            $input['imagenamemedium'] = time() . '.' . $image->extension();

            $imgThumbnail = Image::make($image->path());
            $imgMedium = Image::make($image->path());

            //tratamiento de imagen para miniatura
            $imgThumbnail->resize(75, 50, function ($constraint) {

                $constraint->aspectRatio();
            })->save($destinationPaththumbnail . '/' . $input['imagenamethumbnail']);


            //tratamiento de imagen para media
            $imgMedium->resize(300, 150, function ($constraint) {

                $constraint->aspectRatio();
            })->save($destinationPathMedium . '/' . $input['imagenamemedium']);

            $post->thumbnail = $input['imagenamethumbnail'];
            $post->medium = $input['imagenamemedium'];
        }

        if ($post->slug !=  Str::slug($request->title, '-')) {
            if (Post::where('slug', Str::slug($request->title, '-'))->exists()) {
                return redirect()->back()->with('error-message','Ya existe una publicación con este título, por favor cambie el título de la publicación.');
            }
        }

        $post->slug = Str::slug($request->title, '-');

        $post->save();

        return redirect()->back()->with('success-message', 'Post Actualizado con éxito.');
    }

    public function destroy($id)
    {
        Post::destroy($id);

        return redirect()->route('admin.posts')
        ->with('success-message','Post eliminado con éxito.');
    }
}

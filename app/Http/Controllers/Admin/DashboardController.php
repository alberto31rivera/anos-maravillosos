<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertising;
use App\Models\Currency;
use App\Models\Post;
use App\Models\TransactionPlan;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    public function index(Request $request)
    {
        $advertisements = count(Advertising::all());
        $vendors = count(Vendor::all());
        $posts = count(Post::all());
        $netEarnings = TransactionPlan::where('status', 1)->sum('cost');

        // Statistics of the month

        // Today
        $stat_revenue_today = TransactionPlan::where('created_at', '>=', date('Y-m-d H:i:s', strtotime('today')))
            ->where('status', 1)
            ->sum('cost');

        // Week
        $stat_revenue_week = TransactionPlan::whereBetween('created_at', [
            Carbon::parse()->startOfWeek(),
            Carbon::parse()->endOfWeek(),
        ])
            ->where('status', 1)
            ->sum('cost');

        // Month
        $stat_revenue_month = TransactionPlan::whereBetween('created_at', [
            Carbon::parse()->startOfMonth(),
            Carbon::parse()->endOfMonth(),
        ])
            ->where('status', 1)
            ->sum('cost');

        if (isset($request->monthFilter)) {
            $month = $request->monthFilter;
        } else {
            $month = date('m');
        }

        $year = date('Y');
        $daysMonth = cal_days_in_month(0, $month, $year);
        $dateFormat = "$year-$month-";

        $dateCarbon = Carbon::createFromFormat('m', $month);
        $monthFormat = $dateCarbon->locale('es');

        for ($i = 1; $i <= $daysMonth; ++$i) {
            $date = date('Y-m-d', strtotime($dateFormat . $i));

            $transactions = TransactionPlan::where('status', 1)->whereDate('created_at', '=', $date)->sum('cost');

            $monthsData[] =  "'$monthFormat->monthName $i'";

            $earningNet = $transactions;

            $earningNetSum[] = $earningNet;
          }

          $label = implode(',', $monthsData);
          $data = implode(',', $earningNetSum);

        return view('admin.dashboard', [
            'advertisements' => $advertisements,
            'vendors' => $vendors,
            'posts' => $posts,
            'label' => $label,
            'data' => $data,
            'monthNumber' => $month,
            'netEarnings' => $netEarnings,
            'currencyLocal' => Currency::where('id', 2)->first(),
            'stat_revenue_today' => $stat_revenue_today,
            'stat_revenue_week' => $stat_revenue_week,
            'stat_revenue_month' => $stat_revenue_month,
        ]);
    }
}

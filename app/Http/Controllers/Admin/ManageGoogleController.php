<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ManageGoogle;
use Illuminate\Http\Request;

class ManageGoogleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    public function index()
    {
        $manageGoogle = ManageGoogle::first();

        return view('admin.manage-google.index', [
            'manageGoogle' => $manageGoogle,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = ManageGoogle::all();

        if (count($query)) {
            //Actualizar registros
            $query = $query->first();
            $manageGoogle = ManageGoogle::find($query->id);
            $manageGoogle->recaptcha_key = $request->recaptcha_key;
            $manageGoogle->recaptcha_secret_key = $request->recaptcha_secret_key;
            $manageGoogle->google_analytics = $request->google_analytics;

            $manageGoogle->save();

            return redirect()->back()->with('success-message', 'Datos Actualizados con éxito.');

        } else {
            $manageGoogle = new ManageGoogle();
            $manageGoogle->recaptcha_key = $request->recaptcha_key;
            $manageGoogle->recaptcha_secret_key = $request->recaptcha_secret_key;
            $manageGoogle->google_analytics = $request->google_analytics;

            $manageGoogle->save();

            return redirect()->back()->with('success-message', 'Datos Guardados con éxito.');
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertising;
use App\Models\Vendor;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    public function index(){
        return view('admin.vendors.index');
    }

    public function destroy($id)
    {
        $advertising = Advertising::where('vendor_id', $id)->first();

        if($advertising != null){
            return response()->json(['error' => 'error', 'code' => 422], 422);
        }else{
            Vendor::destroy($id);
            return response()->json(['data' => 'success', 'code' => 200], 200);
        }
    }
}

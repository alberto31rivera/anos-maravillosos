<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    const ADMIN = 1;

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = User::where('role_id', $this::ADMIN)->first();
        return view('admin.profile.index', [
            'admin' => $admin,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'number_document' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'description' => 'required',
            'description_contact' => 'required',
            'cellphone' => 'required',
            'whatsapp' => 'required',
            ]);

        $admin = User::where('role_id', $this::ADMIN)->first();
        $admin->name = $request->name;
        $admin->number_document = $request->number_document;
        $admin->address = $request->address;
        $admin->city = $request->city;
        $admin->country = $request->country;
        $admin->description = $request->description;
        $admin->description_contact = $request->description_contact;
        $admin->phone = $request->phone;
        $admin->cellphone = $request->cellphone;
        $admin->whatsapp = $request->whatsapp;
        $admin->web_site = $request->web_site;
        $admin->facebook = $request->facebook;
        $admin->instagram = $request->instagram;
        $admin->twitter = $request->twitter;
        $admin->linkedin = $request->linkedin;
        $admin->youtube = $request->youtube;

        if (!empty($request->avatar)) {

            $file = $request->file('avatar');

            //obtenemos el nombre del archivo
            $nameImage =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameImage = str_replace(' ', '-', $nameImage);

            //movemos el archivo a la ruta indicada
            $file->move('img/avatar-users/', $nameImage);

            //guardamos el nombre de la ruta en base de datos
            $admin->avatar = $nameImage;
        }


        if (!empty($request->logo)) {

            $file = $request->file('logo');

            //obtenemos el nombre del archivo
            $nameImage =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameImage = str_replace(' ', '-', $nameImage);

            //movemos el archivo a la ruta indicada
            $file->move('img/logos-users/', $nameImage);

            //guardamos el nombre de la ruta en base de datos
            $admin->logo = $nameImage;
        }

        if(!empty($request->oldPassword && $request->newPassword)){

            if (Hash::check($request->oldPassword, Auth::user()->password)) {
                $admin->password = Hash::make($request->newPassword);
            } else {
                return redirect()->back()->with('error-message', 'La contraseña actual no es correcta.');
            }
        }

        $admin->save();

        return redirect()->back()->with('success-message', 'Datos actualizados con éxito.');
    }
}

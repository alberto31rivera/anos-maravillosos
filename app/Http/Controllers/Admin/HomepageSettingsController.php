<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HomepageSettings;
use Illuminate\Http\Request;

class HomepageSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    public function index()
    {
        $homepageSettings = HomepageSettings::first();
        return view('admin.homepage-settings.index', [
            'homepageSettings' => $homepageSettings,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = HomepageSettings::all();

        if (count($query)) {
            //Actualizar registros
            $query = $query->first();
            $homepageSettings = HomepageSettings::find($query->id);
            $homepageSettings->url_advertising_one = $request->url_advertising_one;
            $homepageSettings->url_advertising_two = $request->url_advertising_two;
            $homepageSettings->url_advertising_three = $request->url_advertising_three;
            $homepageSettings->url_advertising_four = $request->url_advertising_four;
            $homepageSettings->video_one = $request->video_one;
            $homepageSettings->video_two = $request->video_two;
            $homepageSettings->video_three = $request->video_three;
            $homepageSettings->video_four = $request->video_four;
            $homepageSettings->video_five = $request->video_five;
            $homepageSettings->video_six = $request->video_six;
            $homepageSettings->description_one = $request->description_one;
            $homepageSettings->description_two = $request->description_two;
            $homepageSettings->description_three = $request->description_three;
            $homepageSettings->description_four = $request->description_four;
            $homepageSettings->description_five = $request->description_five;
            $homepageSettings->description_six = $request->description_six;
            $homepageSettings->title_one = $request->title_one;
            $homepageSettings->title_two = $request->title_two;
            $homepageSettings->title_three = $request->title_three;
            $homepageSettings->title_four = $request->title_four;
            $homepageSettings->title_five = $request->title_five;
            $homepageSettings->title_six = $request->title_six;
            $homepageSettings->title_seven = $request->title_seven;
            $homepageSettings->title_eight = $request->title_eight;

            if (!empty($request->advertising_one)) {

                $file = $request->file('advertising_one');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->advertising_one = $nameImage;
            }

            if (!empty($request->advertising_two)) {

                $file = $request->file('advertising_two');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->advertising_two = $nameImage;
            }

            if (!empty($request->advertising_three)) {

                $file = $request->file('advertising_three');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->advertising_three = $nameImage;
            }

            if (!empty($request->advertising_four)) {

                $file = $request->file('advertising_four');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->advertising_four = $nameImage;
            }

            if (!empty($request->image_one)) {

                $file = $request->file('image_one');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_one = $nameImage;
            }

            if (!empty($request->image_two)) {

                $file = $request->file('image_two');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_two = $nameImage;
            }

            if (!empty($request->image_three)) {

                $file = $request->file('image_three');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_three = $nameImage;
            }

            if (!empty($request->image_four)) {

                $file = $request->file('image_four');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_four = $nameImage;
            }

            if (!empty($request->image_five)) {

                $file = $request->file('image_five');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_five = $nameImage;
            }

            if (!empty($request->image_six)) {

                $file = $request->file('image_six');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_six = $nameImage;
            }

            $homepageSettings->save();

            return redirect()->back()->with('success-message', 'Datos Actualizados con éxito.');

        } else {
            //Crear registro
            $homepageSettings = new HomepageSettings();
            $homepageSettings->url_advertising_one = $request->url_advertising_one;
            $homepageSettings->url_advertising_two = $request->url_advertising_two;
            $homepageSettings->url_advertising_three = $request->url_advertising_three;
            $homepageSettings->url_advertising_four = $request->url_advertising_four;
            $homepageSettings->video_one = $request->video_one;
            $homepageSettings->video_two = $request->video_two;
            $homepageSettings->video_three = $request->video_three;
            $homepageSettings->video_four = $request->video_four;
            $homepageSettings->video_five = $request->video_five;
            $homepageSettings->video_six = $request->video_six;
            $homepageSettings->description_one = $request->description_one;
            $homepageSettings->description_two = $request->description_two;
            $homepageSettings->description_three = $request->description_three;
            $homepageSettings->description_four = $request->description_four;
            $homepageSettings->description_five = $request->description_five;
            $homepageSettings->description_six = $request->description_six;
            $homepageSettings->title_one = $request->title_one;
            $homepageSettings->title_two = $request->title_two;
            $homepageSettings->title_three = $request->title_three;
            $homepageSettings->title_four = $request->title_four;
            $homepageSettings->title_five = $request->title_five;
            $homepageSettings->title_six = $request->title_six;
            $homepageSettings->title_seven = $request->title_seven;
            $homepageSettings->title_eight = $request->title_eight;

            if (!empty($request->advertising_one)) {

                $file = $request->file('advertising_one');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->advertising_one = $nameImage;
            }

            if (!empty($request->advertising_two)) {

                $file = $request->file('advertising_two');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->advertising_two = $nameImage;
            }

            if (!empty($request->advertising_three)) {

                $file = $request->file('advertising_three');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->advertising_three = $nameImage;
            }

            if (!empty($request->advertising_four)) {

                $file = $request->file('advertising_four');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->advertising_four = $nameImage;
            }

            if (!empty($request->image_one)) {

                $file = $request->file('image_one');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_one = $nameImage;
            }

            if (!empty($request->image_two)) {

                $file = $request->file('image_two');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_two = $nameImage;
            }

            if (!empty($request->image_three)) {

                $file = $request->file('image_three');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_three = $nameImage;
            }

            if (!empty($request->image_four)) {

                $file = $request->file('image_four');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_four = $nameImage;
            }

            if (!empty($request->image_five)) {

                $file = $request->file('image_five');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_five = $nameImage;
            }

            if (!empty($request->image_six)) {

                $file = $request->file('image_six');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/homepage/advertisements/videos', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $homepageSettings->image_six = $nameImage;
            }

            $homepageSettings->save();
        }

        return redirect()->back()->with('success-message', 'Datos Guardados con éxito.');
    }

}

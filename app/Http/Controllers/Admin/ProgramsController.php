<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Program;
use App\Models\Sponsor;
use Illuminate\Http\Request;

class ProgramsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    public function index(){
        return view('admin.programs.index');
    }

    public function destroy($id)
    {
        $sponsor = Sponsor::where('program_id', $id)->first();

        if($sponsor != null){
            return response()->json(['error' => 'error', 'code' => 422], 422);
        }else{
            Program::destroy($id);
            return response()->json(['data' => 'success', 'code' => 200], 200);
        }
    }
}

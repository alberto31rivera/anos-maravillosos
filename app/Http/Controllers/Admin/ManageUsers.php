<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ManageUsers extends Controller
{
    public $view = 'create';

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    public function index()
    {
        return view('admin.users.index', [
            'view' => $this->view,
            'users' => User::all(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'cellphone' => 'required',
            ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->type_document = $request->type_document;
        $user->number_document = $request->number_document;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->country = $request->country;
        $user->email = $request->email;
        $user->cellphone = $request->cellphone;
        $user->is_banned = $request->is_banned;

        if (!empty($request->avatar)) {

            $file = $request->file('avatar');

            //obtenemos el nombre del archivo
            $nameImage =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameImage = str_replace(' ', '-', $nameImage);

            //movemos el archivo a la ruta indicada
            $file->move('img/avatar-users/', $nameImage);

            //guardamos el nombre de la ruta en base de datos
            $user->avatar = $nameImage;
        }

        if(!empty($request->password && $request->confirm_password)){

            if ($request->password != $request->confirm_password) {
                return redirect()->back()->with('error-message', 'Las contraseñas no coinciden.');
            } else {
                $user->password = Hash::make($request->password);
            }
        }

        $user->save();

        return redirect()->back()->with('success-message', 'Datos actualizados con éxito.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.users.index', [
            'user' => $user,
            'view' => 'edit',
        ]);
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertising;
use App\Models\Currency;
use App\Models\Plan;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $view = 'create';

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }


    public function index()
    {
        return view('admin.plans.index', [
            'view' => $this->view,
            'currencyDolar' => Currency::where('id', 1)->first(),
            'currencyLocal' => Currency::where('id', 2)->first(),
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'cost' => 'required',
            'cost_dolar' => 'required',
            'duration' => 'required',
            'description' => 'required',
            ]);

        $plan = new Plan();
        $plan->title = $request->title;
        $plan->cost = $request->cost;
        $plan->cost_dolar = $request->cost_dolar;
        $plan->duration = $request->duration;
        $plan->description = $request->description;
        $plan->save();

        return redirect()->route('plans.index')
                        ->with('success-message','Nuevo Plan creado con éxito.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plan = Plan::find($id);

        return view('admin.plans.index', [
            'plan' => $plan,
            'view' => 'edit',
            'currencyDolar' => Currency::where('id', 1)->first(),
            'currencyLocal' => Currency::where('id', 2)->first(),
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'cost' => 'required',
            'cost_dolar' => 'required',
            'duration' => 'required',
            'description' => 'required',
            ]);

        $plan = Plan::find($id);
        $plan->title = $request->title;
        $plan->cost = $request->cost;
        $plan->cost_dolar = $request->cost_dolar;
        $plan->duration = $request->duration;
        $plan->description = $request->description;
        $plan->save();

        return redirect()->back()->with('success-message', 'Plan actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertising = Advertising::where('plan_id', $id)->first();

        if($advertising != null){
            return response()->json(['error' => 'error', 'code' => 422], 422);
        }else{
            Plan::destroy($id);
            return response()->json(['data' => 'success', 'code' => 200], 200);
        }
    }
}

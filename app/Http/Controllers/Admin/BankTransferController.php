<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentConfiguration;
use Illuminate\Http\Request;

class BankTransferController extends Controller
{

    const TRANSFER = 3;

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transferSettings = PaymentConfiguration::where('payment_method_id', $this::TRANSFER)->first();
        return view('admin.bank-transfer.index', [
            'transferSettings' => $transferSettings,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bank_details' => 'required',
            ]);

        $query = PaymentConfiguration::where('payment_method_id', $this::TRANSFER)->get();

        if (count($query)) {
            //Actualizar registros
            $query = $query->first();
            $transferSettings = PaymentConfiguration::find($query->id);
            $transferSettings->payment_method_id = $this::TRANSFER;
            $transferSettings->bank_details = $request->bank_details;
            $transferSettings->status = $request->status;

            $transferSettings->save();

            return redirect()->back()->with('success-message', 'Datos Actualizados con éxito.');

        } else {
            $transferSettings = new PaymentConfiguration();
            $transferSettings->payment_method_id = $this::TRANSFER;
            $transferSettings->bank_details = $request->bank_details;
            $transferSettings->status = $request->status;

            $transferSettings->save();

            return redirect()->back()->with('success-message', 'Datos Guardados con éxito.');
        }
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ServiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::first();

        return view('admin.services.index', [
            'services' => $services,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = Service::all();

        if (count($query)) {
            //Actualizar registros
            $query = $query->first();
            $services = Service::find($query->id);
            $services->title_one = $request->title_one;
            $services->slug_one = Str::slug($request->title_one, '-');
            $services->description_one = $request->description_one;
            $services->status_one = $request->status_one;
            $services->title_two = $request->title_two;
            $services->slug_two = Str::slug($request->title_two, '-');
            $services->description_two = $request->description_two;
            $services->status_two = $request->status_two;
            $services->title_three = $request->title_three;
            $services->slug_three = Str::slug($request->title_three, '-');
            $services->description_three = $request->description_three;
            $services->status_three = $request->status_three;
            $services->title_four = $request->title_four;
            $services->slug_four = Str::slug($request->title_four, '-');
            $services->description_four = $request->description_four;
            $services->status_four = $request->status_four;
            $services->title_five = $request->title_five;
            $services->slug_five = Str::slug($request->title_five, '-');
            $services->description_five = $request->description_five;
            $services->status_five = $request->status_five;
            $services->title_six = $request->title_six;
            $services->slug_six= Str::slug($request->title_six, '-');
            $services->description_six = $request->description_six;
            $services->status_six = $request->status_six;

            $services->save();

            return redirect()->back()->with('success-message', 'Datos Actualizados con éxito.');

        } else {
            $services = new Service();
            $services->title_one = $request->title_one;
            $services->slug_one = Str::slug($request->title_one, '-');
            $services->description_one = $request->description_one;
            $services->status_one = $request->status_one;
            $services->title_two = $request->title_two;
            $services->slug_two = Str::slug($request->title_two, '-');
            $services->description_two = $request->description_two;
            $services->status_two = $request->status_two;
            $services->title_three = $request->title_three;
            $services->slug_three = Str::slug($request->title_three, '-');
            $services->description_three = $request->description_three;
            $services->status_three = $request->status_three;
            $services->title_four = $request->title_four;
            $services->slug_four = Str::slug($request->title_four, '-');
            $services->description_four = $request->description_four;
            $services->status_four = $request->status_four;
            $services->title_five = $request->title_five;
            $services->slug_five = Str::slug($request->title_five, '-');
            $services->description_five = $request->description_five;
            $services->status_five = $request->status_five;
            $services->title_six = $request->title_six;
            $services->slug_six= Str::slug($request->title_six, '-');
            $services->description_six = $request->description_six;
            $services->status_six = $request->status_six;

            $services->save();

            return redirect()->back()->with('success-message', 'Datos Guardados con éxito.');
        }
    }

}

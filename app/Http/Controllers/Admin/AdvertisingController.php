<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertising;
use App\Models\CategoryAdvertising;
use App\Models\Currency;
use App\Models\Plan;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdvertisingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $view = 'create';

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }


    public function index()
    {
        return view('admin.advertisements.index', [
            'view' => $this->view,
            'advertisements' => Advertising::all(),
            'vendors' => Vendor::all(),
            'categories' => CategoryAdvertising::all(),
            'plans' => Plan::all(),
            'currencyDolar' => Currency::where('id', 1)->first(),
            'currencyLocal' => Currency::where('id', 2)->first(),
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $now = Carbon::now();
        $day = $now->format('Y-m-d');

        $request->validate([
            'vendor_id' => 'required',
            'category_id' => 'required',
            'plan_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'type_advertising' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'start_date' => 'required',
            'end_date' => 'required',
            'audio' => 'file|mimes:mp3,mp4,wav',
        ]);

        $advertising = new Advertising();
        $advertising->vendor_id = $request->vendor_id;
        $advertising->category_id = $request->category_id;
        $advertising->plan_id = $request->plan_id;
        $advertising->cost = $request->cost;
        $advertising->cost_dolar = $request->cost_dolar;
        $advertising->title = $request->title;
        $advertising->description = $request->description;
        $advertising->type_advertising = $request->type_advertising;
        $advertising->pago = $request->pago;
        $advertising->video = $request->video;
        $advertising->start_date = $request->start_date;
        $advertising->end_date = $request->end_date;

        $query = $now->between($request->start_date, $request->end_date);

        if ($query) {
            $advertising->status = 'Activo';
        } else {
            if ($request->start_date == $day && $request->end_date == $day) {
                $advertising->status = 'Activo';
            } else {
                $advertising->status = 'Inactivo';
            }
        }

        if (!empty($request->image)) {
            $file = $request->file('image');

            //obtenemos el nombre del archivo
            $nameImage =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameImage = str_replace(' ', '-', $nameImage);

            //movemos el archivo a la ruta indicada
            $file->move('img/advertisements', $nameImage);

            //guardamos el nombre de la ruta en base de datos
            $advertising->image = $nameImage;
        }

        if (!empty($request->audio)) {
            $file = $request->file('audio');

            //obtenemos el nombre del archivo
            $nameAudio =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameAudio = str_replace(' ', '-', $nameAudio);

            //movemos el archivo a la ruta indicada
            $file->move('audio/advertisements', $nameAudio);

            //guardamos el nombre de la ruta en base de datos
            $advertising->audio = $nameAudio;
        }

        $advertising->save();

        return redirect()->route('advertisements.index')
            ->with('success-message', 'Nueva Publicidad creada con éxito.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertising = Advertising::find($id);

        return view('admin.advertisements.index', [
            'advertising' => $advertising,
            'view' => 'edit',
            'vendors' => Vendor::all(),
            'categories' => CategoryAdvertising::all(),
            'plans' => Plan::all(),
            'currencyDolar' => Currency::where('id', 1)->first(),
            'currencyLocal' => Currency::where('id', 2)->first(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $now = Carbon::now();
        $day = $now->format('Y-m-d');

        $request->validate([
            'vendor_id' => 'required',
            'category_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'type_advertising' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $advertising = Advertising::find($id);
        $advertising->vendor_id = $request->vendor_id;
        $advertising->category_id = $request->category_id;
        $advertising->plan_id = $request->plan_id;
        $advertising->cost = $request->cost;
        $advertising->cost_dolar = $request->cost_dolar;
        $advertising->title = $request->title;
        $advertising->description = $request->description;
        $advertising->type_advertising = $request->type_advertising;
        $advertising->video = $request->video;
        $advertising->pago = $request->pago;
        $advertising->start_date = $request->start_date;
        $advertising->end_date = $request->end_date;

        $query = $now->between($request->start_date, $request->end_date);

        if ($query) {
            $advertising->status = 'Activo';
        } else {
            if ($request->start_date == $day && $request->end_date == $day) {
                $advertising->status = 'Activo';
            } else {
                $advertising->status = 'Inactivo';
            }
        }

        if (!empty($request->image)) {
            $file = $request->file('image');

            //obtenemos el nombre del archivo
            $nameImage =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameImage = str_replace(' ', '-', $nameImage);

            //movemos el archivo a la ruta indicada
            $file->move('img/advertisements', $nameImage);

            //guardamos el nombre de la ruta en base de datos
            $advertising->image = $nameImage;
        }

        if (!empty($request->audio)) {
            $file = $request->file('audio');

            //obtenemos el nombre del archivo
            $nameAudio =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameAudio = str_replace(' ', '-', $nameAudio);

            //movemos el archivo a la ruta indicada
            $file->move('audio/advertisements', $nameAudio);

            //guardamos el nombre de la ruta en base de datos
            $advertising->audio = $nameAudio;
        }

        $advertising->save();

        return redirect()->back()->with('success-message', 'Publicidad actualizada con éxito.');
    }

    public function
    default()
    {
        $this->reset();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Advertising::destroy($id);

        return redirect()->route('advertisements.index')
            ->with('success-message', 'Publicidad eliminada con éxito.');
    }

    public function cost(Request $request)
    {
        $data = Plan::where('id', $request->idPlan)->first();
        $code = 200;
        return response()->json($data, $code);
    }
}

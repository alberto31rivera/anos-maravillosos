<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transmission;
use Illuminate\Http\Request;

class TransmissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transmission = Transmission::all()->first();
        return view('admin.transmissions.index', [
            'transmission' => $transmission,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = Transmission::all()->first();

        if ($query != null) {
            $transmission = Transmission::find($query->id);
            $transmission->online_transmission = $request->online_transmission;
            $transmission->online_chat = $request->online_chat;

            $transmission->save();

            return redirect()->back()->with('success-message', 'Datos Actualizados con éxito.');

        }else{

            $transmission = new Transmission();
            $transmission->online_transmission = $request->online_transmission;
            $transmission->online_chat = $request->online_chat;

            $transmission->save();

            return redirect()->back()->with('success-message', 'Datos Guardados con éxito.');
        }
    }

}

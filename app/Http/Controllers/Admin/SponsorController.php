<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Program;
use App\Models\Sponsor;
use Illuminate\Http\Request;

class SponsorController extends Controller
{

    public $view = 'create';

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sponsors.index', [
            'view' => $this->view,
            'programs' => Program::all(),
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'title' => 'required',
        'program_id' => 'required',
        'image' => 'required|mimes:jpg,jpeg,bmp,png',
        ]);

        $sponsor = new Sponsor();
        $sponsor->title = $request->title;
        $sponsor->url = $request->url;
        $sponsor->program_id = $request->program_id;

        if (!empty($request->image)) {

            $file = $request->file('image');

            //obtenemos el nombre del archivo
            $nameImage =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameImage = str_replace(' ', '-', $nameImage);

            //movemos el archivo a la ruta indicada
            $file->move('img/sponsors', $nameImage);

            //guardamos el nombre de la ruta en base de datos
            $sponsor->image = $nameImage;
        }


        $sponsor->save();

        return redirect()->route('sponsor.index')
                        ->with('success-message','Nuevo Auspiciador creado con éxito.');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sponsor = Sponsor::find($id);

        return view('admin.sponsors.index', [
            'sponsor' => $sponsor,
            'view' => 'edit',
            'programs' => Program::all(),
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'program_id' => 'required',
            ]);

        $sponsor = Sponsor::find($id);
        $sponsor->title = $request->title;
        $sponsor->url = $request->url;
        $sponsor->program_id = $request->program_id;

        if (!empty($request->image)) {

            $file = $request->file('image');

            //obtenemos el nombre del archivo
            $nameImage =  time() . "_" . $file->getClientOriginalName();

            //agregar guion al nombre que tenga espacio
            $nameImage = str_replace(' ', '-', $nameImage);

            //movemos el archivo a la ruta indicada
            $file->move('img/sponsors', $nameImage);

            //guardamos el nombre de la ruta en base de datos
            $sponsor->image = $nameImage;
        }

        $sponsor->save();

        return redirect()->back()->with('success-message', 'Auspiciador actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sponsor::destroy($id);

        return redirect()->route('sponsor.index')
        ->with('success-message','PAuspiciador eliminado con éxito.');
    }
}

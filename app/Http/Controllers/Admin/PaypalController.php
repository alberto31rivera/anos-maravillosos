<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentConfiguration;
use Illuminate\Http\Request;

class PaypalController extends Controller
{
    const PAYPAL = 1;

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paypalSettings = PaymentConfiguration::where('payment_method_id', $this::PAYPAL)->first();
        return view('admin.paypal.index', [
            'paypalSettings' => $paypalSettings,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required',
            ]);

        $query = PaymentConfiguration::where('payment_method_id', $this::PAYPAL)->get();

        if (count($query)) {
            //Actualizar registros
            $query = $query->first();
            $paypalSettings = PaymentConfiguration::find($query->id);
            $paypalSettings->payment_method_id = $this::PAYPAL;
            $paypalSettings->email = $request->email;
            $paypalSettings->status = $request->status;
            $paypalSettings->sandbox = $request->sandbox;

            if (!empty($request->image)) {

                $file = $request->file('image');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/payments/paypal', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $paypalSettings->image = $nameImage;
            }

            $paypalSettings->save();

            return redirect()->back()->with('success-message', 'Datos Actualizados con éxito.');

        } else {
            $paypalSettings = new PaymentConfiguration();
            $paypalSettings->payment_method_id = $this::PAYPAL;
            $paypalSettings->email = $request->email;
            $paypalSettings->status = $request->status;
            $paypalSettings->sandbox = $request->sandbox;

            if (!empty($request->image)) {

                $file = $request->file('image');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/payments/paypal', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $paypalSettings->image = $nameImage;
            }

            $paypalSettings->save();

            return redirect()->back()->with('success-message', 'Datos Guardados con éxito.');
        }
    }
}

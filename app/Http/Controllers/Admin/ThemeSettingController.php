<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ThemeSetting;
use Illuminate\Http\Request;

class ThemeSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }


    public function index()
    {
        $themeSettings = ThemeSetting::first();

        return view('admin.theme-settings.index', [
            'themeSettings' => $themeSettings,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = ThemeSetting::all();

        if (count($query)) {
            //Actualizar registros
            $query = $query->first();
            $themeSettings = ThemeSetting::find($query->id);
            $themeSettings->color_regards = $request->color_regards;
            $themeSettings->color_text_regards = $request->color_text_regards;
            $themeSettings->color_header = $request->color_header;
            $themeSettings->color_text_header = $request->color_text_header;
            $themeSettings->color_highlight_header = $request->color_highlight_header;
            $themeSettings->color_footer = $request->color_footer;
            $themeSettings->color_text_footer = $request->color_text_footer;
            $themeSettings->color_contact = $request->color_contact;
            $themeSettings->color_buttons = $request->color_buttons;

            $themeSettings->save();

            return redirect()->back()->with('success-message', 'Datos Actualizados con éxito.');

        } else {
            $themeSettings = new ThemeSetting();
            $themeSettings->color_regards = $request->color_regards;
            $themeSettings->color_text_regards = $request->color_text_regards;
            $themeSettings->color_header = $request->color_header;
            $themeSettings->color_text_header = $request->color_text_header;
            $themeSettings->color_highlight_header = $request->color_highlight_header;
            $themeSettings->color_footer = $request->color_footer;
            $themeSettings->color_text_footer = $request->color_text_footer;
            $themeSettings->color_contact = $request->color_contact;
            $themeSettings->color_buttons = $request->color_buttons;

            $themeSettings->save();

            return redirect()->back()->with('success-message', 'Datos Guardados con éxito.');
        }
    }
}

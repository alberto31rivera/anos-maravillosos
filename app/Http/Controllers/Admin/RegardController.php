<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegardController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    public function index()
    {
        return view('admin.regards.index');
    }
}

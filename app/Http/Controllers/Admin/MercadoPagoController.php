<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentConfiguration;
use Illuminate\Http\Request;

class MercadoPagoController extends Controller
{

    const MERCADOPAGO = 2;

    public function __construct()
    {
        $this->middleware('admin.authenticate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mercadoPagoSettings = PaymentConfiguration::where('payment_method_id', $this::MERCADOPAGO)->first();
        return view('admin.mercado-pago.index', [
            'mercadoPagoSettings' => $mercadoPagoSettings,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'public_key' => 'required',
            'access_token' => 'required',
            'client_id' => 'required',
            'client_secret' => 'required',
            ]);

        $query = PaymentConfiguration::where('payment_method_id', $this::MERCADOPAGO)->get();

        if (count($query)) {
            //Actualizar registros
            $query = $query->first();
            $mercadoPagoSettings = PaymentConfiguration::find($query->id);
            $mercadoPagoSettings->payment_method_id = $this::MERCADOPAGO;
            $mercadoPagoSettings->public_key = $request->public_key;
            $mercadoPagoSettings->access_token = $request->access_token;
            $mercadoPagoSettings->client_id = $request->client_id;
            $mercadoPagoSettings->client_secret = $request->client_secret;
            $mercadoPagoSettings->status = $request->status;
            $mercadoPagoSettings->sandbox = $request->sandbox;

            if (!empty($request->image)) {

                $file = $request->file('image');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/payments/mercado-pago', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $mercadoPagoSettings->image = $nameImage;
            }

            $mercadoPagoSettings->save();

            return redirect()->back()->with('success-message', 'Datos Actualizados con éxito.');

        } else {
            $mercadoPagoSettings = new PaymentConfiguration();
            $mercadoPagoSettings->payment_method_id = $this::MERCADOPAGO;
            $mercadoPagoSettings->public_key = $request->public_key;
            $mercadoPagoSettings->access_token = $request->access_token;
            $mercadoPagoSettings->client_id = $request->client_id;
            $mercadoPagoSettings->client_secret = $request->client_secret;
            $mercadoPagoSettings->status = $request->status;
            $mercadoPagoSettings->sandbox = $request->sandbox;

            if (!empty($request->image)) {

                $file = $request->file('image');

                //obtenemos el nombre del archivo
                $nameImage =  time() . "_" . $file->getClientOriginalName();

                //agregar guion al nombre que tenga espacio
                $nameImage = str_replace(' ', '-', $nameImage);

                //movemos el archivo a la ruta indicada
                $file->move('img/payments/mercado-pago', $nameImage);

                //guardamos el nombre de la ruta en base de datos
                $mercadoPagoSettings->image = $nameImage;
            }

            $mercadoPagoSettings->save();

            return redirect()->back()->with('success-message', 'Datos Guardados con éxito.');
        }
    }
}

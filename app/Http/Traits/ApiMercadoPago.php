<?php

namespace App\Http\Traits;

use App\Models\PaymentMethod;
use Carbon\Carbon;
use MercadoPago;
use Illuminate\Support\Facades\Session;

trait ApiMercadoPago
{

    /**
     * @param Plan [plan]
     * @return Preference [preference]
     */
    public function makePreference($plan)
    {

        /**
         * Summary: [AccessToken setter to MercadoPago\SDK Model]
         */
        $paymentConfiguration = PaymentMethod::find(2)->paymentConfiguration;
        MercadoPago\SDK::setAccessToken($paymentConfiguration->access_token);

        $preference = new MercadoPago\Preference();

        /**
         * New Payer Model to add on Preference
         */
        $payer = new MercadoPago\Payer();
        $payer->name = Session::get('name');
        $payer->surname = "";
        $payer->email = Session::get('email');
        $payer->date_created = Carbon::now();
        $payer->phone = array(
            "area_code" => "",
            "number" => Session::get('cellphone')
        );

        $payer->identification = array(
            "type" => "DNI",
            "number" => Session::get('number_document')
        );


        /**
         * New Item Model to add on Preference
         */
        $item = new MercadoPago\Item();
        $item->id = $plan->id;
        $item->title = $plan->title;
        $item->description = "Plan de publicidad";
        $item->quantity = 1;
        $item->unit_price = $plan->cost;

        $preference->items = array($item);
        $preference->payer = $payer;
        $preference->payment_methods = array(
            "excluded_payment_types" => array(
                array("id" => "atm")
            ),
        );
        $preference->back_urls = array(
            "success" => route('mp.payment.success'),
            "failure" => route('mp.payment.failure'),
            "pending" => route('mp.payment.pending'),
        );
        $preference->auto_return = "approved";


        $preference->save();

        return $preference;
    }
}

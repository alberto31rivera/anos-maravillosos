<?php
namespace App\Http\Traits;

use App\Models\Currency;
use App\Models\PaymentMethod;

trait ApiPaypal {

    public function makePaypalForm($plan)
    {
        $apiContext = null;
        $paymentConfiguration = PaymentMethod::find(1)->paymentConfiguration;


        if ($paymentConfiguration->sandbox == 1) {
            //Sandbox config configuration
            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    'AZlCjF3JqQTLPaqryy6AwiSLF2iYgW4XeA4IAaueJeCWdRJrG-B2UMIYsHBhy3jrmMQBdWIa8R_fdrUl',     // ClientID
                    'EIEZ6hNA4coD0QFbXnvfvwck4vDNFay0HxRiL2UMtxKyme-G25dvlOrcBSEd1yWPGmsnIJ8a7uLwjXbB'      // ClientSecret
                )
            );
        } else {
            //Production config configuration
            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    'ATgUNZT3iMjCwQpRaM2HYnILm75HK1yFIHlMSVu2VuGtxt0TFjiV_aDvZdeak2GyGgYu3-r4qMqvF95f',     // ClientID
                    'EJaSU5-GZigv4EyHV02r6DWJIFarnmoAgZuTbCJBvtIq97W7C5EK775XjpBZPeFF0InGGxZOwBxI0XkB'      // ClientSecret
                )
            );

            $apiContext->setConfig([
                'mode' => 'live',
            ]);
        }


        $payer = new \PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new \PayPal\Api\Amount();
        $amount->setTotal($plan->cost_dolar);
        $amount->setCurrency('USD');

        $transaction = new \PayPal\Api\Transaction();
        $transaction->setAmount($amount);

        $redirectUrls = new \PayPal\Api\RedirectUrls();
        $redirectUrls->setReturnUrl(route('paypal.succes.response'))
            ->setCancelUrl(route('paypal.failed.response'));

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);

        try {
            $payment->create($apiContext);
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getData();
        }

        return view('front.pages.payments.paypal', [
            'layout' => 'silver',
            'day' => $this->day,
            'msg' => $this->msg,
            'regards' => $this->regards,
            'admin' => $this->admin,
            'recentPosts' => $this->recentPosts,
            'allCategories' => $this->allCategories,
            'banner' => $this->banner,
            'themeSettings' => $this->themeSettings,
            'services' => $this->services,
            'plan' => $plan,
            'payment' => $payment,
            'currencyDolar' => Currency::where('id', 1)->first(),
        ]);
    }
}
